# Framework for data processing and ABCNet setup for B->Kee pre-processing
This repo is intended to use ```Coffea``` from the reading of NanoAODs up to generating histograms and structured datasets to be used later on for the DNN setup. More info about coffea can be found here: https://coffeateam.github.io/coffea/installation.html

# Installation

The installation follows the helpful guide from Korbinian (https://github.com/kschweiger/TF4ttHFH) to setup a virtual environment with pyenv.

```bash
curl https://pyenv.run | bash
```

After the set-up the following needs to be added to the file loaded on loging (`.bash_profile` etc.)

```bash
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi
```

Download the repo

```bash
git clone https://:@gitlab.cern.ch:8443/vmikuni/kee-triplet-id.git
```
# Preparing the samples

The data preparation is handled by the scripts in the folder **coffea**.
From that folder, setup the environment for the first time with:
```bash
source init.sh
```
Everytime after that you just need to:

```bash
source setup.sh
```
The **data** folder contains text files listing the dataset used for the training/testing and evaluation steps.
To run the analysis framework you can try the code with:

```bash
cd scripts
python analysis_BKee.py --version test --save_h5 --sample train --maxchunk 2
```
The important options are:
* --points: maximum number of particles to save per event
* --version: string to be added to output file names
* --save_h5: create and save the files relevant for the DNN training
* --samples: Which list of files to load
To see all available options just run the code with options **-h**.

There is also a parsl support available in case your current batch system supports it. Just run the code with the option **--parsl** to enable it.
The output of the program are root files with histograms saved in the folder **coffea/root** and h5 files saved in the folder **data** outside the coffea folder.
**Important**: To select which files will be used for the training/test/evaluation, you should create a **.txt** file inside the **data** folder, listing the files to be used.

# Training the classifier

The necessary files to perform the training are inside the folder **ABCNet**.
As usual, start by first sourcing:

```bash
source setup.sh
```
to train use:
```batch
cd scripts
python train.py  --data_dir ../../data/ --num_point 100 --nfeat 8 --nglob 3 --log_dir test --batch_size 64
```
The important options are:
* --num_point: Number of points to use in the point cloud (same as --points from before).
* --data_dir: Folder containing the data to use.
* --nfeat: Number of features per particle.
* --nglob: Number of global parameters.
* --log_dir: Name of the folder to store the log files and trained model under the **logs** folder
To see all available options just run the code with options **-h**.

Similarly, to evaluate the performance, you can run:

```batch
python evaluate.py --num_point 100 --data_dir ../../data/ --ncat 2 --nfeat 8 --nglob 3 --model_path ../logs/test   --batch 100 --name test
```

This will create an h5 file, stored in the folder names **h5** containing the same data stored in the input file plus the classifier output for each particle in the event.

# Some evaluation scripts

After the evaluation of the model is done, and ```.h5``` file will be created. To convert the output file named ```FILENAME.h5``` to a ```root``` file, run:
```bash
python prepare_root.py -f FILENAME  [--auto] [--make_roc]
```
To automatically create the list of branches to be saves use the flag ```--auto```, while the flag ```--make_roc``` creates a ROC curve per particle.

A general script to run on these root files can be called with:

```bash
python BToKEE.py -f ROOTFILENAME 
```

This script reads the created root file called ```ROOTFILENAME.root``` to create all possible triplets from the combinations of electrons and kaons.

The available flags are:
* --kaon_cut: DNN cut applied only to kaons.
* --e_cut: DNN cut applied only to electrons.
* --sum_cut: DNN cut applied on the sum of the DNN scores (sumDNN) on the triplet.
* --make_roc: Create a ROC curve for the sumDNN.

The output of the script contains plots for the invariant mass of the triplets, sumDNN plus a root file containing the histograms separately.



