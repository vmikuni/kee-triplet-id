ø²!
Ñ)£)
,
Abs
x"T
y"T"
Ttype:

2	
:
Add
x"T
y"T
z"T"
Ttype:
2	
x
Assign
ref"T

value"T

output_ref"T"	
Ttype"
validate_shapebool("
use_lockingbool(
s
	AssignSub
ref"T

value"T

output_ref"T" 
Ttype:
2	"
use_lockingbool( 
k
BatchMatMulV2
x"T
y"T
output"T"
Ttype:

2	"
adj_xbool( "
adj_ybool( 
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype

Conv2D

input"T
filter"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

B
Equal
x"T
y"T
z
"
Ttype:
2	

W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
^
Fill
dims"
index_type

value"T
output"T"	
Ttype"

index_typetype0:
2	
­
GatherV2
params"Tparams
indices"Tindices
axis"Taxis
output"Tparams"

batch_dimsint "
Tparamstype"
Tindicestype:
2	"
Taxistype:
2	
B
GreaterEqual
x"T
y"T
z
"
Ttype:
2	
.
Identity

input"T
output"T"	
Ttype
2
L2Loss
t"T
output"T"
Ttype:
2
\
	LeakyRelu
features"T
activations"T"
alphafloat%ÍÌL>"
Ttype0:
2

Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
Ô
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C

Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
N
Merge
inputs"T*N
output"T
value_index"	
Ttype"
Nint(0
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(
=
Mul
x"T
y"T
z"T"
Ttype:
2	
.
Neg
x"T
y"T"
Ttype:

2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
X
PlaceholderWithDefault
input"dtype
output"dtype"
dtypetype"
shapeshape
~
RandomUniform

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
a
Range
start"Tidx
limit"Tidx
delta"Tidx
output"Tidx"
Tidxtype0:	
2	
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
\
	RefSwitch
data"T
pred

output_false"T
output_true"T"	
Ttype
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
.
Rsqrt
x"T
y"T"
Ttype:

2
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
9
Softmax
logits"T
softmax"T"
Ttype:
2

#SparseSoftmaxCrossEntropyWithLogits
features"T
labels"Tlabels	
loss"T
backprop"T"
Ttype:
2"
Tlabelstype0	:
2	
1
Square
x"T
y"T"
Ttype:

2	
G
SquaredDifference
x"T
y"T
z"T"
Ttype:

2	
N
Squeeze

input"T
output"T"	
Ttype"
squeeze_dims	list(int)
 (
2
StopGradient

input"T
output"T"	
Ttype
ö
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
:
Sub
x"T
y"T
z"T"
Ttype:
2	

Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
M
Switch	
data"T
pred

output_false"T
output_true"T"	
Ttype
c
Tile

input"T
	multiples"
Tmultiples
output"T"	
Ttype"

Tmultiplestype0:
2	
f
TopKV2

input"T
k
values"T
indices"
sortedbool("
Ttype:
2	
P
	Transpose
x"T
perm"Tperm
y"T"	
Ttype"
Tpermtype0:
2	
s

VariableV2
ref"dtype"
shapeshape"
dtypetype"
	containerstring "
shared_namestring "serve*1.14.02v1.14.0-rc1-22-gaf24dc91b5Ç
d
PlaceholderPlaceholder*
shape:d
*
dtype0*"
_output_shapes
:d

^
Placeholder_1Placeholder*
shape
:d*
dtype0*
_output_shapes

:d
^
Placeholder_2Placeholder*
shape
:*
dtype0*
_output_shapes

:
N
Placeholder_3Placeholder*
dtype0
*
shape: *
_output_shapes
: 
Y
ExpandDims/dimConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
r

ExpandDims
ExpandDimsPlaceholderExpandDims/dim*

Tdim0*
T0*&
_output_shapes
:d

h
strided_slice/stackConst*!
valueB"            *
dtype0*
_output_shapes
:
j
strided_slice/stack_1Const*!
valueB"           *
dtype0*
_output_shapes
:
j
strided_slice/stack_2Const*!
valueB"         *
dtype0*
_output_shapes
:

strided_sliceStridedSlicePlaceholderstrided_slice/stackstrided_slice/stack_1strided_slice/stack_2*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*
T0*
Index0*"
_output_shapes
:d
^
SqueezeSqueezestrided_slice*
squeeze_dims
 *
T0*
_output_shapes

:d
R
ExpandDims_1/dimConst*
value	B : *
dtype0*
_output_shapes
: 
n
ExpandDims_1
ExpandDimsSqueezeExpandDims_1/dim*

Tdim0*
T0*"
_output_shapes
:d
j
strided_slice_1/stackConst*!
valueB"           *
dtype0*
_output_shapes
:
l
strided_slice_1/stack_1Const*!
valueB"           *
dtype0*
_output_shapes
:
l
strided_slice_1/stack_2Const*!
valueB"         *
dtype0*
_output_shapes
:

strided_slice_1StridedSliceExpandDims_1strided_slice_1/stackstrided_slice_1/stack_1strided_slice_1/stack_2*
new_axis_mask *
end_mask*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
_output_shapes

:d
[
ExpandDims_2/dimConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
v
ExpandDims_2
ExpandDimsstrided_slice_1ExpandDims_2/dim*

Tdim0*
T0*"
_output_shapes
:d
f
strided_slice_2/stackConst*
valueB"        *
dtype0*
_output_shapes
:
h
strided_slice_2/stack_1Const*
valueB"        *
dtype0*
_output_shapes
:
h
strided_slice_2/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:

strided_slice_2StridedSliceExpandDims_1strided_slice_2/stackstrided_slice_2/stack_1strided_slice_2/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*"
_output_shapes
:d
L
Equal/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
R
EqualEqualExpandDims_2Equal/y*
T0*"
_output_shapes
:d
d
ones_like/ShapeConst*!
valueB"   d      *
dtype0*
_output_shapes
:
T
ones_like/ConstConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
r
	ones_likeFillones_like/Shapeones_like/Const*
T0*

index_type0*"
_output_shapes
:d
Z
ShapeConst*!
valueB"   d      *
dtype0*
_output_shapes
:
J
ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
Y
FillFillShapeConst*
T0*

index_type0*"
_output_shapes
:d
U
SelectSelectEqual	ones_likeFill*
T0*"
_output_shapes
:d
J
mul/xConst*
valueB
 *  zD*
dtype0*
_output_shapes
: 
F
mulMulmul/xSelect*
T0*"
_output_shapes
:d
c
transpose/permConst*!
valueB"          *
dtype0*
_output_shapes
:
e
	transpose	Transposemultranspose/perm*
Tperm0*
T0*"
_output_shapes
:d
j
strided_slice_3/stackConst*!
valueB"            *
dtype0*
_output_shapes
:
l
strided_slice_3/stack_1Const*!
valueB"           *
dtype0*
_output_shapes
:
l
strided_slice_3/stack_2Const*!
valueB"         *
dtype0*
_output_shapes
:

strided_slice_3StridedSliceExpandDims_1strided_slice_3/stackstrided_slice_3/stack_1strided_slice_3/stack_2*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*
Index0*
T0*"
_output_shapes
:d
e
transpose_1/permConst*!
valueB"          *
dtype0*
_output_shapes
:
u
transpose_1	Transposestrided_slice_3transpose_1/perm*
Tperm0*
T0*"
_output_shapes
:d
j
strided_slice_4/stackConst*!
valueB"           *
dtype0*
_output_shapes
:
l
strided_slice_4/stack_1Const*!
valueB"            *
dtype0*
_output_shapes
:
l
strided_slice_4/stack_2Const*!
valueB"         *
dtype0*
_output_shapes
:

strided_slice_4StridedSlicetranspose_1strided_slice_4/stackstrided_slice_4/stack_1strided_slice_4/stack_2*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*"
_output_shapes
:d
c
Tile/multiplesConst*!
valueB"   d      *
dtype0*
_output_shapes
:
l
TileTilestrided_slice_4Tile/multiples*

Tmultiples0*
T0*"
_output_shapes
:dd
e
transpose_2/permConst*!
valueB"          *
dtype0*
_output_shapes
:
j
transpose_2	TransposeTiletranspose_2/perm*
T0*
Tperm0*"
_output_shapes
:dd
J
subSubTiletranspose_2*
T0*"
_output_shapes
:dd
<
AbsAbssub*
T0*"
_output_shapes
:dd
>
Abs_1AbsAbs*
T0*"
_output_shapes
:dd
S
GreaterEqual/yConst*
valueB
 *ÛÉ@*
dtype0*
_output_shapes
: 
`
GreaterEqualGreaterEqualAbs_1GreaterEqual/y*
T0*"
_output_shapes
:dd
L
mul_1/xConst*
valueB
 *ÛIA*
dtype0*
_output_shapes
: 
G
mul_1Mulmul_1/xAbs*
T0*"
_output_shapes
:dd
L
sub_1/xConst*
valueB
 *æéB*
dtype0*
_output_shapes
: 
I
sub_1Subsub_1/xmul_1*
T0*"
_output_shapes
:dd
C
sub_2SubAbsAbs*
T0*"
_output_shapes
:dd
[
Select_1SelectGreaterEqualsub_1sub_2*
T0*"
_output_shapes
:dd
|
MatMulBatchMatMulV2strided_slice_3transpose_1*
adj_y( *
T0*
adj_x( *"
_output_shapes
:dd
L
mul_2/xConst*
valueB
 *   À*
dtype0*
_output_shapes
: 
J
mul_2Mulmul_2/xMatMul*
T0*"
_output_shapes
:dd
N
SquareSquarestrided_slice_3*
T0*"
_output_shapes
:d
`
Sum/reduction_indicesConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
s
SumSumSquareSum/reduction_indices*

Tidx0*
	keep_dims(*
T0*"
_output_shapes
:d
e
transpose_3/permConst*!
valueB"          *
dtype0*
_output_shapes
:
i
transpose_3	TransposeSumtranspose_3/perm*
Tperm0*
T0*"
_output_shapes
:d
C
addAddSummul_2*
T0*"
_output_shapes
:dd
K
add_1Addaddtranspose_3*
T0*"
_output_shapes
:dd
J
add_2Addadd_1Select_1*
T0*"
_output_shapes
:dd
E
add_3Addadd_2mul*
T0*"
_output_shapes
:dd
K
add_4Addadd_3	transpose*
T0*"
_output_shapes
:dd
>
NegNegadd_4*
T0*"
_output_shapes
:dd
J
TopKV2/kConst*
value	B :*
dtype0*
_output_shapes
: 
h
TopKV2TopKV2NegTopKV2/k*
sorted(*
T0*0
_output_shapes
:d:d
^
	Squeeze_1SqueezePlaceholder*
squeeze_dims
 *
T0*
_output_shapes

:d

R
ExpandDims_3/dimConst*
value	B : *
dtype0*
_output_shapes
: 
p
ExpandDims_3
ExpandDims	Squeeze_1ExpandDims_3/dim*

Tdim0*
T0*"
_output_shapes
:d

[
ExpandDims_4/dimConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
w
ExpandDims_4
ExpandDimsExpandDims_3ExpandDims_4/dim*

Tdim0*
T0*&
_output_shapes
:d

Ý
Hlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/shapeConst*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*%
valueB"      
       *
dtype0*
_output_shapes
:
Ç
Flayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/minConst*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*
valueB
 *Á¾*
dtype0*
_output_shapes
: 
Ç
Flayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/maxConst*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*
valueB
 *Á>*
dtype0*
_output_shapes
: 
¾
Playerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/RandomUniformRandomUniformHlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/shape*
seed2 *

seed *
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*
dtype0*&
_output_shapes
:
 
º
Flayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/subSubFlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/maxFlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/min*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*
_output_shapes
: 
Ô
Flayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/mulMulPlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/RandomUniformFlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/sub*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*&
_output_shapes
:
 
Æ
Blayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniformAddFlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/mulFlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform/min*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*&
_output_shapes
:
 
ö
'layerfilter0_newfea_conv_head_0/weights
VariableV2"/device:CPU:0*
	container *
shape:
 *
shared_name *:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*
dtype0*&
_output_shapes
:
 
Ê
.layerfilter0_newfea_conv_head_0/weights/AssignAssign'layerfilter0_newfea_conv_head_0/weightsBlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*
validate_shape(*&
_output_shapes
:
 
Ý
,layerfilter0_newfea_conv_head_0/weights/readIdentity'layerfilter0_newfea_conv_head_0/weights"/device:CPU:0*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*&
_output_shapes
:
 

&layerfilter0_newfea_conv_head_0/L2LossL2Loss,layerfilter0_newfea_conv_head_0/weights/read*
T0*
_output_shapes
: 
r
-layerfilter0_newfea_conv_head_0/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
ª
+layerfilter0_newfea_conv_head_0/weight_lossMul&layerfilter0_newfea_conv_head_0/L2Loss-layerfilter0_newfea_conv_head_0/weight_loss/y*
T0*
_output_shapes
: 
¥
&layerfilter0_newfea_conv_head_0/Conv2DConv2DExpandDims_4,layerfilter0_newfea_conv_head_0/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
explicit_paddings
 *
paddingVALID*&
_output_shapes
:d 
u
(layerfilter0_newfea_conv_head_0/bn/ConstConst*
valueB *    *
dtype0*
_output_shapes
: 

'layerfilter0_newfea_conv_head_0/bn/beta
VariableV2*
	container *
shape: *
shared_name *
dtype0*
_output_shapes
: 

.layerfilter0_newfea_conv_head_0/bn/beta/AssignAssign'layerfilter0_newfea_conv_head_0/bn/beta(layerfilter0_newfea_conv_head_0/bn/Const*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/bn/beta*
validate_shape(*
use_locking(*
T0*
_output_shapes
: 
Â
,layerfilter0_newfea_conv_head_0/bn/beta/readIdentity'layerfilter0_newfea_conv_head_0/bn/beta*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/bn/beta*
_output_shapes
: 
w
*layerfilter0_newfea_conv_head_0/bn/Const_1Const*
valueB *  ?*
dtype0*
_output_shapes
: 

(layerfilter0_newfea_conv_head_0/bn/gamma
VariableV2*
shared_name *
dtype0*
	container *
shape: *
_output_shapes
: 

/layerfilter0_newfea_conv_head_0/bn/gamma/AssignAssign(layerfilter0_newfea_conv_head_0/bn/gamma*layerfilter0_newfea_conv_head_0/bn/Const_1*
use_locking(*
T0*;
_class1
/-loc:@layerfilter0_newfea_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
: 
Å
-layerfilter0_newfea_conv_head_0/bn/gamma/readIdentity(layerfilter0_newfea_conv_head_0/bn/gamma*
T0*;
_class1
/-loc:@layerfilter0_newfea_conv_head_0/bn/gamma*
_output_shapes
: 

Alayerfilter0_newfea_conv_head_0/bn/moments/mean/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
ð
/layerfilter0_newfea_conv_head_0/bn/moments/meanMean&layerfilter0_newfea_conv_head_0/Conv2DAlayerfilter0_newfea_conv_head_0/bn/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
: 
©
7layerfilter0_newfea_conv_head_0/bn/moments/StopGradientStopGradient/layerfilter0_newfea_conv_head_0/bn/moments/mean*
T0*&
_output_shapes
: 
ã
<layerfilter0_newfea_conv_head_0/bn/moments/SquaredDifferenceSquaredDifference&layerfilter0_newfea_conv_head_0/Conv2D7layerfilter0_newfea_conv_head_0/bn/moments/StopGradient*
T0*&
_output_shapes
:d 

Elayerfilter0_newfea_conv_head_0/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:

3layerfilter0_newfea_conv_head_0/bn/moments/varianceMean<layerfilter0_newfea_conv_head_0/bn/moments/SquaredDifferenceElayerfilter0_newfea_conv_head_0/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
: 
¬
2layerfilter0_newfea_conv_head_0/bn/moments/SqueezeSqueeze/layerfilter0_newfea_conv_head_0/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes
: 
²
4layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1Squeeze3layerfilter0_newfea_conv_head_0/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes
: 
y
.layerfilter0_newfea_conv_head_0/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

0layerfilter0_newfea_conv_head_0/bn/cond/switch_tIdentity0layerfilter0_newfea_conv_head_0/bn/cond/Switch:1*
T0
*
_output_shapes
: 

0layerfilter0_newfea_conv_head_0/bn/cond/switch_fIdentity.layerfilter0_newfea_conv_head_0/bn/cond/Switch*
T0
*
_output_shapes
: 
k
/layerfilter0_newfea_conv_head_0/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ò
layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
valueB *    *
dtype0*
_output_shapes
: 
Þ
nlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
shape: *
shared_name *
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
_output_shapes
: 
Ä
ulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssignnlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragelayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
: 

slayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/readIdentitynlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0*
_output_shapes
: 
Ö
layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB *    *
dtype0*
_output_shapes
: 
â
playerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
shape: *
shared_name *
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
_output_shapes
: 
Ì
wlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssignplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
: 

ulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentityplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*
_output_shapes
: 
¾
Flayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/decayConst1^layerfilter0_newfea_conv_head_0/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
Î
Vlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst1^layerfilter0_newfea_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 

Tlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubVlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xFlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Æ
Vlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Sub_layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1alayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
: 
¼
]layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchslayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read/layerfilter0_newfea_conv_head_0/bn/cond/pred_id*
T0*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
: : 
À
_layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch2layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/layerfilter0_newfea_conv_head_0/bn/cond/pred_id*
T0*E
_class;
97loc:@layerfilter0_newfea_conv_head_0/bn/moments/Squeeze* 
_output_shapes
: : 
®
Tlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulVlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Tlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
: 
Ê
Playerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSubYlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Tlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
: 
´
Wlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitchnlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/layerfilter0_newfea_conv_head_0/bn/cond/pred_id*
T0*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
: : 
Ð
Xlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst1^layerfilter0_newfea_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
 
Vlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubXlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xFlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ì
Xlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Subalayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1clayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
: 
Â
_layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read/layerfilter0_newfea_conv_head_0/bn/cond/pred_id*
T0*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
: : 
Æ
alayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch4layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/layerfilter0_newfea_conv_head_0/bn/cond/pred_id*
T0*G
_class=
;9loc:@layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
: : 
´
Vlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulXlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Vlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
: 
Ò
Rlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSub[layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Vlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
: 
º
Ylayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/layerfilter0_newfea_conv_head_0/bn/cond/pred_id*
T0*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
: : 
ð
@layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverageNoOpQ^layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvgS^layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
¡
:layerfilter0_newfea_conv_head_0/bn/cond/control_dependencyIdentity0layerfilter0_newfea_conv_head_0/bn/cond/switch_tA^layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage*C
_class9
75loc:@layerfilter0_newfea_conv_head_0/bn/cond/switch_t*
T0
*
_output_shapes
: 
g
,layerfilter0_newfea_conv_head_0/bn/cond/NoOpNoOp1^layerfilter0_newfea_conv_head_0/bn/cond/switch_f

<layerfilter0_newfea_conv_head_0/bn/cond/control_dependency_1Identity0layerfilter0_newfea_conv_head_0/bn/cond/switch_f-^layerfilter0_newfea_conv_head_0/bn/cond/NoOp*
T0
*C
_class9
75loc:@layerfilter0_newfea_conv_head_0/bn/cond/switch_f*
_output_shapes
: 
Ü
-layerfilter0_newfea_conv_head_0/bn/cond/MergeMerge<layerfilter0_newfea_conv_head_0/bn/cond/control_dependency_1:layerfilter0_newfea_conv_head_0/bn/cond/control_dependency*
N*
T0
*
_output_shapes
: : 
{
0layerfilter0_newfea_conv_head_0/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

2layerfilter0_newfea_conv_head_0/bn/cond_1/switch_tIdentity2layerfilter0_newfea_conv_head_0/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 

2layerfilter0_newfea_conv_head_0/bn/cond_1/switch_fIdentity0layerfilter0_newfea_conv_head_0/bn/cond_1/Switch*
T0
*
_output_shapes
: 
m
1layerfilter0_newfea_conv_head_0/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ð
2layerfilter0_newfea_conv_head_0/bn/cond_1/IdentityIdentity;layerfilter0_newfea_conv_head_0/bn/cond_1/Identity/Switch:1.^layerfilter0_newfea_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
: 

9layerfilter0_newfea_conv_head_0/bn/cond_1/Identity/SwitchSwitch2layerfilter0_newfea_conv_head_0/bn/moments/Squeeze1layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id*
T0*E
_class;
97loc:@layerfilter0_newfea_conv_head_0/bn/moments/Squeeze* 
_output_shapes
: : 
Ô
4layerfilter0_newfea_conv_head_0/bn/cond_1/Identity_1Identity=layerfilter0_newfea_conv_head_0/bn/cond_1/Identity_1/Switch:1.^layerfilter0_newfea_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
: 
¢
;layerfilter0_newfea_conv_head_0/bn/cond_1/Identity_1/SwitchSwitch4layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_11layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id*
T0*G
_class=
;9loc:@layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
: : 

2layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_1Switchslayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read1layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id*
T0*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
: : 

2layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_2Switchulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read1layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0* 
_output_shapes
: : 
Ð
/layerfilter0_newfea_conv_head_0/bn/cond_1/MergeMerge2layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_12layerfilter0_newfea_conv_head_0/bn/cond_1/Identity*
T0*
N*
_output_shapes

: : 
Ô
1layerfilter0_newfea_conv_head_0/bn/cond_1/Merge_1Merge2layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_24layerfilter0_newfea_conv_head_0/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

: : 
w
2layerfilter0_newfea_conv_head_0/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 
Ã
0layerfilter0_newfea_conv_head_0/bn/batchnorm/addAdd1layerfilter0_newfea_conv_head_0/bn/cond_1/Merge_12layerfilter0_newfea_conv_head_0/bn/batchnorm/add/y*
T0*
_output_shapes
: 

2layerfilter0_newfea_conv_head_0/bn/batchnorm/RsqrtRsqrt0layerfilter0_newfea_conv_head_0/bn/batchnorm/add*
T0*
_output_shapes
: 
¿
0layerfilter0_newfea_conv_head_0/bn/batchnorm/mulMul2layerfilter0_newfea_conv_head_0/bn/batchnorm/Rsqrt-layerfilter0_newfea_conv_head_0/bn/gamma/read*
T0*
_output_shapes
: 
Ä
2layerfilter0_newfea_conv_head_0/bn/batchnorm/mul_1Mul&layerfilter0_newfea_conv_head_0/Conv2D0layerfilter0_newfea_conv_head_0/bn/batchnorm/mul*
T0*&
_output_shapes
:d 
Á
2layerfilter0_newfea_conv_head_0/bn/batchnorm/mul_2Mul/layerfilter0_newfea_conv_head_0/bn/cond_1/Merge0layerfilter0_newfea_conv_head_0/bn/batchnorm/mul*
T0*
_output_shapes
: 
¾
0layerfilter0_newfea_conv_head_0/bn/batchnorm/subSub,layerfilter0_newfea_conv_head_0/bn/beta/read2layerfilter0_newfea_conv_head_0/bn/batchnorm/mul_2*
T0*
_output_shapes
: 
Ð
2layerfilter0_newfea_conv_head_0/bn/batchnorm/add_1Add2layerfilter0_newfea_conv_head_0/bn/batchnorm/mul_10layerfilter0_newfea_conv_head_0/bn/batchnorm/sub*
T0*&
_output_shapes
:d 

$layerfilter0_newfea_conv_head_0/ReluRelu2layerfilter0_newfea_conv_head_0/bn/batchnorm/add_1*
T0*&
_output_shapes
:d 
_
	Squeeze_2SqueezeExpandDims_4*
squeeze_dims
 *
T0*
_output_shapes

:d

R
ExpandDims_5/dimConst*
value	B : *
dtype0*
_output_shapes
: 
p
ExpandDims_5
ExpandDims	Squeeze_2ExpandDims_5/dim*

Tdim0*
T0*"
_output_shapes
:d

M
range/startConst*
value	B : *
dtype0*
_output_shapes
: 
M
range/limitConst*
value	B :*
dtype0*
_output_shapes
: 
M
range/deltaConst*
value	B :*
dtype0*
_output_shapes
: 
]
rangeRangerange/startrange/limitrange/delta*

Tidx0*
_output_shapes
:
I
mul_3/yConst*
value	B :d*
dtype0*
_output_shapes
: 
A
mul_3Mulrangemul_3/y*
T0*
_output_shapes
:
b
Reshape/shapeConst*!
valueB"         *
dtype0*
_output_shapes
:
c
ReshapeReshapemul_3Reshape/shape*
T0*
Tshape0*"
_output_shapes
:
`
Reshape_1/shapeConst*
valueB"ÿÿÿÿ
   *
dtype0*
_output_shapes
:
j
	Reshape_1ReshapeExpandDims_5Reshape_1/shape*
T0*
Tshape0*
_output_shapes

:d

L
add_5AddTopKV2:1Reshape*
T0*"
_output_shapes
:d
O
GatherV2/axisConst*
value	B : *
dtype0*
_output_shapes
: 

GatherV2GatherV2	Reshape_1add_5GatherV2/axis*
Taxis0*

batch_dims *
Tindices0*
Tparams0*&
_output_shapes
:d

i
Tile_1/multiplesConst*%
valueB"            *
dtype0*
_output_shapes
:
q
Tile_1TileExpandDims_4Tile_1/multiples*

Tmultiples0*
T0*&
_output_shapes
:d

O
sub_3SubTile_1GatherV2*
T0*&
_output_shapes
:d

Ë
?layerfilter0_edgefea_0/weights/Initializer/random_uniform/shapeConst*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*%
valueB"      
       *
dtype0*
_output_shapes
:
µ
=layerfilter0_edgefea_0/weights/Initializer/random_uniform/minConst*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*
valueB
 *Á¾*
dtype0*
_output_shapes
: 
µ
=layerfilter0_edgefea_0/weights/Initializer/random_uniform/maxConst*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*
valueB
 *Á>*
dtype0*
_output_shapes
: 
£
Glayerfilter0_edgefea_0/weights/Initializer/random_uniform/RandomUniformRandomUniform?layerfilter0_edgefea_0/weights/Initializer/random_uniform/shape*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*
dtype0*
seed2 *

seed *
T0*&
_output_shapes
:
 

=layerfilter0_edgefea_0/weights/Initializer/random_uniform/subSub=layerfilter0_edgefea_0/weights/Initializer/random_uniform/max=layerfilter0_edgefea_0/weights/Initializer/random_uniform/min*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*
_output_shapes
: 
°
=layerfilter0_edgefea_0/weights/Initializer/random_uniform/mulMulGlayerfilter0_edgefea_0/weights/Initializer/random_uniform/RandomUniform=layerfilter0_edgefea_0/weights/Initializer/random_uniform/sub*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*&
_output_shapes
:
 
¢
9layerfilter0_edgefea_0/weights/Initializer/random_uniformAdd=layerfilter0_edgefea_0/weights/Initializer/random_uniform/mul=layerfilter0_edgefea_0/weights/Initializer/random_uniform/min*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*&
_output_shapes
:
 
ä
layerfilter0_edgefea_0/weights
VariableV2"/device:CPU:0*
	container *
shape:
 *
shared_name *1
_class'
%#loc:@layerfilter0_edgefea_0/weights*
dtype0*&
_output_shapes
:
 
¦
%layerfilter0_edgefea_0/weights/AssignAssignlayerfilter0_edgefea_0/weights9layerfilter0_edgefea_0/weights/Initializer/random_uniform"/device:CPU:0*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*
validate_shape(*
use_locking(*
T0*&
_output_shapes
:
 
Â
#layerfilter0_edgefea_0/weights/readIdentitylayerfilter0_edgefea_0/weights"/device:CPU:0*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*&
_output_shapes
:
 
m
layerfilter0_edgefea_0/L2LossL2Loss#layerfilter0_edgefea_0/weights/read*
T0*
_output_shapes
: 
i
$layerfilter0_edgefea_0/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 

"layerfilter0_edgefea_0/weight_lossMullayerfilter0_edgefea_0/L2Loss$layerfilter0_edgefea_0/weight_loss/y*
T0*
_output_shapes
: 

layerfilter0_edgefea_0/Conv2DConv2Dsub_3#layerfilter0_edgefea_0/weights/read*
paddingVALID*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
explicit_paddings
 *&
_output_shapes
:d 
®
/layerfilter0_edgefea_0/biases/Initializer/ConstConst*0
_class&
$"loc:@layerfilter0_edgefea_0/biases*
valueB *    *
dtype0*
_output_shapes
: 
Ê
layerfilter0_edgefea_0/biases
VariableV2"/device:CPU:0*
shared_name *0
_class&
$"loc:@layerfilter0_edgefea_0/biases*
dtype0*
	container *
shape: *
_output_shapes
: 

$layerfilter0_edgefea_0/biases/AssignAssignlayerfilter0_edgefea_0/biases/layerfilter0_edgefea_0/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*0
_class&
$"loc:@layerfilter0_edgefea_0/biases*
validate_shape(*
_output_shapes
: 
³
"layerfilter0_edgefea_0/biases/readIdentitylayerfilter0_edgefea_0/biases"/device:CPU:0*
T0*0
_class&
$"loc:@layerfilter0_edgefea_0/biases*
_output_shapes
: 
´
layerfilter0_edgefea_0/BiasAddBiasAddlayerfilter0_edgefea_0/Conv2D"layerfilter0_edgefea_0/biases/read*
T0*
data_formatNHWC*&
_output_shapes
:d 
l
layerfilter0_edgefea_0/bn/ConstConst*
valueB *    *
dtype0*
_output_shapes
: 

layerfilter0_edgefea_0/bn/beta
VariableV2*
shape: *
shared_name *
dtype0*
	container *
_output_shapes
: 
ñ
%layerfilter0_edgefea_0/bn/beta/AssignAssignlayerfilter0_edgefea_0/bn/betalayerfilter0_edgefea_0/bn/Const*
use_locking(*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/bn/beta*
validate_shape(*
_output_shapes
: 
§
#layerfilter0_edgefea_0/bn/beta/readIdentitylayerfilter0_edgefea_0/bn/beta*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/bn/beta*
_output_shapes
: 
n
!layerfilter0_edgefea_0/bn/Const_1Const*
valueB *  ?*
dtype0*
_output_shapes
: 

layerfilter0_edgefea_0/bn/gamma
VariableV2*
shared_name *
dtype0*
	container *
shape: *
_output_shapes
: 
ö
&layerfilter0_edgefea_0/bn/gamma/AssignAssignlayerfilter0_edgefea_0/bn/gamma!layerfilter0_edgefea_0/bn/Const_1*
use_locking(*
T0*2
_class(
&$loc:@layerfilter0_edgefea_0/bn/gamma*
validate_shape(*
_output_shapes
: 
ª
$layerfilter0_edgefea_0/bn/gamma/readIdentitylayerfilter0_edgefea_0/bn/gamma*
T0*2
_class(
&$loc:@layerfilter0_edgefea_0/bn/gamma*
_output_shapes
: 

8layerfilter0_edgefea_0/bn/moments/mean/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
Ö
&layerfilter0_edgefea_0/bn/moments/meanMeanlayerfilter0_edgefea_0/BiasAdd8layerfilter0_edgefea_0/bn/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
: 

.layerfilter0_edgefea_0/bn/moments/StopGradientStopGradient&layerfilter0_edgefea_0/bn/moments/mean*
T0*&
_output_shapes
: 
É
3layerfilter0_edgefea_0/bn/moments/SquaredDifferenceSquaredDifferencelayerfilter0_edgefea_0/BiasAdd.layerfilter0_edgefea_0/bn/moments/StopGradient*
T0*&
_output_shapes
:d 

<layerfilter0_edgefea_0/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
ó
*layerfilter0_edgefea_0/bn/moments/varianceMean3layerfilter0_edgefea_0/bn/moments/SquaredDifference<layerfilter0_edgefea_0/bn/moments/variance/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
: 

)layerfilter0_edgefea_0/bn/moments/SqueezeSqueeze&layerfilter0_edgefea_0/bn/moments/mean*
T0*
squeeze_dims
 *
_output_shapes
: 
 
+layerfilter0_edgefea_0/bn/moments/Squeeze_1Squeeze*layerfilter0_edgefea_0/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes
: 
p
%layerfilter0_edgefea_0/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
}
'layerfilter0_edgefea_0/bn/cond/switch_tIdentity'layerfilter0_edgefea_0/bn/cond/Switch:1*
T0
*
_output_shapes
: 
{
'layerfilter0_edgefea_0/bn/cond/switch_fIdentity%layerfilter0_edgefea_0/bn/cond/Switch*
T0
*
_output_shapes
: 
b
&layerfilter0_edgefea_0/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
¬
nlayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
valueB *    *
dtype0*
_output_shapes
: 
¹
\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
	container *
shape: *
shared_name *o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
_output_shapes
: 
ú
clayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssign\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAveragenlayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
: 
á
alayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/readIdentity\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
: 
°
playerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*
dtype0*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB *    *
_output_shapes
: 
½
^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
shared_name *q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
shape: *
_output_shapes
: 

elayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssign^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageplayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
validate_shape(*
use_locking(*
T0*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
: 
ç
clayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentity^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
: 
¬
=layerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/decayConst(^layerfilter0_edgefea_0/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
¼
Mlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst(^layerfilter0_edgefea_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 

Klayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubMlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x=layerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
«
Mlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1SubVlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1Xlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
: 

Tlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchalayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read&layerfilter0_edgefea_0/bn/cond/pred_id*
T0*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
: : 

Vlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch)layerfilter0_edgefea_0/bn/moments/Squeeze&layerfilter0_edgefea_0/bn/cond/pred_id*
T0*<
_class2
0.loc:@layerfilter0_edgefea_0/bn/moments/Squeeze* 
_output_shapes
: : 

Klayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulMlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Klayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
: 

Glayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSubPlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Klayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
: 
ý
Nlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitch\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage&layerfilter0_edgefea_0/bn/cond/pred_id*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0* 
_output_shapes
: : 
¾
Olayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst(^layerfilter0_edgefea_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 

Mlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubOlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x=layerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
±
Olayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1SubXlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1Zlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
: 

Vlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchclayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read&layerfilter0_edgefea_0/bn/cond/pred_id*
T0*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
: : 
¢
Xlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch+layerfilter0_edgefea_0/bn/moments/Squeeze_1&layerfilter0_edgefea_0/bn/cond/pred_id*
T0*>
_class4
20loc:@layerfilter0_edgefea_0/bn/moments/Squeeze_1* 
_output_shapes
: : 

Mlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulOlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Mlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
: 
¤
Ilayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSubRlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Mlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
: 

Playerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitch^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage&layerfilter0_edgefea_0/bn/cond/pred_id*
T0*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
: : 
Õ
7layerfilter0_edgefea_0/bn/cond/ExponentialMovingAverageNoOpH^layerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvgJ^layerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
ý
1layerfilter0_edgefea_0/bn/cond/control_dependencyIdentity'layerfilter0_edgefea_0/bn/cond/switch_t8^layerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage*
T0
*:
_class0
.,loc:@layerfilter0_edgefea_0/bn/cond/switch_t*
_output_shapes
: 
U
#layerfilter0_edgefea_0/bn/cond/NoOpNoOp(^layerfilter0_edgefea_0/bn/cond/switch_f
ë
3layerfilter0_edgefea_0/bn/cond/control_dependency_1Identity'layerfilter0_edgefea_0/bn/cond/switch_f$^layerfilter0_edgefea_0/bn/cond/NoOp*
T0
*:
_class0
.,loc:@layerfilter0_edgefea_0/bn/cond/switch_f*
_output_shapes
: 
Á
$layerfilter0_edgefea_0/bn/cond/MergeMerge3layerfilter0_edgefea_0/bn/cond/control_dependency_11layerfilter0_edgefea_0/bn/cond/control_dependency*
N*
T0
*
_output_shapes
: : 
r
'layerfilter0_edgefea_0/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

)layerfilter0_edgefea_0/bn/cond_1/switch_tIdentity)layerfilter0_edgefea_0/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 

)layerfilter0_edgefea_0/bn/cond_1/switch_fIdentity'layerfilter0_edgefea_0/bn/cond_1/Switch*
T0
*
_output_shapes
: 
d
(layerfilter0_edgefea_0/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
µ
)layerfilter0_edgefea_0/bn/cond_1/IdentityIdentity2layerfilter0_edgefea_0/bn/cond_1/Identity/Switch:1%^layerfilter0_edgefea_0/bn/cond/Merge*
T0*
_output_shapes
: 
ø
0layerfilter0_edgefea_0/bn/cond_1/Identity/SwitchSwitch)layerfilter0_edgefea_0/bn/moments/Squeeze(layerfilter0_edgefea_0/bn/cond_1/pred_id*<
_class2
0.loc:@layerfilter0_edgefea_0/bn/moments/Squeeze*
T0* 
_output_shapes
: : 
¹
+layerfilter0_edgefea_0/bn/cond_1/Identity_1Identity4layerfilter0_edgefea_0/bn/cond_1/Identity_1/Switch:1%^layerfilter0_edgefea_0/bn/cond/Merge*
T0*
_output_shapes
: 
þ
2layerfilter0_edgefea_0/bn/cond_1/Identity_1/SwitchSwitch+layerfilter0_edgefea_0/bn/moments/Squeeze_1(layerfilter0_edgefea_0/bn/cond_1/pred_id*
T0*>
_class4
20loc:@layerfilter0_edgefea_0/bn/moments/Squeeze_1* 
_output_shapes
: : 
Ü
)layerfilter0_edgefea_0/bn/cond_1/Switch_1Switchalayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read(layerfilter0_edgefea_0/bn/cond_1/pred_id*
T0*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
: : 
à
)layerfilter0_edgefea_0/bn/cond_1/Switch_2Switchclayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read(layerfilter0_edgefea_0/bn/cond_1/pred_id*
T0*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
: : 
µ
&layerfilter0_edgefea_0/bn/cond_1/MergeMerge)layerfilter0_edgefea_0/bn/cond_1/Switch_1)layerfilter0_edgefea_0/bn/cond_1/Identity*
T0*
N*
_output_shapes

: : 
¹
(layerfilter0_edgefea_0/bn/cond_1/Merge_1Merge)layerfilter0_edgefea_0/bn/cond_1/Switch_2+layerfilter0_edgefea_0/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

: : 
n
)layerfilter0_edgefea_0/bn/batchnorm/add/yConst*
dtype0*
valueB
 *o:*
_output_shapes
: 
¨
'layerfilter0_edgefea_0/bn/batchnorm/addAdd(layerfilter0_edgefea_0/bn/cond_1/Merge_1)layerfilter0_edgefea_0/bn/batchnorm/add/y*
T0*
_output_shapes
: 

)layerfilter0_edgefea_0/bn/batchnorm/RsqrtRsqrt'layerfilter0_edgefea_0/bn/batchnorm/add*
T0*
_output_shapes
: 
¤
'layerfilter0_edgefea_0/bn/batchnorm/mulMul)layerfilter0_edgefea_0/bn/batchnorm/Rsqrt$layerfilter0_edgefea_0/bn/gamma/read*
T0*
_output_shapes
: 
ª
)layerfilter0_edgefea_0/bn/batchnorm/mul_1Mullayerfilter0_edgefea_0/BiasAdd'layerfilter0_edgefea_0/bn/batchnorm/mul*
T0*&
_output_shapes
:d 
¦
)layerfilter0_edgefea_0/bn/batchnorm/mul_2Mul&layerfilter0_edgefea_0/bn/cond_1/Merge'layerfilter0_edgefea_0/bn/batchnorm/mul*
T0*
_output_shapes
: 
£
'layerfilter0_edgefea_0/bn/batchnorm/subSub#layerfilter0_edgefea_0/bn/beta/read)layerfilter0_edgefea_0/bn/batchnorm/mul_2*
T0*
_output_shapes
: 
µ
)layerfilter0_edgefea_0/bn/batchnorm/add_1Add)layerfilter0_edgefea_0/bn/batchnorm/mul_1'layerfilter0_edgefea_0/bn/batchnorm/sub*
T0*&
_output_shapes
:d 

layerfilter0_edgefea_0/ReluRelu)layerfilter0_edgefea_0/bn/batchnorm/add_1*
T0*&
_output_shapes
:d 
á
Jlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/shapeConst*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*%
valueB"             *
dtype0*
_output_shapes
:
Ë
Hlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/minConst*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*
valueB
 *JQÚ¾*
dtype0*
_output_shapes
: 
Ë
Hlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/maxConst*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*
valueB
 *JQÚ>*
dtype0*
_output_shapes
: 
Ä
Rlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/RandomUniformRandomUniformJlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/shape*

seed *
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*
dtype0*
seed2 *&
_output_shapes
: 
Â
Hlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/subSubHlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/maxHlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/min*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*
_output_shapes
: 
Ü
Hlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/mulMulRlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/RandomUniformHlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/sub*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*&
_output_shapes
: 
Î
Dlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniformAddHlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/mulHlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform/min*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*&
_output_shapes
: 
ú
)layerfilter0_self_att_conv_head_0/weights
VariableV2"/device:CPU:0*
dtype0*
	container *
shape: *
shared_name *<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*&
_output_shapes
: 
Ò
0layerfilter0_self_att_conv_head_0/weights/AssignAssign)layerfilter0_self_att_conv_head_0/weightsDlayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*
validate_shape(*&
_output_shapes
: 
ã
.layerfilter0_self_att_conv_head_0/weights/readIdentity)layerfilter0_self_att_conv_head_0/weights"/device:CPU:0*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*&
_output_shapes
: 

(layerfilter0_self_att_conv_head_0/L2LossL2Loss.layerfilter0_self_att_conv_head_0/weights/read*
T0*
_output_shapes
: 
t
/layerfilter0_self_att_conv_head_0/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
°
-layerfilter0_self_att_conv_head_0/weight_lossMul(layerfilter0_self_att_conv_head_0/L2Loss/layerfilter0_self_att_conv_head_0/weight_loss/y*
T0*
_output_shapes
: 
Á
(layerfilter0_self_att_conv_head_0/Conv2DConv2D$layerfilter0_newfea_conv_head_0/Relu.layerfilter0_self_att_conv_head_0/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
explicit_paddings
 *
paddingVALID*&
_output_shapes
:d
Ä
:layerfilter0_self_att_conv_head_0/biases/Initializer/ConstConst*;
_class1
/-loc:@layerfilter0_self_att_conv_head_0/biases*
valueB*    *
dtype0*
_output_shapes
:
à
(layerfilter0_self_att_conv_head_0/biases
VariableV2"/device:CPU:0*;
_class1
/-loc:@layerfilter0_self_att_conv_head_0/biases*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:
¹
/layerfilter0_self_att_conv_head_0/biases/AssignAssign(layerfilter0_self_att_conv_head_0/biases:layerfilter0_self_att_conv_head_0/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*;
_class1
/-loc:@layerfilter0_self_att_conv_head_0/biases*
validate_shape(*
_output_shapes
:
Ô
-layerfilter0_self_att_conv_head_0/biases/readIdentity(layerfilter0_self_att_conv_head_0/biases"/device:CPU:0*
T0*;
_class1
/-loc:@layerfilter0_self_att_conv_head_0/biases*
_output_shapes
:
Õ
)layerfilter0_self_att_conv_head_0/BiasAddBiasAdd(layerfilter0_self_att_conv_head_0/Conv2D-layerfilter0_self_att_conv_head_0/biases/read*
data_formatNHWC*
T0*&
_output_shapes
:d
w
*layerfilter0_self_att_conv_head_0/bn/ConstConst*
valueB*    *
dtype0*
_output_shapes
:

)layerfilter0_self_att_conv_head_0/bn/beta
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:

0layerfilter0_self_att_conv_head_0/bn/beta/AssignAssign)layerfilter0_self_att_conv_head_0/bn/beta*layerfilter0_self_att_conv_head_0/bn/Const*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
È
.layerfilter0_self_att_conv_head_0/bn/beta/readIdentity)layerfilter0_self_att_conv_head_0/bn/beta*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/bn/beta*
T0*
_output_shapes
:
y
,layerfilter0_self_att_conv_head_0/bn/Const_1Const*
valueB*  ?*
dtype0*
_output_shapes
:

*layerfilter0_self_att_conv_head_0/bn/gamma
VariableV2*
shape:*
shared_name *
dtype0*
	container *
_output_shapes
:
¢
1layerfilter0_self_att_conv_head_0/bn/gamma/AssignAssign*layerfilter0_self_att_conv_head_0/bn/gamma,layerfilter0_self_att_conv_head_0/bn/Const_1*
use_locking(*
T0*=
_class3
1/loc:@layerfilter0_self_att_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
:
Ë
/layerfilter0_self_att_conv_head_0/bn/gamma/readIdentity*layerfilter0_self_att_conv_head_0/bn/gamma*
T0*=
_class3
1/loc:@layerfilter0_self_att_conv_head_0/bn/gamma*
_output_shapes
:

Clayerfilter0_self_att_conv_head_0/bn/moments/mean/reduction_indicesConst*
dtype0*!
valueB"          *
_output_shapes
:
÷
1layerfilter0_self_att_conv_head_0/bn/moments/meanMean)layerfilter0_self_att_conv_head_0/BiasAddClayerfilter0_self_att_conv_head_0/bn/moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:
­
9layerfilter0_self_att_conv_head_0/bn/moments/StopGradientStopGradient1layerfilter0_self_att_conv_head_0/bn/moments/mean*
T0*&
_output_shapes
:
ê
>layerfilter0_self_att_conv_head_0/bn/moments/SquaredDifferenceSquaredDifference)layerfilter0_self_att_conv_head_0/BiasAdd9layerfilter0_self_att_conv_head_0/bn/moments/StopGradient*
T0*&
_output_shapes
:d

Glayerfilter0_self_att_conv_head_0/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:

5layerfilter0_self_att_conv_head_0/bn/moments/varianceMean>layerfilter0_self_att_conv_head_0/bn/moments/SquaredDifferenceGlayerfilter0_self_att_conv_head_0/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:
°
4layerfilter0_self_att_conv_head_0/bn/moments/SqueezeSqueeze1layerfilter0_self_att_conv_head_0/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes
:
¶
6layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1Squeeze5layerfilter0_self_att_conv_head_0/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes
:
{
0layerfilter0_self_att_conv_head_0/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

2layerfilter0_self_att_conv_head_0/bn/cond/switch_tIdentity2layerfilter0_self_att_conv_head_0/bn/cond/Switch:1*
T0
*
_output_shapes
: 

2layerfilter0_self_att_conv_head_0/bn/cond/switch_fIdentity0layerfilter0_self_att_conv_head_0/bn/cond/Switch*
T0
*
_output_shapes
: 
m
1layerfilter0_self_att_conv_head_0/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ú
layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes
:
æ
rlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:
Ô
ylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssignrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragelayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
:
¤
wlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/readIdentityrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
Þ
layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes
:
ê
tlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
shared_name *
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
shape:*
_output_shapes
:
Ü
{layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssigntlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
validate_shape(*
use_locking(*
T0*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
ª
ylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentitytlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
Â
Hlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/decayConst3^layerfilter0_self_att_conv_head_0/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
Ò
Xlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst3^layerfilter0_self_att_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
¢
Vlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubXlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xHlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ì
Xlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Subalayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1clayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
:
È
_layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchwlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read1layerfilter0_self_att_conv_head_0/bn/cond/pred_id*
T0*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
È
alayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch4layerfilter0_self_att_conv_head_0/bn/moments/Squeeze1layerfilter0_self_att_conv_head_0/bn/cond/pred_id*
T0*G
_class=
;9loc:@layerfilter0_self_att_conv_head_0/bn/moments/Squeeze* 
_output_shapes
::
´
Vlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulXlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Vlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
:
Ô
Rlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSub[layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Vlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
À
Ylayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitchrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage1layerfilter0_self_att_conv_head_0/bn/cond/pred_id*
T0*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
Ô
Zlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst3^layerfilter0_self_att_conv_head_0/bn/cond/switch_t*
dtype0*
valueB
 *  ?*
_output_shapes
: 
¦
Xlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubZlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xHlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ò
Zlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Subclayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1elayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
:
Î
alayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read1layerfilter0_self_att_conv_head_0/bn/cond/pred_id*
T0*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
Î
clayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch6layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_11layerfilter0_self_att_conv_head_0/bn/cond/pred_id*
T0*I
_class?
=;loc:@layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
::
º
Xlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulZlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Xlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
:
Ü
Tlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSub]layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Xlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
Æ
[layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchtlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage1layerfilter0_self_att_conv_head_0/bn/cond/pred_id*
T0*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
ö
Blayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverageNoOpS^layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvgU^layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
©
<layerfilter0_self_att_conv_head_0/bn/cond/control_dependencyIdentity2layerfilter0_self_att_conv_head_0/bn/cond/switch_tC^layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage*
T0
*E
_class;
97loc:@layerfilter0_self_att_conv_head_0/bn/cond/switch_t*
_output_shapes
: 
k
.layerfilter0_self_att_conv_head_0/bn/cond/NoOpNoOp3^layerfilter0_self_att_conv_head_0/bn/cond/switch_f

>layerfilter0_self_att_conv_head_0/bn/cond/control_dependency_1Identity2layerfilter0_self_att_conv_head_0/bn/cond/switch_f/^layerfilter0_self_att_conv_head_0/bn/cond/NoOp*
T0
*E
_class;
97loc:@layerfilter0_self_att_conv_head_0/bn/cond/switch_f*
_output_shapes
: 
â
/layerfilter0_self_att_conv_head_0/bn/cond/MergeMerge>layerfilter0_self_att_conv_head_0/bn/cond/control_dependency_1<layerfilter0_self_att_conv_head_0/bn/cond/control_dependency*
T0
*
N*
_output_shapes
: : 
}
2layerfilter0_self_att_conv_head_0/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

4layerfilter0_self_att_conv_head_0/bn/cond_1/switch_tIdentity4layerfilter0_self_att_conv_head_0/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 

4layerfilter0_self_att_conv_head_0/bn/cond_1/switch_fIdentity2layerfilter0_self_att_conv_head_0/bn/cond_1/Switch*
T0
*
_output_shapes
: 
o
3layerfilter0_self_att_conv_head_0/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ö
4layerfilter0_self_att_conv_head_0/bn/cond_1/IdentityIdentity=layerfilter0_self_att_conv_head_0/bn/cond_1/Identity/Switch:10^layerfilter0_self_att_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:
¤
;layerfilter0_self_att_conv_head_0/bn/cond_1/Identity/SwitchSwitch4layerfilter0_self_att_conv_head_0/bn/moments/Squeeze3layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id*
T0*G
_class=
;9loc:@layerfilter0_self_att_conv_head_0/bn/moments/Squeeze* 
_output_shapes
::
Ú
6layerfilter0_self_att_conv_head_0/bn/cond_1/Identity_1Identity?layerfilter0_self_att_conv_head_0/bn/cond_1/Identity_1/Switch:10^layerfilter0_self_att_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:
ª
=layerfilter0_self_att_conv_head_0/bn/cond_1/Identity_1/SwitchSwitch6layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_13layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id*I
_class?
=;loc:@layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1*
T0* 
_output_shapes
::

4layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_1Switchwlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read3layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id*
T0*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
£
4layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_2Switchylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read3layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id*
T0*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
Ö
1layerfilter0_self_att_conv_head_0/bn/cond_1/MergeMerge4layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_14layerfilter0_self_att_conv_head_0/bn/cond_1/Identity*
T0*
N*
_output_shapes

:: 
Ú
3layerfilter0_self_att_conv_head_0/bn/cond_1/Merge_1Merge4layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_26layerfilter0_self_att_conv_head_0/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

:: 
y
4layerfilter0_self_att_conv_head_0/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 
É
2layerfilter0_self_att_conv_head_0/bn/batchnorm/addAdd3layerfilter0_self_att_conv_head_0/bn/cond_1/Merge_14layerfilter0_self_att_conv_head_0/bn/batchnorm/add/y*
T0*
_output_shapes
:

4layerfilter0_self_att_conv_head_0/bn/batchnorm/RsqrtRsqrt2layerfilter0_self_att_conv_head_0/bn/batchnorm/add*
T0*
_output_shapes
:
Å
2layerfilter0_self_att_conv_head_0/bn/batchnorm/mulMul4layerfilter0_self_att_conv_head_0/bn/batchnorm/Rsqrt/layerfilter0_self_att_conv_head_0/bn/gamma/read*
T0*
_output_shapes
:
Ë
4layerfilter0_self_att_conv_head_0/bn/batchnorm/mul_1Mul)layerfilter0_self_att_conv_head_0/BiasAdd2layerfilter0_self_att_conv_head_0/bn/batchnorm/mul*
T0*&
_output_shapes
:d
Ç
4layerfilter0_self_att_conv_head_0/bn/batchnorm/mul_2Mul1layerfilter0_self_att_conv_head_0/bn/cond_1/Merge2layerfilter0_self_att_conv_head_0/bn/batchnorm/mul*
T0*
_output_shapes
:
Ä
2layerfilter0_self_att_conv_head_0/bn/batchnorm/subSub.layerfilter0_self_att_conv_head_0/bn/beta/read4layerfilter0_self_att_conv_head_0/bn/batchnorm/mul_2*
T0*
_output_shapes
:
Ö
4layerfilter0_self_att_conv_head_0/bn/batchnorm/add_1Add4layerfilter0_self_att_conv_head_0/bn/batchnorm/mul_12layerfilter0_self_att_conv_head_0/bn/batchnorm/sub*
T0*&
_output_shapes
:d

&layerfilter0_self_att_conv_head_0/ReluRelu4layerfilter0_self_att_conv_head_0/bn/batchnorm/add_1*
T0*&
_output_shapes
:d
á
Jlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/shapeConst*
dtype0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*%
valueB"             *
_output_shapes
:
Ë
Hlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/minConst*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*
valueB
 *JQÚ¾*
dtype0*
_output_shapes
: 
Ë
Hlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/maxConst*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*
valueB
 *JQÚ>*
dtype0*
_output_shapes
: 
Ä
Rlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/RandomUniformRandomUniformJlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/shape*
dtype0*
seed2 *

seed *
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*&
_output_shapes
: 
Â
Hlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/subSubHlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/maxHlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/min*
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*
_output_shapes
: 
Ü
Hlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/mulMulRlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/RandomUniformHlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/sub*
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*&
_output_shapes
: 
Î
Dlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniformAddHlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/mulHlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform/min*
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*&
_output_shapes
: 
ú
)layerfilter0_neib_att_conv_head_0/weights
VariableV2"/device:CPU:0*
shared_name *<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*
dtype0*
	container *
shape: *&
_output_shapes
: 
Ò
0layerfilter0_neib_att_conv_head_0/weights/AssignAssign)layerfilter0_neib_att_conv_head_0/weightsDlayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform"/device:CPU:0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*
validate_shape(*
use_locking(*
T0*&
_output_shapes
: 
ã
.layerfilter0_neib_att_conv_head_0/weights/readIdentity)layerfilter0_neib_att_conv_head_0/weights"/device:CPU:0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*
T0*&
_output_shapes
: 

(layerfilter0_neib_att_conv_head_0/L2LossL2Loss.layerfilter0_neib_att_conv_head_0/weights/read*
T0*
_output_shapes
: 
t
/layerfilter0_neib_att_conv_head_0/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
°
-layerfilter0_neib_att_conv_head_0/weight_lossMul(layerfilter0_neib_att_conv_head_0/L2Loss/layerfilter0_neib_att_conv_head_0/weight_loss/y*
T0*
_output_shapes
: 
¸
(layerfilter0_neib_att_conv_head_0/Conv2DConv2Dlayerfilter0_edgefea_0/Relu.layerfilter0_neib_att_conv_head_0/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
:d
Ä
:layerfilter0_neib_att_conv_head_0/biases/Initializer/ConstConst*;
_class1
/-loc:@layerfilter0_neib_att_conv_head_0/biases*
valueB*    *
dtype0*
_output_shapes
:
à
(layerfilter0_neib_att_conv_head_0/biases
VariableV2"/device:CPU:0*
shared_name *;
_class1
/-loc:@layerfilter0_neib_att_conv_head_0/biases*
dtype0*
	container *
shape:*
_output_shapes
:
¹
/layerfilter0_neib_att_conv_head_0/biases/AssignAssign(layerfilter0_neib_att_conv_head_0/biases:layerfilter0_neib_att_conv_head_0/biases/Initializer/Const"/device:CPU:0*
validate_shape(*
use_locking(*
T0*;
_class1
/-loc:@layerfilter0_neib_att_conv_head_0/biases*
_output_shapes
:
Ô
-layerfilter0_neib_att_conv_head_0/biases/readIdentity(layerfilter0_neib_att_conv_head_0/biases"/device:CPU:0*
T0*;
_class1
/-loc:@layerfilter0_neib_att_conv_head_0/biases*
_output_shapes
:
Õ
)layerfilter0_neib_att_conv_head_0/BiasAddBiasAdd(layerfilter0_neib_att_conv_head_0/Conv2D-layerfilter0_neib_att_conv_head_0/biases/read*
data_formatNHWC*
T0*&
_output_shapes
:d
w
*layerfilter0_neib_att_conv_head_0/bn/ConstConst*
valueB*    *
dtype0*
_output_shapes
:

)layerfilter0_neib_att_conv_head_0/bn/beta
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:

0layerfilter0_neib_att_conv_head_0/bn/beta/AssignAssign)layerfilter0_neib_att_conv_head_0/bn/beta*layerfilter0_neib_att_conv_head_0/bn/Const*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
È
.layerfilter0_neib_att_conv_head_0/bn/beta/readIdentity)layerfilter0_neib_att_conv_head_0/bn/beta*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/bn/beta*
T0*
_output_shapes
:
y
,layerfilter0_neib_att_conv_head_0/bn/Const_1Const*
valueB*  ?*
dtype0*
_output_shapes
:

*layerfilter0_neib_att_conv_head_0/bn/gamma
VariableV2*
shared_name *
dtype0*
	container *
shape:*
_output_shapes
:
¢
1layerfilter0_neib_att_conv_head_0/bn/gamma/AssignAssign*layerfilter0_neib_att_conv_head_0/bn/gamma,layerfilter0_neib_att_conv_head_0/bn/Const_1*
T0*=
_class3
1/loc:@layerfilter0_neib_att_conv_head_0/bn/gamma*
validate_shape(*
use_locking(*
_output_shapes
:
Ë
/layerfilter0_neib_att_conv_head_0/bn/gamma/readIdentity*layerfilter0_neib_att_conv_head_0/bn/gamma*
T0*=
_class3
1/loc:@layerfilter0_neib_att_conv_head_0/bn/gamma*
_output_shapes
:

Clayerfilter0_neib_att_conv_head_0/bn/moments/mean/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
÷
1layerfilter0_neib_att_conv_head_0/bn/moments/meanMean)layerfilter0_neib_att_conv_head_0/BiasAddClayerfilter0_neib_att_conv_head_0/bn/moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:
­
9layerfilter0_neib_att_conv_head_0/bn/moments/StopGradientStopGradient1layerfilter0_neib_att_conv_head_0/bn/moments/mean*
T0*&
_output_shapes
:
ê
>layerfilter0_neib_att_conv_head_0/bn/moments/SquaredDifferenceSquaredDifference)layerfilter0_neib_att_conv_head_0/BiasAdd9layerfilter0_neib_att_conv_head_0/bn/moments/StopGradient*
T0*&
_output_shapes
:d

Glayerfilter0_neib_att_conv_head_0/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:

5layerfilter0_neib_att_conv_head_0/bn/moments/varianceMean>layerfilter0_neib_att_conv_head_0/bn/moments/SquaredDifferenceGlayerfilter0_neib_att_conv_head_0/bn/moments/variance/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:
°
4layerfilter0_neib_att_conv_head_0/bn/moments/SqueezeSqueeze1layerfilter0_neib_att_conv_head_0/bn/moments/mean*
T0*
squeeze_dims
 *
_output_shapes
:
¶
6layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1Squeeze5layerfilter0_neib_att_conv_head_0/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes
:
{
0layerfilter0_neib_att_conv_head_0/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

2layerfilter0_neib_att_conv_head_0/bn/cond/switch_tIdentity2layerfilter0_neib_att_conv_head_0/bn/cond/Switch:1*
T0
*
_output_shapes
: 

2layerfilter0_neib_att_conv_head_0/bn/cond/switch_fIdentity0layerfilter0_neib_att_conv_head_0/bn/cond/Switch*
T0
*
_output_shapes
: 
m
1layerfilter0_neib_att_conv_head_0/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ú
layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes
:
æ
rlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
shared_name *
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
shape:*
_output_shapes
:
Ô
ylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssignrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragelayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
T0*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:
¤
wlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/readIdentityrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
Þ
layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes
:
ê
tlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
shared_name *
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
shape:*
_output_shapes
:
Ü
{layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssigntlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ª
ylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentitytlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
Â
Hlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/decayConst3^layerfilter0_neib_att_conv_head_0/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
Ò
Xlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst3^layerfilter0_neib_att_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
¢
Vlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubXlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xHlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ì
Xlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Subalayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1clayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
:
È
_layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchwlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read1layerfilter0_neib_att_conv_head_0/bn/cond/pred_id*
T0*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
È
alayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch4layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze1layerfilter0_neib_att_conv_head_0/bn/cond/pred_id*G
_class=
;9loc:@layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze*
T0* 
_output_shapes
::
´
Vlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulXlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Vlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
:
Ô
Rlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSub[layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Vlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
À
Ylayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitchrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage1layerfilter0_neib_att_conv_head_0/bn/cond/pred_id*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0* 
_output_shapes
::
Ô
Zlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst3^layerfilter0_neib_att_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
¦
Xlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubZlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xHlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ò
Zlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Subclayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1elayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
:
Î
alayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read1layerfilter0_neib_att_conv_head_0/bn/cond/pred_id*
T0*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
Î
clayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch6layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_11layerfilter0_neib_att_conv_head_0/bn/cond/pred_id*
T0*I
_class?
=;loc:@layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
::
º
Xlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulZlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Xlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
:
Ü
Tlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSub]layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Xlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
T0*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
use_locking( *
_output_shapes
:
Æ
[layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchtlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage1layerfilter0_neib_att_conv_head_0/bn/cond/pred_id*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0* 
_output_shapes
::
ö
Blayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverageNoOpS^layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvgU^layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
©
<layerfilter0_neib_att_conv_head_0/bn/cond/control_dependencyIdentity2layerfilter0_neib_att_conv_head_0/bn/cond/switch_tC^layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage*
T0
*E
_class;
97loc:@layerfilter0_neib_att_conv_head_0/bn/cond/switch_t*
_output_shapes
: 
k
.layerfilter0_neib_att_conv_head_0/bn/cond/NoOpNoOp3^layerfilter0_neib_att_conv_head_0/bn/cond/switch_f

>layerfilter0_neib_att_conv_head_0/bn/cond/control_dependency_1Identity2layerfilter0_neib_att_conv_head_0/bn/cond/switch_f/^layerfilter0_neib_att_conv_head_0/bn/cond/NoOp*
T0
*E
_class;
97loc:@layerfilter0_neib_att_conv_head_0/bn/cond/switch_f*
_output_shapes
: 
â
/layerfilter0_neib_att_conv_head_0/bn/cond/MergeMerge>layerfilter0_neib_att_conv_head_0/bn/cond/control_dependency_1<layerfilter0_neib_att_conv_head_0/bn/cond/control_dependency*
N*
T0
*
_output_shapes
: : 
}
2layerfilter0_neib_att_conv_head_0/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

4layerfilter0_neib_att_conv_head_0/bn/cond_1/switch_tIdentity4layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 

4layerfilter0_neib_att_conv_head_0/bn/cond_1/switch_fIdentity2layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch*
T0
*
_output_shapes
: 
o
3layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ö
4layerfilter0_neib_att_conv_head_0/bn/cond_1/IdentityIdentity=layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity/Switch:10^layerfilter0_neib_att_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:
¤
;layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity/SwitchSwitch4layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze3layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id*
T0*G
_class=
;9loc:@layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze* 
_output_shapes
::
Ú
6layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity_1Identity?layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity_1/Switch:10^layerfilter0_neib_att_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:
ª
=layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity_1/SwitchSwitch6layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_13layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id*
T0*I
_class?
=;loc:@layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
::

4layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_1Switchwlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read3layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id*
T0*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
£
4layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_2Switchylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read3layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id*
T0*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
Ö
1layerfilter0_neib_att_conv_head_0/bn/cond_1/MergeMerge4layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_14layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity*
T0*
N*
_output_shapes

:: 
Ú
3layerfilter0_neib_att_conv_head_0/bn/cond_1/Merge_1Merge4layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_26layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

:: 
y
4layerfilter0_neib_att_conv_head_0/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 
É
2layerfilter0_neib_att_conv_head_0/bn/batchnorm/addAdd3layerfilter0_neib_att_conv_head_0/bn/cond_1/Merge_14layerfilter0_neib_att_conv_head_0/bn/batchnorm/add/y*
T0*
_output_shapes
:

4layerfilter0_neib_att_conv_head_0/bn/batchnorm/RsqrtRsqrt2layerfilter0_neib_att_conv_head_0/bn/batchnorm/add*
T0*
_output_shapes
:
Å
2layerfilter0_neib_att_conv_head_0/bn/batchnorm/mulMul4layerfilter0_neib_att_conv_head_0/bn/batchnorm/Rsqrt/layerfilter0_neib_att_conv_head_0/bn/gamma/read*
T0*
_output_shapes
:
Ë
4layerfilter0_neib_att_conv_head_0/bn/batchnorm/mul_1Mul)layerfilter0_neib_att_conv_head_0/BiasAdd2layerfilter0_neib_att_conv_head_0/bn/batchnorm/mul*
T0*&
_output_shapes
:d
Ç
4layerfilter0_neib_att_conv_head_0/bn/batchnorm/mul_2Mul1layerfilter0_neib_att_conv_head_0/bn/cond_1/Merge2layerfilter0_neib_att_conv_head_0/bn/batchnorm/mul*
T0*
_output_shapes
:
Ä
2layerfilter0_neib_att_conv_head_0/bn/batchnorm/subSub.layerfilter0_neib_att_conv_head_0/bn/beta/read4layerfilter0_neib_att_conv_head_0/bn/batchnorm/mul_2*
T0*
_output_shapes
:
Ö
4layerfilter0_neib_att_conv_head_0/bn/batchnorm/add_1Add4layerfilter0_neib_att_conv_head_0/bn/batchnorm/mul_12layerfilter0_neib_att_conv_head_0/bn/batchnorm/sub*
T0*&
_output_shapes
:d

&layerfilter0_neib_att_conv_head_0/ReluRelu4layerfilter0_neib_att_conv_head_0/bn/batchnorm/add_1*
T0*&
_output_shapes
:d

add_6Add&layerfilter0_self_att_conv_head_0/Relu&layerfilter0_neib_att_conv_head_0/Relu*
T0*&
_output_shapes
:d
i
transpose_4/permConst*%
valueB"             *
dtype0*
_output_shapes
:
o
transpose_4	Transposeadd_6transpose_4/perm*
T0*
Tperm0*&
_output_shapes
:d
d
	LeakyRelu	LeakyRelutranspose_4*
T0*
alpha%ÍÌL>*&
_output_shapes
:d
N
SoftmaxSoftmax	LeakyRelu*
T0*&
_output_shapes
:d

MatMul_1BatchMatMulV2Softmaxlayerfilter0_edgefea_0/Relu*
T0*
adj_x( *
adj_y( *&
_output_shapes
:d 

 BiasAdd/biases/Initializer/zerosConst*!
_class
loc:@BiasAdd/biases*
valueB *    *
dtype0*
_output_shapes
: 

BiasAdd/biases
VariableV2*
dtype0*
	container *
shape: *
shared_name *!
_class
loc:@BiasAdd/biases*
_output_shapes
: 
Â
BiasAdd/biases/AssignAssignBiasAdd/biases BiasAdd/biases/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@BiasAdd/biases*
validate_shape(*
_output_shapes
: 
w
BiasAdd/biases/readIdentityBiasAdd/biases*
T0*!
_class
loc:@BiasAdd/biases*
_output_shapes
: 

BiasAdd/BiasAddBiasAddMatMul_1BiasAdd/biases/read*
data_formatNHWC*
T0*&
_output_shapes
:d 
N
ReluReluBiasAdd/BiasAdd*
T0*&
_output_shapes
:d 
\
concat/concat_dimConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
I
concatIdentityRelu*
T0*&
_output_shapes
:d 
[
ExpandDims_6/dimConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
v
ExpandDims_6
ExpandDimsPlaceholderExpandDims_6/dim*

Tdim0*
T0*&
_output_shapes
:d

X
concat_1/axisConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 

concat_1ConcatV2ExpandDims_6concatconcat_1/axis*

Tidx0*
T0*
N*&
_output_shapes
:d*
^
concat_2/concat_dimConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
b
concat_2Identitylayerfilter0_edgefea_0/Relu*
T0*&
_output_shapes
:d 
`
Max/reduction_indicesConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
y
MaxMaxconcat_2Max/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:d 
¯
1gapnet00/weights/Initializer/random_uniform/shapeConst*#
_class
loc:@gapnet00/weights*%
valueB"      *   @   *
dtype0*
_output_shapes
:

/gapnet00/weights/Initializer/random_uniform/minConst*#
_class
loc:@gapnet00/weights*
valueB
 * s¾*
dtype0*
_output_shapes
: 

/gapnet00/weights/Initializer/random_uniform/maxConst*#
_class
loc:@gapnet00/weights*
valueB
 * s>*
dtype0*
_output_shapes
: 
ù
9gapnet00/weights/Initializer/random_uniform/RandomUniformRandomUniform1gapnet00/weights/Initializer/random_uniform/shape*
T0*#
_class
loc:@gapnet00/weights*
dtype0*
seed2 *

seed *&
_output_shapes
:*@
Þ
/gapnet00/weights/Initializer/random_uniform/subSub/gapnet00/weights/Initializer/random_uniform/max/gapnet00/weights/Initializer/random_uniform/min*
T0*#
_class
loc:@gapnet00/weights*
_output_shapes
: 
ø
/gapnet00/weights/Initializer/random_uniform/mulMul9gapnet00/weights/Initializer/random_uniform/RandomUniform/gapnet00/weights/Initializer/random_uniform/sub*
T0*#
_class
loc:@gapnet00/weights*&
_output_shapes
:*@
ê
+gapnet00/weights/Initializer/random_uniformAdd/gapnet00/weights/Initializer/random_uniform/mul/gapnet00/weights/Initializer/random_uniform/min*
T0*#
_class
loc:@gapnet00/weights*&
_output_shapes
:*@
È
gapnet00/weights
VariableV2"/device:CPU:0*
shared_name *#
_class
loc:@gapnet00/weights*
dtype0*
	container *
shape:*@*&
_output_shapes
:*@
î
gapnet00/weights/AssignAssigngapnet00/weights+gapnet00/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@gapnet00/weights*
validate_shape(*&
_output_shapes
:*@

gapnet00/weights/readIdentitygapnet00/weights"/device:CPU:0*
T0*#
_class
loc:@gapnet00/weights*&
_output_shapes
:*@
Q
gapnet00/L2LossL2Lossgapnet00/weights/read*
T0*
_output_shapes
: 
[
gapnet00/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
e
gapnet00/weight_lossMulgapnet00/L2Lossgapnet00/weight_loss/y*
T0*
_output_shapes
: 
ó
gapnet00/Conv2DConv2Dconcat_1gapnet00/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
:d@

!gapnet00/biases/Initializer/ConstConst*"
_class
loc:@gapnet00/biases*
valueB@*    *
dtype0*
_output_shapes
:@
®
gapnet00/biases
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:@*
shared_name *"
_class
loc:@gapnet00/biases*
_output_shapes
:@
Õ
gapnet00/biases/AssignAssigngapnet00/biases!gapnet00/biases/Initializer/Const"/device:CPU:0*
T0*"
_class
loc:@gapnet00/biases*
validate_shape(*
use_locking(*
_output_shapes
:@

gapnet00/biases/readIdentitygapnet00/biases"/device:CPU:0*
T0*"
_class
loc:@gapnet00/biases*
_output_shapes
:@

gapnet00/BiasAddBiasAddgapnet00/Conv2Dgapnet00/biases/read*
T0*
data_formatNHWC*&
_output_shapes
:d@
^
gapnet00/bn/ConstConst*
valueB@*    *
dtype0*
_output_shapes
:@
|
gapnet00/bn/beta
VariableV2*
shape:@*
shared_name *
dtype0*
	container *
_output_shapes
:@
¹
gapnet00/bn/beta/AssignAssigngapnet00/bn/betagapnet00/bn/Const*
use_locking(*
T0*#
_class
loc:@gapnet00/bn/beta*
validate_shape(*
_output_shapes
:@
}
gapnet00/bn/beta/readIdentitygapnet00/bn/beta*
T0*#
_class
loc:@gapnet00/bn/beta*
_output_shapes
:@
`
gapnet00/bn/Const_1Const*
valueB@*  ?*
dtype0*
_output_shapes
:@
}
gapnet00/bn/gamma
VariableV2*
shared_name *
dtype0*
	container *
shape:@*
_output_shapes
:@
¾
gapnet00/bn/gamma/AssignAssigngapnet00/bn/gammagapnet00/bn/Const_1*$
_class
loc:@gapnet00/bn/gamma*
validate_shape(*
use_locking(*
T0*
_output_shapes
:@

gapnet00/bn/gamma/readIdentitygapnet00/bn/gamma*
T0*$
_class
loc:@gapnet00/bn/gamma*
_output_shapes
:@

*gapnet00/bn/moments/mean/reduction_indicesConst*
dtype0*!
valueB"          *
_output_shapes
:
¬
gapnet00/bn/moments/meanMeangapnet00/BiasAdd*gapnet00/bn/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:@
{
 gapnet00/bn/moments/StopGradientStopGradientgapnet00/bn/moments/mean*
T0*&
_output_shapes
:@

%gapnet00/bn/moments/SquaredDifferenceSquaredDifferencegapnet00/BiasAdd gapnet00/bn/moments/StopGradient*
T0*&
_output_shapes
:d@

.gapnet00/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
É
gapnet00/bn/moments/varianceMean%gapnet00/bn/moments/SquaredDifference.gapnet00/bn/moments/variance/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:@
~
gapnet00/bn/moments/SqueezeSqueezegapnet00/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes
:@

gapnet00/bn/moments/Squeeze_1Squeezegapnet00/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes
:@
b
gapnet00/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
a
gapnet00/bn/cond/switch_tIdentitygapnet00/bn/cond/Switch:1*
T0
*
_output_shapes
: 
_
gapnet00/bn/cond/switch_fIdentitygapnet00/bn/cond/Switch*
T0
*
_output_shapes
: 
T
gapnet00/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
ô
Rgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage*
valueB@*    *
dtype0*
_output_shapes
:@

@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
dtype0*
	container *
shape:@*
shared_name *S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:@

Ggapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssign@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverageRgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@

Egapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/readIdentity@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage*
T0*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:@
ø
Tgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB@*    *
dtype0*
_output_shapes
:@

Bgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
dtype0*
	container *
shape:@*
shared_name *U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:@

Igapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssignBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverageTgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
:@

Ggapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentityBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:@

/gapnet00/bn/cond/ExponentialMovingAverage/decayConst^gapnet00/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
 
?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst^gapnet00/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
×
=gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSub?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x/gapnet00/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 

?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1SubHgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1Jgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
:@
±
Fgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchEgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/readgapnet00/bn/cond/pred_id*
T0*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
:@:@
ä
Hgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switchgapnet00/bn/moments/Squeezegapnet00/bn/cond/pred_id*
T0*.
_class$
" loc:@gapnet00/bn/moments/Squeeze* 
_output_shapes
:@:@
é
=gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMul?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1=gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
:@
Ö
9gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSubBgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1=gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:@
©
@gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitch@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAveragegapnet00/bn/cond/pred_id*
T0*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
:@:@
¢
Agapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst^gapnet00/bn/cond/switch_t*
dtype0*
valueB
 *  ?*
_output_shapes
: 
Û
?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubAgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x/gapnet00/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 

Agapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1SubJgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1Lgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
:@
·
Hgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchGgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/readgapnet00/bn/cond/pred_id*
T0*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@
ê
Jgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switchgapnet00/bn/moments/Squeeze_1gapnet00/bn/cond/pred_id*
T0*0
_class&
$"loc:@gapnet00/bn/moments/Squeeze_1* 
_output_shapes
:@:@
ï
?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulAgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
:@
Þ
;gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSubDgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:@
¯
Bgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAveragegapnet00/bn/cond/pred_id*
T0*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@
«
)gapnet00/bn/cond/ExponentialMovingAverageNoOp:^gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg<^gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
Å
#gapnet00/bn/cond/control_dependencyIdentitygapnet00/bn/cond/switch_t*^gapnet00/bn/cond/ExponentialMovingAverage*
T0
*,
_class"
 loc:@gapnet00/bn/cond/switch_t*
_output_shapes
: 
9
gapnet00/bn/cond/NoOpNoOp^gapnet00/bn/cond/switch_f
³
%gapnet00/bn/cond/control_dependency_1Identitygapnet00/bn/cond/switch_f^gapnet00/bn/cond/NoOp*
T0
*,
_class"
 loc:@gapnet00/bn/cond/switch_f*
_output_shapes
: 

gapnet00/bn/cond/MergeMerge%gapnet00/bn/cond/control_dependency_1#gapnet00/bn/cond/control_dependency*
T0
*
N*
_output_shapes
: : 
d
gapnet00/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
e
gapnet00/bn/cond_1/switch_tIdentitygapnet00/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 
c
gapnet00/bn/cond_1/switch_fIdentitygapnet00/bn/cond_1/Switch*
T0
*
_output_shapes
: 
V
gapnet00/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 

gapnet00/bn/cond_1/IdentityIdentity$gapnet00/bn/cond_1/Identity/Switch:1^gapnet00/bn/cond/Merge*
T0*
_output_shapes
:@
À
"gapnet00/bn/cond_1/Identity/SwitchSwitchgapnet00/bn/moments/Squeezegapnet00/bn/cond_1/pred_id*
T0*.
_class$
" loc:@gapnet00/bn/moments/Squeeze* 
_output_shapes
:@:@

gapnet00/bn/cond_1/Identity_1Identity&gapnet00/bn/cond_1/Identity_1/Switch:1^gapnet00/bn/cond/Merge*
T0*
_output_shapes
:@
Æ
$gapnet00/bn/cond_1/Identity_1/SwitchSwitchgapnet00/bn/moments/Squeeze_1gapnet00/bn/cond_1/pred_id*
T0*0
_class&
$"loc:@gapnet00/bn/moments/Squeeze_1* 
_output_shapes
:@:@

gapnet00/bn/cond_1/Switch_1SwitchEgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/readgapnet00/bn/cond_1/pred_id*
T0*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
:@:@

gapnet00/bn/cond_1/Switch_2SwitchGgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/readgapnet00/bn/cond_1/pred_id*
T0*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@

gapnet00/bn/cond_1/MergeMergegapnet00/bn/cond_1/Switch_1gapnet00/bn/cond_1/Identity*
N*
T0*
_output_shapes

:@: 

gapnet00/bn/cond_1/Merge_1Mergegapnet00/bn/cond_1/Switch_2gapnet00/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

:@: 
`
gapnet00/bn/batchnorm/add/yConst*
dtype0*
valueB
 *o:*
_output_shapes
: 
~
gapnet00/bn/batchnorm/addAddgapnet00/bn/cond_1/Merge_1gapnet00/bn/batchnorm/add/y*
T0*
_output_shapes
:@
d
gapnet00/bn/batchnorm/RsqrtRsqrtgapnet00/bn/batchnorm/add*
T0*
_output_shapes
:@
z
gapnet00/bn/batchnorm/mulMulgapnet00/bn/batchnorm/Rsqrtgapnet00/bn/gamma/read*
T0*
_output_shapes
:@

gapnet00/bn/batchnorm/mul_1Mulgapnet00/BiasAddgapnet00/bn/batchnorm/mul*
T0*&
_output_shapes
:d@
|
gapnet00/bn/batchnorm/mul_2Mulgapnet00/bn/cond_1/Mergegapnet00/bn/batchnorm/mul*
T0*
_output_shapes
:@
y
gapnet00/bn/batchnorm/subSubgapnet00/bn/beta/readgapnet00/bn/batchnorm/mul_2*
T0*
_output_shapes
:@

gapnet00/bn/batchnorm/add_1Addgapnet00/bn/batchnorm/mul_1gapnet00/bn/batchnorm/sub*
T0*&
_output_shapes
:d@
c
gapnet00/ReluRelugapnet00/bn/batchnorm/add_1*
T0*&
_output_shapes
:d@
¯
1gapnet01/weights/Initializer/random_uniform/shapeConst*
dtype0*#
_class
loc:@gapnet01/weights*%
valueB"      @      *
_output_shapes
:

/gapnet01/weights/Initializer/random_uniform/minConst*#
_class
loc:@gapnet01/weights*
valueB
 *ó5¾*
dtype0*
_output_shapes
: 

/gapnet01/weights/Initializer/random_uniform/maxConst*#
_class
loc:@gapnet01/weights*
valueB
 *ó5>*
dtype0*
_output_shapes
: 
ú
9gapnet01/weights/Initializer/random_uniform/RandomUniformRandomUniform1gapnet01/weights/Initializer/random_uniform/shape*
seed2 *

seed *
T0*#
_class
loc:@gapnet01/weights*
dtype0*'
_output_shapes
:@
Þ
/gapnet01/weights/Initializer/random_uniform/subSub/gapnet01/weights/Initializer/random_uniform/max/gapnet01/weights/Initializer/random_uniform/min*
T0*#
_class
loc:@gapnet01/weights*
_output_shapes
: 
ù
/gapnet01/weights/Initializer/random_uniform/mulMul9gapnet01/weights/Initializer/random_uniform/RandomUniform/gapnet01/weights/Initializer/random_uniform/sub*
T0*#
_class
loc:@gapnet01/weights*'
_output_shapes
:@
ë
+gapnet01/weights/Initializer/random_uniformAdd/gapnet01/weights/Initializer/random_uniform/mul/gapnet01/weights/Initializer/random_uniform/min*
T0*#
_class
loc:@gapnet01/weights*'
_output_shapes
:@
Ê
gapnet01/weights
VariableV2"/device:CPU:0*
shared_name *#
_class
loc:@gapnet01/weights*
dtype0*
	container *
shape:@*'
_output_shapes
:@
ï
gapnet01/weights/AssignAssigngapnet01/weights+gapnet01/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@gapnet01/weights*
validate_shape(*'
_output_shapes
:@

gapnet01/weights/readIdentitygapnet01/weights"/device:CPU:0*
T0*#
_class
loc:@gapnet01/weights*'
_output_shapes
:@
Q
gapnet01/L2LossL2Lossgapnet01/weights/read*
T0*
_output_shapes
: 
[
gapnet01/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
e
gapnet01/weight_lossMulgapnet01/L2Lossgapnet01/weight_loss/y*
T0*
_output_shapes
: 
ù
gapnet01/Conv2DConv2Dgapnet00/Relugapnet01/weights/read*
data_formatNHWC*
strides
*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*
	dilations
*
T0*'
_output_shapes
:d

!gapnet01/biases/Initializer/ConstConst*"
_class
loc:@gapnet01/biases*
valueB*    *
dtype0*
_output_shapes	
:
°
gapnet01/biases
VariableV2"/device:CPU:0*
shape:*
shared_name *"
_class
loc:@gapnet01/biases*
dtype0*
	container *
_output_shapes	
:
Ö
gapnet01/biases/AssignAssigngapnet01/biases!gapnet01/biases/Initializer/Const"/device:CPU:0*"
_class
loc:@gapnet01/biases*
validate_shape(*
use_locking(*
T0*
_output_shapes	
:

gapnet01/biases/readIdentitygapnet01/biases"/device:CPU:0*"
_class
loc:@gapnet01/biases*
T0*
_output_shapes	
:

gapnet01/BiasAddBiasAddgapnet01/Conv2Dgapnet01/biases/read*
T0*
data_formatNHWC*'
_output_shapes
:d
`
gapnet01/bn/ConstConst*
valueB*    *
dtype0*
_output_shapes	
:
~
gapnet01/bn/beta
VariableV2*
shared_name *
dtype0*
	container *
shape:*
_output_shapes	
:
º
gapnet01/bn/beta/AssignAssigngapnet01/bn/betagapnet01/bn/Const*
use_locking(*
T0*#
_class
loc:@gapnet01/bn/beta*
validate_shape(*
_output_shapes	
:
~
gapnet01/bn/beta/readIdentitygapnet01/bn/beta*#
_class
loc:@gapnet01/bn/beta*
T0*
_output_shapes	
:
b
gapnet01/bn/Const_1Const*
dtype0*
valueB*  ?*
_output_shapes	
:

gapnet01/bn/gamma
VariableV2*
shape:*
shared_name *
dtype0*
	container *
_output_shapes	
:
¿
gapnet01/bn/gamma/AssignAssigngapnet01/bn/gammagapnet01/bn/Const_1*
use_locking(*
T0*$
_class
loc:@gapnet01/bn/gamma*
validate_shape(*
_output_shapes	
:

gapnet01/bn/gamma/readIdentitygapnet01/bn/gamma*
T0*$
_class
loc:@gapnet01/bn/gamma*
_output_shapes	
:

*gapnet01/bn/moments/mean/reduction_indicesConst*
dtype0*!
valueB"          *
_output_shapes
:
­
gapnet01/bn/moments/meanMeangapnet01/BiasAdd*gapnet01/bn/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:
|
 gapnet01/bn/moments/StopGradientStopGradientgapnet01/bn/moments/mean*
T0*'
_output_shapes
:
 
%gapnet01/bn/moments/SquaredDifferenceSquaredDifferencegapnet01/BiasAdd gapnet01/bn/moments/StopGradient*
T0*'
_output_shapes
:d

.gapnet01/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
Ê
gapnet01/bn/moments/varianceMean%gapnet01/bn/moments/SquaredDifference.gapnet01/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:

gapnet01/bn/moments/SqueezeSqueezegapnet01/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes	
:

gapnet01/bn/moments/Squeeze_1Squeezegapnet01/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes	
:
b
gapnet01/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
a
gapnet01/bn/cond/switch_tIdentitygapnet01/bn/cond/Switch:1*
T0
*
_output_shapes
: 
_
gapnet01/bn/cond/switch_fIdentitygapnet01/bn/cond/Switch*
T0
*
_output_shapes
: 
T
gapnet01/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
ö
Rgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*
dtype0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*
valueB*    *
_output_shapes	
:

@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
shape:*
shared_name *S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
_output_shapes	
:

Ggapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssign@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverageRgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:

Egapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/readIdentity@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*
T0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:
ú
Tgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes	
:

Bgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:

Igapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssignBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverageTgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:

Ggapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentityBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes	
:

/gapnet01/bn/cond/ExponentialMovingAverage/decayConst^gapnet01/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
 
?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst^gapnet01/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
×
=gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSub?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x/gapnet01/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 

?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1SubHgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1Jgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes	
:
³
Fgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchEgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/readgapnet01/bn/cond/pred_id*
T0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::
æ
Hgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switchgapnet01/bn/moments/Squeezegapnet01/bn/cond/pred_id*
T0*.
_class$
" loc:@gapnet01/bn/moments/Squeeze*"
_output_shapes
::
ê
=gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMul?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1=gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes	
:
×
9gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSubBgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1=gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:
«
@gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitch@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAveragegapnet01/bn/cond/pred_id*
T0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::
¢
Agapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst^gapnet01/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Û
?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubAgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x/gapnet01/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 

Agapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1SubJgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1Lgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes	
:
¹
Hgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchGgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/readgapnet01/bn/cond/pred_id*
T0*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::
ì
Jgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switchgapnet01/bn/moments/Squeeze_1gapnet01/bn/cond/pred_id*
T0*0
_class&
$"loc:@gapnet01/bn/moments/Squeeze_1*"
_output_shapes
::
ð
?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulAgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes	
:
ß
;gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSubDgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes	
:
±
Bgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAveragegapnet01/bn/cond/pred_id*
T0*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::
«
)gapnet01/bn/cond/ExponentialMovingAverageNoOp:^gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg<^gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
Å
#gapnet01/bn/cond/control_dependencyIdentitygapnet01/bn/cond/switch_t*^gapnet01/bn/cond/ExponentialMovingAverage*,
_class"
 loc:@gapnet01/bn/cond/switch_t*
T0
*
_output_shapes
: 
9
gapnet01/bn/cond/NoOpNoOp^gapnet01/bn/cond/switch_f
³
%gapnet01/bn/cond/control_dependency_1Identitygapnet01/bn/cond/switch_f^gapnet01/bn/cond/NoOp*
T0
*,
_class"
 loc:@gapnet01/bn/cond/switch_f*
_output_shapes
: 

gapnet01/bn/cond/MergeMerge%gapnet01/bn/cond/control_dependency_1#gapnet01/bn/cond/control_dependency*
T0
*
N*
_output_shapes
: : 
d
gapnet01/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
e
gapnet01/bn/cond_1/switch_tIdentitygapnet01/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 
c
gapnet01/bn/cond_1/switch_fIdentitygapnet01/bn/cond_1/Switch*
T0
*
_output_shapes
: 
V
gapnet01/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 

gapnet01/bn/cond_1/IdentityIdentity$gapnet01/bn/cond_1/Identity/Switch:1^gapnet01/bn/cond/Merge*
T0*
_output_shapes	
:
Â
"gapnet01/bn/cond_1/Identity/SwitchSwitchgapnet01/bn/moments/Squeezegapnet01/bn/cond_1/pred_id*
T0*.
_class$
" loc:@gapnet01/bn/moments/Squeeze*"
_output_shapes
::

gapnet01/bn/cond_1/Identity_1Identity&gapnet01/bn/cond_1/Identity_1/Switch:1^gapnet01/bn/cond/Merge*
T0*
_output_shapes	
:
È
$gapnet01/bn/cond_1/Identity_1/SwitchSwitchgapnet01/bn/moments/Squeeze_1gapnet01/bn/cond_1/pred_id*
T0*0
_class&
$"loc:@gapnet01/bn/moments/Squeeze_1*"
_output_shapes
::

gapnet01/bn/cond_1/Switch_1SwitchEgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/readgapnet01/bn/cond_1/pred_id*
T0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::

gapnet01/bn/cond_1/Switch_2SwitchGgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/readgapnet01/bn/cond_1/pred_id*
T0*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::

gapnet01/bn/cond_1/MergeMergegapnet01/bn/cond_1/Switch_1gapnet01/bn/cond_1/Identity*
N*
T0*
_output_shapes
	:: 

gapnet01/bn/cond_1/Merge_1Mergegapnet01/bn/cond_1/Switch_2gapnet01/bn/cond_1/Identity_1*
T0*
N*
_output_shapes
	:: 
`
gapnet01/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 

gapnet01/bn/batchnorm/addAddgapnet01/bn/cond_1/Merge_1gapnet01/bn/batchnorm/add/y*
T0*
_output_shapes	
:
e
gapnet01/bn/batchnorm/RsqrtRsqrtgapnet01/bn/batchnorm/add*
T0*
_output_shapes	
:
{
gapnet01/bn/batchnorm/mulMulgapnet01/bn/batchnorm/Rsqrtgapnet01/bn/gamma/read*
T0*
_output_shapes	
:

gapnet01/bn/batchnorm/mul_1Mulgapnet01/BiasAddgapnet01/bn/batchnorm/mul*
T0*'
_output_shapes
:d
}
gapnet01/bn/batchnorm/mul_2Mulgapnet01/bn/cond_1/Mergegapnet01/bn/batchnorm/mul*
T0*
_output_shapes	
:
z
gapnet01/bn/batchnorm/subSubgapnet01/bn/beta/readgapnet01/bn/batchnorm/mul_2*
T0*
_output_shapes	
:

gapnet01/bn/batchnorm/add_1Addgapnet01/bn/batchnorm/mul_1gapnet01/bn/batchnorm/sub*
T0*'
_output_shapes
:d
d
gapnet01/ReluRelugapnet01/bn/batchnorm/add_1*
T0*'
_output_shapes
:d
b
Max_1/reduction_indicesConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 

Max_1Maxgapnet01/ReluMax_1/reduction_indices*
T0*

Tidx0*
	keep_dims(*'
_output_shapes
:d
a
	Squeeze_3Squeezegapnet01/Relu*
squeeze_dims
 *
T0*
_output_shapes
:	d
R
ExpandDims_7/dimConst*
value	B : *
dtype0*
_output_shapes
: 
q
ExpandDims_7
ExpandDims	Squeeze_3ExpandDims_7/dim*

Tdim0*
T0*#
_output_shapes
:d
e
transpose_5/permConst*!
valueB"          *
dtype0*
_output_shapes
:
s
transpose_5	TransposeExpandDims_7transpose_5/perm*
T0*
Tperm0*#
_output_shapes
:d
{
MatMul_2BatchMatMulV2ExpandDims_7transpose_5*
adj_x( *
adj_y( *
T0*"
_output_shapes
:dd
L
mul_4/xConst*
valueB
 *   À*
dtype0*
_output_shapes
: 
L
mul_4Mulmul_4/xMatMul_2*
T0*"
_output_shapes
:dd
N
Square_1SquareExpandDims_7*
T0*#
_output_shapes
:d
b
Sum_1/reduction_indicesConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
y
Sum_1SumSquare_1Sum_1/reduction_indices*

Tidx0*
	keep_dims(*
T0*"
_output_shapes
:d
e
transpose_6/permConst*!
valueB"          *
dtype0*
_output_shapes
:
k
transpose_6	TransposeSum_1transpose_6/perm*
T0*
Tperm0*"
_output_shapes
:d
G
add_7AddSum_1mul_4*
T0*"
_output_shapes
:dd
M
add_8Addadd_7transpose_6*
T0*"
_output_shapes
:dd
@
Neg_1Negadd_8*
T0*"
_output_shapes
:dd
L

TopKV2_1/kConst*
value	B :*
dtype0*
_output_shapes
: 
n
TopKV2_1TopKV2Neg_1
TopKV2_1/k*
T0*
sorted(*0
_output_shapes
:d:d
a
	Squeeze_4Squeezegapnet01/Relu*
squeeze_dims
 *
T0*
_output_shapes
:	d
R
ExpandDims_8/dimConst*
value	B : *
dtype0*
_output_shapes
: 
q
ExpandDims_8
ExpandDims	Squeeze_4ExpandDims_8/dim*

Tdim0*
T0*#
_output_shapes
:d
[
ExpandDims_9/dimConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
x
ExpandDims_9
ExpandDimsExpandDims_8ExpandDims_9/dim*

Tdim0*
T0*'
_output_shapes
:d
Ý
Hlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/shapeConst*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*%
valueB"         @   *
dtype0*
_output_shapes
:
Ç
Flayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/minConst*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*
valueB
 *ó5¾*
dtype0*
_output_shapes
: 
Ç
Flayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/maxConst*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*
valueB
 *ó5>*
dtype0*
_output_shapes
: 
¿
Playerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/RandomUniformRandomUniformHlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/shape*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*
dtype0*
seed2 *

seed *'
_output_shapes
:@
º
Flayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/subSubFlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/maxFlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/min*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*
T0*
_output_shapes
: 
Õ
Flayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/mulMulPlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/RandomUniformFlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/sub*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*'
_output_shapes
:@
Ç
Blayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniformAddFlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/mulFlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform/min*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*'
_output_shapes
:@
ø
'layerfilter1_newfea_conv_head_0/weights
VariableV2"/device:CPU:0*
shape:@*
shared_name *:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*
dtype0*
	container *'
_output_shapes
:@
Ë
.layerfilter1_newfea_conv_head_0/weights/AssignAssign'layerfilter1_newfea_conv_head_0/weightsBlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform"/device:CPU:0*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*
validate_shape(*
use_locking(*'
_output_shapes
:@
Þ
,layerfilter1_newfea_conv_head_0/weights/readIdentity'layerfilter1_newfea_conv_head_0/weights"/device:CPU:0*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*'
_output_shapes
:@

&layerfilter1_newfea_conv_head_0/L2LossL2Loss,layerfilter1_newfea_conv_head_0/weights/read*
T0*
_output_shapes
: 
r
-layerfilter1_newfea_conv_head_0/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
ª
+layerfilter1_newfea_conv_head_0/weight_lossMul&layerfilter1_newfea_conv_head_0/L2Loss-layerfilter1_newfea_conv_head_0/weight_loss/y*
T0*
_output_shapes
: 
¥
&layerfilter1_newfea_conv_head_0/Conv2DConv2DExpandDims_9,layerfilter1_newfea_conv_head_0/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
:d@
u
(layerfilter1_newfea_conv_head_0/bn/ConstConst*
valueB@*    *
dtype0*
_output_shapes
:@

'layerfilter1_newfea_conv_head_0/bn/beta
VariableV2*
dtype0*
	container *
shape:@*
shared_name *
_output_shapes
:@

.layerfilter1_newfea_conv_head_0/bn/beta/AssignAssign'layerfilter1_newfea_conv_head_0/bn/beta(layerfilter1_newfea_conv_head_0/bn/Const*
use_locking(*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:@
Â
,layerfilter1_newfea_conv_head_0/bn/beta/readIdentity'layerfilter1_newfea_conv_head_0/bn/beta*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/bn/beta*
_output_shapes
:@
w
*layerfilter1_newfea_conv_head_0/bn/Const_1Const*
dtype0*
valueB@*  ?*
_output_shapes
:@

(layerfilter1_newfea_conv_head_0/bn/gamma
VariableV2*
shape:@*
shared_name *
dtype0*
	container *
_output_shapes
:@

/layerfilter1_newfea_conv_head_0/bn/gamma/AssignAssign(layerfilter1_newfea_conv_head_0/bn/gamma*layerfilter1_newfea_conv_head_0/bn/Const_1*
T0*;
_class1
/-loc:@layerfilter1_newfea_conv_head_0/bn/gamma*
validate_shape(*
use_locking(*
_output_shapes
:@
Å
-layerfilter1_newfea_conv_head_0/bn/gamma/readIdentity(layerfilter1_newfea_conv_head_0/bn/gamma*
T0*;
_class1
/-loc:@layerfilter1_newfea_conv_head_0/bn/gamma*
_output_shapes
:@

Alayerfilter1_newfea_conv_head_0/bn/moments/mean/reduction_indicesConst*
dtype0*!
valueB"          *
_output_shapes
:
ð
/layerfilter1_newfea_conv_head_0/bn/moments/meanMean&layerfilter1_newfea_conv_head_0/Conv2DAlayerfilter1_newfea_conv_head_0/bn/moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:@
©
7layerfilter1_newfea_conv_head_0/bn/moments/StopGradientStopGradient/layerfilter1_newfea_conv_head_0/bn/moments/mean*
T0*&
_output_shapes
:@
ã
<layerfilter1_newfea_conv_head_0/bn/moments/SquaredDifferenceSquaredDifference&layerfilter1_newfea_conv_head_0/Conv2D7layerfilter1_newfea_conv_head_0/bn/moments/StopGradient*
T0*&
_output_shapes
:d@

Elayerfilter1_newfea_conv_head_0/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:

3layerfilter1_newfea_conv_head_0/bn/moments/varianceMean<layerfilter1_newfea_conv_head_0/bn/moments/SquaredDifferenceElayerfilter1_newfea_conv_head_0/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:@
¬
2layerfilter1_newfea_conv_head_0/bn/moments/SqueezeSqueeze/layerfilter1_newfea_conv_head_0/bn/moments/mean*
T0*
squeeze_dims
 *
_output_shapes
:@
²
4layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1Squeeze3layerfilter1_newfea_conv_head_0/bn/moments/variance*
T0*
squeeze_dims
 *
_output_shapes
:@
y
.layerfilter1_newfea_conv_head_0/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

0layerfilter1_newfea_conv_head_0/bn/cond/switch_tIdentity0layerfilter1_newfea_conv_head_0/bn/cond/Switch:1*
T0
*
_output_shapes
: 

0layerfilter1_newfea_conv_head_0/bn/cond/switch_fIdentity.layerfilter1_newfea_conv_head_0/bn/cond/Switch*
T0
*
_output_shapes
: 
k
/layerfilter1_newfea_conv_head_0/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ò
layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
valueB@*    *
dtype0*
_output_shapes
:@
Þ
nlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
shared_name *
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
shape:@*
_output_shapes
:@
Ä
ulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssignnlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragelayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@

slayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/readIdentitynlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:@
Ö
layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB@*    *
dtype0*
_output_shapes
:@
â
playerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
shape:@*
shared_name *
_output_shapes
:@
Ì
wlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssignplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@

ulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentityplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:@
¾
Flayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/decayConst1^layerfilter1_newfea_conv_head_0/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
Î
Vlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst1^layerfilter1_newfea_conv_head_0/bn/cond/switch_t*
dtype0*
valueB
 *  ?*
_output_shapes
: 

Tlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubVlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xFlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Æ
Vlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Sub_layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1alayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
:@
¼
]layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchslayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read/layerfilter1_newfea_conv_head_0/bn/cond/pred_id*
T0*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
:@:@
À
_layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch2layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/layerfilter1_newfea_conv_head_0/bn/cond/pred_id*
T0*E
_class;
97loc:@layerfilter1_newfea_conv_head_0/bn/moments/Squeeze* 
_output_shapes
:@:@
®
Tlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulVlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Tlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
:@
Ê
Playerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSubYlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Tlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
use_locking( *
T0*
_output_shapes
:@
´
Wlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitchnlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/layerfilter1_newfea_conv_head_0/bn/cond/pred_id*
T0*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
:@:@
Ð
Xlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst1^layerfilter1_newfea_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
 
Vlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubXlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xFlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ì
Xlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Subalayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1clayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
:@
Â
_layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read/layerfilter1_newfea_conv_head_0/bn/cond/pred_id*
T0*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@
Æ
alayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch4layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/layerfilter1_newfea_conv_head_0/bn/cond/pred_id*
T0*G
_class=
;9loc:@layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
:@:@
´
Vlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulXlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Vlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
:@
Ò
Rlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSub[layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Vlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:@
º
Ylayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/layerfilter1_newfea_conv_head_0/bn/cond/pred_id*
T0*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@
ð
@layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverageNoOpQ^layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvgS^layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
¡
:layerfilter1_newfea_conv_head_0/bn/cond/control_dependencyIdentity0layerfilter1_newfea_conv_head_0/bn/cond/switch_tA^layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage*
T0
*C
_class9
75loc:@layerfilter1_newfea_conv_head_0/bn/cond/switch_t*
_output_shapes
: 
g
,layerfilter1_newfea_conv_head_0/bn/cond/NoOpNoOp1^layerfilter1_newfea_conv_head_0/bn/cond/switch_f

<layerfilter1_newfea_conv_head_0/bn/cond/control_dependency_1Identity0layerfilter1_newfea_conv_head_0/bn/cond/switch_f-^layerfilter1_newfea_conv_head_0/bn/cond/NoOp*
T0
*C
_class9
75loc:@layerfilter1_newfea_conv_head_0/bn/cond/switch_f*
_output_shapes
: 
Ü
-layerfilter1_newfea_conv_head_0/bn/cond/MergeMerge<layerfilter1_newfea_conv_head_0/bn/cond/control_dependency_1:layerfilter1_newfea_conv_head_0/bn/cond/control_dependency*
T0
*
N*
_output_shapes
: : 
{
0layerfilter1_newfea_conv_head_0/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

2layerfilter1_newfea_conv_head_0/bn/cond_1/switch_tIdentity2layerfilter1_newfea_conv_head_0/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 

2layerfilter1_newfea_conv_head_0/bn/cond_1/switch_fIdentity0layerfilter1_newfea_conv_head_0/bn/cond_1/Switch*
T0
*
_output_shapes
: 
m
1layerfilter1_newfea_conv_head_0/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ð
2layerfilter1_newfea_conv_head_0/bn/cond_1/IdentityIdentity;layerfilter1_newfea_conv_head_0/bn/cond_1/Identity/Switch:1.^layerfilter1_newfea_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:@

9layerfilter1_newfea_conv_head_0/bn/cond_1/Identity/SwitchSwitch2layerfilter1_newfea_conv_head_0/bn/moments/Squeeze1layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id*
T0*E
_class;
97loc:@layerfilter1_newfea_conv_head_0/bn/moments/Squeeze* 
_output_shapes
:@:@
Ô
4layerfilter1_newfea_conv_head_0/bn/cond_1/Identity_1Identity=layerfilter1_newfea_conv_head_0/bn/cond_1/Identity_1/Switch:1.^layerfilter1_newfea_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:@
¢
;layerfilter1_newfea_conv_head_0/bn/cond_1/Identity_1/SwitchSwitch4layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_11layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id*
T0*G
_class=
;9loc:@layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
:@:@

2layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_1Switchslayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read1layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0* 
_output_shapes
:@:@

2layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_2Switchulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read1layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id*
T0*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@
Ð
/layerfilter1_newfea_conv_head_0/bn/cond_1/MergeMerge2layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_12layerfilter1_newfea_conv_head_0/bn/cond_1/Identity*
T0*
N*
_output_shapes

:@: 
Ô
1layerfilter1_newfea_conv_head_0/bn/cond_1/Merge_1Merge2layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_24layerfilter1_newfea_conv_head_0/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

:@: 
w
2layerfilter1_newfea_conv_head_0/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 
Ã
0layerfilter1_newfea_conv_head_0/bn/batchnorm/addAdd1layerfilter1_newfea_conv_head_0/bn/cond_1/Merge_12layerfilter1_newfea_conv_head_0/bn/batchnorm/add/y*
T0*
_output_shapes
:@

2layerfilter1_newfea_conv_head_0/bn/batchnorm/RsqrtRsqrt0layerfilter1_newfea_conv_head_0/bn/batchnorm/add*
T0*
_output_shapes
:@
¿
0layerfilter1_newfea_conv_head_0/bn/batchnorm/mulMul2layerfilter1_newfea_conv_head_0/bn/batchnorm/Rsqrt-layerfilter1_newfea_conv_head_0/bn/gamma/read*
T0*
_output_shapes
:@
Ä
2layerfilter1_newfea_conv_head_0/bn/batchnorm/mul_1Mul&layerfilter1_newfea_conv_head_0/Conv2D0layerfilter1_newfea_conv_head_0/bn/batchnorm/mul*
T0*&
_output_shapes
:d@
Á
2layerfilter1_newfea_conv_head_0/bn/batchnorm/mul_2Mul/layerfilter1_newfea_conv_head_0/bn/cond_1/Merge0layerfilter1_newfea_conv_head_0/bn/batchnorm/mul*
T0*
_output_shapes
:@
¾
0layerfilter1_newfea_conv_head_0/bn/batchnorm/subSub,layerfilter1_newfea_conv_head_0/bn/beta/read2layerfilter1_newfea_conv_head_0/bn/batchnorm/mul_2*
T0*
_output_shapes
:@
Ð
2layerfilter1_newfea_conv_head_0/bn/batchnorm/add_1Add2layerfilter1_newfea_conv_head_0/bn/batchnorm/mul_10layerfilter1_newfea_conv_head_0/bn/batchnorm/sub*
T0*&
_output_shapes
:d@

$layerfilter1_newfea_conv_head_0/ReluRelu2layerfilter1_newfea_conv_head_0/bn/batchnorm/add_1*
T0*&
_output_shapes
:d@
`
	Squeeze_5SqueezeExpandDims_9*
squeeze_dims
 *
T0*
_output_shapes
:	d
S
ExpandDims_10/dimConst*
value	B : *
dtype0*
_output_shapes
: 
s
ExpandDims_10
ExpandDims	Squeeze_5ExpandDims_10/dim*

Tdim0*
T0*#
_output_shapes
:d
O
range_1/startConst*
value	B : *
dtype0*
_output_shapes
: 
O
range_1/limitConst*
value	B :*
dtype0*
_output_shapes
: 
O
range_1/deltaConst*
value	B :*
dtype0*
_output_shapes
: 
e
range_1Rangerange_1/startrange_1/limitrange_1/delta*

Tidx0*
_output_shapes
:
I
mul_5/yConst*
value	B :d*
dtype0*
_output_shapes
: 
C
mul_5Mulrange_1mul_5/y*
T0*
_output_shapes
:
d
Reshape_2/shapeConst*!
valueB"         *
dtype0*
_output_shapes
:
g
	Reshape_2Reshapemul_5Reshape_2/shape*
T0*
Tshape0*"
_output_shapes
:
`
Reshape_3/shapeConst*
valueB"ÿÿÿÿ   *
dtype0*
_output_shapes
:
l
	Reshape_3ReshapeExpandDims_10Reshape_3/shape*
T0*
Tshape0*
_output_shapes
:	d
P
add_9Add
TopKV2_1:1	Reshape_2*
T0*"
_output_shapes
:d
Q
GatherV2_1/axisConst*
dtype0*
value	B : *
_output_shapes
: 
 

GatherV2_1GatherV2	Reshape_3add_9GatherV2_1/axis*
Taxis0*

batch_dims *
Tindices0*
Tparams0*'
_output_shapes
:d
i
Tile_2/multiplesConst*%
valueB"            *
dtype0*
_output_shapes
:
r
Tile_2TileExpandDims_9Tile_2/multiples*

Tmultiples0*
T0*'
_output_shapes
:d
R
sub_4SubTile_2
GatherV2_1*
T0*'
_output_shapes
:d
Ë
?layerfilter1_edgefea_0/weights/Initializer/random_uniform/shapeConst*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*%
valueB"         @   *
dtype0*
_output_shapes
:
µ
=layerfilter1_edgefea_0/weights/Initializer/random_uniform/minConst*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*
valueB
 *ó5¾*
dtype0*
_output_shapes
: 
µ
=layerfilter1_edgefea_0/weights/Initializer/random_uniform/maxConst*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*
valueB
 *ó5>*
dtype0*
_output_shapes
: 
¤
Glayerfilter1_edgefea_0/weights/Initializer/random_uniform/RandomUniformRandomUniform?layerfilter1_edgefea_0/weights/Initializer/random_uniform/shape*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*
dtype0*
seed2 *

seed *'
_output_shapes
:@

=layerfilter1_edgefea_0/weights/Initializer/random_uniform/subSub=layerfilter1_edgefea_0/weights/Initializer/random_uniform/max=layerfilter1_edgefea_0/weights/Initializer/random_uniform/min*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*
_output_shapes
: 
±
=layerfilter1_edgefea_0/weights/Initializer/random_uniform/mulMulGlayerfilter1_edgefea_0/weights/Initializer/random_uniform/RandomUniform=layerfilter1_edgefea_0/weights/Initializer/random_uniform/sub*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*'
_output_shapes
:@
£
9layerfilter1_edgefea_0/weights/Initializer/random_uniformAdd=layerfilter1_edgefea_0/weights/Initializer/random_uniform/mul=layerfilter1_edgefea_0/weights/Initializer/random_uniform/min*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*'
_output_shapes
:@
æ
layerfilter1_edgefea_0/weights
VariableV2"/device:CPU:0*
shared_name *1
_class'
%#loc:@layerfilter1_edgefea_0/weights*
dtype0*
	container *
shape:@*'
_output_shapes
:@
§
%layerfilter1_edgefea_0/weights/AssignAssignlayerfilter1_edgefea_0/weights9layerfilter1_edgefea_0/weights/Initializer/random_uniform"/device:CPU:0*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*
validate_shape(*
use_locking(*'
_output_shapes
:@
Ã
#layerfilter1_edgefea_0/weights/readIdentitylayerfilter1_edgefea_0/weights"/device:CPU:0*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*'
_output_shapes
:@
m
layerfilter1_edgefea_0/L2LossL2Loss#layerfilter1_edgefea_0/weights/read*
T0*
_output_shapes
: 
i
$layerfilter1_edgefea_0/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 

"layerfilter1_edgefea_0/weight_lossMullayerfilter1_edgefea_0/L2Loss$layerfilter1_edgefea_0/weight_loss/y*
T0*
_output_shapes
: 

layerfilter1_edgefea_0/Conv2DConv2Dsub_4#layerfilter1_edgefea_0/weights/read*
paddingVALID*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
explicit_paddings
 *&
_output_shapes
:d@
®
/layerfilter1_edgefea_0/biases/Initializer/ConstConst*0
_class&
$"loc:@layerfilter1_edgefea_0/biases*
valueB@*    *
dtype0*
_output_shapes
:@
Ê
layerfilter1_edgefea_0/biases
VariableV2"/device:CPU:0*
	container *
shape:@*
shared_name *0
_class&
$"loc:@layerfilter1_edgefea_0/biases*
dtype0*
_output_shapes
:@

$layerfilter1_edgefea_0/biases/AssignAssignlayerfilter1_edgefea_0/biases/layerfilter1_edgefea_0/biases/Initializer/Const"/device:CPU:0*
validate_shape(*
use_locking(*
T0*0
_class&
$"loc:@layerfilter1_edgefea_0/biases*
_output_shapes
:@
³
"layerfilter1_edgefea_0/biases/readIdentitylayerfilter1_edgefea_0/biases"/device:CPU:0*0
_class&
$"loc:@layerfilter1_edgefea_0/biases*
T0*
_output_shapes
:@
´
layerfilter1_edgefea_0/BiasAddBiasAddlayerfilter1_edgefea_0/Conv2D"layerfilter1_edgefea_0/biases/read*
T0*
data_formatNHWC*&
_output_shapes
:d@
l
layerfilter1_edgefea_0/bn/ConstConst*
valueB@*    *
dtype0*
_output_shapes
:@

layerfilter1_edgefea_0/bn/beta
VariableV2*
dtype0*
	container *
shape:@*
shared_name *
_output_shapes
:@
ñ
%layerfilter1_edgefea_0/bn/beta/AssignAssignlayerfilter1_edgefea_0/bn/betalayerfilter1_edgefea_0/bn/Const*
use_locking(*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/bn/beta*
validate_shape(*
_output_shapes
:@
§
#layerfilter1_edgefea_0/bn/beta/readIdentitylayerfilter1_edgefea_0/bn/beta*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/bn/beta*
_output_shapes
:@
n
!layerfilter1_edgefea_0/bn/Const_1Const*
valueB@*  ?*
dtype0*
_output_shapes
:@

layerfilter1_edgefea_0/bn/gamma
VariableV2*
shared_name *
dtype0*
	container *
shape:@*
_output_shapes
:@
ö
&layerfilter1_edgefea_0/bn/gamma/AssignAssignlayerfilter1_edgefea_0/bn/gamma!layerfilter1_edgefea_0/bn/Const_1*
use_locking(*
T0*2
_class(
&$loc:@layerfilter1_edgefea_0/bn/gamma*
validate_shape(*
_output_shapes
:@
ª
$layerfilter1_edgefea_0/bn/gamma/readIdentitylayerfilter1_edgefea_0/bn/gamma*
T0*2
_class(
&$loc:@layerfilter1_edgefea_0/bn/gamma*
_output_shapes
:@

8layerfilter1_edgefea_0/bn/moments/mean/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
Ö
&layerfilter1_edgefea_0/bn/moments/meanMeanlayerfilter1_edgefea_0/BiasAdd8layerfilter1_edgefea_0/bn/moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:@

.layerfilter1_edgefea_0/bn/moments/StopGradientStopGradient&layerfilter1_edgefea_0/bn/moments/mean*
T0*&
_output_shapes
:@
É
3layerfilter1_edgefea_0/bn/moments/SquaredDifferenceSquaredDifferencelayerfilter1_edgefea_0/BiasAdd.layerfilter1_edgefea_0/bn/moments/StopGradient*
T0*&
_output_shapes
:d@

<layerfilter1_edgefea_0/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
ó
*layerfilter1_edgefea_0/bn/moments/varianceMean3layerfilter1_edgefea_0/bn/moments/SquaredDifference<layerfilter1_edgefea_0/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:@

)layerfilter1_edgefea_0/bn/moments/SqueezeSqueeze&layerfilter1_edgefea_0/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes
:@
 
+layerfilter1_edgefea_0/bn/moments/Squeeze_1Squeeze*layerfilter1_edgefea_0/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes
:@
p
%layerfilter1_edgefea_0/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
}
'layerfilter1_edgefea_0/bn/cond/switch_tIdentity'layerfilter1_edgefea_0/bn/cond/Switch:1*
T0
*
_output_shapes
: 
{
'layerfilter1_edgefea_0/bn/cond/switch_fIdentity%layerfilter1_edgefea_0/bn/cond/Switch*
T0
*
_output_shapes
: 
b
&layerfilter1_edgefea_0/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
¬
nlayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
valueB@*    *
dtype0*
_output_shapes
:@
¹
\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
shared_name *o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
shape:@*
_output_shapes
:@
ú
clayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssign\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAveragenlayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
T0*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:@
á
alayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/readIdentity\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0*
_output_shapes
:@
°
playerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB@*    *
dtype0*
_output_shapes
:@
½
^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
shape:@*
shared_name *q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
_output_shapes
:@

elayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssign^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageplayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
T0*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:@
ç
clayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentity^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:@
¬
=layerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/decayConst(^layerfilter1_edgefea_0/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
¼
Mlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst(^layerfilter1_edgefea_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 

Klayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubMlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x=layerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
«
Mlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1SubVlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1Xlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
:@

Tlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchalayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read&layerfilter1_edgefea_0/bn/cond/pred_id*
T0*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
:@:@

Vlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch)layerfilter1_edgefea_0/bn/moments/Squeeze&layerfilter1_edgefea_0/bn/cond/pred_id*
T0*<
_class2
0.loc:@layerfilter1_edgefea_0/bn/moments/Squeeze* 
_output_shapes
:@:@

Klayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulMlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Klayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
:@

Glayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSubPlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Klayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:@
ý
Nlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitch\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage&layerfilter1_edgefea_0/bn/cond/pred_id*
T0*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
:@:@
¾
Olayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst(^layerfilter1_edgefea_0/bn/cond/switch_t*
dtype0*
valueB
 *  ?*
_output_shapes
: 

Mlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubOlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x=layerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
±
Olayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1SubXlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1Zlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
:@

Vlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchclayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read&layerfilter1_edgefea_0/bn/cond/pred_id*
T0*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@
¢
Xlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch+layerfilter1_edgefea_0/bn/moments/Squeeze_1&layerfilter1_edgefea_0/bn/cond/pred_id*>
_class4
20loc:@layerfilter1_edgefea_0/bn/moments/Squeeze_1*
T0* 
_output_shapes
:@:@

Mlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulOlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Mlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
:@
¤
Ilayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSubRlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Mlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:@

Playerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitch^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage&layerfilter1_edgefea_0/bn/cond/pred_id*
T0*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@
Õ
7layerfilter1_edgefea_0/bn/cond/ExponentialMovingAverageNoOpH^layerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvgJ^layerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
ý
1layerfilter1_edgefea_0/bn/cond/control_dependencyIdentity'layerfilter1_edgefea_0/bn/cond/switch_t8^layerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage*
T0
*:
_class0
.,loc:@layerfilter1_edgefea_0/bn/cond/switch_t*
_output_shapes
: 
U
#layerfilter1_edgefea_0/bn/cond/NoOpNoOp(^layerfilter1_edgefea_0/bn/cond/switch_f
ë
3layerfilter1_edgefea_0/bn/cond/control_dependency_1Identity'layerfilter1_edgefea_0/bn/cond/switch_f$^layerfilter1_edgefea_0/bn/cond/NoOp*
T0
*:
_class0
.,loc:@layerfilter1_edgefea_0/bn/cond/switch_f*
_output_shapes
: 
Á
$layerfilter1_edgefea_0/bn/cond/MergeMerge3layerfilter1_edgefea_0/bn/cond/control_dependency_11layerfilter1_edgefea_0/bn/cond/control_dependency*
N*
T0
*
_output_shapes
: : 
r
'layerfilter1_edgefea_0/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

)layerfilter1_edgefea_0/bn/cond_1/switch_tIdentity)layerfilter1_edgefea_0/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 

)layerfilter1_edgefea_0/bn/cond_1/switch_fIdentity'layerfilter1_edgefea_0/bn/cond_1/Switch*
T0
*
_output_shapes
: 
d
(layerfilter1_edgefea_0/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
µ
)layerfilter1_edgefea_0/bn/cond_1/IdentityIdentity2layerfilter1_edgefea_0/bn/cond_1/Identity/Switch:1%^layerfilter1_edgefea_0/bn/cond/Merge*
T0*
_output_shapes
:@
ø
0layerfilter1_edgefea_0/bn/cond_1/Identity/SwitchSwitch)layerfilter1_edgefea_0/bn/moments/Squeeze(layerfilter1_edgefea_0/bn/cond_1/pred_id*
T0*<
_class2
0.loc:@layerfilter1_edgefea_0/bn/moments/Squeeze* 
_output_shapes
:@:@
¹
+layerfilter1_edgefea_0/bn/cond_1/Identity_1Identity4layerfilter1_edgefea_0/bn/cond_1/Identity_1/Switch:1%^layerfilter1_edgefea_0/bn/cond/Merge*
T0*
_output_shapes
:@
þ
2layerfilter1_edgefea_0/bn/cond_1/Identity_1/SwitchSwitch+layerfilter1_edgefea_0/bn/moments/Squeeze_1(layerfilter1_edgefea_0/bn/cond_1/pred_id*
T0*>
_class4
20loc:@layerfilter1_edgefea_0/bn/moments/Squeeze_1* 
_output_shapes
:@:@
Ü
)layerfilter1_edgefea_0/bn/cond_1/Switch_1Switchalayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read(layerfilter1_edgefea_0/bn/cond_1/pred_id*
T0*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
:@:@
à
)layerfilter1_edgefea_0/bn/cond_1/Switch_2Switchclayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read(layerfilter1_edgefea_0/bn/cond_1/pred_id*
T0*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
:@:@
µ
&layerfilter1_edgefea_0/bn/cond_1/MergeMerge)layerfilter1_edgefea_0/bn/cond_1/Switch_1)layerfilter1_edgefea_0/bn/cond_1/Identity*
T0*
N*
_output_shapes

:@: 
¹
(layerfilter1_edgefea_0/bn/cond_1/Merge_1Merge)layerfilter1_edgefea_0/bn/cond_1/Switch_2+layerfilter1_edgefea_0/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

:@: 
n
)layerfilter1_edgefea_0/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 
¨
'layerfilter1_edgefea_0/bn/batchnorm/addAdd(layerfilter1_edgefea_0/bn/cond_1/Merge_1)layerfilter1_edgefea_0/bn/batchnorm/add/y*
T0*
_output_shapes
:@

)layerfilter1_edgefea_0/bn/batchnorm/RsqrtRsqrt'layerfilter1_edgefea_0/bn/batchnorm/add*
T0*
_output_shapes
:@
¤
'layerfilter1_edgefea_0/bn/batchnorm/mulMul)layerfilter1_edgefea_0/bn/batchnorm/Rsqrt$layerfilter1_edgefea_0/bn/gamma/read*
T0*
_output_shapes
:@
ª
)layerfilter1_edgefea_0/bn/batchnorm/mul_1Mullayerfilter1_edgefea_0/BiasAdd'layerfilter1_edgefea_0/bn/batchnorm/mul*
T0*&
_output_shapes
:d@
¦
)layerfilter1_edgefea_0/bn/batchnorm/mul_2Mul&layerfilter1_edgefea_0/bn/cond_1/Merge'layerfilter1_edgefea_0/bn/batchnorm/mul*
T0*
_output_shapes
:@
£
'layerfilter1_edgefea_0/bn/batchnorm/subSub#layerfilter1_edgefea_0/bn/beta/read)layerfilter1_edgefea_0/bn/batchnorm/mul_2*
T0*
_output_shapes
:@
µ
)layerfilter1_edgefea_0/bn/batchnorm/add_1Add)layerfilter1_edgefea_0/bn/batchnorm/mul_1'layerfilter1_edgefea_0/bn/batchnorm/sub*
T0*&
_output_shapes
:d@

layerfilter1_edgefea_0/ReluRelu)layerfilter1_edgefea_0/bn/batchnorm/add_1*
T0*&
_output_shapes
:d@
á
Jlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/shapeConst*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*%
valueB"      @      *
dtype0*
_output_shapes
:
Ë
Hlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/minConst*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*
valueB
 *¾*
dtype0*
_output_shapes
: 
Ë
Hlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/maxConst*
dtype0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*
valueB
 *>*
_output_shapes
: 
Ä
Rlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/RandomUniformRandomUniformJlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/shape*
seed2 *

seed *
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*
dtype0*&
_output_shapes
:@
Â
Hlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/subSubHlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/maxHlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/min*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*
_output_shapes
: 
Ü
Hlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/mulMulRlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/RandomUniformHlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/sub*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*&
_output_shapes
:@
Î
Dlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniformAddHlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/mulHlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform/min*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*&
_output_shapes
:@
ú
)layerfilter1_self_att_conv_head_0/weights
VariableV2"/device:CPU:0*
shared_name *<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*
dtype0*
	container *
shape:@*&
_output_shapes
:@
Ò
0layerfilter1_self_att_conv_head_0/weights/AssignAssign)layerfilter1_self_att_conv_head_0/weightsDlayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform"/device:CPU:0*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*
validate_shape(*
use_locking(*&
_output_shapes
:@
ã
.layerfilter1_self_att_conv_head_0/weights/readIdentity)layerfilter1_self_att_conv_head_0/weights"/device:CPU:0*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*&
_output_shapes
:@

(layerfilter1_self_att_conv_head_0/L2LossL2Loss.layerfilter1_self_att_conv_head_0/weights/read*
T0*
_output_shapes
: 
t
/layerfilter1_self_att_conv_head_0/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
°
-layerfilter1_self_att_conv_head_0/weight_lossMul(layerfilter1_self_att_conv_head_0/L2Loss/layerfilter1_self_att_conv_head_0/weight_loss/y*
T0*
_output_shapes
: 
Á
(layerfilter1_self_att_conv_head_0/Conv2DConv2D$layerfilter1_newfea_conv_head_0/Relu.layerfilter1_self_att_conv_head_0/weights/read*
paddingVALID*
	dilations
*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
explicit_paddings
 *&
_output_shapes
:d
Ä
:layerfilter1_self_att_conv_head_0/biases/Initializer/ConstConst*;
_class1
/-loc:@layerfilter1_self_att_conv_head_0/biases*
valueB*    *
dtype0*
_output_shapes
:
à
(layerfilter1_self_att_conv_head_0/biases
VariableV2"/device:CPU:0*
shape:*
shared_name *;
_class1
/-loc:@layerfilter1_self_att_conv_head_0/biases*
dtype0*
	container *
_output_shapes
:
¹
/layerfilter1_self_att_conv_head_0/biases/AssignAssign(layerfilter1_self_att_conv_head_0/biases:layerfilter1_self_att_conv_head_0/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*;
_class1
/-loc:@layerfilter1_self_att_conv_head_0/biases*
validate_shape(*
_output_shapes
:
Ô
-layerfilter1_self_att_conv_head_0/biases/readIdentity(layerfilter1_self_att_conv_head_0/biases"/device:CPU:0*
T0*;
_class1
/-loc:@layerfilter1_self_att_conv_head_0/biases*
_output_shapes
:
Õ
)layerfilter1_self_att_conv_head_0/BiasAddBiasAdd(layerfilter1_self_att_conv_head_0/Conv2D-layerfilter1_self_att_conv_head_0/biases/read*
data_formatNHWC*
T0*&
_output_shapes
:d
w
*layerfilter1_self_att_conv_head_0/bn/ConstConst*
valueB*    *
dtype0*
_output_shapes
:

)layerfilter1_self_att_conv_head_0/bn/beta
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:

0layerfilter1_self_att_conv_head_0/bn/beta/AssignAssign)layerfilter1_self_att_conv_head_0/bn/beta*layerfilter1_self_att_conv_head_0/bn/Const*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/bn/beta*
validate_shape(*
use_locking(*
_output_shapes
:
È
.layerfilter1_self_att_conv_head_0/bn/beta/readIdentity)layerfilter1_self_att_conv_head_0/bn/beta*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/bn/beta*
_output_shapes
:
y
,layerfilter1_self_att_conv_head_0/bn/Const_1Const*
valueB*  ?*
dtype0*
_output_shapes
:

*layerfilter1_self_att_conv_head_0/bn/gamma
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:
¢
1layerfilter1_self_att_conv_head_0/bn/gamma/AssignAssign*layerfilter1_self_att_conv_head_0/bn/gamma,layerfilter1_self_att_conv_head_0/bn/Const_1*
T0*=
_class3
1/loc:@layerfilter1_self_att_conv_head_0/bn/gamma*
validate_shape(*
use_locking(*
_output_shapes
:
Ë
/layerfilter1_self_att_conv_head_0/bn/gamma/readIdentity*layerfilter1_self_att_conv_head_0/bn/gamma*
T0*=
_class3
1/loc:@layerfilter1_self_att_conv_head_0/bn/gamma*
_output_shapes
:

Clayerfilter1_self_att_conv_head_0/bn/moments/mean/reduction_indicesConst*
dtype0*!
valueB"          *
_output_shapes
:
÷
1layerfilter1_self_att_conv_head_0/bn/moments/meanMean)layerfilter1_self_att_conv_head_0/BiasAddClayerfilter1_self_att_conv_head_0/bn/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:
­
9layerfilter1_self_att_conv_head_0/bn/moments/StopGradientStopGradient1layerfilter1_self_att_conv_head_0/bn/moments/mean*
T0*&
_output_shapes
:
ê
>layerfilter1_self_att_conv_head_0/bn/moments/SquaredDifferenceSquaredDifference)layerfilter1_self_att_conv_head_0/BiasAdd9layerfilter1_self_att_conv_head_0/bn/moments/StopGradient*
T0*&
_output_shapes
:d

Glayerfilter1_self_att_conv_head_0/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:

5layerfilter1_self_att_conv_head_0/bn/moments/varianceMean>layerfilter1_self_att_conv_head_0/bn/moments/SquaredDifferenceGlayerfilter1_self_att_conv_head_0/bn/moments/variance/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:
°
4layerfilter1_self_att_conv_head_0/bn/moments/SqueezeSqueeze1layerfilter1_self_att_conv_head_0/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes
:
¶
6layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1Squeeze5layerfilter1_self_att_conv_head_0/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes
:
{
0layerfilter1_self_att_conv_head_0/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

2layerfilter1_self_att_conv_head_0/bn/cond/switch_tIdentity2layerfilter1_self_att_conv_head_0/bn/cond/Switch:1*
T0
*
_output_shapes
: 

2layerfilter1_self_att_conv_head_0/bn/cond/switch_fIdentity0layerfilter1_self_att_conv_head_0/bn/cond/Switch*
T0
*
_output_shapes
: 
m
1layerfilter1_self_att_conv_head_0/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ú
layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes
:
æ
rlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
shared_name *
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
shape:*
_output_shapes
:
Ô
ylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssignrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragelayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
¤
wlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/readIdentityrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
Þ
layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*
dtype0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB*    *
_output_shapes
:
ê
tlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:
Ü
{layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssigntlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
T0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:
ª
ylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentitytlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
Â
Hlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/decayConst3^layerfilter1_self_att_conv_head_0/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
Ò
Xlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst3^layerfilter1_self_att_conv_head_0/bn/cond/switch_t*
dtype0*
valueB
 *  ?*
_output_shapes
: 
¢
Vlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubXlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xHlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ì
Xlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Subalayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1clayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
:
È
_layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchwlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read1layerfilter1_self_att_conv_head_0/bn/cond/pred_id*
T0*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
È
alayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch4layerfilter1_self_att_conv_head_0/bn/moments/Squeeze1layerfilter1_self_att_conv_head_0/bn/cond/pred_id*
T0*G
_class=
;9loc:@layerfilter1_self_att_conv_head_0/bn/moments/Squeeze* 
_output_shapes
::
´
Vlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulXlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Vlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
:
Ô
Rlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSub[layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Vlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
use_locking( *
T0*
_output_shapes
:
À
Ylayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitchrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage1layerfilter1_self_att_conv_head_0/bn/cond/pred_id*
T0*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
Ô
Zlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst3^layerfilter1_self_att_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
¦
Xlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubZlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xHlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ò
Zlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Subclayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1elayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
:
Î
alayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read1layerfilter1_self_att_conv_head_0/bn/cond/pred_id*
T0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
Î
clayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch6layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_11layerfilter1_self_att_conv_head_0/bn/cond/pred_id*
T0*I
_class?
=;loc:@layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
::
º
Xlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulZlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Xlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
:
Ü
Tlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSub]layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Xlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
Æ
[layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchtlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage1layerfilter1_self_att_conv_head_0/bn/cond/pred_id*
T0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
ö
Blayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverageNoOpS^layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvgU^layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
©
<layerfilter1_self_att_conv_head_0/bn/cond/control_dependencyIdentity2layerfilter1_self_att_conv_head_0/bn/cond/switch_tC^layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage*
T0
*E
_class;
97loc:@layerfilter1_self_att_conv_head_0/bn/cond/switch_t*
_output_shapes
: 
k
.layerfilter1_self_att_conv_head_0/bn/cond/NoOpNoOp3^layerfilter1_self_att_conv_head_0/bn/cond/switch_f

>layerfilter1_self_att_conv_head_0/bn/cond/control_dependency_1Identity2layerfilter1_self_att_conv_head_0/bn/cond/switch_f/^layerfilter1_self_att_conv_head_0/bn/cond/NoOp*
T0
*E
_class;
97loc:@layerfilter1_self_att_conv_head_0/bn/cond/switch_f*
_output_shapes
: 
â
/layerfilter1_self_att_conv_head_0/bn/cond/MergeMerge>layerfilter1_self_att_conv_head_0/bn/cond/control_dependency_1<layerfilter1_self_att_conv_head_0/bn/cond/control_dependency*
N*
T0
*
_output_shapes
: : 
}
2layerfilter1_self_att_conv_head_0/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

4layerfilter1_self_att_conv_head_0/bn/cond_1/switch_tIdentity4layerfilter1_self_att_conv_head_0/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 

4layerfilter1_self_att_conv_head_0/bn/cond_1/switch_fIdentity2layerfilter1_self_att_conv_head_0/bn/cond_1/Switch*
T0
*
_output_shapes
: 
o
3layerfilter1_self_att_conv_head_0/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ö
4layerfilter1_self_att_conv_head_0/bn/cond_1/IdentityIdentity=layerfilter1_self_att_conv_head_0/bn/cond_1/Identity/Switch:10^layerfilter1_self_att_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:
¤
;layerfilter1_self_att_conv_head_0/bn/cond_1/Identity/SwitchSwitch4layerfilter1_self_att_conv_head_0/bn/moments/Squeeze3layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id*
T0*G
_class=
;9loc:@layerfilter1_self_att_conv_head_0/bn/moments/Squeeze* 
_output_shapes
::
Ú
6layerfilter1_self_att_conv_head_0/bn/cond_1/Identity_1Identity?layerfilter1_self_att_conv_head_0/bn/cond_1/Identity_1/Switch:10^layerfilter1_self_att_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:
ª
=layerfilter1_self_att_conv_head_0/bn/cond_1/Identity_1/SwitchSwitch6layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_13layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id*
T0*I
_class?
=;loc:@layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
::

4layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_1Switchwlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read3layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id*
T0*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
£
4layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_2Switchylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read3layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id*
T0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
Ö
1layerfilter1_self_att_conv_head_0/bn/cond_1/MergeMerge4layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_14layerfilter1_self_att_conv_head_0/bn/cond_1/Identity*
T0*
N*
_output_shapes

:: 
Ú
3layerfilter1_self_att_conv_head_0/bn/cond_1/Merge_1Merge4layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_26layerfilter1_self_att_conv_head_0/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

:: 
y
4layerfilter1_self_att_conv_head_0/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 
É
2layerfilter1_self_att_conv_head_0/bn/batchnorm/addAdd3layerfilter1_self_att_conv_head_0/bn/cond_1/Merge_14layerfilter1_self_att_conv_head_0/bn/batchnorm/add/y*
T0*
_output_shapes
:

4layerfilter1_self_att_conv_head_0/bn/batchnorm/RsqrtRsqrt2layerfilter1_self_att_conv_head_0/bn/batchnorm/add*
T0*
_output_shapes
:
Å
2layerfilter1_self_att_conv_head_0/bn/batchnorm/mulMul4layerfilter1_self_att_conv_head_0/bn/batchnorm/Rsqrt/layerfilter1_self_att_conv_head_0/bn/gamma/read*
T0*
_output_shapes
:
Ë
4layerfilter1_self_att_conv_head_0/bn/batchnorm/mul_1Mul)layerfilter1_self_att_conv_head_0/BiasAdd2layerfilter1_self_att_conv_head_0/bn/batchnorm/mul*
T0*&
_output_shapes
:d
Ç
4layerfilter1_self_att_conv_head_0/bn/batchnorm/mul_2Mul1layerfilter1_self_att_conv_head_0/bn/cond_1/Merge2layerfilter1_self_att_conv_head_0/bn/batchnorm/mul*
T0*
_output_shapes
:
Ä
2layerfilter1_self_att_conv_head_0/bn/batchnorm/subSub.layerfilter1_self_att_conv_head_0/bn/beta/read4layerfilter1_self_att_conv_head_0/bn/batchnorm/mul_2*
T0*
_output_shapes
:
Ö
4layerfilter1_self_att_conv_head_0/bn/batchnorm/add_1Add4layerfilter1_self_att_conv_head_0/bn/batchnorm/mul_12layerfilter1_self_att_conv_head_0/bn/batchnorm/sub*
T0*&
_output_shapes
:d

&layerfilter1_self_att_conv_head_0/ReluRelu4layerfilter1_self_att_conv_head_0/bn/batchnorm/add_1*
T0*&
_output_shapes
:d
á
Jlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/shapeConst*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*%
valueB"      @      *
dtype0*
_output_shapes
:
Ë
Hlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/minConst*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*
valueB
 *¾*
dtype0*
_output_shapes
: 
Ë
Hlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/maxConst*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*
valueB
 *>*
dtype0*
_output_shapes
: 
Ä
Rlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/RandomUniformRandomUniformJlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/shape*

seed *
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*
dtype0*
seed2 *&
_output_shapes
:@
Â
Hlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/subSubHlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/maxHlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/min*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*
_output_shapes
: 
Ü
Hlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/mulMulRlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/RandomUniformHlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/sub*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*&
_output_shapes
:@
Î
Dlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniformAddHlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/mulHlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform/min*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*&
_output_shapes
:@
ú
)layerfilter1_neib_att_conv_head_0/weights
VariableV2"/device:CPU:0*
shared_name *<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*
dtype0*
	container *
shape:@*&
_output_shapes
:@
Ò
0layerfilter1_neib_att_conv_head_0/weights/AssignAssign)layerfilter1_neib_att_conv_head_0/weightsDlayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*
validate_shape(*&
_output_shapes
:@
ã
.layerfilter1_neib_att_conv_head_0/weights/readIdentity)layerfilter1_neib_att_conv_head_0/weights"/device:CPU:0*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*&
_output_shapes
:@

(layerfilter1_neib_att_conv_head_0/L2LossL2Loss.layerfilter1_neib_att_conv_head_0/weights/read*
T0*
_output_shapes
: 
t
/layerfilter1_neib_att_conv_head_0/weight_loss/yConst*
dtype0*
valueB
 *    *
_output_shapes
: 
°
-layerfilter1_neib_att_conv_head_0/weight_lossMul(layerfilter1_neib_att_conv_head_0/L2Loss/layerfilter1_neib_att_conv_head_0/weight_loss/y*
T0*
_output_shapes
: 
¸
(layerfilter1_neib_att_conv_head_0/Conv2DConv2Dlayerfilter1_edgefea_0/Relu.layerfilter1_neib_att_conv_head_0/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
:d
Ä
:layerfilter1_neib_att_conv_head_0/biases/Initializer/ConstConst*;
_class1
/-loc:@layerfilter1_neib_att_conv_head_0/biases*
valueB*    *
dtype0*
_output_shapes
:
à
(layerfilter1_neib_att_conv_head_0/biases
VariableV2"/device:CPU:0*;
_class1
/-loc:@layerfilter1_neib_att_conv_head_0/biases*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:
¹
/layerfilter1_neib_att_conv_head_0/biases/AssignAssign(layerfilter1_neib_att_conv_head_0/biases:layerfilter1_neib_att_conv_head_0/biases/Initializer/Const"/device:CPU:0*;
_class1
/-loc:@layerfilter1_neib_att_conv_head_0/biases*
validate_shape(*
use_locking(*
T0*
_output_shapes
:
Ô
-layerfilter1_neib_att_conv_head_0/biases/readIdentity(layerfilter1_neib_att_conv_head_0/biases"/device:CPU:0*
T0*;
_class1
/-loc:@layerfilter1_neib_att_conv_head_0/biases*
_output_shapes
:
Õ
)layerfilter1_neib_att_conv_head_0/BiasAddBiasAdd(layerfilter1_neib_att_conv_head_0/Conv2D-layerfilter1_neib_att_conv_head_0/biases/read*
data_formatNHWC*
T0*&
_output_shapes
:d
w
*layerfilter1_neib_att_conv_head_0/bn/ConstConst*
valueB*    *
dtype0*
_output_shapes
:

)layerfilter1_neib_att_conv_head_0/bn/beta
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:

0layerfilter1_neib_att_conv_head_0/bn/beta/AssignAssign)layerfilter1_neib_att_conv_head_0/bn/beta*layerfilter1_neib_att_conv_head_0/bn/Const*
validate_shape(*
use_locking(*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/bn/beta*
_output_shapes
:
È
.layerfilter1_neib_att_conv_head_0/bn/beta/readIdentity)layerfilter1_neib_att_conv_head_0/bn/beta*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/bn/beta*
_output_shapes
:
y
,layerfilter1_neib_att_conv_head_0/bn/Const_1Const*
valueB*  ?*
dtype0*
_output_shapes
:

*layerfilter1_neib_att_conv_head_0/bn/gamma
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:
¢
1layerfilter1_neib_att_conv_head_0/bn/gamma/AssignAssign*layerfilter1_neib_att_conv_head_0/bn/gamma,layerfilter1_neib_att_conv_head_0/bn/Const_1*
use_locking(*
T0*=
_class3
1/loc:@layerfilter1_neib_att_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
:
Ë
/layerfilter1_neib_att_conv_head_0/bn/gamma/readIdentity*layerfilter1_neib_att_conv_head_0/bn/gamma*
T0*=
_class3
1/loc:@layerfilter1_neib_att_conv_head_0/bn/gamma*
_output_shapes
:

Clayerfilter1_neib_att_conv_head_0/bn/moments/mean/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
÷
1layerfilter1_neib_att_conv_head_0/bn/moments/meanMean)layerfilter1_neib_att_conv_head_0/BiasAddClayerfilter1_neib_att_conv_head_0/bn/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:
­
9layerfilter1_neib_att_conv_head_0/bn/moments/StopGradientStopGradient1layerfilter1_neib_att_conv_head_0/bn/moments/mean*
T0*&
_output_shapes
:
ê
>layerfilter1_neib_att_conv_head_0/bn/moments/SquaredDifferenceSquaredDifference)layerfilter1_neib_att_conv_head_0/BiasAdd9layerfilter1_neib_att_conv_head_0/bn/moments/StopGradient*
T0*&
_output_shapes
:d

Glayerfilter1_neib_att_conv_head_0/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:

5layerfilter1_neib_att_conv_head_0/bn/moments/varianceMean>layerfilter1_neib_att_conv_head_0/bn/moments/SquaredDifferenceGlayerfilter1_neib_att_conv_head_0/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:
°
4layerfilter1_neib_att_conv_head_0/bn/moments/SqueezeSqueeze1layerfilter1_neib_att_conv_head_0/bn/moments/mean*
T0*
squeeze_dims
 *
_output_shapes
:
¶
6layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1Squeeze5layerfilter1_neib_att_conv_head_0/bn/moments/variance*
T0*
squeeze_dims
 *
_output_shapes
:
{
0layerfilter1_neib_att_conv_head_0/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

2layerfilter1_neib_att_conv_head_0/bn/cond/switch_tIdentity2layerfilter1_neib_att_conv_head_0/bn/cond/Switch:1*
T0
*
_output_shapes
: 

2layerfilter1_neib_att_conv_head_0/bn/cond/switch_fIdentity0layerfilter1_neib_att_conv_head_0/bn/cond/Switch*
T0
*
_output_shapes
: 
m
1layerfilter1_neib_att_conv_head_0/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ú
layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*
dtype0*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
valueB*    *
_output_shapes
:
æ
rlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
shared_name *
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
shape:*
_output_shapes
:
Ô
ylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssignrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragelayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
¤
wlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/readIdentityrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
T0*
_output_shapes
:
Þ
layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes
:
ê
tlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
	container *
shape:*
shared_name *
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
_output_shapes
:
Ü
{layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssigntlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
validate_shape(*
use_locking(*
T0*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
ª
ylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentitytlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
Â
Hlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/decayConst3^layerfilter1_neib_att_conv_head_0/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
Ò
Xlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst3^layerfilter1_neib_att_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
¢
Vlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubXlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xHlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ì
Xlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Subalayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1clayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
:
È
_layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchwlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read1layerfilter1_neib_att_conv_head_0/bn/cond/pred_id*
T0*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
È
alayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch4layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze1layerfilter1_neib_att_conv_head_0/bn/cond/pred_id*
T0*G
_class=
;9loc:@layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze* 
_output_shapes
::
´
Vlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulXlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Vlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
:
Ô
Rlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSub[layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Vlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
À
Ylayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitchrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage1layerfilter1_neib_att_conv_head_0/bn/cond/pred_id*
T0*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
Ô
Zlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst3^layerfilter1_neib_att_conv_head_0/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
¦
Xlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubZlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xHlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
Ò
Zlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Subclayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1elayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
:
Î
alayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read1layerfilter1_neib_att_conv_head_0/bn/cond/pred_id*
T0*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
Î
clayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch6layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_11layerfilter1_neib_att_conv_head_0/bn/cond/pred_id*
T0*I
_class?
=;loc:@layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
::
º
Xlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulZlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Xlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
:
Ü
Tlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSub]layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Xlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
Æ
[layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchtlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage1layerfilter1_neib_att_conv_head_0/bn/cond/pred_id*
T0*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
ö
Blayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverageNoOpS^layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvgU^layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
©
<layerfilter1_neib_att_conv_head_0/bn/cond/control_dependencyIdentity2layerfilter1_neib_att_conv_head_0/bn/cond/switch_tC^layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage*
T0
*E
_class;
97loc:@layerfilter1_neib_att_conv_head_0/bn/cond/switch_t*
_output_shapes
: 
k
.layerfilter1_neib_att_conv_head_0/bn/cond/NoOpNoOp3^layerfilter1_neib_att_conv_head_0/bn/cond/switch_f

>layerfilter1_neib_att_conv_head_0/bn/cond/control_dependency_1Identity2layerfilter1_neib_att_conv_head_0/bn/cond/switch_f/^layerfilter1_neib_att_conv_head_0/bn/cond/NoOp*
T0
*E
_class;
97loc:@layerfilter1_neib_att_conv_head_0/bn/cond/switch_f*
_output_shapes
: 
â
/layerfilter1_neib_att_conv_head_0/bn/cond/MergeMerge>layerfilter1_neib_att_conv_head_0/bn/cond/control_dependency_1<layerfilter1_neib_att_conv_head_0/bn/cond/control_dependency*
T0
*
N*
_output_shapes
: : 
}
2layerfilter1_neib_att_conv_head_0/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 

4layerfilter1_neib_att_conv_head_0/bn/cond_1/switch_tIdentity4layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 

4layerfilter1_neib_att_conv_head_0/bn/cond_1/switch_fIdentity2layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch*
T0
*
_output_shapes
: 
o
3layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
Ö
4layerfilter1_neib_att_conv_head_0/bn/cond_1/IdentityIdentity=layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity/Switch:10^layerfilter1_neib_att_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:
¤
;layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity/SwitchSwitch4layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze3layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id*
T0*G
_class=
;9loc:@layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze* 
_output_shapes
::
Ú
6layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity_1Identity?layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity_1/Switch:10^layerfilter1_neib_att_conv_head_0/bn/cond/Merge*
T0*
_output_shapes
:
ª
=layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity_1/SwitchSwitch6layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_13layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id*
T0*I
_class?
=;loc:@layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1* 
_output_shapes
::

4layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_1Switchwlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read3layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id*
T0*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
£
4layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_2Switchylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read3layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id*
T0*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
Ö
1layerfilter1_neib_att_conv_head_0/bn/cond_1/MergeMerge4layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_14layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity*
T0*
N*
_output_shapes

:: 
Ú
3layerfilter1_neib_att_conv_head_0/bn/cond_1/Merge_1Merge4layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_26layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity_1*
T0*
N*
_output_shapes

:: 
y
4layerfilter1_neib_att_conv_head_0/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 
É
2layerfilter1_neib_att_conv_head_0/bn/batchnorm/addAdd3layerfilter1_neib_att_conv_head_0/bn/cond_1/Merge_14layerfilter1_neib_att_conv_head_0/bn/batchnorm/add/y*
T0*
_output_shapes
:

4layerfilter1_neib_att_conv_head_0/bn/batchnorm/RsqrtRsqrt2layerfilter1_neib_att_conv_head_0/bn/batchnorm/add*
T0*
_output_shapes
:
Å
2layerfilter1_neib_att_conv_head_0/bn/batchnorm/mulMul4layerfilter1_neib_att_conv_head_0/bn/batchnorm/Rsqrt/layerfilter1_neib_att_conv_head_0/bn/gamma/read*
T0*
_output_shapes
:
Ë
4layerfilter1_neib_att_conv_head_0/bn/batchnorm/mul_1Mul)layerfilter1_neib_att_conv_head_0/BiasAdd2layerfilter1_neib_att_conv_head_0/bn/batchnorm/mul*
T0*&
_output_shapes
:d
Ç
4layerfilter1_neib_att_conv_head_0/bn/batchnorm/mul_2Mul1layerfilter1_neib_att_conv_head_0/bn/cond_1/Merge2layerfilter1_neib_att_conv_head_0/bn/batchnorm/mul*
T0*
_output_shapes
:
Ä
2layerfilter1_neib_att_conv_head_0/bn/batchnorm/subSub.layerfilter1_neib_att_conv_head_0/bn/beta/read4layerfilter1_neib_att_conv_head_0/bn/batchnorm/mul_2*
T0*
_output_shapes
:
Ö
4layerfilter1_neib_att_conv_head_0/bn/batchnorm/add_1Add4layerfilter1_neib_att_conv_head_0/bn/batchnorm/mul_12layerfilter1_neib_att_conv_head_0/bn/batchnorm/sub*
T0*&
_output_shapes
:d

&layerfilter1_neib_att_conv_head_0/ReluRelu4layerfilter1_neib_att_conv_head_0/bn/batchnorm/add_1*
T0*&
_output_shapes
:d

add_10Add&layerfilter1_self_att_conv_head_0/Relu&layerfilter1_neib_att_conv_head_0/Relu*
T0*&
_output_shapes
:d
i
transpose_7/permConst*%
valueB"             *
dtype0*
_output_shapes
:
p
transpose_7	Transposeadd_10transpose_7/perm*
T0*
Tperm0*&
_output_shapes
:d
f
LeakyRelu_1	LeakyRelutranspose_7*
alpha%ÍÌL>*
T0*&
_output_shapes
:d
R
	Softmax_1SoftmaxLeakyRelu_1*
T0*&
_output_shapes
:d

MatMul_3BatchMatMulV2	Softmax_1layerfilter1_edgefea_0/Relu*
adj_x( *
adj_y( *
T0*&
_output_shapes
:d@

"BiasAdd_1/biases/Initializer/zerosConst*#
_class
loc:@BiasAdd_1/biases*
valueB@*    *
dtype0*
_output_shapes
:@
¡
BiasAdd_1/biases
VariableV2*#
_class
loc:@BiasAdd_1/biases*
dtype0*
	container *
shape:@*
shared_name *
_output_shapes
:@
Ê
BiasAdd_1/biases/AssignAssignBiasAdd_1/biases"BiasAdd_1/biases/Initializer/zeros*#
_class
loc:@BiasAdd_1/biases*
validate_shape(*
use_locking(*
T0*
_output_shapes
:@
}
BiasAdd_1/biases/readIdentityBiasAdd_1/biases*#
_class
loc:@BiasAdd_1/biases*
T0*
_output_shapes
:@

BiasAdd_1/BiasAddBiasAddMatMul_3BiasAdd_1/biases/read*
T0*
data_formatNHWC*&
_output_shapes
:d@
R
Relu_1ReluBiasAdd_1/BiasAdd*
T0*&
_output_shapes
:d@
^
concat_3/concat_dimConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
M
concat_3IdentityRelu_1*
T0*&
_output_shapes
:d@
\
ExpandDims_11/dimConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
x
ExpandDims_11
ExpandDimsPlaceholderExpandDims_11/dim*

Tdim0*
T0*&
_output_shapes
:d

X
concat_4/axisConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 

concat_4ConcatV2ExpandDims_11concat_3concat_4/axis*
T0*
N*

Tidx0*&
_output_shapes
:dJ
^
concat_5/concat_dimConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
b
concat_5Identitylayerfilter1_edgefea_0/Relu*
T0*&
_output_shapes
:d@
b
Max_2/reduction_indicesConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
}
Max_2Maxconcat_5Max_2/reduction_indices*
T0*

Tidx0*
	keep_dims(*&
_output_shapes
:d@
¯
1gapnet11/weights/Initializer/random_uniform/shapeConst*#
_class
loc:@gapnet11/weights*%
valueB"      J      *
dtype0*
_output_shapes
:

/gapnet11/weights/Initializer/random_uniform/minConst*#
_class
loc:@gapnet11/weights*
valueB
 *}
¾*
dtype0*
_output_shapes
: 

/gapnet11/weights/Initializer/random_uniform/maxConst*#
_class
loc:@gapnet11/weights*
valueB
 *}
>*
dtype0*
_output_shapes
: 
ú
9gapnet11/weights/Initializer/random_uniform/RandomUniformRandomUniform1gapnet11/weights/Initializer/random_uniform/shape*

seed *
T0*#
_class
loc:@gapnet11/weights*
dtype0*
seed2 *'
_output_shapes
:J
Þ
/gapnet11/weights/Initializer/random_uniform/subSub/gapnet11/weights/Initializer/random_uniform/max/gapnet11/weights/Initializer/random_uniform/min*
T0*#
_class
loc:@gapnet11/weights*
_output_shapes
: 
ù
/gapnet11/weights/Initializer/random_uniform/mulMul9gapnet11/weights/Initializer/random_uniform/RandomUniform/gapnet11/weights/Initializer/random_uniform/sub*
T0*#
_class
loc:@gapnet11/weights*'
_output_shapes
:J
ë
+gapnet11/weights/Initializer/random_uniformAdd/gapnet11/weights/Initializer/random_uniform/mul/gapnet11/weights/Initializer/random_uniform/min*
T0*#
_class
loc:@gapnet11/weights*'
_output_shapes
:J
Ê
gapnet11/weights
VariableV2"/device:CPU:0*
shared_name *#
_class
loc:@gapnet11/weights*
dtype0*
	container *
shape:J*'
_output_shapes
:J
ï
gapnet11/weights/AssignAssigngapnet11/weights+gapnet11/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@gapnet11/weights*
validate_shape(*'
_output_shapes
:J

gapnet11/weights/readIdentitygapnet11/weights"/device:CPU:0*
T0*#
_class
loc:@gapnet11/weights*'
_output_shapes
:J
Q
gapnet11/L2LossL2Lossgapnet11/weights/read*
T0*
_output_shapes
: 
[
gapnet11/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
e
gapnet11/weight_lossMulgapnet11/L2Lossgapnet11/weight_loss/y*
T0*
_output_shapes
: 
ô
gapnet11/Conv2DConv2Dconcat_4gapnet11/weights/read*
data_formatNHWC*
strides
*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*
	dilations
*
T0*'
_output_shapes
:d

!gapnet11/biases/Initializer/ConstConst*"
_class
loc:@gapnet11/biases*
valueB*    *
dtype0*
_output_shapes	
:
°
gapnet11/biases
VariableV2"/device:CPU:0*"
_class
loc:@gapnet11/biases*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
Ö
gapnet11/biases/AssignAssigngapnet11/biases!gapnet11/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*"
_class
loc:@gapnet11/biases*
validate_shape(*
_output_shapes	
:

gapnet11/biases/readIdentitygapnet11/biases"/device:CPU:0*
T0*"
_class
loc:@gapnet11/biases*
_output_shapes	
:

gapnet11/BiasAddBiasAddgapnet11/Conv2Dgapnet11/biases/read*
T0*
data_formatNHWC*'
_output_shapes
:d
`
gapnet11/bn/ConstConst*
valueB*    *
dtype0*
_output_shapes	
:
~
gapnet11/bn/beta
VariableV2*
shape:*
shared_name *
dtype0*
	container *
_output_shapes	
:
º
gapnet11/bn/beta/AssignAssigngapnet11/bn/betagapnet11/bn/Const*
validate_shape(*
use_locking(*
T0*#
_class
loc:@gapnet11/bn/beta*
_output_shapes	
:
~
gapnet11/bn/beta/readIdentitygapnet11/bn/beta*
T0*#
_class
loc:@gapnet11/bn/beta*
_output_shapes	
:
b
gapnet11/bn/Const_1Const*
valueB*  ?*
dtype0*
_output_shapes	
:

gapnet11/bn/gamma
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
¿
gapnet11/bn/gamma/AssignAssigngapnet11/bn/gammagapnet11/bn/Const_1*
T0*$
_class
loc:@gapnet11/bn/gamma*
validate_shape(*
use_locking(*
_output_shapes	
:

gapnet11/bn/gamma/readIdentitygapnet11/bn/gamma*
T0*$
_class
loc:@gapnet11/bn/gamma*
_output_shapes	
:

*gapnet11/bn/moments/mean/reduction_indicesConst*
dtype0*!
valueB"          *
_output_shapes
:
­
gapnet11/bn/moments/meanMeangapnet11/BiasAdd*gapnet11/bn/moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(*'
_output_shapes
:
|
 gapnet11/bn/moments/StopGradientStopGradientgapnet11/bn/moments/mean*
T0*'
_output_shapes
:
 
%gapnet11/bn/moments/SquaredDifferenceSquaredDifferencegapnet11/BiasAdd gapnet11/bn/moments/StopGradient*
T0*'
_output_shapes
:d

.gapnet11/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
Ê
gapnet11/bn/moments/varianceMean%gapnet11/bn/moments/SquaredDifference.gapnet11/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:

gapnet11/bn/moments/SqueezeSqueezegapnet11/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes	
:

gapnet11/bn/moments/Squeeze_1Squeezegapnet11/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes	
:
b
gapnet11/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
a
gapnet11/bn/cond/switch_tIdentitygapnet11/bn/cond/Switch:1*
T0
*
_output_shapes
: 
_
gapnet11/bn/cond/switch_fIdentitygapnet11/bn/cond/Switch*
T0
*
_output_shapes
: 
T
gapnet11/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
ö
Rgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes	
:

@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:

Ggapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssign@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverageRgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
validate_shape(*
use_locking(*
T0*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:

Egapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/readIdentity@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*
T0*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:
ú
Tgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes	
:

Bgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
shared_name *U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
shape:*
_output_shapes	
:

Igapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssignBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverageTgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:

Ggapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentityBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*
_output_shapes	
:

/gapnet11/bn/cond/ExponentialMovingAverage/decayConst^gapnet11/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
 
?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst^gapnet11/bn/cond/switch_t*
dtype0*
valueB
 *  ?*
_output_shapes
: 
×
=gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSub?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x/gapnet11/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 

?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1SubHgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1Jgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes	
:
³
Fgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchEgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/readgapnet11/bn/cond/pred_id*
T0*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::
æ
Hgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switchgapnet11/bn/moments/Squeezegapnet11/bn/cond/pred_id*
T0*.
_class$
" loc:@gapnet11/bn/moments/Squeeze*"
_output_shapes
::
ê
=gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMul?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1=gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes	
:
×
9gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSubBgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1=gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:
«
@gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitch@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAveragegapnet11/bn/cond/pred_id*
T0*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::
¢
Agapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst^gapnet11/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Û
?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubAgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x/gapnet11/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 

Agapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1SubJgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1Lgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes	
:
¹
Hgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchGgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/readgapnet11/bn/cond/pred_id*
T0*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::
ì
Jgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switchgapnet11/bn/moments/Squeeze_1gapnet11/bn/cond/pred_id*
T0*0
_class&
$"loc:@gapnet11/bn/moments/Squeeze_1*"
_output_shapes
::
ð
?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulAgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes	
:
ß
;gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSubDgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes	
:
±
Bgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAveragegapnet11/bn/cond/pred_id*
T0*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::
«
)gapnet11/bn/cond/ExponentialMovingAverageNoOp:^gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg<^gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
Å
#gapnet11/bn/cond/control_dependencyIdentitygapnet11/bn/cond/switch_t*^gapnet11/bn/cond/ExponentialMovingAverage*
T0
*,
_class"
 loc:@gapnet11/bn/cond/switch_t*
_output_shapes
: 
9
gapnet11/bn/cond/NoOpNoOp^gapnet11/bn/cond/switch_f
³
%gapnet11/bn/cond/control_dependency_1Identitygapnet11/bn/cond/switch_f^gapnet11/bn/cond/NoOp*,
_class"
 loc:@gapnet11/bn/cond/switch_f*
T0
*
_output_shapes
: 

gapnet11/bn/cond/MergeMerge%gapnet11/bn/cond/control_dependency_1#gapnet11/bn/cond/control_dependency*
T0
*
N*
_output_shapes
: : 
d
gapnet11/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
e
gapnet11/bn/cond_1/switch_tIdentitygapnet11/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 
c
gapnet11/bn/cond_1/switch_fIdentitygapnet11/bn/cond_1/Switch*
T0
*
_output_shapes
: 
V
gapnet11/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 

gapnet11/bn/cond_1/IdentityIdentity$gapnet11/bn/cond_1/Identity/Switch:1^gapnet11/bn/cond/Merge*
T0*
_output_shapes	
:
Â
"gapnet11/bn/cond_1/Identity/SwitchSwitchgapnet11/bn/moments/Squeezegapnet11/bn/cond_1/pred_id*
T0*.
_class$
" loc:@gapnet11/bn/moments/Squeeze*"
_output_shapes
::

gapnet11/bn/cond_1/Identity_1Identity&gapnet11/bn/cond_1/Identity_1/Switch:1^gapnet11/bn/cond/Merge*
T0*
_output_shapes	
:
È
$gapnet11/bn/cond_1/Identity_1/SwitchSwitchgapnet11/bn/moments/Squeeze_1gapnet11/bn/cond_1/pred_id*
T0*0
_class&
$"loc:@gapnet11/bn/moments/Squeeze_1*"
_output_shapes
::

gapnet11/bn/cond_1/Switch_1SwitchEgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/readgapnet11/bn/cond_1/pred_id*
T0*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::

gapnet11/bn/cond_1/Switch_2SwitchGgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/readgapnet11/bn/cond_1/pred_id*
T0*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::

gapnet11/bn/cond_1/MergeMergegapnet11/bn/cond_1/Switch_1gapnet11/bn/cond_1/Identity*
T0*
N*
_output_shapes
	:: 

gapnet11/bn/cond_1/Merge_1Mergegapnet11/bn/cond_1/Switch_2gapnet11/bn/cond_1/Identity_1*
T0*
N*
_output_shapes
	:: 
`
gapnet11/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 

gapnet11/bn/batchnorm/addAddgapnet11/bn/cond_1/Merge_1gapnet11/bn/batchnorm/add/y*
T0*
_output_shapes	
:
e
gapnet11/bn/batchnorm/RsqrtRsqrtgapnet11/bn/batchnorm/add*
T0*
_output_shapes	
:
{
gapnet11/bn/batchnorm/mulMulgapnet11/bn/batchnorm/Rsqrtgapnet11/bn/gamma/read*
T0*
_output_shapes	
:

gapnet11/bn/batchnorm/mul_1Mulgapnet11/BiasAddgapnet11/bn/batchnorm/mul*
T0*'
_output_shapes
:d
}
gapnet11/bn/batchnorm/mul_2Mulgapnet11/bn/cond_1/Mergegapnet11/bn/batchnorm/mul*
T0*
_output_shapes	
:
z
gapnet11/bn/batchnorm/subSubgapnet11/bn/beta/readgapnet11/bn/batchnorm/mul_2*
T0*
_output_shapes	
:

gapnet11/bn/batchnorm/add_1Addgapnet11/bn/batchnorm/mul_1gapnet11/bn/batchnorm/sub*
T0*'
_output_shapes
:d
d
gapnet11/ReluRelugapnet11/bn/batchnorm/add_1*
T0*'
_output_shapes
:d
b
Max_3/reduction_indicesConst*
valueB :
þÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 

Max_3Maxgapnet11/ReluMax_3/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:d
h
Reshape_4/shapeConst*%
valueB"         ÿÿÿÿ*
dtype0*
_output_shapes
:
s
	Reshape_4ReshapePlaceholder_2Reshape_4/shape*
T0*
Tshape0*&
_output_shapes
:
i
Tile_3/multiplesConst*%
valueB"   d         *
dtype0*
_output_shapes
:
n
Tile_3Tile	Reshape_4Tile_3/multiples*

Tmultiples0*
T0*&
_output_shapes
:d
¹
6global_expand/weights/Initializer/random_uniform/shapeConst*(
_class
loc:@global_expand/weights*%
valueB"            *
dtype0*
_output_shapes
:
£
4global_expand/weights/Initializer/random_uniform/minConst*(
_class
loc:@global_expand/weights*
valueB
 *Ü¿*
dtype0*
_output_shapes
: 
£
4global_expand/weights/Initializer/random_uniform/maxConst*(
_class
loc:@global_expand/weights*
valueB
 *Ü?*
dtype0*
_output_shapes
: 

>global_expand/weights/Initializer/random_uniform/RandomUniformRandomUniform6global_expand/weights/Initializer/random_uniform/shape*
seed2 *

seed *
T0*(
_class
loc:@global_expand/weights*
dtype0*&
_output_shapes
:
ò
4global_expand/weights/Initializer/random_uniform/subSub4global_expand/weights/Initializer/random_uniform/max4global_expand/weights/Initializer/random_uniform/min*
T0*(
_class
loc:@global_expand/weights*
_output_shapes
: 

4global_expand/weights/Initializer/random_uniform/mulMul>global_expand/weights/Initializer/random_uniform/RandomUniform4global_expand/weights/Initializer/random_uniform/sub*
T0*(
_class
loc:@global_expand/weights*&
_output_shapes
:
þ
0global_expand/weights/Initializer/random_uniformAdd4global_expand/weights/Initializer/random_uniform/mul4global_expand/weights/Initializer/random_uniform/min*
T0*(
_class
loc:@global_expand/weights*&
_output_shapes
:
Ò
global_expand/weights
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:*
shared_name *(
_class
loc:@global_expand/weights*&
_output_shapes
:

global_expand/weights/AssignAssignglobal_expand/weights0global_expand/weights/Initializer/random_uniform"/device:CPU:0*
T0*(
_class
loc:@global_expand/weights*
validate_shape(*
use_locking(*&
_output_shapes
:
§
global_expand/weights/readIdentityglobal_expand/weights"/device:CPU:0*
T0*(
_class
loc:@global_expand/weights*&
_output_shapes
:
[
global_expand/L2LossL2Lossglobal_expand/weights/read*
T0*
_output_shapes
: 
`
global_expand/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
t
global_expand/weight_lossMulglobal_expand/L2Lossglobal_expand/weight_loss/y*
T0*
_output_shapes
: 
û
global_expand/Conv2DConv2DTile_3global_expand/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
:d

&global_expand/biases/Initializer/ConstConst*'
_class
loc:@global_expand/biases*
valueB*    *
dtype0*
_output_shapes
:
¸
global_expand/biases
VariableV2"/device:CPU:0*
	container *
shape:*
shared_name *'
_class
loc:@global_expand/biases*
dtype0*
_output_shapes
:
é
global_expand/biases/AssignAssignglobal_expand/biases&global_expand/biases/Initializer/Const"/device:CPU:0*'
_class
loc:@global_expand/biases*
validate_shape(*
use_locking(*
T0*
_output_shapes
:

global_expand/biases/readIdentityglobal_expand/biases"/device:CPU:0*
T0*'
_class
loc:@global_expand/biases*
_output_shapes
:

global_expand/BiasAddBiasAddglobal_expand/Conv2Dglobal_expand/biases/read*
T0*
data_formatNHWC*&
_output_shapes
:d
c
global_expand/bn/ConstConst*
valueB*    *
dtype0*
_output_shapes
:

global_expand/bn/beta
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:
Í
global_expand/bn/beta/AssignAssignglobal_expand/bn/betaglobal_expand/bn/Const*
use_locking(*
T0*(
_class
loc:@global_expand/bn/beta*
validate_shape(*
_output_shapes
:

global_expand/bn/beta/readIdentityglobal_expand/bn/beta*
T0*(
_class
loc:@global_expand/bn/beta*
_output_shapes
:
e
global_expand/bn/Const_1Const*
valueB*  ?*
dtype0*
_output_shapes
:

global_expand/bn/gamma
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes
:
Ò
global_expand/bn/gamma/AssignAssignglobal_expand/bn/gammaglobal_expand/bn/Const_1*
use_locking(*
T0*)
_class
loc:@global_expand/bn/gamma*
validate_shape(*
_output_shapes
:

global_expand/bn/gamma/readIdentityglobal_expand/bn/gamma*
T0*)
_class
loc:@global_expand/bn/gamma*
_output_shapes
:

/global_expand/bn/moments/mean/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
»
global_expand/bn/moments/meanMeanglobal_expand/BiasAdd/global_expand/bn/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:

%global_expand/bn/moments/StopGradientStopGradientglobal_expand/bn/moments/mean*
T0*&
_output_shapes
:
®
*global_expand/bn/moments/SquaredDifferenceSquaredDifferenceglobal_expand/BiasAdd%global_expand/bn/moments/StopGradient*
T0*&
_output_shapes
:d

3global_expand/bn/moments/variance/reduction_indicesConst*
dtype0*!
valueB"          *
_output_shapes
:
Ø
!global_expand/bn/moments/varianceMean*global_expand/bn/moments/SquaredDifference3global_expand/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*&
_output_shapes
:

 global_expand/bn/moments/SqueezeSqueezeglobal_expand/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes
:

"global_expand/bn/moments/Squeeze_1Squeeze!global_expand/bn/moments/variance*
squeeze_dims
 *
T0*
_output_shapes
:
g
global_expand/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
k
global_expand/bn/cond/switch_tIdentityglobal_expand/bn/cond/Switch:1*
T0
*
_output_shapes
: 
i
global_expand/bn/cond/switch_fIdentityglobal_expand/bn/cond/Switch*
T0
*
_output_shapes
: 
Y
global_expand/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 

\global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes
:

Jglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
dtype0*
	container *
shape:*
shared_name *]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
²
Qglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssignJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage\global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
«
Oglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/readIdentityJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage*
T0*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:

^global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes
:

Lglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
shape:*
shared_name *_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
_output_shapes
:
º
Sglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssignLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage^global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
±
Qglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentityLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:

4global_expand/bn/cond/ExponentialMovingAverage/decayConst^global_expand/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
ª
Dglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst^global_expand/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
æ
Bglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSubDglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x4global_expand/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 

Dglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1SubMglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1Oglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes
:
Ï
Kglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitchOglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/readglobal_expand/bn/cond/pred_id*
T0*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
ø
Mglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switch global_expand/bn/moments/Squeezeglobal_expand/bn/cond/pred_id*3
_class)
'%loc:@global_expand/bn/moments/Squeeze*
T0* 
_output_shapes
::
ø
Bglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMulDglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1Bglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes
:
ï
>global_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSubGglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Bglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
Ç
Eglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitchJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverageglobal_expand/bn/cond/pred_id*
T0*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
¬
Fglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst^global_expand/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
ê
Dglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSubFglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x4global_expand/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 

Fglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1SubOglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1Qglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes
:
Õ
Mglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitchQglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/readglobal_expand/bn/cond/pred_id*
T0*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
þ
Oglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switch"global_expand/bn/moments/Squeeze_1global_expand/bn/cond/pred_id*
T0*5
_class+
)'loc:@global_expand/bn/moments/Squeeze_1* 
_output_shapes
::
þ
Dglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMulFglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1Dglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes
:
÷
@global_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSubIglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1Dglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
Í
Gglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitchLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverageglobal_expand/bn/cond/pred_id*
T0*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::
º
.global_expand/bn/cond/ExponentialMovingAverageNoOp?^global_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvgA^global_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
Ù
(global_expand/bn/cond/control_dependencyIdentityglobal_expand/bn/cond/switch_t/^global_expand/bn/cond/ExponentialMovingAverage*1
_class'
%#loc:@global_expand/bn/cond/switch_t*
T0
*
_output_shapes
: 
C
global_expand/bn/cond/NoOpNoOp^global_expand/bn/cond/switch_f
Ç
*global_expand/bn/cond/control_dependency_1Identityglobal_expand/bn/cond/switch_f^global_expand/bn/cond/NoOp*
T0
*1
_class'
%#loc:@global_expand/bn/cond/switch_f*
_output_shapes
: 
¦
global_expand/bn/cond/MergeMerge*global_expand/bn/cond/control_dependency_1(global_expand/bn/cond/control_dependency*
T0
*
N*
_output_shapes
: : 
i
global_expand/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
o
 global_expand/bn/cond_1/switch_tIdentity global_expand/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 
m
 global_expand/bn/cond_1/switch_fIdentityglobal_expand/bn/cond_1/Switch*
T0
*
_output_shapes
: 
[
global_expand/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 

 global_expand/bn/cond_1/IdentityIdentity)global_expand/bn/cond_1/Identity/Switch:1^global_expand/bn/cond/Merge*
T0*
_output_shapes
:
Ô
'global_expand/bn/cond_1/Identity/SwitchSwitch global_expand/bn/moments/Squeezeglobal_expand/bn/cond_1/pred_id*
T0*3
_class)
'%loc:@global_expand/bn/moments/Squeeze* 
_output_shapes
::

"global_expand/bn/cond_1/Identity_1Identity+global_expand/bn/cond_1/Identity_1/Switch:1^global_expand/bn/cond/Merge*
T0*
_output_shapes
:
Ú
)global_expand/bn/cond_1/Identity_1/SwitchSwitch"global_expand/bn/moments/Squeeze_1global_expand/bn/cond_1/pred_id*
T0*5
_class+
)'loc:@global_expand/bn/moments/Squeeze_1* 
_output_shapes
::
¦
 global_expand/bn/cond_1/Switch_1SwitchOglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/readglobal_expand/bn/cond_1/pred_id*
T0*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage* 
_output_shapes
::
ª
 global_expand/bn/cond_1/Switch_2SwitchQglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/readglobal_expand/bn/cond_1/pred_id*
T0*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage* 
_output_shapes
::

global_expand/bn/cond_1/MergeMerge global_expand/bn/cond_1/Switch_1 global_expand/bn/cond_1/Identity*
T0*
N*
_output_shapes

:: 

global_expand/bn/cond_1/Merge_1Merge global_expand/bn/cond_1/Switch_2"global_expand/bn/cond_1/Identity_1*
N*
T0*
_output_shapes

:: 
e
 global_expand/bn/batchnorm/add/yConst*
valueB
 *o:*
dtype0*
_output_shapes
: 

global_expand/bn/batchnorm/addAddglobal_expand/bn/cond_1/Merge_1 global_expand/bn/batchnorm/add/y*
T0*
_output_shapes
:
n
 global_expand/bn/batchnorm/RsqrtRsqrtglobal_expand/bn/batchnorm/add*
T0*
_output_shapes
:

global_expand/bn/batchnorm/mulMul global_expand/bn/batchnorm/Rsqrtglobal_expand/bn/gamma/read*
T0*
_output_shapes
:

 global_expand/bn/batchnorm/mul_1Mulglobal_expand/BiasAddglobal_expand/bn/batchnorm/mul*
T0*&
_output_shapes
:d

 global_expand/bn/batchnorm/mul_2Mulglobal_expand/bn/cond_1/Mergeglobal_expand/bn/batchnorm/mul*
T0*
_output_shapes
:

global_expand/bn/batchnorm/subSubglobal_expand/bn/beta/read global_expand/bn/batchnorm/mul_2*
T0*
_output_shapes
:

 global_expand/bn/batchnorm/add_1Add global_expand/bn/batchnorm/mul_1global_expand/bn/batchnorm/sub*
T0*&
_output_shapes
:d
m
global_expand/ReluRelu global_expand/bn/batchnorm/add_1*
T0*&
_output_shapes
:d
X
concat_6/axisConst*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 

concat_6ConcatV2Max_1Max_3global_expand/ReluMaxMax_2concat_6/axis*
T0*
N*

Tidx0*'
_output_shapes
:dð
¥
,agg/weights/Initializer/random_uniform/shapeConst*
_class
loc:@agg/weights*%
valueB"      ð     *
dtype0*
_output_shapes
:

*agg/weights/Initializer/random_uniform/minConst*
dtype0*
_class
loc:@agg/weights*
valueB
 *Xï¶½*
_output_shapes
: 

*agg/weights/Initializer/random_uniform/maxConst*
_class
loc:@agg/weights*
valueB
 *Xï¶=*
dtype0*
_output_shapes
: 
ì
4agg/weights/Initializer/random_uniform/RandomUniformRandomUniform,agg/weights/Initializer/random_uniform/shape*
T0*
_class
loc:@agg/weights*
dtype0*
seed2 *

seed *(
_output_shapes
:ð
Ê
*agg/weights/Initializer/random_uniform/subSub*agg/weights/Initializer/random_uniform/max*agg/weights/Initializer/random_uniform/min*
T0*
_class
loc:@agg/weights*
_output_shapes
: 
æ
*agg/weights/Initializer/random_uniform/mulMul4agg/weights/Initializer/random_uniform/RandomUniform*agg/weights/Initializer/random_uniform/sub*
T0*
_class
loc:@agg/weights*(
_output_shapes
:ð
Ø
&agg/weights/Initializer/random_uniformAdd*agg/weights/Initializer/random_uniform/mul*agg/weights/Initializer/random_uniform/min*
_class
loc:@agg/weights*
T0*(
_output_shapes
:ð
Â
agg/weights
VariableV2"/device:CPU:0*
_class
loc:@agg/weights*
dtype0*
	container *
shape:ð*
shared_name *(
_output_shapes
:ð
Ü
agg/weights/AssignAssignagg/weights&agg/weights/Initializer/random_uniform"/device:CPU:0*
T0*
_class
loc:@agg/weights*
validate_shape(*
use_locking(*(
_output_shapes
:ð

agg/weights/readIdentityagg/weights"/device:CPU:0*
T0*
_class
loc:@agg/weights*(
_output_shapes
:ð
G

agg/L2LossL2Lossagg/weights/read*
T0*
_output_shapes
: 
V
agg/weight_loss/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
V
agg/weight_lossMul
agg/L2Lossagg/weight_loss/y*
T0*
_output_shapes
: 
ê

agg/Conv2DConv2Dconcat_6agg/weights/read*
paddingVALID*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
explicit_paddings
 *'
_output_shapes
:d

agg/biases/Initializer/ConstConst*
_class
loc:@agg/biases*
valueB*    *
dtype0*
_output_shapes	
:
¦

agg/biases
VariableV2"/device:CPU:0*
_class
loc:@agg/biases*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
Â
agg/biases/AssignAssign
agg/biasesagg/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*
_class
loc:@agg/biases*
validate_shape(*
_output_shapes	
:
{
agg/biases/readIdentity
agg/biases"/device:CPU:0*
_class
loc:@agg/biases*
T0*
_output_shapes	
:
|
agg/BiasAddBiasAdd
agg/Conv2Dagg/biases/read*
T0*
data_formatNHWC*'
_output_shapes
:d
[
agg/bn/ConstConst*
valueB*    *
dtype0*
_output_shapes	
:
y
agg/bn/beta
VariableV2*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
¦
agg/bn/beta/AssignAssignagg/bn/betaagg/bn/Const*
use_locking(*
T0*
_class
loc:@agg/bn/beta*
validate_shape(*
_output_shapes	
:
o
agg/bn/beta/readIdentityagg/bn/beta*
T0*
_class
loc:@agg/bn/beta*
_output_shapes	
:
]
agg/bn/Const_1Const*
valueB*  ?*
dtype0*
_output_shapes	
:
z
agg/bn/gamma
VariableV2*
shape:*
shared_name *
dtype0*
	container *
_output_shapes	
:
«
agg/bn/gamma/AssignAssignagg/bn/gammaagg/bn/Const_1*
use_locking(*
T0*
_class
loc:@agg/bn/gamma*
validate_shape(*
_output_shapes	
:
r
agg/bn/gamma/readIdentityagg/bn/gamma*
T0*
_class
loc:@agg/bn/gamma*
_output_shapes	
:
z
%agg/bn/moments/mean/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:

agg/bn/moments/meanMeanagg/BiasAdd%agg/bn/moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(*'
_output_shapes
:
r
agg/bn/moments/StopGradientStopGradientagg/bn/moments/mean*
T0*'
_output_shapes
:

 agg/bn/moments/SquaredDifferenceSquaredDifferenceagg/BiasAddagg/bn/moments/StopGradient*
T0*'
_output_shapes
:d
~
)agg/bn/moments/variance/reduction_indicesConst*!
valueB"          *
dtype0*
_output_shapes
:
»
agg/bn/moments/varianceMean agg/bn/moments/SquaredDifference)agg/bn/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:
u
agg/bn/moments/SqueezeSqueezeagg/bn/moments/mean*
squeeze_dims
 *
T0*
_output_shapes	
:
{
agg/bn/moments/Squeeze_1Squeezeagg/bn/moments/variance*
T0*
squeeze_dims
 *
_output_shapes	
:
]
agg/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
W
agg/bn/cond/switch_tIdentityagg/bn/cond/Switch:1*
T0
*
_output_shapes
: 
U
agg/bn/cond/switch_fIdentityagg/bn/cond/Switch*
T0
*
_output_shapes
: 
O
agg/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
â
Hagg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zerosConst*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes	
:
ï
6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage
VariableV2*
shape:*
shared_name *I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*
dtype0*
	container *
_output_shapes	
:
ã
=agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/AssignAssign6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverageHagg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros*
T0*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes	
:
ð
;agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/readIdentity6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*
T0*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:
æ
Jagg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zerosConst*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*
valueB*    *
dtype0*
_output_shapes	
:
ó
8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage
VariableV2*
shared_name *K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
	container *
shape:*
_output_shapes	
:
ë
?agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignAssign8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverageJagg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros*
use_locking(*
T0*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:
ö
=agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/readIdentity8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*
T0*
_output_shapes	
:

*agg/bn/cond/ExponentialMovingAverage/decayConst^agg/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 

:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/xConst^agg/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
È
8agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/subSub:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x*agg/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
ó
:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1SubCagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1Eagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1*
T0*
_output_shapes	
:

Aagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/SwitchSwitch;agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/readagg/bn/cond/pred_id*
T0*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::
Ò
Cagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1Switchagg/bn/moments/Squeezeagg/bn/cond/pred_id*
T0*)
_class
loc:@agg/bn/moments/Squeeze*"
_output_shapes
::
Û
8agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mulMul:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_18agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub*
T0*
_output_shapes	
:
¾
4agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg	AssignSub=agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:18agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul*
use_locking( *
T0*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:

;agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch	RefSwitch6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverageagg/bn/cond/pred_id*
T0*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::

<agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/xConst^agg/bn/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Ì
:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/subSub<agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x*agg/bn/cond/ExponentialMovingAverage/decay*
T0*
_output_shapes
: 
ù
<agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1SubEagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1Gagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1*
T0*
_output_shapes	
:

Cagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/SwitchSwitch=agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/readagg/bn/cond/pred_id*
T0*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::
Ø
Eagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1Switchagg/bn/moments/Squeeze_1agg/bn/cond/pred_id*
T0*+
_class!
loc:@agg/bn/moments/Squeeze_1*"
_output_shapes
::
á
:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mulMul<agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub*
T0*
_output_shapes	
:
Æ
6agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1	AssignSub?agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul*
use_locking( *
T0*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes	
:

=agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch	RefSwitch8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverageagg/bn/cond/pred_id*
T0*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::

$agg/bn/cond/ExponentialMovingAverageNoOp5^agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg7^agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1
±
agg/bn/cond/control_dependencyIdentityagg/bn/cond/switch_t%^agg/bn/cond/ExponentialMovingAverage*
T0
*'
_class
loc:@agg/bn/cond/switch_t*
_output_shapes
: 
/
agg/bn/cond/NoOpNoOp^agg/bn/cond/switch_f

 agg/bn/cond/control_dependency_1Identityagg/bn/cond/switch_f^agg/bn/cond/NoOp*
T0
*'
_class
loc:@agg/bn/cond/switch_f*
_output_shapes
: 

agg/bn/cond/MergeMerge agg/bn/cond/control_dependency_1agg/bn/cond/control_dependency*
T0
*
N*
_output_shapes
: : 
_
agg/bn/cond_1/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
[
agg/bn/cond_1/switch_tIdentityagg/bn/cond_1/Switch:1*
T0
*
_output_shapes
: 
Y
agg/bn/cond_1/switch_fIdentityagg/bn/cond_1/Switch*
T0
*
_output_shapes
: 
Q
agg/bn/cond_1/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
}
agg/bn/cond_1/IdentityIdentityagg/bn/cond_1/Identity/Switch:1^agg/bn/cond/Merge*
T0*
_output_shapes	
:
®
agg/bn/cond_1/Identity/SwitchSwitchagg/bn/moments/Squeezeagg/bn/cond_1/pred_id*
T0*)
_class
loc:@agg/bn/moments/Squeeze*"
_output_shapes
::

agg/bn/cond_1/Identity_1Identity!agg/bn/cond_1/Identity_1/Switch:1^agg/bn/cond/Merge*
T0*
_output_shapes	
:
´
agg/bn/cond_1/Identity_1/SwitchSwitchagg/bn/moments/Squeeze_1agg/bn/cond_1/pred_id*
T0*+
_class!
loc:@agg/bn/moments/Squeeze_1*"
_output_shapes
::
ì
agg/bn/cond_1/Switch_1Switch;agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/readagg/bn/cond_1/pred_id*
T0*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*"
_output_shapes
::
ð
agg/bn/cond_1/Switch_2Switch=agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/readagg/bn/cond_1/pred_id*
T0*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*"
_output_shapes
::
}
agg/bn/cond_1/MergeMergeagg/bn/cond_1/Switch_1agg/bn/cond_1/Identity*
T0*
N*
_output_shapes
	:: 

agg/bn/cond_1/Merge_1Mergeagg/bn/cond_1/Switch_2agg/bn/cond_1/Identity_1*
T0*
N*
_output_shapes
	:: 
[
agg/bn/batchnorm/add/yConst*
dtype0*
valueB
 *o:*
_output_shapes
: 
p
agg/bn/batchnorm/addAddagg/bn/cond_1/Merge_1agg/bn/batchnorm/add/y*
T0*
_output_shapes	
:
[
agg/bn/batchnorm/RsqrtRsqrtagg/bn/batchnorm/add*
T0*
_output_shapes	
:
l
agg/bn/batchnorm/mulMulagg/bn/batchnorm/Rsqrtagg/bn/gamma/read*
T0*
_output_shapes	
:
r
agg/bn/batchnorm/mul_1Mulagg/BiasAddagg/bn/batchnorm/mul*
T0*'
_output_shapes
:d
n
agg/bn/batchnorm/mul_2Mulagg/bn/cond_1/Mergeagg/bn/batchnorm/mul*
T0*
_output_shapes	
:
k
agg/bn/batchnorm/subSubagg/bn/beta/readagg/bn/batchnorm/mul_2*
T0*
_output_shapes	
:
}
agg/bn/batchnorm/add_1Addagg/bn/batchnorm/mul_1agg/bn/batchnorm/sub*
T0*'
_output_shapes
:d
Z
agg/ReluReluagg/bn/batchnorm/add_1*
T0*'
_output_shapes
:d
©
avgpool/avgpoolMaxPoolagg/Relu*
ksize
d*
paddingVALID*
T0*
strides
*
data_formatNHWC*'
_output_shapes
:
i
Tile_4/multiplesConst*%
valueB"   d         *
dtype0*
_output_shapes
:
u
Tile_4Tileavgpool/avgpoolTile_4/multiples*

Tmultiples0*
T0*'
_output_shapes
:d
O
concat_7/axisConst*
value	B :*
dtype0*
_output_shapes
: 

concat_7ConcatV2Tile_4Max_1Max_3concat_7/axis*
T0*
N*

Tidx0*'
_output_shapes
:d
±
2seg/conv2/weights/Initializer/random_uniform/shapeConst*$
_class
loc:@seg/conv2/weights*%
valueB"           *
dtype0*
_output_shapes
:

0seg/conv2/weights/Initializer/random_uniform/minConst*$
_class
loc:@seg/conv2/weights*
valueB
 *b§½*
dtype0*
_output_shapes
: 

0seg/conv2/weights/Initializer/random_uniform/maxConst*$
_class
loc:@seg/conv2/weights*
valueB
 *b§=*
dtype0*
_output_shapes
: 
þ
:seg/conv2/weights/Initializer/random_uniform/RandomUniformRandomUniform2seg/conv2/weights/Initializer/random_uniform/shape*
T0*$
_class
loc:@seg/conv2/weights*
dtype0*
seed2 *

seed *(
_output_shapes
:
â
0seg/conv2/weights/Initializer/random_uniform/subSub0seg/conv2/weights/Initializer/random_uniform/max0seg/conv2/weights/Initializer/random_uniform/min*
T0*$
_class
loc:@seg/conv2/weights*
_output_shapes
: 
þ
0seg/conv2/weights/Initializer/random_uniform/mulMul:seg/conv2/weights/Initializer/random_uniform/RandomUniform0seg/conv2/weights/Initializer/random_uniform/sub*
T0*$
_class
loc:@seg/conv2/weights*(
_output_shapes
:
ð
,seg/conv2/weights/Initializer/random_uniformAdd0seg/conv2/weights/Initializer/random_uniform/mul0seg/conv2/weights/Initializer/random_uniform/min*
T0*$
_class
loc:@seg/conv2/weights*(
_output_shapes
:
Î
seg/conv2/weights
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:*
shared_name *$
_class
loc:@seg/conv2/weights*(
_output_shapes
:
ô
seg/conv2/weights/AssignAssignseg/conv2/weights,seg/conv2/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv2/weights*
validate_shape(*(
_output_shapes
:

seg/conv2/weights/readIdentityseg/conv2/weights"/device:CPU:0*
T0*$
_class
loc:@seg/conv2/weights*(
_output_shapes
:
ö
seg/conv2/Conv2DConv2Dconcat_7seg/conv2/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
explicit_paddings
 *
paddingVALID*'
_output_shapes
:d

"seg/conv2/biases/Initializer/ConstConst*#
_class
loc:@seg/conv2/biases*
valueB*    *
dtype0*
_output_shapes	
:
²
seg/conv2/biases
VariableV2"/device:CPU:0*
shared_name *#
_class
loc:@seg/conv2/biases*
dtype0*
	container *
shape:*
_output_shapes	
:
Ú
seg/conv2/biases/AssignAssignseg/conv2/biases"seg/conv2/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv2/biases*
validate_shape(*
_output_shapes	
:

seg/conv2/biases/readIdentityseg/conv2/biases"/device:CPU:0*
T0*#
_class
loc:@seg/conv2/biases*
_output_shapes	
:

seg/conv2/BiasAddBiasAddseg/conv2/Conv2Dseg/conv2/biases/read*
T0*
data_formatNHWC*'
_output_shapes
:d

#seg/conv2/bn/beta/Initializer/zerosConst*$
_class
loc:@seg/conv2/bn/beta*
valueB*    *
dtype0*
_output_shapes	
:
´
seg/conv2/bn/beta
VariableV2"/device:CPU:0*
shape:*
shared_name *$
_class
loc:@seg/conv2/bn/beta*
dtype0*
	container *
_output_shapes	
:
Þ
seg/conv2/bn/beta/AssignAssignseg/conv2/bn/beta#seg/conv2/bn/beta/Initializer/zeros"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv2/bn/beta*
validate_shape(*
_output_shapes	
:

seg/conv2/bn/beta/readIdentityseg/conv2/bn/beta"/device:CPU:0*
T0*$
_class
loc:@seg/conv2/bn/beta*
_output_shapes	
:

#seg/conv2/bn/gamma/Initializer/onesConst*%
_class
loc:@seg/conv2/bn/gamma*
valueB*  ?*
dtype0*
_output_shapes	
:
¶
seg/conv2/bn/gamma
VariableV2"/device:CPU:0*%
_class
loc:@seg/conv2/bn/gamma*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
á
seg/conv2/bn/gamma/AssignAssignseg/conv2/bn/gamma#seg/conv2/bn/gamma/Initializer/ones"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@seg/conv2/bn/gamma*
validate_shape(*
_output_shapes	
:

seg/conv2/bn/gamma/readIdentityseg/conv2/bn/gamma"/device:CPU:0*
T0*%
_class
loc:@seg/conv2/bn/gamma*
_output_shapes	
:
 
'seg/conv2/bn/pop_mean/Initializer/zerosConst*(
_class
loc:@seg/conv2/bn/pop_mean*
valueB*    *
dtype0*
_output_shapes	
:
¼
seg/conv2/bn/pop_mean
VariableV2"/device:CPU:0*(
_class
loc:@seg/conv2/bn/pop_mean*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
î
seg/conv2/bn/pop_mean/AssignAssignseg/conv2/bn/pop_mean'seg/conv2/bn/pop_mean/Initializer/zeros"/device:CPU:0*
validate_shape(*
use_locking(*
T0*(
_class
loc:@seg/conv2/bn/pop_mean*
_output_shapes	
:

seg/conv2/bn/pop_mean/readIdentityseg/conv2/bn/pop_mean"/device:CPU:0*(
_class
loc:@seg/conv2/bn/pop_mean*
T0*
_output_shapes	
:

%seg/conv2/bn/pop_var/Initializer/onesConst*'
_class
loc:@seg/conv2/bn/pop_var*
valueB*  ?*
dtype0*
_output_shapes	
:
º
seg/conv2/bn/pop_var
VariableV2"/device:CPU:0*'
_class
loc:@seg/conv2/bn/pop_var*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
é
seg/conv2/bn/pop_var/AssignAssignseg/conv2/bn/pop_var%seg/conv2/bn/pop_var/Initializer/ones"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@seg/conv2/bn/pop_var*
validate_shape(*
_output_shapes	
:

seg/conv2/bn/pop_var/readIdentityseg/conv2/bn/pop_var"/device:CPU:0*
T0*'
_class
loc:@seg/conv2/bn/pop_var*
_output_shapes	
:
c
seg/conv2/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
c
seg/conv2/bn/cond/switch_tIdentityseg/conv2/bn/cond/Switch:1*
T0
*
_output_shapes
: 
a
seg/conv2/bn/cond/switch_fIdentityseg/conv2/bn/cond/Switch*
T0
*
_output_shapes
: 
U
seg/conv2/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
¢
0seg/conv2/bn/cond/moments/mean/reduction_indicesConst^seg/conv2/bn/cond/switch_t*!
valueB"          *
dtype0*
_output_shapes
:
Ð
seg/conv2/bn/cond/moments/meanMean'seg/conv2/bn/cond/moments/mean/Switch:10seg/conv2/bn/cond/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:
È
%seg/conv2/bn/cond/moments/mean/SwitchSwitchseg/conv2/BiasAddseg/conv2/bn/cond/pred_id*
T0*$
_class
loc:@seg/conv2/BiasAdd*:
_output_shapes(
&:d:d

&seg/conv2/bn/cond/moments/StopGradientStopGradientseg/conv2/bn/cond/moments/mean*
T0*'
_output_shapes
:
Ã
+seg/conv2/bn/cond/moments/SquaredDifferenceSquaredDifference'seg/conv2/bn/cond/moments/mean/Switch:1&seg/conv2/bn/cond/moments/StopGradient*
T0*'
_output_shapes
:d
¦
4seg/conv2/bn/cond/moments/variance/reduction_indicesConst^seg/conv2/bn/cond/switch_t*
dtype0*!
valueB"          *
_output_shapes
:
Ü
"seg/conv2/bn/cond/moments/varianceMean+seg/conv2/bn/cond/moments/SquaredDifference4seg/conv2/bn/cond/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:

!seg/conv2/bn/cond/moments/SqueezeSqueezeseg/conv2/bn/cond/moments/mean*
squeeze_dims
 *
T0*
_output_shapes	
:

#seg/conv2/bn/cond/moments/Squeeze_1Squeeze"seg/conv2/bn/cond/moments/variance*
squeeze_dims
 *
T0*
_output_shapes	
:
y
seg/conv2/bn/cond/mul/yConst^seg/conv2/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
{
seg/conv2/bn/cond/mulMulseg/conv2/bn/cond/mul/Switch:1seg/conv2/bn/cond/mul/y*
T0*
_output_shapes	
:
Ã
seg/conv2/bn/cond/mul/SwitchSwitchseg/conv2/bn/pop_mean/readseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*(
_class
loc:@seg/conv2/bn/pop_mean*"
_output_shapes
::
{
seg/conv2/bn/cond/mul_1/yConst^seg/conv2/bn/cond/switch_t*
valueB
 *ÍÌÌ=*
dtype0*
_output_shapes
: 

seg/conv2/bn/cond/mul_1Mul!seg/conv2/bn/cond/moments/Squeezeseg/conv2/bn/cond/mul_1/y*
T0*
_output_shapes	
:
r
seg/conv2/bn/cond/addAddseg/conv2/bn/cond/mulseg/conv2/bn/cond/mul_1*
T0*
_output_shapes	
:
ä
seg/conv2/bn/cond/AssignAssign!seg/conv2/bn/cond/Assign/Switch:1seg/conv2/bn/cond/add"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@seg/conv2/bn/pop_mean*
validate_shape(*
_output_shapes	
:
Ä
seg/conv2/bn/cond/Assign/Switch	RefSwitchseg/conv2/bn/pop_meanseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*(
_class
loc:@seg/conv2/bn/pop_mean*"
_output_shapes
::
{
seg/conv2/bn/cond/mul_2/yConst^seg/conv2/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 

seg/conv2/bn/cond/mul_2Mul seg/conv2/bn/cond/mul_2/Switch:1seg/conv2/bn/cond/mul_2/y*
T0*
_output_shapes	
:
Ã
seg/conv2/bn/cond/mul_2/SwitchSwitchseg/conv2/bn/pop_var/readseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*'
_class
loc:@seg/conv2/bn/pop_var*"
_output_shapes
::
{
seg/conv2/bn/cond/mul_3/yConst^seg/conv2/bn/cond/switch_t*
valueB
 *ÍÌÌ=*
dtype0*
_output_shapes
: 

seg/conv2/bn/cond/mul_3Mul#seg/conv2/bn/cond/moments/Squeeze_1seg/conv2/bn/cond/mul_3/y*
T0*
_output_shapes	
:
v
seg/conv2/bn/cond/add_1Addseg/conv2/bn/cond/mul_2seg/conv2/bn/cond/mul_3*
T0*
_output_shapes	
:
é
seg/conv2/bn/cond/Assign_1Assign#seg/conv2/bn/cond/Assign_1/Switch:1seg/conv2/bn/cond/add_1"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@seg/conv2/bn/pop_var*
validate_shape(*
_output_shapes	
:
Ä
!seg/conv2/bn/cond/Assign_1/Switch	RefSwitchseg/conv2/bn/pop_varseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*'
_class
loc:@seg/conv2/bn/pop_var*"
_output_shapes
::

!seg/conv2/bn/cond/batchnorm/add/yConst^seg/conv2/bn/cond/Assign^seg/conv2/bn/cond/Assign_1*
valueB
 *o:*
dtype0*
_output_shapes
: 

seg/conv2/bn/cond/batchnorm/addAdd#seg/conv2/bn/cond/moments/Squeeze_1!seg/conv2/bn/cond/batchnorm/add/y*
T0*
_output_shapes	
:
q
!seg/conv2/bn/cond/batchnorm/RsqrtRsqrtseg/conv2/bn/cond/batchnorm/add*
T0*
_output_shapes	
:

seg/conv2/bn/cond/batchnorm/mulMul!seg/conv2/bn/cond/batchnorm/Rsqrt(seg/conv2/bn/cond/batchnorm/mul/Switch:1*
T0*
_output_shapes	
:
Ç
&seg/conv2/bn/cond/batchnorm/mul/SwitchSwitchseg/conv2/bn/gamma/readseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*%
_class
loc:@seg/conv2/bn/gamma*"
_output_shapes
::
¤
!seg/conv2/bn/cond/batchnorm/mul_1Mul'seg/conv2/bn/cond/moments/mean/Switch:1seg/conv2/bn/cond/batchnorm/mul*
T0*'
_output_shapes
:d

!seg/conv2/bn/cond/batchnorm/mul_2Mul!seg/conv2/bn/cond/moments/Squeezeseg/conv2/bn/cond/batchnorm/mul*
T0*
_output_shapes	
:

seg/conv2/bn/cond/batchnorm/subSub(seg/conv2/bn/cond/batchnorm/sub/Switch:1!seg/conv2/bn/cond/batchnorm/mul_2*
T0*
_output_shapes	
:
Å
&seg/conv2/bn/cond/batchnorm/sub/SwitchSwitchseg/conv2/bn/beta/readseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*$
_class
loc:@seg/conv2/bn/beta*"
_output_shapes
::

!seg/conv2/bn/cond/batchnorm/add_1Add!seg/conv2/bn/cond/batchnorm/mul_1seg/conv2/bn/cond/batchnorm/sub*
T0*'
_output_shapes
:d

#seg/conv2/bn/cond/batchnorm_1/add/yConst^seg/conv2/bn/cond/switch_f*
valueB
 *o:*
dtype0*
_output_shapes
: 

!seg/conv2/bn/cond/batchnorm_1/addAdd(seg/conv2/bn/cond/batchnorm_1/add/Switch#seg/conv2/bn/cond/batchnorm_1/add/y*
T0*
_output_shapes	
:
Í
(seg/conv2/bn/cond/batchnorm_1/add/SwitchSwitchseg/conv2/bn/pop_var/readseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*'
_class
loc:@seg/conv2/bn/pop_var*"
_output_shapes
::
u
#seg/conv2/bn/cond/batchnorm_1/RsqrtRsqrt!seg/conv2/bn/cond/batchnorm_1/add*
T0*
_output_shapes	
:

!seg/conv2/bn/cond/batchnorm_1/mulMul#seg/conv2/bn/cond/batchnorm_1/Rsqrt(seg/conv2/bn/cond/batchnorm_1/mul/Switch*
T0*
_output_shapes	
:
É
(seg/conv2/bn/cond/batchnorm_1/mul/SwitchSwitchseg/conv2/bn/gamma/readseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*%
_class
loc:@seg/conv2/bn/gamma*"
_output_shapes
::
«
#seg/conv2/bn/cond/batchnorm_1/mul_1Mul*seg/conv2/bn/cond/batchnorm_1/mul_1/Switch!seg/conv2/bn/cond/batchnorm_1/mul*
T0*'
_output_shapes
:d
Í
*seg/conv2/bn/cond/batchnorm_1/mul_1/SwitchSwitchseg/conv2/BiasAddseg/conv2/bn/cond/pred_id*
T0*$
_class
loc:@seg/conv2/BiasAdd*:
_output_shapes(
&:d:d

#seg/conv2/bn/cond/batchnorm_1/mul_2Mul*seg/conv2/bn/cond/batchnorm_1/mul_2/Switch!seg/conv2/bn/cond/batchnorm_1/mul*
T0*
_output_shapes	
:
Ñ
*seg/conv2/bn/cond/batchnorm_1/mul_2/SwitchSwitchseg/conv2/bn/pop_mean/readseg/conv2/bn/cond/pred_id"/device:CPU:0*(
_class
loc:@seg/conv2/bn/pop_mean*
T0*"
_output_shapes
::

!seg/conv2/bn/cond/batchnorm_1/subSub(seg/conv2/bn/cond/batchnorm_1/sub/Switch#seg/conv2/bn/cond/batchnorm_1/mul_2*
T0*
_output_shapes	
:
Ç
(seg/conv2/bn/cond/batchnorm_1/sub/SwitchSwitchseg/conv2/bn/beta/readseg/conv2/bn/cond/pred_id"/device:CPU:0*
T0*$
_class
loc:@seg/conv2/bn/beta*"
_output_shapes
::
¤
#seg/conv2/bn/cond/batchnorm_1/add_1Add#seg/conv2/bn/cond/batchnorm_1/mul_1!seg/conv2/bn/cond/batchnorm_1/sub*
T0*'
_output_shapes
:d
¥
seg/conv2/bn/cond/MergeMerge#seg/conv2/bn/cond/batchnorm_1/add_1!seg/conv2/bn/cond/batchnorm/add_1*
T0*
N*)
_output_shapes
:d: 
a
seg/conv2/ReluReluseg/conv2/bn/cond/Merge*
T0*'
_output_shapes
:d
^
seg/dp1/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
Y
seg/dp1/cond/switch_tIdentityseg/dp1/cond/Switch:1*
T0
*
_output_shapes
: 
W
seg/dp1/cond/switch_fIdentityseg/dp1/cond/Switch*
T0
*
_output_shapes
: 
P
seg/dp1/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
v
seg/dp1/cond/dropout/rateConst^seg/dp1/cond/switch_t*
valueB
 *ÍÌÌ>*
dtype0*
_output_shapes
: 

seg/dp1/cond/dropout/ShapeConst^seg/dp1/cond/switch_t*%
valueB"   d         *
dtype0*
_output_shapes
:

'seg/dp1/cond/dropout/random_uniform/minConst^seg/dp1/cond/switch_t*
valueB
 *    *
dtype0*
_output_shapes
: 

'seg/dp1/cond/dropout/random_uniform/maxConst^seg/dp1/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
¶
1seg/dp1/cond/dropout/random_uniform/RandomUniformRandomUniformseg/dp1/cond/dropout/Shape*
seed2 *

seed *
T0*
dtype0*'
_output_shapes
:d
¡
'seg/dp1/cond/dropout/random_uniform/subSub'seg/dp1/cond/dropout/random_uniform/max'seg/dp1/cond/dropout/random_uniform/min*
T0*
_output_shapes
: 
¼
'seg/dp1/cond/dropout/random_uniform/mulMul1seg/dp1/cond/dropout/random_uniform/RandomUniform'seg/dp1/cond/dropout/random_uniform/sub*
T0*'
_output_shapes
:d
®
#seg/dp1/cond/dropout/random_uniformAdd'seg/dp1/cond/dropout/random_uniform/mul'seg/dp1/cond/dropout/random_uniform/min*
T0*'
_output_shapes
:d
w
seg/dp1/cond/dropout/sub/xConst^seg/dp1/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
w
seg/dp1/cond/dropout/subSubseg/dp1/cond/dropout/sub/xseg/dp1/cond/dropout/rate*
T0*
_output_shapes
: 
{
seg/dp1/cond/dropout/truediv/xConst^seg/dp1/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 

seg/dp1/cond/dropout/truedivRealDivseg/dp1/cond/dropout/truediv/xseg/dp1/cond/dropout/sub*
T0*
_output_shapes
: 
£
!seg/dp1/cond/dropout/GreaterEqualGreaterEqual#seg/dp1/cond/dropout/random_uniformseg/dp1/cond/dropout/rate*
T0*'
_output_shapes
:d

seg/dp1/cond/dropout/mulMul!seg/dp1/cond/dropout/mul/Switch:1seg/dp1/cond/dropout/truediv*
T0*'
_output_shapes
:d
·
seg/dp1/cond/dropout/mul/SwitchSwitchseg/conv2/Reluseg/dp1/cond/pred_id*
T0*!
_class
loc:@seg/conv2/Relu*:
_output_shapes(
&:d:d

seg/dp1/cond/dropout/CastCast!seg/dp1/cond/dropout/GreaterEqual*

DstT0*

SrcT0
*
Truncate( *'
_output_shapes
:d

seg/dp1/cond/dropout/mul_1Mulseg/dp1/cond/dropout/mulseg/dp1/cond/dropout/Cast*
T0*'
_output_shapes
:d
­
seg/dp1/cond/Switch_1Switchseg/conv2/Reluseg/dp1/cond/pred_id*
T0*!
_class
loc:@seg/conv2/Relu*:
_output_shapes(
&:d:d

seg/dp1/cond/MergeMergeseg/dp1/cond/Switch_1seg/dp1/cond/dropout/mul_1*
T0*
N*)
_output_shapes
:d: 
±
2seg/conv3/weights/Initializer/random_uniform/shapeConst*$
_class
loc:@seg/conv3/weights*%
valueB"            *
dtype0*
_output_shapes
:

0seg/conv3/weights/Initializer/random_uniform/minConst*$
_class
loc:@seg/conv3/weights*
valueB
 *×³Ý½*
dtype0*
_output_shapes
: 

0seg/conv3/weights/Initializer/random_uniform/maxConst*$
_class
loc:@seg/conv3/weights*
valueB
 *×³Ý=*
dtype0*
_output_shapes
: 
þ
:seg/conv3/weights/Initializer/random_uniform/RandomUniformRandomUniform2seg/conv3/weights/Initializer/random_uniform/shape*
T0*$
_class
loc:@seg/conv3/weights*
dtype0*
seed2 *

seed *(
_output_shapes
:
â
0seg/conv3/weights/Initializer/random_uniform/subSub0seg/conv3/weights/Initializer/random_uniform/max0seg/conv3/weights/Initializer/random_uniform/min*
T0*$
_class
loc:@seg/conv3/weights*
_output_shapes
: 
þ
0seg/conv3/weights/Initializer/random_uniform/mulMul:seg/conv3/weights/Initializer/random_uniform/RandomUniform0seg/conv3/weights/Initializer/random_uniform/sub*
T0*$
_class
loc:@seg/conv3/weights*(
_output_shapes
:
ð
,seg/conv3/weights/Initializer/random_uniformAdd0seg/conv3/weights/Initializer/random_uniform/mul0seg/conv3/weights/Initializer/random_uniform/min*
T0*$
_class
loc:@seg/conv3/weights*(
_output_shapes
:
Î
seg/conv3/weights
VariableV2"/device:CPU:0*
	container *
shape:*
shared_name *$
_class
loc:@seg/conv3/weights*
dtype0*(
_output_shapes
:
ô
seg/conv3/weights/AssignAssignseg/conv3/weights,seg/conv3/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv3/weights*
validate_shape(*(
_output_shapes
:

seg/conv3/weights/readIdentityseg/conv3/weights"/device:CPU:0*
T0*$
_class
loc:@seg/conv3/weights*(
_output_shapes
:

seg/conv3/Conv2DConv2Dseg/dp1/cond/Mergeseg/conv3/weights/read*
paddingVALID*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
explicit_paddings
 *'
_output_shapes
:d

"seg/conv3/biases/Initializer/ConstConst*#
_class
loc:@seg/conv3/biases*
valueB*    *
dtype0*
_output_shapes	
:
²
seg/conv3/biases
VariableV2"/device:CPU:0*#
_class
loc:@seg/conv3/biases*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
Ú
seg/conv3/biases/AssignAssignseg/conv3/biases"seg/conv3/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv3/biases*
validate_shape(*
_output_shapes	
:

seg/conv3/biases/readIdentityseg/conv3/biases"/device:CPU:0*
T0*#
_class
loc:@seg/conv3/biases*
_output_shapes	
:

seg/conv3/BiasAddBiasAddseg/conv3/Conv2Dseg/conv3/biases/read*
T0*
data_formatNHWC*'
_output_shapes
:d

#seg/conv3/bn/beta/Initializer/zerosConst*$
_class
loc:@seg/conv3/bn/beta*
valueB*    *
dtype0*
_output_shapes	
:
´
seg/conv3/bn/beta
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:*
shared_name *$
_class
loc:@seg/conv3/bn/beta*
_output_shapes	
:
Þ
seg/conv3/bn/beta/AssignAssignseg/conv3/bn/beta#seg/conv3/bn/beta/Initializer/zeros"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv3/bn/beta*
validate_shape(*
_output_shapes	
:

seg/conv3/bn/beta/readIdentityseg/conv3/bn/beta"/device:CPU:0*$
_class
loc:@seg/conv3/bn/beta*
T0*
_output_shapes	
:

#seg/conv3/bn/gamma/Initializer/onesConst*%
_class
loc:@seg/conv3/bn/gamma*
valueB*  ?*
dtype0*
_output_shapes	
:
¶
seg/conv3/bn/gamma
VariableV2"/device:CPU:0*
	container *
shape:*
shared_name *%
_class
loc:@seg/conv3/bn/gamma*
dtype0*
_output_shapes	
:
á
seg/conv3/bn/gamma/AssignAssignseg/conv3/bn/gamma#seg/conv3/bn/gamma/Initializer/ones"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@seg/conv3/bn/gamma*
validate_shape(*
_output_shapes	
:

seg/conv3/bn/gamma/readIdentityseg/conv3/bn/gamma"/device:CPU:0*
T0*%
_class
loc:@seg/conv3/bn/gamma*
_output_shapes	
:
 
'seg/conv3/bn/pop_mean/Initializer/zerosConst*(
_class
loc:@seg/conv3/bn/pop_mean*
valueB*    *
dtype0*
_output_shapes	
:
¼
seg/conv3/bn/pop_mean
VariableV2"/device:CPU:0*(
_class
loc:@seg/conv3/bn/pop_mean*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
î
seg/conv3/bn/pop_mean/AssignAssignseg/conv3/bn/pop_mean'seg/conv3/bn/pop_mean/Initializer/zeros"/device:CPU:0*(
_class
loc:@seg/conv3/bn/pop_mean*
validate_shape(*
use_locking(*
T0*
_output_shapes	
:

seg/conv3/bn/pop_mean/readIdentityseg/conv3/bn/pop_mean"/device:CPU:0*
T0*(
_class
loc:@seg/conv3/bn/pop_mean*
_output_shapes	
:

%seg/conv3/bn/pop_var/Initializer/onesConst*'
_class
loc:@seg/conv3/bn/pop_var*
valueB*  ?*
dtype0*
_output_shapes	
:
º
seg/conv3/bn/pop_var
VariableV2"/device:CPU:0*
shared_name *'
_class
loc:@seg/conv3/bn/pop_var*
dtype0*
	container *
shape:*
_output_shapes	
:
é
seg/conv3/bn/pop_var/AssignAssignseg/conv3/bn/pop_var%seg/conv3/bn/pop_var/Initializer/ones"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@seg/conv3/bn/pop_var*
validate_shape(*
_output_shapes	
:

seg/conv3/bn/pop_var/readIdentityseg/conv3/bn/pop_var"/device:CPU:0*
T0*'
_class
loc:@seg/conv3/bn/pop_var*
_output_shapes	
:
c
seg/conv3/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
c
seg/conv3/bn/cond/switch_tIdentityseg/conv3/bn/cond/Switch:1*
T0
*
_output_shapes
: 
a
seg/conv3/bn/cond/switch_fIdentityseg/conv3/bn/cond/Switch*
T0
*
_output_shapes
: 
U
seg/conv3/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
¢
0seg/conv3/bn/cond/moments/mean/reduction_indicesConst^seg/conv3/bn/cond/switch_t*
dtype0*!
valueB"          *
_output_shapes
:
Ð
seg/conv3/bn/cond/moments/meanMean'seg/conv3/bn/cond/moments/mean/Switch:10seg/conv3/bn/cond/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:
È
%seg/conv3/bn/cond/moments/mean/SwitchSwitchseg/conv3/BiasAddseg/conv3/bn/cond/pred_id*$
_class
loc:@seg/conv3/BiasAdd*
T0*:
_output_shapes(
&:d:d

&seg/conv3/bn/cond/moments/StopGradientStopGradientseg/conv3/bn/cond/moments/mean*
T0*'
_output_shapes
:
Ã
+seg/conv3/bn/cond/moments/SquaredDifferenceSquaredDifference'seg/conv3/bn/cond/moments/mean/Switch:1&seg/conv3/bn/cond/moments/StopGradient*
T0*'
_output_shapes
:d
¦
4seg/conv3/bn/cond/moments/variance/reduction_indicesConst^seg/conv3/bn/cond/switch_t*!
valueB"          *
dtype0*
_output_shapes
:
Ü
"seg/conv3/bn/cond/moments/varianceMean+seg/conv3/bn/cond/moments/SquaredDifference4seg/conv3/bn/cond/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:

!seg/conv3/bn/cond/moments/SqueezeSqueezeseg/conv3/bn/cond/moments/mean*
squeeze_dims
 *
T0*
_output_shapes	
:

#seg/conv3/bn/cond/moments/Squeeze_1Squeeze"seg/conv3/bn/cond/moments/variance*
squeeze_dims
 *
T0*
_output_shapes	
:
y
seg/conv3/bn/cond/mul/yConst^seg/conv3/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
{
seg/conv3/bn/cond/mulMulseg/conv3/bn/cond/mul/Switch:1seg/conv3/bn/cond/mul/y*
T0*
_output_shapes	
:
Ã
seg/conv3/bn/cond/mul/SwitchSwitchseg/conv3/bn/pop_mean/readseg/conv3/bn/cond/pred_id"/device:CPU:0*
T0*(
_class
loc:@seg/conv3/bn/pop_mean*"
_output_shapes
::
{
seg/conv3/bn/cond/mul_1/yConst^seg/conv3/bn/cond/switch_t*
valueB
 *ÍÌÌ=*
dtype0*
_output_shapes
: 

seg/conv3/bn/cond/mul_1Mul!seg/conv3/bn/cond/moments/Squeezeseg/conv3/bn/cond/mul_1/y*
T0*
_output_shapes	
:
r
seg/conv3/bn/cond/addAddseg/conv3/bn/cond/mulseg/conv3/bn/cond/mul_1*
T0*
_output_shapes	
:
ä
seg/conv3/bn/cond/AssignAssign!seg/conv3/bn/cond/Assign/Switch:1seg/conv3/bn/cond/add"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@seg/conv3/bn/pop_mean*
validate_shape(*
_output_shapes	
:
Ä
seg/conv3/bn/cond/Assign/Switch	RefSwitchseg/conv3/bn/pop_meanseg/conv3/bn/cond/pred_id"/device:CPU:0*(
_class
loc:@seg/conv3/bn/pop_mean*
T0*"
_output_shapes
::
{
seg/conv3/bn/cond/mul_2/yConst^seg/conv3/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 

seg/conv3/bn/cond/mul_2Mul seg/conv3/bn/cond/mul_2/Switch:1seg/conv3/bn/cond/mul_2/y*
T0*
_output_shapes	
:
Ã
seg/conv3/bn/cond/mul_2/SwitchSwitchseg/conv3/bn/pop_var/readseg/conv3/bn/cond/pred_id"/device:CPU:0*
T0*'
_class
loc:@seg/conv3/bn/pop_var*"
_output_shapes
::
{
seg/conv3/bn/cond/mul_3/yConst^seg/conv3/bn/cond/switch_t*
valueB
 *ÍÌÌ=*
dtype0*
_output_shapes
: 

seg/conv3/bn/cond/mul_3Mul#seg/conv3/bn/cond/moments/Squeeze_1seg/conv3/bn/cond/mul_3/y*
T0*
_output_shapes	
:
v
seg/conv3/bn/cond/add_1Addseg/conv3/bn/cond/mul_2seg/conv3/bn/cond/mul_3*
T0*
_output_shapes	
:
é
seg/conv3/bn/cond/Assign_1Assign#seg/conv3/bn/cond/Assign_1/Switch:1seg/conv3/bn/cond/add_1"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@seg/conv3/bn/pop_var*
validate_shape(*
_output_shapes	
:
Ä
!seg/conv3/bn/cond/Assign_1/Switch	RefSwitchseg/conv3/bn/pop_varseg/conv3/bn/cond/pred_id"/device:CPU:0*
T0*'
_class
loc:@seg/conv3/bn/pop_var*"
_output_shapes
::

!seg/conv3/bn/cond/batchnorm/add/yConst^seg/conv3/bn/cond/Assign^seg/conv3/bn/cond/Assign_1*
valueB
 *o:*
dtype0*
_output_shapes
: 

seg/conv3/bn/cond/batchnorm/addAdd#seg/conv3/bn/cond/moments/Squeeze_1!seg/conv3/bn/cond/batchnorm/add/y*
T0*
_output_shapes	
:
q
!seg/conv3/bn/cond/batchnorm/RsqrtRsqrtseg/conv3/bn/cond/batchnorm/add*
T0*
_output_shapes	
:

seg/conv3/bn/cond/batchnorm/mulMul!seg/conv3/bn/cond/batchnorm/Rsqrt(seg/conv3/bn/cond/batchnorm/mul/Switch:1*
T0*
_output_shapes	
:
Ç
&seg/conv3/bn/cond/batchnorm/mul/SwitchSwitchseg/conv3/bn/gamma/readseg/conv3/bn/cond/pred_id"/device:CPU:0*
T0*%
_class
loc:@seg/conv3/bn/gamma*"
_output_shapes
::
¤
!seg/conv3/bn/cond/batchnorm/mul_1Mul'seg/conv3/bn/cond/moments/mean/Switch:1seg/conv3/bn/cond/batchnorm/mul*
T0*'
_output_shapes
:d

!seg/conv3/bn/cond/batchnorm/mul_2Mul!seg/conv3/bn/cond/moments/Squeezeseg/conv3/bn/cond/batchnorm/mul*
T0*
_output_shapes	
:

seg/conv3/bn/cond/batchnorm/subSub(seg/conv3/bn/cond/batchnorm/sub/Switch:1!seg/conv3/bn/cond/batchnorm/mul_2*
T0*
_output_shapes	
:
Å
&seg/conv3/bn/cond/batchnorm/sub/SwitchSwitchseg/conv3/bn/beta/readseg/conv3/bn/cond/pred_id"/device:CPU:0*
T0*$
_class
loc:@seg/conv3/bn/beta*"
_output_shapes
::

!seg/conv3/bn/cond/batchnorm/add_1Add!seg/conv3/bn/cond/batchnorm/mul_1seg/conv3/bn/cond/batchnorm/sub*
T0*'
_output_shapes
:d

#seg/conv3/bn/cond/batchnorm_1/add/yConst^seg/conv3/bn/cond/switch_f*
valueB
 *o:*
dtype0*
_output_shapes
: 

!seg/conv3/bn/cond/batchnorm_1/addAdd(seg/conv3/bn/cond/batchnorm_1/add/Switch#seg/conv3/bn/cond/batchnorm_1/add/y*
T0*
_output_shapes	
:
Í
(seg/conv3/bn/cond/batchnorm_1/add/SwitchSwitchseg/conv3/bn/pop_var/readseg/conv3/bn/cond/pred_id"/device:CPU:0*
T0*'
_class
loc:@seg/conv3/bn/pop_var*"
_output_shapes
::
u
#seg/conv3/bn/cond/batchnorm_1/RsqrtRsqrt!seg/conv3/bn/cond/batchnorm_1/add*
T0*
_output_shapes	
:

!seg/conv3/bn/cond/batchnorm_1/mulMul#seg/conv3/bn/cond/batchnorm_1/Rsqrt(seg/conv3/bn/cond/batchnorm_1/mul/Switch*
T0*
_output_shapes	
:
É
(seg/conv3/bn/cond/batchnorm_1/mul/SwitchSwitchseg/conv3/bn/gamma/readseg/conv3/bn/cond/pred_id"/device:CPU:0*%
_class
loc:@seg/conv3/bn/gamma*
T0*"
_output_shapes
::
«
#seg/conv3/bn/cond/batchnorm_1/mul_1Mul*seg/conv3/bn/cond/batchnorm_1/mul_1/Switch!seg/conv3/bn/cond/batchnorm_1/mul*
T0*'
_output_shapes
:d
Í
*seg/conv3/bn/cond/batchnorm_1/mul_1/SwitchSwitchseg/conv3/BiasAddseg/conv3/bn/cond/pred_id*
T0*$
_class
loc:@seg/conv3/BiasAdd*:
_output_shapes(
&:d:d

#seg/conv3/bn/cond/batchnorm_1/mul_2Mul*seg/conv3/bn/cond/batchnorm_1/mul_2/Switch!seg/conv3/bn/cond/batchnorm_1/mul*
T0*
_output_shapes	
:
Ñ
*seg/conv3/bn/cond/batchnorm_1/mul_2/SwitchSwitchseg/conv3/bn/pop_mean/readseg/conv3/bn/cond/pred_id"/device:CPU:0*
T0*(
_class
loc:@seg/conv3/bn/pop_mean*"
_output_shapes
::

!seg/conv3/bn/cond/batchnorm_1/subSub(seg/conv3/bn/cond/batchnorm_1/sub/Switch#seg/conv3/bn/cond/batchnorm_1/mul_2*
T0*
_output_shapes	
:
Ç
(seg/conv3/bn/cond/batchnorm_1/sub/SwitchSwitchseg/conv3/bn/beta/readseg/conv3/bn/cond/pred_id"/device:CPU:0*
T0*$
_class
loc:@seg/conv3/bn/beta*"
_output_shapes
::
¤
#seg/conv3/bn/cond/batchnorm_1/add_1Add#seg/conv3/bn/cond/batchnorm_1/mul_1!seg/conv3/bn/cond/batchnorm_1/sub*
T0*'
_output_shapes
:d
¥
seg/conv3/bn/cond/MergeMerge#seg/conv3/bn/cond/batchnorm_1/add_1!seg/conv3/bn/cond/batchnorm/add_1*
T0*
N*)
_output_shapes
:d: 
a
seg/conv3/ReluReluseg/conv3/bn/cond/Merge*
T0*'
_output_shapes
:d
`
seg/dp1_1/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
]
seg/dp1_1/cond/switch_tIdentityseg/dp1_1/cond/Switch:1*
T0
*
_output_shapes
: 
[
seg/dp1_1/cond/switch_fIdentityseg/dp1_1/cond/Switch*
T0
*
_output_shapes
: 
R
seg/dp1_1/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
z
seg/dp1_1/cond/dropout/rateConst^seg/dp1_1/cond/switch_t*
valueB
 *ÍÌÌ>*
dtype0*
_output_shapes
: 

seg/dp1_1/cond/dropout/ShapeConst^seg/dp1_1/cond/switch_t*%
valueB"   d         *
dtype0*
_output_shapes
:

)seg/dp1_1/cond/dropout/random_uniform/minConst^seg/dp1_1/cond/switch_t*
valueB
 *    *
dtype0*
_output_shapes
: 

)seg/dp1_1/cond/dropout/random_uniform/maxConst^seg/dp1_1/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
º
3seg/dp1_1/cond/dropout/random_uniform/RandomUniformRandomUniformseg/dp1_1/cond/dropout/Shape*
T0*
dtype0*
seed2 *

seed *'
_output_shapes
:d
§
)seg/dp1_1/cond/dropout/random_uniform/subSub)seg/dp1_1/cond/dropout/random_uniform/max)seg/dp1_1/cond/dropout/random_uniform/min*
T0*
_output_shapes
: 
Â
)seg/dp1_1/cond/dropout/random_uniform/mulMul3seg/dp1_1/cond/dropout/random_uniform/RandomUniform)seg/dp1_1/cond/dropout/random_uniform/sub*
T0*'
_output_shapes
:d
´
%seg/dp1_1/cond/dropout/random_uniformAdd)seg/dp1_1/cond/dropout/random_uniform/mul)seg/dp1_1/cond/dropout/random_uniform/min*
T0*'
_output_shapes
:d
{
seg/dp1_1/cond/dropout/sub/xConst^seg/dp1_1/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 
}
seg/dp1_1/cond/dropout/subSubseg/dp1_1/cond/dropout/sub/xseg/dp1_1/cond/dropout/rate*
T0*
_output_shapes
: 

 seg/dp1_1/cond/dropout/truediv/xConst^seg/dp1_1/cond/switch_t*
valueB
 *  ?*
dtype0*
_output_shapes
: 

seg/dp1_1/cond/dropout/truedivRealDiv seg/dp1_1/cond/dropout/truediv/xseg/dp1_1/cond/dropout/sub*
T0*
_output_shapes
: 
©
#seg/dp1_1/cond/dropout/GreaterEqualGreaterEqual%seg/dp1_1/cond/dropout/random_uniformseg/dp1_1/cond/dropout/rate*
T0*'
_output_shapes
:d

seg/dp1_1/cond/dropout/mulMul#seg/dp1_1/cond/dropout/mul/Switch:1seg/dp1_1/cond/dropout/truediv*
T0*'
_output_shapes
:d
»
!seg/dp1_1/cond/dropout/mul/SwitchSwitchseg/conv3/Reluseg/dp1_1/cond/pred_id*
T0*!
_class
loc:@seg/conv3/Relu*:
_output_shapes(
&:d:d

seg/dp1_1/cond/dropout/CastCast#seg/dp1_1/cond/dropout/GreaterEqual*
Truncate( *

DstT0*

SrcT0
*'
_output_shapes
:d

seg/dp1_1/cond/dropout/mul_1Mulseg/dp1_1/cond/dropout/mulseg/dp1_1/cond/dropout/Cast*
T0*'
_output_shapes
:d
±
seg/dp1_1/cond/Switch_1Switchseg/conv3/Reluseg/dp1_1/cond/pred_id*
T0*!
_class
loc:@seg/conv3/Relu*:
_output_shapes(
&:d:d

seg/dp1_1/cond/MergeMergeseg/dp1_1/cond/Switch_1seg/dp1_1/cond/dropout/mul_1*
T0*
N*)
_output_shapes
:d: 
±
2seg/conv4/weights/Initializer/random_uniform/shapeConst*
dtype0*$
_class
loc:@seg/conv4/weights*%
valueB"            *
_output_shapes
:

0seg/conv4/weights/Initializer/random_uniform/minConst*
dtype0*$
_class
loc:@seg/conv4/weights*
valueB
 *×³Ý½*
_output_shapes
: 

0seg/conv4/weights/Initializer/random_uniform/maxConst*
dtype0*$
_class
loc:@seg/conv4/weights*
valueB
 *×³Ý=*
_output_shapes
: 
þ
:seg/conv4/weights/Initializer/random_uniform/RandomUniformRandomUniform2seg/conv4/weights/Initializer/random_uniform/shape*

seed *
T0*$
_class
loc:@seg/conv4/weights*
dtype0*
seed2 *(
_output_shapes
:
â
0seg/conv4/weights/Initializer/random_uniform/subSub0seg/conv4/weights/Initializer/random_uniform/max0seg/conv4/weights/Initializer/random_uniform/min*
T0*$
_class
loc:@seg/conv4/weights*
_output_shapes
: 
þ
0seg/conv4/weights/Initializer/random_uniform/mulMul:seg/conv4/weights/Initializer/random_uniform/RandomUniform0seg/conv4/weights/Initializer/random_uniform/sub*
T0*$
_class
loc:@seg/conv4/weights*(
_output_shapes
:
ð
,seg/conv4/weights/Initializer/random_uniformAdd0seg/conv4/weights/Initializer/random_uniform/mul0seg/conv4/weights/Initializer/random_uniform/min*
T0*$
_class
loc:@seg/conv4/weights*(
_output_shapes
:
Î
seg/conv4/weights
VariableV2"/device:CPU:0*
shared_name *$
_class
loc:@seg/conv4/weights*
dtype0*
	container *
shape:*(
_output_shapes
:
ô
seg/conv4/weights/AssignAssignseg/conv4/weights,seg/conv4/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv4/weights*
validate_shape(*(
_output_shapes
:

seg/conv4/weights/readIdentityseg/conv4/weights"/device:CPU:0*
T0*$
_class
loc:@seg/conv4/weights*(
_output_shapes
:

seg/conv4/Conv2DConv2Dseg/dp1_1/cond/Mergeseg/conv4/weights/read*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*
	dilations
*
T0*
data_formatNHWC*
strides
*'
_output_shapes
:d

"seg/conv4/biases/Initializer/ConstConst*#
_class
loc:@seg/conv4/biases*
valueB*    *
dtype0*
_output_shapes	
:
²
seg/conv4/biases
VariableV2"/device:CPU:0*
shape:*
shared_name *#
_class
loc:@seg/conv4/biases*
dtype0*
	container *
_output_shapes	
:
Ú
seg/conv4/biases/AssignAssignseg/conv4/biases"seg/conv4/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv4/biases*
validate_shape(*
_output_shapes	
:

seg/conv4/biases/readIdentityseg/conv4/biases"/device:CPU:0*
T0*#
_class
loc:@seg/conv4/biases*
_output_shapes	
:

seg/conv4/BiasAddBiasAddseg/conv4/Conv2Dseg/conv4/biases/read*
data_formatNHWC*
T0*'
_output_shapes
:d

#seg/conv4/bn/beta/Initializer/zerosConst*
dtype0*$
_class
loc:@seg/conv4/bn/beta*
valueB*    *
_output_shapes	
:
´
seg/conv4/bn/beta
VariableV2"/device:CPU:0*
	container *
shape:*
shared_name *$
_class
loc:@seg/conv4/bn/beta*
dtype0*
_output_shapes	
:
Þ
seg/conv4/bn/beta/AssignAssignseg/conv4/bn/beta#seg/conv4/bn/beta/Initializer/zeros"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv4/bn/beta*
validate_shape(*
_output_shapes	
:

seg/conv4/bn/beta/readIdentityseg/conv4/bn/beta"/device:CPU:0*
T0*$
_class
loc:@seg/conv4/bn/beta*
_output_shapes	
:

#seg/conv4/bn/gamma/Initializer/onesConst*
dtype0*%
_class
loc:@seg/conv4/bn/gamma*
valueB*  ?*
_output_shapes	
:
¶
seg/conv4/bn/gamma
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:*
shared_name *%
_class
loc:@seg/conv4/bn/gamma*
_output_shapes	
:
á
seg/conv4/bn/gamma/AssignAssignseg/conv4/bn/gamma#seg/conv4/bn/gamma/Initializer/ones"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@seg/conv4/bn/gamma*
validate_shape(*
_output_shapes	
:

seg/conv4/bn/gamma/readIdentityseg/conv4/bn/gamma"/device:CPU:0*
T0*%
_class
loc:@seg/conv4/bn/gamma*
_output_shapes	
:
 
'seg/conv4/bn/pop_mean/Initializer/zerosConst*(
_class
loc:@seg/conv4/bn/pop_mean*
valueB*    *
dtype0*
_output_shapes	
:
¼
seg/conv4/bn/pop_mean
VariableV2"/device:CPU:0*(
_class
loc:@seg/conv4/bn/pop_mean*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
î
seg/conv4/bn/pop_mean/AssignAssignseg/conv4/bn/pop_mean'seg/conv4/bn/pop_mean/Initializer/zeros"/device:CPU:0*
T0*(
_class
loc:@seg/conv4/bn/pop_mean*
validate_shape(*
use_locking(*
_output_shapes	
:

seg/conv4/bn/pop_mean/readIdentityseg/conv4/bn/pop_mean"/device:CPU:0*
T0*(
_class
loc:@seg/conv4/bn/pop_mean*
_output_shapes	
:

%seg/conv4/bn/pop_var/Initializer/onesConst*'
_class
loc:@seg/conv4/bn/pop_var*
valueB*  ?*
dtype0*
_output_shapes	
:
º
seg/conv4/bn/pop_var
VariableV2"/device:CPU:0*'
_class
loc:@seg/conv4/bn/pop_var*
dtype0*
	container *
shape:*
shared_name *
_output_shapes	
:
é
seg/conv4/bn/pop_var/AssignAssignseg/conv4/bn/pop_var%seg/conv4/bn/pop_var/Initializer/ones"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@seg/conv4/bn/pop_var*
validate_shape(*
_output_shapes	
:

seg/conv4/bn/pop_var/readIdentityseg/conv4/bn/pop_var"/device:CPU:0*
T0*'
_class
loc:@seg/conv4/bn/pop_var*
_output_shapes	
:
c
seg/conv4/bn/cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
c
seg/conv4/bn/cond/switch_tIdentityseg/conv4/bn/cond/Switch:1*
T0
*
_output_shapes
: 
a
seg/conv4/bn/cond/switch_fIdentityseg/conv4/bn/cond/Switch*
T0
*
_output_shapes
: 
U
seg/conv4/bn/cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
¢
0seg/conv4/bn/cond/moments/mean/reduction_indicesConst^seg/conv4/bn/cond/switch_t*
dtype0*!
valueB"          *
_output_shapes
:
Ð
seg/conv4/bn/cond/moments/meanMean'seg/conv4/bn/cond/moments/mean/Switch:10seg/conv4/bn/cond/moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:
È
%seg/conv4/bn/cond/moments/mean/SwitchSwitchseg/conv4/BiasAddseg/conv4/bn/cond/pred_id*
T0*$
_class
loc:@seg/conv4/BiasAdd*:
_output_shapes(
&:d:d

&seg/conv4/bn/cond/moments/StopGradientStopGradientseg/conv4/bn/cond/moments/mean*
T0*'
_output_shapes
:
Ã
+seg/conv4/bn/cond/moments/SquaredDifferenceSquaredDifference'seg/conv4/bn/cond/moments/mean/Switch:1&seg/conv4/bn/cond/moments/StopGradient*
T0*'
_output_shapes
:d
¦
4seg/conv4/bn/cond/moments/variance/reduction_indicesConst^seg/conv4/bn/cond/switch_t*!
valueB"          *
dtype0*
_output_shapes
:
Ü
"seg/conv4/bn/cond/moments/varianceMean+seg/conv4/bn/cond/moments/SquaredDifference4seg/conv4/bn/cond/moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0*'
_output_shapes
:

!seg/conv4/bn/cond/moments/SqueezeSqueezeseg/conv4/bn/cond/moments/mean*
squeeze_dims
 *
T0*
_output_shapes	
:

#seg/conv4/bn/cond/moments/Squeeze_1Squeeze"seg/conv4/bn/cond/moments/variance*
squeeze_dims
 *
T0*
_output_shapes	
:
y
seg/conv4/bn/cond/mul/yConst^seg/conv4/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 
{
seg/conv4/bn/cond/mulMulseg/conv4/bn/cond/mul/Switch:1seg/conv4/bn/cond/mul/y*
T0*
_output_shapes	
:
Ã
seg/conv4/bn/cond/mul/SwitchSwitchseg/conv4/bn/pop_mean/readseg/conv4/bn/cond/pred_id"/device:CPU:0*
T0*(
_class
loc:@seg/conv4/bn/pop_mean*"
_output_shapes
::
{
seg/conv4/bn/cond/mul_1/yConst^seg/conv4/bn/cond/switch_t*
dtype0*
valueB
 *ÍÌÌ=*
_output_shapes
: 

seg/conv4/bn/cond/mul_1Mul!seg/conv4/bn/cond/moments/Squeezeseg/conv4/bn/cond/mul_1/y*
T0*
_output_shapes	
:
r
seg/conv4/bn/cond/addAddseg/conv4/bn/cond/mulseg/conv4/bn/cond/mul_1*
T0*
_output_shapes	
:
ä
seg/conv4/bn/cond/AssignAssign!seg/conv4/bn/cond/Assign/Switch:1seg/conv4/bn/cond/add"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@seg/conv4/bn/pop_mean*
validate_shape(*
_output_shapes	
:
Ä
seg/conv4/bn/cond/Assign/Switch	RefSwitchseg/conv4/bn/pop_meanseg/conv4/bn/cond/pred_id"/device:CPU:0*
T0*(
_class
loc:@seg/conv4/bn/pop_mean*"
_output_shapes
::
{
seg/conv4/bn/cond/mul_2/yConst^seg/conv4/bn/cond/switch_t*
valueB
 *fff?*
dtype0*
_output_shapes
: 

seg/conv4/bn/cond/mul_2Mul seg/conv4/bn/cond/mul_2/Switch:1seg/conv4/bn/cond/mul_2/y*
T0*
_output_shapes	
:
Ã
seg/conv4/bn/cond/mul_2/SwitchSwitchseg/conv4/bn/pop_var/readseg/conv4/bn/cond/pred_id"/device:CPU:0*'
_class
loc:@seg/conv4/bn/pop_var*
T0*"
_output_shapes
::
{
seg/conv4/bn/cond/mul_3/yConst^seg/conv4/bn/cond/switch_t*
dtype0*
valueB
 *ÍÌÌ=*
_output_shapes
: 

seg/conv4/bn/cond/mul_3Mul#seg/conv4/bn/cond/moments/Squeeze_1seg/conv4/bn/cond/mul_3/y*
T0*
_output_shapes	
:
v
seg/conv4/bn/cond/add_1Addseg/conv4/bn/cond/mul_2seg/conv4/bn/cond/mul_3*
T0*
_output_shapes	
:
é
seg/conv4/bn/cond/Assign_1Assign#seg/conv4/bn/cond/Assign_1/Switch:1seg/conv4/bn/cond/add_1"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@seg/conv4/bn/pop_var*
validate_shape(*
_output_shapes	
:
Ä
!seg/conv4/bn/cond/Assign_1/Switch	RefSwitchseg/conv4/bn/pop_varseg/conv4/bn/cond/pred_id"/device:CPU:0*
T0*'
_class
loc:@seg/conv4/bn/pop_var*"
_output_shapes
::

!seg/conv4/bn/cond/batchnorm/add/yConst^seg/conv4/bn/cond/Assign^seg/conv4/bn/cond/Assign_1*
valueB
 *o:*
dtype0*
_output_shapes
: 

seg/conv4/bn/cond/batchnorm/addAdd#seg/conv4/bn/cond/moments/Squeeze_1!seg/conv4/bn/cond/batchnorm/add/y*
T0*
_output_shapes	
:
q
!seg/conv4/bn/cond/batchnorm/RsqrtRsqrtseg/conv4/bn/cond/batchnorm/add*
T0*
_output_shapes	
:

seg/conv4/bn/cond/batchnorm/mulMul!seg/conv4/bn/cond/batchnorm/Rsqrt(seg/conv4/bn/cond/batchnorm/mul/Switch:1*
T0*
_output_shapes	
:
Ç
&seg/conv4/bn/cond/batchnorm/mul/SwitchSwitchseg/conv4/bn/gamma/readseg/conv4/bn/cond/pred_id"/device:CPU:0*%
_class
loc:@seg/conv4/bn/gamma*
T0*"
_output_shapes
::
¤
!seg/conv4/bn/cond/batchnorm/mul_1Mul'seg/conv4/bn/cond/moments/mean/Switch:1seg/conv4/bn/cond/batchnorm/mul*
T0*'
_output_shapes
:d

!seg/conv4/bn/cond/batchnorm/mul_2Mul!seg/conv4/bn/cond/moments/Squeezeseg/conv4/bn/cond/batchnorm/mul*
T0*
_output_shapes	
:

seg/conv4/bn/cond/batchnorm/subSub(seg/conv4/bn/cond/batchnorm/sub/Switch:1!seg/conv4/bn/cond/batchnorm/mul_2*
T0*
_output_shapes	
:
Å
&seg/conv4/bn/cond/batchnorm/sub/SwitchSwitchseg/conv4/bn/beta/readseg/conv4/bn/cond/pred_id"/device:CPU:0*
T0*$
_class
loc:@seg/conv4/bn/beta*"
_output_shapes
::

!seg/conv4/bn/cond/batchnorm/add_1Add!seg/conv4/bn/cond/batchnorm/mul_1seg/conv4/bn/cond/batchnorm/sub*
T0*'
_output_shapes
:d

#seg/conv4/bn/cond/batchnorm_1/add/yConst^seg/conv4/bn/cond/switch_f*
valueB
 *o:*
dtype0*
_output_shapes
: 

!seg/conv4/bn/cond/batchnorm_1/addAdd(seg/conv4/bn/cond/batchnorm_1/add/Switch#seg/conv4/bn/cond/batchnorm_1/add/y*
T0*
_output_shapes	
:
Í
(seg/conv4/bn/cond/batchnorm_1/add/SwitchSwitchseg/conv4/bn/pop_var/readseg/conv4/bn/cond/pred_id"/device:CPU:0*
T0*'
_class
loc:@seg/conv4/bn/pop_var*"
_output_shapes
::
u
#seg/conv4/bn/cond/batchnorm_1/RsqrtRsqrt!seg/conv4/bn/cond/batchnorm_1/add*
T0*
_output_shapes	
:

!seg/conv4/bn/cond/batchnorm_1/mulMul#seg/conv4/bn/cond/batchnorm_1/Rsqrt(seg/conv4/bn/cond/batchnorm_1/mul/Switch*
T0*
_output_shapes	
:
É
(seg/conv4/bn/cond/batchnorm_1/mul/SwitchSwitchseg/conv4/bn/gamma/readseg/conv4/bn/cond/pred_id"/device:CPU:0*
T0*%
_class
loc:@seg/conv4/bn/gamma*"
_output_shapes
::
«
#seg/conv4/bn/cond/batchnorm_1/mul_1Mul*seg/conv4/bn/cond/batchnorm_1/mul_1/Switch!seg/conv4/bn/cond/batchnorm_1/mul*
T0*'
_output_shapes
:d
Í
*seg/conv4/bn/cond/batchnorm_1/mul_1/SwitchSwitchseg/conv4/BiasAddseg/conv4/bn/cond/pred_id*$
_class
loc:@seg/conv4/BiasAdd*
T0*:
_output_shapes(
&:d:d

#seg/conv4/bn/cond/batchnorm_1/mul_2Mul*seg/conv4/bn/cond/batchnorm_1/mul_2/Switch!seg/conv4/bn/cond/batchnorm_1/mul*
T0*
_output_shapes	
:
Ñ
*seg/conv4/bn/cond/batchnorm_1/mul_2/SwitchSwitchseg/conv4/bn/pop_mean/readseg/conv4/bn/cond/pred_id"/device:CPU:0*
T0*(
_class
loc:@seg/conv4/bn/pop_mean*"
_output_shapes
::

!seg/conv4/bn/cond/batchnorm_1/subSub(seg/conv4/bn/cond/batchnorm_1/sub/Switch#seg/conv4/bn/cond/batchnorm_1/mul_2*
T0*
_output_shapes	
:
Ç
(seg/conv4/bn/cond/batchnorm_1/sub/SwitchSwitchseg/conv4/bn/beta/readseg/conv4/bn/cond/pred_id"/device:CPU:0*
T0*$
_class
loc:@seg/conv4/bn/beta*"
_output_shapes
::
¤
#seg/conv4/bn/cond/batchnorm_1/add_1Add#seg/conv4/bn/cond/batchnorm_1/mul_1!seg/conv4/bn/cond/batchnorm_1/sub*
T0*'
_output_shapes
:d
¥
seg/conv4/bn/cond/MergeMerge#seg/conv4/bn/cond/batchnorm_1/add_1!seg/conv4/bn/cond/batchnorm/add_1*
T0*
N*)
_output_shapes
:d: 
a
seg/conv4/ReluReluseg/conv4/bn/cond/Merge*
T0*'
_output_shapes
:d
±
2seg/conv5/weights/Initializer/random_uniform/shapeConst*$
_class
loc:@seg/conv5/weights*%
valueB"            *
dtype0*
_output_shapes
:

0seg/conv5/weights/Initializer/random_uniform/minConst*$
_class
loc:@seg/conv5/weights*
valueB
 *(¾*
dtype0*
_output_shapes
: 

0seg/conv5/weights/Initializer/random_uniform/maxConst*$
_class
loc:@seg/conv5/weights*
valueB
 *(>*
dtype0*
_output_shapes
: 
ý
:seg/conv5/weights/Initializer/random_uniform/RandomUniformRandomUniform2seg/conv5/weights/Initializer/random_uniform/shape*

seed *
T0*$
_class
loc:@seg/conv5/weights*
dtype0*
seed2 *'
_output_shapes
:
â
0seg/conv5/weights/Initializer/random_uniform/subSub0seg/conv5/weights/Initializer/random_uniform/max0seg/conv5/weights/Initializer/random_uniform/min*
T0*$
_class
loc:@seg/conv5/weights*
_output_shapes
: 
ý
0seg/conv5/weights/Initializer/random_uniform/mulMul:seg/conv5/weights/Initializer/random_uniform/RandomUniform0seg/conv5/weights/Initializer/random_uniform/sub*
T0*$
_class
loc:@seg/conv5/weights*'
_output_shapes
:
ï
,seg/conv5/weights/Initializer/random_uniformAdd0seg/conv5/weights/Initializer/random_uniform/mul0seg/conv5/weights/Initializer/random_uniform/min*
T0*$
_class
loc:@seg/conv5/weights*'
_output_shapes
:
Ì
seg/conv5/weights
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:*
shared_name *$
_class
loc:@seg/conv5/weights*'
_output_shapes
:
ó
seg/conv5/weights/AssignAssignseg/conv5/weights,seg/conv5/weights/Initializer/random_uniform"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv5/weights*
validate_shape(*'
_output_shapes
:

seg/conv5/weights/readIdentityseg/conv5/weights"/device:CPU:0*
T0*$
_class
loc:@seg/conv5/weights*'
_output_shapes
:
û
seg/conv5/Conv2DConv2Dseg/conv4/Reluseg/conv5/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
explicit_paddings
 *
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
:d

"seg/conv5/biases/Initializer/ConstConst*#
_class
loc:@seg/conv5/biases*
valueB*    *
dtype0*
_output_shapes
:
°
seg/conv5/biases
VariableV2"/device:CPU:0*
shared_name *#
_class
loc:@seg/conv5/biases*
dtype0*
	container *
shape:*
_output_shapes
:
Ù
seg/conv5/biases/AssignAssignseg/conv5/biases"seg/conv5/biases/Initializer/Const"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv5/biases*
validate_shape(*
_output_shapes
:

seg/conv5/biases/readIdentityseg/conv5/biases"/device:CPU:0*
T0*#
_class
loc:@seg/conv5/biases*
_output_shapes
:

seg/conv5/BiasAddBiasAddseg/conv5/Conv2Dseg/conv5/biases/read*
T0*
data_formatNHWC*&
_output_shapes
:d
V
cond/SwitchSwitchPlaceholder_3Placeholder_3*
T0
*
_output_shapes
: : 
I
cond/switch_tIdentitycond/Switch:1*
T0
*
_output_shapes
: 
G
cond/switch_fIdentitycond/Switch*
T0
*
_output_shapes
: 
H
cond/pred_idIdentityPlaceholder_3*
T0
*
_output_shapes
: 
¡
cond/Switch_1Switchseg/conv5/BiasAddcond/pred_id*
T0*$
_class
loc:@seg/conv5/BiasAdd*8
_output_shapes&
$:d:d
]
cond/SoftmaxSoftmaxcond/Softmax/Switch*
T0*&
_output_shapes
:d
§
cond/Softmax/SwitchSwitchseg/conv5/BiasAddcond/pred_id*
T0*$
_class
loc:@seg/conv5/BiasAdd*8
_output_shapes&
$:d:d
n

cond/MergeMergecond/Softmaxcond/Switch_1:1*
T0*
N*(
_output_shapes
:d: 
d
Reshape_5/shapeConst*!
valueB"   d      *
dtype0*
_output_shapes
:
l
	Reshape_5Reshape
cond/MergeReshape_5/shape*
T0*
Tshape0*"
_output_shapes
:d
z
)SparseSoftmaxCrossEntropyWithLogits/ShapeConst*
valueB"   d   *
dtype0*
_output_shapes
:

+SparseSoftmaxCrossEntropyWithLogits/Shape_1Const*
dtype0*!
valueB"   d      *
_output_shapes
:
j
(SparseSoftmaxCrossEntropyWithLogits/RankConst*
value	B :*
dtype0*
_output_shapes
: 
k
)SparseSoftmaxCrossEntropyWithLogits/sub/yConst*
value	B :*
dtype0*
_output_shapes
: 
¤
'SparseSoftmaxCrossEntropyWithLogits/subSub(SparseSoftmaxCrossEntropyWithLogits/Rank)SparseSoftmaxCrossEntropyWithLogits/sub/y*
T0*
_output_shapes
: 
k
)SparseSoftmaxCrossEntropyWithLogits/add/yConst*
value	B :*
dtype0*
_output_shapes
: 
£
'SparseSoftmaxCrossEntropyWithLogits/addAdd'SparseSoftmaxCrossEntropyWithLogits/sub)SparseSoftmaxCrossEntropyWithLogits/add/y*
T0*
_output_shapes
: 
¢
7SparseSoftmaxCrossEntropyWithLogits/strided_slice/stackPack'SparseSoftmaxCrossEntropyWithLogits/sub*
T0*

axis *
N*
_output_shapes
:
¤
9SparseSoftmaxCrossEntropyWithLogits/strided_slice/stack_1Pack'SparseSoftmaxCrossEntropyWithLogits/add*
T0*

axis *
N*
_output_shapes
:

9SparseSoftmaxCrossEntropyWithLogits/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
¯
1SparseSoftmaxCrossEntropyWithLogits/strided_sliceStridedSlice+SparseSoftmaxCrossEntropyWithLogits/Shape_17SparseSoftmaxCrossEntropyWithLogits/strided_slice/stack9SparseSoftmaxCrossEntropyWithLogits/strided_slice/stack_19SparseSoftmaxCrossEntropyWithLogits/strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: 
~
3SparseSoftmaxCrossEntropyWithLogits/Reshape/shape/0Const*
valueB :
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
: 
Û
1SparseSoftmaxCrossEntropyWithLogits/Reshape/shapePack3SparseSoftmaxCrossEntropyWithLogits/Reshape/shape/01SparseSoftmaxCrossEntropyWithLogits/strided_slice*

axis *
N*
T0*
_output_shapes
:
«
+SparseSoftmaxCrossEntropyWithLogits/ReshapeReshape	Reshape_51SparseSoftmaxCrossEntropyWithLogits/Reshape/shape*
T0*
Tshape0*
_output_shapes

:d

3SparseSoftmaxCrossEntropyWithLogits/Reshape_1/shapeConst*
valueB:
ÿÿÿÿÿÿÿÿÿ*
dtype0*
_output_shapes
:
¯
-SparseSoftmaxCrossEntropyWithLogits/Reshape_1ReshapePlaceholder_13SparseSoftmaxCrossEntropyWithLogits/Reshape_1/shape*
T0*
Tshape0*
_output_shapes
:d

GSparseSoftmaxCrossEntropyWithLogits/SparseSoftmaxCrossEntropyWithLogits#SparseSoftmaxCrossEntropyWithLogits+SparseSoftmaxCrossEntropyWithLogits/Reshape-SparseSoftmaxCrossEntropyWithLogits/Reshape_1*
Tlabels0*
T0*$
_output_shapes
:d:d
ã
-SparseSoftmaxCrossEntropyWithLogits/Reshape_2ReshapeGSparseSoftmaxCrossEntropyWithLogits/SparseSoftmaxCrossEntropyWithLogits)SparseSoftmaxCrossEntropyWithLogits/Shape*
T0*
Tshape0*
_output_shapes

:d
X
Mean/reduction_indicesConst*
value	B :*
dtype0*
_output_shapes
: 

MeanMean-SparseSoftmaxCrossEntropyWithLogits/Reshape_2Mean/reduction_indices*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Q
Const_1Const*
dtype0*
valueB: *
_output_shapes
:
[
Mean_1MeanMeanConst_1*
T0*

Tidx0*
	keep_dims( *
_output_shapes
: 
Y
save/filename/inputConst*
valueB Bmodel*
dtype0*
_output_shapes
: 
n
save/filenamePlaceholderWithDefaultsave/filename/input*
dtype0*
shape: *
_output_shapes
: 
e

save/ConstPlaceholderWithDefaultsave/filename*
dtype0*
shape: *
_output_shapes
: 
ª#
save/SaveV2/tensor_namesConst*Ý"
valueÓ"BÐ"bBBiasAdd/biasesBBiasAdd_1/biasesB
agg/biasesB6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverageB8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverageBagg/bn/betaBagg/bn/gammaBagg/weightsBgapnet00/biasesBgapnet00/bn/betaBgapnet00/bn/gammaB@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet00/weightsBgapnet01/biasesBgapnet01/bn/betaBgapnet01/bn/gammaB@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet01/weightsBgapnet11/biasesBgapnet11/bn/betaBgapnet11/bn/gammaB@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet11/weightsBglobal_expand/biasesBglobal_expand/bn/betaBglobal_expand/bn/gammaBJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverageBLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverageBglobal_expand/weightsBlayerfilter0_edgefea_0/biasesBlayerfilter0_edgefea_0/bn/betaBlayerfilter0_edgefea_0/bn/gammaB\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverageB^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageBlayerfilter0_edgefea_0/weightsB(layerfilter0_neib_att_conv_head_0/biasesB)layerfilter0_neib_att_conv_head_0/bn/betaB*layerfilter0_neib_att_conv_head_0/bn/gammaBrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter0_neib_att_conv_head_0/weightsB'layerfilter0_newfea_conv_head_0/bn/betaB(layerfilter0_newfea_conv_head_0/bn/gammaBnlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB'layerfilter0_newfea_conv_head_0/weightsB(layerfilter0_self_att_conv_head_0/biasesB)layerfilter0_self_att_conv_head_0/bn/betaB*layerfilter0_self_att_conv_head_0/bn/gammaBrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter0_self_att_conv_head_0/weightsBlayerfilter1_edgefea_0/biasesBlayerfilter1_edgefea_0/bn/betaBlayerfilter1_edgefea_0/bn/gammaB\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverageB^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageBlayerfilter1_edgefea_0/weightsB(layerfilter1_neib_att_conv_head_0/biasesB)layerfilter1_neib_att_conv_head_0/bn/betaB*layerfilter1_neib_att_conv_head_0/bn/gammaBrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter1_neib_att_conv_head_0/weightsB'layerfilter1_newfea_conv_head_0/bn/betaB(layerfilter1_newfea_conv_head_0/bn/gammaBnlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB'layerfilter1_newfea_conv_head_0/weightsB(layerfilter1_self_att_conv_head_0/biasesB)layerfilter1_self_att_conv_head_0/bn/betaB*layerfilter1_self_att_conv_head_0/bn/gammaBrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter1_self_att_conv_head_0/weightsBseg/conv2/biasesBseg/conv2/bn/betaBseg/conv2/bn/gammaBseg/conv2/bn/pop_meanBseg/conv2/bn/pop_varBseg/conv2/weightsBseg/conv3/biasesBseg/conv3/bn/betaBseg/conv3/bn/gammaBseg/conv3/bn/pop_meanBseg/conv3/bn/pop_varBseg/conv3/weightsBseg/conv4/biasesBseg/conv4/bn/betaBseg/conv4/bn/gammaBseg/conv4/bn/pop_meanBseg/conv4/bn/pop_varBseg/conv4/weightsBseg/conv5/biasesBseg/conv5/weights*
dtype0*
_output_shapes
:b
ª
save/SaveV2/shape_and_slicesConst*
dtype0*Ù
valueÏBÌbB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
_output_shapes
:b
$
save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesBiasAdd/biasesBiasAdd_1/biases
agg/biases6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverageagg/bn/betaagg/bn/gammaagg/weightsgapnet00/biasesgapnet00/bn/betagapnet00/bn/gamma@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverageBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAveragegapnet00/weightsgapnet01/biasesgapnet01/bn/betagapnet01/bn/gamma@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverageBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAveragegapnet01/weightsgapnet11/biasesgapnet11/bn/betagapnet11/bn/gamma@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverageBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAveragegapnet11/weightsglobal_expand/biasesglobal_expand/bn/betaglobal_expand/bn/gammaJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverageLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverageglobal_expand/weightslayerfilter0_edgefea_0/biaseslayerfilter0_edgefea_0/bn/betalayerfilter0_edgefea_0/bn/gamma\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter0_edgefea_0/weights(layerfilter0_neib_att_conv_head_0/biases)layerfilter0_neib_att_conv_head_0/bn/beta*layerfilter0_neib_att_conv_head_0/bn/gammarlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragetlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage)layerfilter0_neib_att_conv_head_0/weights'layerfilter0_newfea_conv_head_0/bn/beta(layerfilter0_newfea_conv_head_0/bn/gammanlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage'layerfilter0_newfea_conv_head_0/weights(layerfilter0_self_att_conv_head_0/biases)layerfilter0_self_att_conv_head_0/bn/beta*layerfilter0_self_att_conv_head_0/bn/gammarlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragetlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage)layerfilter0_self_att_conv_head_0/weightslayerfilter1_edgefea_0/biaseslayerfilter1_edgefea_0/bn/betalayerfilter1_edgefea_0/bn/gamma\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter1_edgefea_0/weights(layerfilter1_neib_att_conv_head_0/biases)layerfilter1_neib_att_conv_head_0/bn/beta*layerfilter1_neib_att_conv_head_0/bn/gammarlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragetlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage)layerfilter1_neib_att_conv_head_0/weights'layerfilter1_newfea_conv_head_0/bn/beta(layerfilter1_newfea_conv_head_0/bn/gammanlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage'layerfilter1_newfea_conv_head_0/weights(layerfilter1_self_att_conv_head_0/biases)layerfilter1_self_att_conv_head_0/bn/beta*layerfilter1_self_att_conv_head_0/bn/gammarlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragetlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage)layerfilter1_self_att_conv_head_0/weightsseg/conv2/biasesseg/conv2/bn/betaseg/conv2/bn/gammaseg/conv2/bn/pop_meanseg/conv2/bn/pop_varseg/conv2/weightsseg/conv3/biasesseg/conv3/bn/betaseg/conv3/bn/gammaseg/conv3/bn/pop_meanseg/conv3/bn/pop_varseg/conv3/weightsseg/conv4/biasesseg/conv4/bn/betaseg/conv4/bn/gammaseg/conv4/bn/pop_meanseg/conv4/bn/pop_varseg/conv4/weightsseg/conv5/biasesseg/conv5/weights*p
dtypesf
d2b
}
save/control_dependencyIdentity
save/Const^save/SaveV2*
T0*
_class
loc:@save/Const*
_output_shapes
: 
¼#
save/RestoreV2/tensor_namesConst"/device:CPU:0*Ý"
valueÓ"BÐ"bBBiasAdd/biasesBBiasAdd_1/biasesB
agg/biasesB6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverageB8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverageBagg/bn/betaBagg/bn/gammaBagg/weightsBgapnet00/biasesBgapnet00/bn/betaBgapnet00/bn/gammaB@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet00/weightsBgapnet01/biasesBgapnet01/bn/betaBgapnet01/bn/gammaB@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet01/weightsBgapnet11/biasesBgapnet11/bn/betaBgapnet11/bn/gammaB@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet11/weightsBglobal_expand/biasesBglobal_expand/bn/betaBglobal_expand/bn/gammaBJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverageBLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverageBglobal_expand/weightsBlayerfilter0_edgefea_0/biasesBlayerfilter0_edgefea_0/bn/betaBlayerfilter0_edgefea_0/bn/gammaB\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverageB^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageBlayerfilter0_edgefea_0/weightsB(layerfilter0_neib_att_conv_head_0/biasesB)layerfilter0_neib_att_conv_head_0/bn/betaB*layerfilter0_neib_att_conv_head_0/bn/gammaBrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter0_neib_att_conv_head_0/weightsB'layerfilter0_newfea_conv_head_0/bn/betaB(layerfilter0_newfea_conv_head_0/bn/gammaBnlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB'layerfilter0_newfea_conv_head_0/weightsB(layerfilter0_self_att_conv_head_0/biasesB)layerfilter0_self_att_conv_head_0/bn/betaB*layerfilter0_self_att_conv_head_0/bn/gammaBrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter0_self_att_conv_head_0/weightsBlayerfilter1_edgefea_0/biasesBlayerfilter1_edgefea_0/bn/betaBlayerfilter1_edgefea_0/bn/gammaB\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverageB^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageBlayerfilter1_edgefea_0/weightsB(layerfilter1_neib_att_conv_head_0/biasesB)layerfilter1_neib_att_conv_head_0/bn/betaB*layerfilter1_neib_att_conv_head_0/bn/gammaBrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter1_neib_att_conv_head_0/weightsB'layerfilter1_newfea_conv_head_0/bn/betaB(layerfilter1_newfea_conv_head_0/bn/gammaBnlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB'layerfilter1_newfea_conv_head_0/weightsB(layerfilter1_self_att_conv_head_0/biasesB)layerfilter1_self_att_conv_head_0/bn/betaB*layerfilter1_self_att_conv_head_0/bn/gammaBrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter1_self_att_conv_head_0/weightsBseg/conv2/biasesBseg/conv2/bn/betaBseg/conv2/bn/gammaBseg/conv2/bn/pop_meanBseg/conv2/bn/pop_varBseg/conv2/weightsBseg/conv3/biasesBseg/conv3/bn/betaBseg/conv3/bn/gammaBseg/conv3/bn/pop_meanBseg/conv3/bn/pop_varBseg/conv3/weightsBseg/conv4/biasesBseg/conv4/bn/betaBseg/conv4/bn/gammaBseg/conv4/bn/pop_meanBseg/conv4/bn/pop_varBseg/conv4/weightsBseg/conv5/biasesBseg/conv5/weights*
dtype0*
_output_shapes
:b
¼
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*Ù
valueÏBÌbB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:b

save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*p
dtypesf
d2b*
_output_shapes
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
¦
save/AssignAssignBiasAdd/biasessave/RestoreV2*
use_locking(*
T0*!
_class
loc:@BiasAdd/biases*
validate_shape(*
_output_shapes
: 
®
save/Assign_1AssignBiasAdd_1/biasessave/RestoreV2:1*
use_locking(*
T0*#
_class
loc:@BiasAdd_1/biases*
validate_shape(*
_output_shapes
:@
²
save/Assign_2Assign
agg/biasessave/RestoreV2:2"/device:CPU:0*
use_locking(*
T0*
_class
loc:@agg/biases*
validate_shape(*
_output_shapes	
:
û
save/Assign_3Assign6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:3*
validate_shape(*
use_locking(*
T0*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:
ÿ
save/Assign_4Assign8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:4*
validate_shape(*
use_locking(*
T0*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes	
:
¥
save/Assign_5Assignagg/bn/betasave/RestoreV2:5*
use_locking(*
T0*
_class
loc:@agg/bn/beta*
validate_shape(*
_output_shapes	
:
§
save/Assign_6Assignagg/bn/gammasave/RestoreV2:6*
_class
loc:@agg/bn/gamma*
validate_shape(*
use_locking(*
T0*
_output_shapes	
:
Á
save/Assign_7Assignagg/weightssave/RestoreV2:7"/device:CPU:0*
use_locking(*
T0*
_class
loc:@agg/weights*
validate_shape(*(
_output_shapes
:ð
»
save/Assign_8Assigngapnet00/biasessave/RestoreV2:8"/device:CPU:0*
use_locking(*
T0*"
_class
loc:@gapnet00/biases*
validate_shape(*
_output_shapes
:@
®
save/Assign_9Assigngapnet00/bn/betasave/RestoreV2:9*
T0*#
_class
loc:@gapnet00/bn/beta*
validate_shape(*
use_locking(*
_output_shapes
:@
²
save/Assign_10Assigngapnet00/bn/gammasave/RestoreV2:10*
use_locking(*
T0*$
_class
loc:@gapnet00/bn/gamma*
validate_shape(*
_output_shapes
:@

save/Assign_11Assign@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:11*
T0*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:@

save/Assign_12AssignBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:12*
use_locking(*
T0*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@
Ë
save/Assign_13Assigngapnet00/weightssave/RestoreV2:13"/device:CPU:0*
T0*#
_class
loc:@gapnet00/weights*
validate_shape(*
use_locking(*&
_output_shapes
:*@
¾
save/Assign_14Assigngapnet01/biasessave/RestoreV2:14"/device:CPU:0*
validate_shape(*
use_locking(*
T0*"
_class
loc:@gapnet01/biases*
_output_shapes	
:
±
save/Assign_15Assigngapnet01/bn/betasave/RestoreV2:15*
validate_shape(*
use_locking(*
T0*#
_class
loc:@gapnet01/bn/beta*
_output_shapes	
:
³
save/Assign_16Assigngapnet01/bn/gammasave/RestoreV2:16*
use_locking(*
T0*$
_class
loc:@gapnet01/bn/gamma*
validate_shape(*
_output_shapes	
:

save/Assign_17Assign@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:17*
use_locking(*
T0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:

save/Assign_18AssignBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:18*
use_locking(*
T0*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:
Ì
save/Assign_19Assigngapnet01/weightssave/RestoreV2:19"/device:CPU:0*
validate_shape(*
use_locking(*
T0*#
_class
loc:@gapnet01/weights*'
_output_shapes
:@
¾
save/Assign_20Assigngapnet11/biasessave/RestoreV2:20"/device:CPU:0*
use_locking(*
T0*"
_class
loc:@gapnet11/biases*
validate_shape(*
_output_shapes	
:
±
save/Assign_21Assigngapnet11/bn/betasave/RestoreV2:21*
use_locking(*
T0*#
_class
loc:@gapnet11/bn/beta*
validate_shape(*
_output_shapes	
:
³
save/Assign_22Assigngapnet11/bn/gammasave/RestoreV2:22*
use_locking(*
T0*$
_class
loc:@gapnet11/bn/gamma*
validate_shape(*
_output_shapes	
:

save/Assign_23Assign@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:23*
validate_shape(*
use_locking(*
T0*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes	
:

save/Assign_24AssignBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:24*
use_locking(*
T0*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:
Ì
save/Assign_25Assigngapnet11/weightssave/RestoreV2:25"/device:CPU:0*
T0*#
_class
loc:@gapnet11/weights*
validate_shape(*
use_locking(*'
_output_shapes
:J
Ç
save/Assign_26Assignglobal_expand/biasessave/RestoreV2:26"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@global_expand/biases*
validate_shape(*
_output_shapes
:
º
save/Assign_27Assignglobal_expand/bn/betasave/RestoreV2:27*
use_locking(*
T0*(
_class
loc:@global_expand/bn/beta*
validate_shape(*
_output_shapes
:
¼
save/Assign_28Assignglobal_expand/bn/gammasave/RestoreV2:28*
use_locking(*
T0*)
_class
loc:@global_expand/bn/gamma*
validate_shape(*
_output_shapes
:
¤
save/Assign_29AssignJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:29*
validate_shape(*
use_locking(*
T0*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
¨
save/Assign_30AssignLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:30*
use_locking(*
T0*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
Õ
save/Assign_31Assignglobal_expand/weightssave/RestoreV2:31"/device:CPU:0*
validate_shape(*
use_locking(*
T0*(
_class
loc:@global_expand/weights*&
_output_shapes
:
Ù
save/Assign_32Assignlayerfilter0_edgefea_0/biasessave/RestoreV2:32"/device:CPU:0*
use_locking(*
T0*0
_class&
$"loc:@layerfilter0_edgefea_0/biases*
validate_shape(*
_output_shapes
: 
Ì
save/Assign_33Assignlayerfilter0_edgefea_0/bn/betasave/RestoreV2:33*
validate_shape(*
use_locking(*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/bn/beta*
_output_shapes
: 
Î
save/Assign_34Assignlayerfilter0_edgefea_0/bn/gammasave/RestoreV2:34*
use_locking(*
T0*2
_class(
&$loc:@layerfilter0_edgefea_0/bn/gamma*
validate_shape(*
_output_shapes
: 
È
save/Assign_35Assign\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:35*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
: 
Ì
save/Assign_36Assign^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:36*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
: 
ç
save/Assign_37Assignlayerfilter0_edgefea_0/weightssave/RestoreV2:37"/device:CPU:0*
use_locking(*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*
validate_shape(*&
_output_shapes
:
 
ï
save/Assign_38Assign(layerfilter0_neib_att_conv_head_0/biasessave/RestoreV2:38"/device:CPU:0*
use_locking(*
T0*;
_class1
/-loc:@layerfilter0_neib_att_conv_head_0/biases*
validate_shape(*
_output_shapes
:
â
save/Assign_39Assign)layerfilter0_neib_att_conv_head_0/bn/betasave/RestoreV2:39*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
ä
save/Assign_40Assign*layerfilter0_neib_att_conv_head_0/bn/gammasave/RestoreV2:40*=
_class3
1/loc:@layerfilter0_neib_att_conv_head_0/bn/gamma*
validate_shape(*
use_locking(*
T0*
_output_shapes
:
õ
save/Assign_41Assignrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:41*
validate_shape(*
use_locking(*
T0*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
_output_shapes
:
ù
save/Assign_42Assigntlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:42*
T0*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:
ý
save/Assign_43Assign)layerfilter0_neib_att_conv_head_0/weightssave/RestoreV2:43"/device:CPU:0*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*
validate_shape(*&
_output_shapes
: 
Þ
save/Assign_44Assign'layerfilter0_newfea_conv_head_0/bn/betasave/RestoreV2:44*
use_locking(*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
: 
à
save/Assign_45Assign(layerfilter0_newfea_conv_head_0/bn/gammasave/RestoreV2:45*
use_locking(*
T0*;
_class1
/-loc:@layerfilter0_newfea_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
: 
í
save/Assign_46Assignnlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:46*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
: 
ñ
save/Assign_47Assignplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:47*
use_locking(*
T0*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
: 
ù
save/Assign_48Assign'layerfilter0_newfea_conv_head_0/weightssave/RestoreV2:48"/device:CPU:0*
use_locking(*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*
validate_shape(*&
_output_shapes
:
 
ï
save/Assign_49Assign(layerfilter0_self_att_conv_head_0/biasessave/RestoreV2:49"/device:CPU:0*
T0*;
_class1
/-loc:@layerfilter0_self_att_conv_head_0/biases*
validate_shape(*
use_locking(*
_output_shapes
:
â
save/Assign_50Assign)layerfilter0_self_att_conv_head_0/bn/betasave/RestoreV2:50*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
ä
save/Assign_51Assign*layerfilter0_self_att_conv_head_0/bn/gammasave/RestoreV2:51*=
_class3
1/loc:@layerfilter0_self_att_conv_head_0/bn/gamma*
validate_shape(*
use_locking(*
T0*
_output_shapes
:
õ
save/Assign_52Assignrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:52*
use_locking(*
T0*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ù
save/Assign_53Assigntlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:53*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
:
ý
save/Assign_54Assign)layerfilter0_self_att_conv_head_0/weightssave/RestoreV2:54"/device:CPU:0*
validate_shape(*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*&
_output_shapes
: 
Ù
save/Assign_55Assignlayerfilter1_edgefea_0/biasessave/RestoreV2:55"/device:CPU:0*
T0*0
_class&
$"loc:@layerfilter1_edgefea_0/biases*
validate_shape(*
use_locking(*
_output_shapes
:@
Ì
save/Assign_56Assignlayerfilter1_edgefea_0/bn/betasave/RestoreV2:56*
use_locking(*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/bn/beta*
validate_shape(*
_output_shapes
:@
Î
save/Assign_57Assignlayerfilter1_edgefea_0/bn/gammasave/RestoreV2:57*
use_locking(*
T0*2
_class(
&$loc:@layerfilter1_edgefea_0/bn/gamma*
validate_shape(*
_output_shapes
:@
È
save/Assign_58Assign\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:58*
use_locking(*
T0*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@
Ì
save/Assign_59Assign^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:59*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
:@
è
save/Assign_60Assignlayerfilter1_edgefea_0/weightssave/RestoreV2:60"/device:CPU:0*
validate_shape(*
use_locking(*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*'
_output_shapes
:@
ï
save/Assign_61Assign(layerfilter1_neib_att_conv_head_0/biasessave/RestoreV2:61"/device:CPU:0*
validate_shape(*
use_locking(*
T0*;
_class1
/-loc:@layerfilter1_neib_att_conv_head_0/biases*
_output_shapes
:
â
save/Assign_62Assign)layerfilter1_neib_att_conv_head_0/bn/betasave/RestoreV2:62*
use_locking(*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
ä
save/Assign_63Assign*layerfilter1_neib_att_conv_head_0/bn/gammasave/RestoreV2:63*
T0*=
_class3
1/loc:@layerfilter1_neib_att_conv_head_0/bn/gamma*
validate_shape(*
use_locking(*
_output_shapes
:
õ
save/Assign_64Assignrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:64*
use_locking(*
T0*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ù
save/Assign_65Assigntlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:65*
use_locking(*
T0*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ý
save/Assign_66Assign)layerfilter1_neib_att_conv_head_0/weightssave/RestoreV2:66"/device:CPU:0*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*
validate_shape(*
use_locking(*&
_output_shapes
:@
Þ
save/Assign_67Assign'layerfilter1_newfea_conv_head_0/bn/betasave/RestoreV2:67*
use_locking(*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:@
à
save/Assign_68Assign(layerfilter1_newfea_conv_head_0/bn/gammasave/RestoreV2:68*
use_locking(*
T0*;
_class1
/-loc:@layerfilter1_newfea_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
:@
í
save/Assign_69Assignnlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:69*
use_locking(*
T0*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@
ñ
save/Assign_70Assignplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:70*
use_locking(*
T0*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@
ú
save/Assign_71Assign'layerfilter1_newfea_conv_head_0/weightssave/RestoreV2:71"/device:CPU:0*
use_locking(*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*
validate_shape(*'
_output_shapes
:@
ï
save/Assign_72Assign(layerfilter1_self_att_conv_head_0/biasessave/RestoreV2:72"/device:CPU:0*
use_locking(*
T0*;
_class1
/-loc:@layerfilter1_self_att_conv_head_0/biases*
validate_shape(*
_output_shapes
:
â
save/Assign_73Assign)layerfilter1_self_att_conv_head_0/bn/betasave/RestoreV2:73*
use_locking(*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
ä
save/Assign_74Assign*layerfilter1_self_att_conv_head_0/bn/gammasave/RestoreV2:74*
validate_shape(*
use_locking(*
T0*=
_class3
1/loc:@layerfilter1_self_att_conv_head_0/bn/gamma*
_output_shapes
:
õ
save/Assign_75Assignrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave/RestoreV2:75*
use_locking(*
T0*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ù
save/Assign_76Assigntlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave/RestoreV2:76*
use_locking(*
T0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ý
save/Assign_77Assign)layerfilter1_self_att_conv_head_0/weightssave/RestoreV2:77"/device:CPU:0*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*
validate_shape(*
use_locking(*&
_output_shapes
:@
À
save/Assign_78Assignseg/conv2/biasessave/RestoreV2:78"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv2/biases*
validate_shape(*
_output_shapes	
:
Â
save/Assign_79Assignseg/conv2/bn/betasave/RestoreV2:79"/device:CPU:0*
validate_shape(*
use_locking(*
T0*$
_class
loc:@seg/conv2/bn/beta*
_output_shapes	
:
Ä
save/Assign_80Assignseg/conv2/bn/gammasave/RestoreV2:80"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@seg/conv2/bn/gamma*
validate_shape(*
_output_shapes	
:
Ê
save/Assign_81Assignseg/conv2/bn/pop_meansave/RestoreV2:81"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@seg/conv2/bn/pop_mean*
validate_shape(*
_output_shapes	
:
È
save/Assign_82Assignseg/conv2/bn/pop_varsave/RestoreV2:82"/device:CPU:0*'
_class
loc:@seg/conv2/bn/pop_var*
validate_shape(*
use_locking(*
T0*
_output_shapes	
:
Ï
save/Assign_83Assignseg/conv2/weightssave/RestoreV2:83"/device:CPU:0*$
_class
loc:@seg/conv2/weights*
validate_shape(*
use_locking(*
T0*(
_output_shapes
:
À
save/Assign_84Assignseg/conv3/biasessave/RestoreV2:84"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv3/biases*
validate_shape(*
_output_shapes	
:
Â
save/Assign_85Assignseg/conv3/bn/betasave/RestoreV2:85"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv3/bn/beta*
validate_shape(*
_output_shapes	
:
Ä
save/Assign_86Assignseg/conv3/bn/gammasave/RestoreV2:86"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@seg/conv3/bn/gamma*
validate_shape(*
_output_shapes	
:
Ê
save/Assign_87Assignseg/conv3/bn/pop_meansave/RestoreV2:87"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@seg/conv3/bn/pop_mean*
validate_shape(*
_output_shapes	
:
È
save/Assign_88Assignseg/conv3/bn/pop_varsave/RestoreV2:88"/device:CPU:0*
T0*'
_class
loc:@seg/conv3/bn/pop_var*
validate_shape(*
use_locking(*
_output_shapes	
:
Ï
save/Assign_89Assignseg/conv3/weightssave/RestoreV2:89"/device:CPU:0*$
_class
loc:@seg/conv3/weights*
validate_shape(*
use_locking(*
T0*(
_output_shapes
:
À
save/Assign_90Assignseg/conv4/biasessave/RestoreV2:90"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv4/biases*
validate_shape(*
_output_shapes	
:
Â
save/Assign_91Assignseg/conv4/bn/betasave/RestoreV2:91"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv4/bn/beta*
validate_shape(*
_output_shapes	
:
Ä
save/Assign_92Assignseg/conv4/bn/gammasave/RestoreV2:92"/device:CPU:0*%
_class
loc:@seg/conv4/bn/gamma*
validate_shape(*
use_locking(*
T0*
_output_shapes	
:
Ê
save/Assign_93Assignseg/conv4/bn/pop_meansave/RestoreV2:93"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@seg/conv4/bn/pop_mean*
validate_shape(*
_output_shapes	
:
È
save/Assign_94Assignseg/conv4/bn/pop_varsave/RestoreV2:94"/device:CPU:0*'
_class
loc:@seg/conv4/bn/pop_var*
validate_shape(*
use_locking(*
T0*
_output_shapes	
:
Ï
save/Assign_95Assignseg/conv4/weightssave/RestoreV2:95"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv4/weights*
validate_shape(*(
_output_shapes
:
¿
save/Assign_96Assignseg/conv5/biasessave/RestoreV2:96"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv5/biases*
validate_shape(*
_output_shapes
:
Î
save/Assign_97Assignseg/conv5/weightssave/RestoreV2:97"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv5/weights*
validate_shape(*'
_output_shapes
:
ª
save/restore_all/NoOpNoOp^save/Assign^save/Assign_1^save/Assign_10^save/Assign_11^save/Assign_12^save/Assign_15^save/Assign_16^save/Assign_17^save/Assign_18^save/Assign_21^save/Assign_22^save/Assign_23^save/Assign_24^save/Assign_27^save/Assign_28^save/Assign_29^save/Assign_3^save/Assign_30^save/Assign_33^save/Assign_34^save/Assign_35^save/Assign_36^save/Assign_39^save/Assign_4^save/Assign_40^save/Assign_41^save/Assign_42^save/Assign_44^save/Assign_45^save/Assign_46^save/Assign_47^save/Assign_5^save/Assign_50^save/Assign_51^save/Assign_52^save/Assign_53^save/Assign_56^save/Assign_57^save/Assign_58^save/Assign_59^save/Assign_6^save/Assign_62^save/Assign_63^save/Assign_64^save/Assign_65^save/Assign_67^save/Assign_68^save/Assign_69^save/Assign_70^save/Assign_73^save/Assign_74^save/Assign_75^save/Assign_76^save/Assign_9

save/restore_all/NoOp_1NoOp^save/Assign_13^save/Assign_14^save/Assign_19^save/Assign_2^save/Assign_20^save/Assign_25^save/Assign_26^save/Assign_31^save/Assign_32^save/Assign_37^save/Assign_38^save/Assign_43^save/Assign_48^save/Assign_49^save/Assign_54^save/Assign_55^save/Assign_60^save/Assign_61^save/Assign_66^save/Assign_7^save/Assign_71^save/Assign_72^save/Assign_77^save/Assign_78^save/Assign_79^save/Assign_8^save/Assign_80^save/Assign_81^save/Assign_82^save/Assign_83^save/Assign_84^save/Assign_85^save/Assign_86^save/Assign_87^save/Assign_88^save/Assign_89^save/Assign_90^save/Assign_91^save/Assign_92^save/Assign_93^save/Assign_94^save/Assign_95^save/Assign_96^save/Assign_97"/device:CPU:0
J
save/restore_allNoOp^save/restore_all/NoOp^save/restore_all/NoOp_1
[
save_1/filename/inputConst*
valueB Bmodel*
dtype0*
_output_shapes
: 
r
save_1/filenamePlaceholderWithDefaultsave_1/filename/input*
dtype0*
shape: *
_output_shapes
: 
i
save_1/ConstPlaceholderWithDefaultsave_1/filename*
shape: *
dtype0*
_output_shapes
: 

save_1/StringJoin/inputs_1Const*<
value3B1 B+_temp_b7d3f7bc7a0a440c8f50fbc760cc21fc/part*
dtype0*
_output_shapes
: 
{
save_1/StringJoin
StringJoinsave_1/Constsave_1/StringJoin/inputs_1*
N*
	separator *
_output_shapes
: 
S
save_1/num_shardsConst*
value	B :*
dtype0*
_output_shapes
: 
m
save_1/ShardedFilename/shardConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 

save_1/ShardedFilenameShardedFilenamesave_1/StringJoinsave_1/ShardedFilename/shardsave_1/num_shards"/device:CPU:0*
_output_shapes
: 
Ø
save_1/SaveV2/tensor_namesConst"/device:CPU:0*ú
valueðBí6BBiasAdd/biasesBBiasAdd_1/biasesB6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverageB8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverageBagg/bn/betaBagg/bn/gammaBgapnet00/bn/betaBgapnet00/bn/gammaB@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet01/bn/betaBgapnet01/bn/gammaB@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet11/bn/betaBgapnet11/bn/gammaB@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverageBglobal_expand/bn/betaBglobal_expand/bn/gammaBJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverageBLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverageBlayerfilter0_edgefea_0/bn/betaBlayerfilter0_edgefea_0/bn/gammaB\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverageB^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter0_neib_att_conv_head_0/bn/betaB*layerfilter0_neib_att_conv_head_0/bn/gammaBrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB'layerfilter0_newfea_conv_head_0/bn/betaB(layerfilter0_newfea_conv_head_0/bn/gammaBnlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter0_self_att_conv_head_0/bn/betaB*layerfilter0_self_att_conv_head_0/bn/gammaBrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageBlayerfilter1_edgefea_0/bn/betaBlayerfilter1_edgefea_0/bn/gammaB\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverageB^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter1_neib_att_conv_head_0/bn/betaB*layerfilter1_neib_att_conv_head_0/bn/gammaBrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB'layerfilter1_newfea_conv_head_0/bn/betaB(layerfilter1_newfea_conv_head_0/bn/gammaBnlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter1_self_att_conv_head_0/bn/betaB*layerfilter1_self_att_conv_head_0/bn/gammaBrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
_output_shapes
:6
à
save_1/SaveV2/shape_and_slicesConst"/device:CPU:0*
valuevBt6B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:6
¥
save_1/SaveV2SaveV2save_1/ShardedFilenamesave_1/SaveV2/tensor_namessave_1/SaveV2/shape_and_slicesBiasAdd/biasesBiasAdd_1/biases6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverageagg/bn/betaagg/bn/gammagapnet00/bn/betagapnet00/bn/gamma@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverageBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAveragegapnet01/bn/betagapnet01/bn/gamma@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverageBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAveragegapnet11/bn/betagapnet11/bn/gamma@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverageBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverageglobal_expand/bn/betaglobal_expand/bn/gammaJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverageLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter0_edgefea_0/bn/betalayerfilter0_edgefea_0/bn/gamma\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage)layerfilter0_neib_att_conv_head_0/bn/beta*layerfilter0_neib_att_conv_head_0/bn/gammarlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragetlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage'layerfilter0_newfea_conv_head_0/bn/beta(layerfilter0_newfea_conv_head_0/bn/gammanlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage)layerfilter0_self_att_conv_head_0/bn/beta*layerfilter0_self_att_conv_head_0/bn/gammarlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragetlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragelayerfilter1_edgefea_0/bn/betalayerfilter1_edgefea_0/bn/gamma\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage)layerfilter1_neib_att_conv_head_0/bn/beta*layerfilter1_neib_att_conv_head_0/bn/gammarlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragetlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage'layerfilter1_newfea_conv_head_0/bn/beta(layerfilter1_newfea_conv_head_0/bn/gammanlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage)layerfilter1_self_att_conv_head_0/bn/beta*layerfilter1_self_att_conv_head_0/bn/gammarlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragetlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage"/device:CPU:0*D
dtypes:
826
¨
save_1/control_dependencyIdentitysave_1/ShardedFilename^save_1/SaveV2"/device:CPU:0*)
_class
loc:@save_1/ShardedFilename*
T0*
_output_shapes
: 
o
save_1/ShardedFilename_1/shardConst"/device:CPU:0*
value	B :*
dtype0*
_output_shapes
: 

save_1/ShardedFilename_1ShardedFilenamesave_1/StringJoinsave_1/ShardedFilename_1/shardsave_1/num_shards"/device:CPU:0*
_output_shapes
: 
Ø	
save_1/SaveV2_1/tensor_namesConst"/device:CPU:0*
dtype0*ø
valueîBë,B
agg/biasesBagg/weightsBgapnet00/biasesBgapnet00/weightsBgapnet01/biasesBgapnet01/weightsBgapnet11/biasesBgapnet11/weightsBglobal_expand/biasesBglobal_expand/weightsBlayerfilter0_edgefea_0/biasesBlayerfilter0_edgefea_0/weightsB(layerfilter0_neib_att_conv_head_0/biasesB)layerfilter0_neib_att_conv_head_0/weightsB'layerfilter0_newfea_conv_head_0/weightsB(layerfilter0_self_att_conv_head_0/biasesB)layerfilter0_self_att_conv_head_0/weightsBlayerfilter1_edgefea_0/biasesBlayerfilter1_edgefea_0/weightsB(layerfilter1_neib_att_conv_head_0/biasesB)layerfilter1_neib_att_conv_head_0/weightsB'layerfilter1_newfea_conv_head_0/weightsB(layerfilter1_self_att_conv_head_0/biasesB)layerfilter1_self_att_conv_head_0/weightsBseg/conv2/biasesBseg/conv2/bn/betaBseg/conv2/bn/gammaBseg/conv2/bn/pop_meanBseg/conv2/bn/pop_varBseg/conv2/weightsBseg/conv3/biasesBseg/conv3/bn/betaBseg/conv3/bn/gammaBseg/conv3/bn/pop_meanBseg/conv3/bn/pop_varBseg/conv3/weightsBseg/conv4/biasesBseg/conv4/bn/betaBseg/conv4/bn/gammaBseg/conv4/bn/pop_meanBseg/conv4/bn/pop_varBseg/conv4/weightsBseg/conv5/biasesBseg/conv5/weights*
_output_shapes
:,
Î
 save_1/SaveV2_1/shape_and_slicesConst"/device:CPU:0*k
valuebB`,B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:,
¡

save_1/SaveV2_1SaveV2save_1/ShardedFilename_1save_1/SaveV2_1/tensor_names save_1/SaveV2_1/shape_and_slices
agg/biasesagg/weightsgapnet00/biasesgapnet00/weightsgapnet01/biasesgapnet01/weightsgapnet11/biasesgapnet11/weightsglobal_expand/biasesglobal_expand/weightslayerfilter0_edgefea_0/biaseslayerfilter0_edgefea_0/weights(layerfilter0_neib_att_conv_head_0/biases)layerfilter0_neib_att_conv_head_0/weights'layerfilter0_newfea_conv_head_0/weights(layerfilter0_self_att_conv_head_0/biases)layerfilter0_self_att_conv_head_0/weightslayerfilter1_edgefea_0/biaseslayerfilter1_edgefea_0/weights(layerfilter1_neib_att_conv_head_0/biases)layerfilter1_neib_att_conv_head_0/weights'layerfilter1_newfea_conv_head_0/weights(layerfilter1_self_att_conv_head_0/biases)layerfilter1_self_att_conv_head_0/weightsseg/conv2/biasesseg/conv2/bn/betaseg/conv2/bn/gammaseg/conv2/bn/pop_meanseg/conv2/bn/pop_varseg/conv2/weightsseg/conv3/biasesseg/conv3/bn/betaseg/conv3/bn/gammaseg/conv3/bn/pop_meanseg/conv3/bn/pop_varseg/conv3/weightsseg/conv4/biasesseg/conv4/bn/betaseg/conv4/bn/gammaseg/conv4/bn/pop_meanseg/conv4/bn/pop_varseg/conv4/weightsseg/conv5/biasesseg/conv5/weights"/device:CPU:0*:
dtypes0
.2,
°
save_1/control_dependency_1Identitysave_1/ShardedFilename_1^save_1/SaveV2_1"/device:CPU:0*
T0*+
_class!
loc:@save_1/ShardedFilename_1*
_output_shapes
: 
ê
-save_1/MergeV2Checkpoints/checkpoint_prefixesPacksave_1/ShardedFilenamesave_1/ShardedFilename_1^save_1/control_dependency^save_1/control_dependency_1"/device:CPU:0*
T0*

axis *
N*
_output_shapes
:

save_1/MergeV2CheckpointsMergeV2Checkpoints-save_1/MergeV2Checkpoints/checkpoint_prefixessave_1/Const"/device:CPU:0*
delete_old_dirs(
¯
save_1/IdentityIdentitysave_1/Const^save_1/MergeV2Checkpoints^save_1/control_dependency^save_1/control_dependency_1"/device:CPU:0*
T0*
_output_shapes
: 
Û
save_1/RestoreV2/tensor_namesConst"/device:CPU:0*ú
valueðBí6BBiasAdd/biasesBBiasAdd_1/biasesB6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverageB8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverageBagg/bn/betaBagg/bn/gammaBgapnet00/bn/betaBgapnet00/bn/gammaB@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet01/bn/betaBgapnet01/bn/gammaB@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverageBgapnet11/bn/betaBgapnet11/bn/gammaB@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverageBBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverageBglobal_expand/bn/betaBglobal_expand/bn/gammaBJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverageBLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverageBlayerfilter0_edgefea_0/bn/betaBlayerfilter0_edgefea_0/bn/gammaB\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverageB^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter0_neib_att_conv_head_0/bn/betaB*layerfilter0_neib_att_conv_head_0/bn/gammaBrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB'layerfilter0_newfea_conv_head_0/bn/betaB(layerfilter0_newfea_conv_head_0/bn/gammaBnlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter0_self_att_conv_head_0/bn/betaB*layerfilter0_self_att_conv_head_0/bn/gammaBrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageBlayerfilter1_edgefea_0/bn/betaBlayerfilter1_edgefea_0/bn/gammaB\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverageB^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter1_neib_att_conv_head_0/bn/betaB*layerfilter1_neib_att_conv_head_0/bn/gammaBrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB'layerfilter1_newfea_conv_head_0/bn/betaB(layerfilter1_newfea_conv_head_0/bn/gammaBnlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverageB)layerfilter1_self_att_conv_head_0/bn/betaB*layerfilter1_self_att_conv_head_0/bn/gammaBrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverageBtlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
dtype0*
_output_shapes
:6
ã
!save_1/RestoreV2/shape_and_slicesConst"/device:CPU:0*
valuevBt6B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:6
³
save_1/RestoreV2	RestoreV2save_1/Constsave_1/RestoreV2/tensor_names!save_1/RestoreV2/shape_and_slices"/device:CPU:0*D
dtypes:
826*î
_output_shapesÛ
Ø::::::::::::::::::::::::::::::::::::::::::::::::::::::
ª
save_1/AssignAssignBiasAdd/biasessave_1/RestoreV2*
use_locking(*
T0*!
_class
loc:@BiasAdd/biases*
validate_shape(*
_output_shapes
: 
²
save_1/Assign_1AssignBiasAdd_1/biasessave_1/RestoreV2:1*#
_class
loc:@BiasAdd_1/biases*
validate_shape(*
use_locking(*
T0*
_output_shapes
:@
ÿ
save_1/Assign_2Assign6agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:2*
T0*I
_class?
=;loc:@agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes	
:

save_1/Assign_3Assign8agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:3*
use_locking(*
T0*K
_classA
?=loc:@agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:
©
save_1/Assign_4Assignagg/bn/betasave_1/RestoreV2:4*
use_locking(*
T0*
_class
loc:@agg/bn/beta*
validate_shape(*
_output_shapes	
:
«
save_1/Assign_5Assignagg/bn/gammasave_1/RestoreV2:5*
use_locking(*
T0*
_class
loc:@agg/bn/gamma*
validate_shape(*
_output_shapes	
:
²
save_1/Assign_6Assigngapnet00/bn/betasave_1/RestoreV2:6*
use_locking(*
T0*#
_class
loc:@gapnet00/bn/beta*
validate_shape(*
_output_shapes
:@
´
save_1/Assign_7Assigngapnet00/bn/gammasave_1/RestoreV2:7*
use_locking(*
T0*$
_class
loc:@gapnet00/bn/gamma*
validate_shape(*
_output_shapes
:@

save_1/Assign_8Assign@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:8*
use_locking(*
T0*S
_classI
GEloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@

save_1/Assign_9AssignBgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:9*U
_classK
IGloc:@gapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
T0*
_output_shapes
:@
µ
save_1/Assign_10Assigngapnet01/bn/betasave_1/RestoreV2:10*
validate_shape(*
use_locking(*
T0*#
_class
loc:@gapnet01/bn/beta*
_output_shapes	
:
·
save_1/Assign_11Assigngapnet01/bn/gammasave_1/RestoreV2:11*
validate_shape(*
use_locking(*
T0*$
_class
loc:@gapnet01/bn/gamma*
_output_shapes	
:

save_1/Assign_12Assign@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:12*
use_locking(*
T0*S
_classI
GEloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:

save_1/Assign_13AssignBgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:13*
T0*U
_classK
IGloc:@gapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes	
:
µ
save_1/Assign_14Assigngapnet11/bn/betasave_1/RestoreV2:14*
use_locking(*
T0*#
_class
loc:@gapnet11/bn/beta*
validate_shape(*
_output_shapes	
:
·
save_1/Assign_15Assigngapnet11/bn/gammasave_1/RestoreV2:15*
validate_shape(*
use_locking(*
T0*$
_class
loc:@gapnet11/bn/gamma*
_output_shapes	
:

save_1/Assign_16Assign@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:16*
use_locking(*
T0*S
_classI
GEloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:

save_1/Assign_17AssignBgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:17*
use_locking(*
T0*U
_classK
IGloc:@gapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes	
:
¾
save_1/Assign_18Assignglobal_expand/bn/betasave_1/RestoreV2:18*
validate_shape(*
use_locking(*
T0*(
_class
loc:@global_expand/bn/beta*
_output_shapes
:
À
save_1/Assign_19Assignglobal_expand/bn/gammasave_1/RestoreV2:19*
validate_shape(*
use_locking(*
T0*)
_class
loc:@global_expand/bn/gamma*
_output_shapes
:
¨
save_1/Assign_20AssignJglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:20*
use_locking(*
T0*]
_classS
QOloc:@global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
¬
save_1/Assign_21AssignLglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:21*
use_locking(*
T0*_
_classU
SQloc:@global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
Ð
save_1/Assign_22Assignlayerfilter0_edgefea_0/bn/betasave_1/RestoreV2:22*1
_class'
%#loc:@layerfilter0_edgefea_0/bn/beta*
validate_shape(*
use_locking(*
T0*
_output_shapes
: 
Ò
save_1/Assign_23Assignlayerfilter0_edgefea_0/bn/gammasave_1/RestoreV2:23*2
_class(
&$loc:@layerfilter0_edgefea_0/bn/gamma*
validate_shape(*
use_locking(*
T0*
_output_shapes
: 
Ì
save_1/Assign_24Assign\layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:24*
use_locking(*
T0*o
_classe
caloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
: 
Ð
save_1/Assign_25Assign^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:25*
T0*q
_classg
ecloc:@layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
: 
æ
save_1/Assign_26Assign)layerfilter0_neib_att_conv_head_0/bn/betasave_1/RestoreV2:26*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
è
save_1/Assign_27Assign*layerfilter0_neib_att_conv_head_0/bn/gammasave_1/RestoreV2:27*
use_locking(*
T0*=
_class3
1/loc:@layerfilter0_neib_att_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
:
ù
save_1/Assign_28Assignrlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:28*
T0*
_class{
ywloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:
ý
save_1/Assign_29Assigntlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:29*
T0*
_class}
{yloc:@layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:
â
save_1/Assign_30Assign'layerfilter0_newfea_conv_head_0/bn/betasave_1/RestoreV2:30*
validate_shape(*
use_locking(*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/bn/beta*
_output_shapes
: 
ä
save_1/Assign_31Assign(layerfilter0_newfea_conv_head_0/bn/gammasave_1/RestoreV2:31*
use_locking(*
T0*;
_class1
/-loc:@layerfilter0_newfea_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
: 
ñ
save_1/Assign_32Assignnlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:32*
use_locking(*
T0*
_classw
usloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
: 
õ
save_1/Assign_33Assignplayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:33*
validate_shape(*
use_locking(*
T0*
_classy
wuloc:@layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
: 
æ
save_1/Assign_34Assign)layerfilter0_self_att_conv_head_0/bn/betasave_1/RestoreV2:34*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
è
save_1/Assign_35Assign*layerfilter0_self_att_conv_head_0/bn/gammasave_1/RestoreV2:35*
use_locking(*
T0*=
_class3
1/loc:@layerfilter0_self_att_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
:
ù
save_1/Assign_36Assignrlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:36*
use_locking(*
T0*
_class{
ywloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ý
save_1/Assign_37Assigntlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:37*
use_locking(*
T0*
_class}
{yloc:@layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
Ð
save_1/Assign_38Assignlayerfilter1_edgefea_0/bn/betasave_1/RestoreV2:38*1
_class'
%#loc:@layerfilter1_edgefea_0/bn/beta*
validate_shape(*
use_locking(*
T0*
_output_shapes
:@
Ò
save_1/Assign_39Assignlayerfilter1_edgefea_0/bn/gammasave_1/RestoreV2:39*
use_locking(*
T0*2
_class(
&$loc:@layerfilter1_edgefea_0/bn/gamma*
validate_shape(*
_output_shapes
:@
Ì
save_1/Assign_40Assign\layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:40*
use_locking(*
T0*o
_classe
caloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@
Ð
save_1/Assign_41Assign^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:41*
use_locking(*
T0*q
_classg
ecloc:@layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@
æ
save_1/Assign_42Assign)layerfilter1_neib_att_conv_head_0/bn/betasave_1/RestoreV2:42*
use_locking(*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:
è
save_1/Assign_43Assign*layerfilter1_neib_att_conv_head_0/bn/gammasave_1/RestoreV2:43*
use_locking(*
T0*=
_class3
1/loc:@layerfilter1_neib_att_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
:
ù
save_1/Assign_44Assignrlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:44*
use_locking(*
T0*
_class{
ywloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ý
save_1/Assign_45Assigntlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:45*
validate_shape(*
use_locking(*
T0*
_class}
{yloc:@layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
_output_shapes
:
â
save_1/Assign_46Assign'layerfilter1_newfea_conv_head_0/bn/betasave_1/RestoreV2:46*
use_locking(*
T0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/bn/beta*
validate_shape(*
_output_shapes
:@
ä
save_1/Assign_47Assign(layerfilter1_newfea_conv_head_0/bn/gammasave_1/RestoreV2:47*
use_locking(*
T0*;
_class1
/-loc:@layerfilter1_newfea_conv_head_0/bn/gamma*
validate_shape(*
_output_shapes
:@
ñ
save_1/Assign_48Assignnlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:48*
use_locking(*
T0*
_classw
usloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:@
õ
save_1/Assign_49Assignplayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:49*
T0*
_classy
wuloc:@layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:@
æ
save_1/Assign_50Assign)layerfilter1_self_att_conv_head_0/bn/betasave_1/RestoreV2:50*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/bn/beta*
validate_shape(*
use_locking(*
T0*
_output_shapes
:
è
save_1/Assign_51Assign*layerfilter1_self_att_conv_head_0/bn/gammasave_1/RestoreV2:51*
T0*=
_class3
1/loc:@layerfilter1_self_att_conv_head_0/bn/gamma*
validate_shape(*
use_locking(*
_output_shapes
:
ù
save_1/Assign_52Assignrlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAveragesave_1/RestoreV2:52*
use_locking(*
T0*
_class{
ywloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage*
validate_shape(*
_output_shapes
:
ý
save_1/Assign_53Assigntlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAveragesave_1/RestoreV2:53*
T0*
_class}
{yloc:@layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage*
validate_shape(*
use_locking(*
_output_shapes
:

save_1/restore_shardNoOp^save_1/Assign^save_1/Assign_1^save_1/Assign_10^save_1/Assign_11^save_1/Assign_12^save_1/Assign_13^save_1/Assign_14^save_1/Assign_15^save_1/Assign_16^save_1/Assign_17^save_1/Assign_18^save_1/Assign_19^save_1/Assign_2^save_1/Assign_20^save_1/Assign_21^save_1/Assign_22^save_1/Assign_23^save_1/Assign_24^save_1/Assign_25^save_1/Assign_26^save_1/Assign_27^save_1/Assign_28^save_1/Assign_29^save_1/Assign_3^save_1/Assign_30^save_1/Assign_31^save_1/Assign_32^save_1/Assign_33^save_1/Assign_34^save_1/Assign_35^save_1/Assign_36^save_1/Assign_37^save_1/Assign_38^save_1/Assign_39^save_1/Assign_4^save_1/Assign_40^save_1/Assign_41^save_1/Assign_42^save_1/Assign_43^save_1/Assign_44^save_1/Assign_45^save_1/Assign_46^save_1/Assign_47^save_1/Assign_48^save_1/Assign_49^save_1/Assign_5^save_1/Assign_50^save_1/Assign_51^save_1/Assign_52^save_1/Assign_53^save_1/Assign_6^save_1/Assign_7^save_1/Assign_8^save_1/Assign_9
Û	
save_1/RestoreV2_1/tensor_namesConst"/device:CPU:0*ø
valueîBë,B
agg/biasesBagg/weightsBgapnet00/biasesBgapnet00/weightsBgapnet01/biasesBgapnet01/weightsBgapnet11/biasesBgapnet11/weightsBglobal_expand/biasesBglobal_expand/weightsBlayerfilter0_edgefea_0/biasesBlayerfilter0_edgefea_0/weightsB(layerfilter0_neib_att_conv_head_0/biasesB)layerfilter0_neib_att_conv_head_0/weightsB'layerfilter0_newfea_conv_head_0/weightsB(layerfilter0_self_att_conv_head_0/biasesB)layerfilter0_self_att_conv_head_0/weightsBlayerfilter1_edgefea_0/biasesBlayerfilter1_edgefea_0/weightsB(layerfilter1_neib_att_conv_head_0/biasesB)layerfilter1_neib_att_conv_head_0/weightsB'layerfilter1_newfea_conv_head_0/weightsB(layerfilter1_self_att_conv_head_0/biasesB)layerfilter1_self_att_conv_head_0/weightsBseg/conv2/biasesBseg/conv2/bn/betaBseg/conv2/bn/gammaBseg/conv2/bn/pop_meanBseg/conv2/bn/pop_varBseg/conv2/weightsBseg/conv3/biasesBseg/conv3/bn/betaBseg/conv3/bn/gammaBseg/conv3/bn/pop_meanBseg/conv3/bn/pop_varBseg/conv3/weightsBseg/conv4/biasesBseg/conv4/bn/betaBseg/conv4/bn/gammaBseg/conv4/bn/pop_meanBseg/conv4/bn/pop_varBseg/conv4/weightsBseg/conv5/biasesBseg/conv5/weights*
dtype0*
_output_shapes
:,
Ñ
#save_1/RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
dtype0*k
valuebB`,B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
_output_shapes
:,

save_1/RestoreV2_1	RestoreV2save_1/Constsave_1/RestoreV2_1/tensor_names#save_1/RestoreV2_1/shape_and_slices"/device:CPU:0*:
dtypes0
.2,*Æ
_output_shapes³
°::::::::::::::::::::::::::::::::::::::::::::
·
save_1/Assign_54Assign
agg/biasessave_1/RestoreV2_1"/device:CPU:0*
T0*
_class
loc:@agg/biases*
validate_shape(*
use_locking(*
_output_shapes	
:
È
save_1/Assign_55Assignagg/weightssave_1/RestoreV2_1:1"/device:CPU:0*
use_locking(*
T0*
_class
loc:@agg/weights*
validate_shape(*(
_output_shapes
:ð
Â
save_1/Assign_56Assigngapnet00/biasessave_1/RestoreV2_1:2"/device:CPU:0*
use_locking(*
T0*"
_class
loc:@gapnet00/biases*
validate_shape(*
_output_shapes
:@
Ð
save_1/Assign_57Assigngapnet00/weightssave_1/RestoreV2_1:3"/device:CPU:0*
validate_shape(*
use_locking(*
T0*#
_class
loc:@gapnet00/weights*&
_output_shapes
:*@
Ã
save_1/Assign_58Assigngapnet01/biasessave_1/RestoreV2_1:4"/device:CPU:0*
validate_shape(*
use_locking(*
T0*"
_class
loc:@gapnet01/biases*
_output_shapes	
:
Ñ
save_1/Assign_59Assigngapnet01/weightssave_1/RestoreV2_1:5"/device:CPU:0*
T0*#
_class
loc:@gapnet01/weights*
validate_shape(*
use_locking(*'
_output_shapes
:@
Ã
save_1/Assign_60Assigngapnet11/biasessave_1/RestoreV2_1:6"/device:CPU:0*
use_locking(*
T0*"
_class
loc:@gapnet11/biases*
validate_shape(*
_output_shapes	
:
Ñ
save_1/Assign_61Assigngapnet11/weightssave_1/RestoreV2_1:7"/device:CPU:0*
validate_shape(*
use_locking(*
T0*#
_class
loc:@gapnet11/weights*'
_output_shapes
:J
Ì
save_1/Assign_62Assignglobal_expand/biasessave_1/RestoreV2_1:8"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@global_expand/biases*
validate_shape(*
_output_shapes
:
Ú
save_1/Assign_63Assignglobal_expand/weightssave_1/RestoreV2_1:9"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@global_expand/weights*
validate_shape(*&
_output_shapes
:
ß
save_1/Assign_64Assignlayerfilter0_edgefea_0/biasessave_1/RestoreV2_1:10"/device:CPU:0*
use_locking(*
T0*0
_class&
$"loc:@layerfilter0_edgefea_0/biases*
validate_shape(*
_output_shapes
: 
í
save_1/Assign_65Assignlayerfilter0_edgefea_0/weightssave_1/RestoreV2_1:11"/device:CPU:0*
use_locking(*
T0*1
_class'
%#loc:@layerfilter0_edgefea_0/weights*
validate_shape(*&
_output_shapes
:
 
õ
save_1/Assign_66Assign(layerfilter0_neib_att_conv_head_0/biasessave_1/RestoreV2_1:12"/device:CPU:0*
use_locking(*
T0*;
_class1
/-loc:@layerfilter0_neib_att_conv_head_0/biases*
validate_shape(*
_output_shapes
:

save_1/Assign_67Assign)layerfilter0_neib_att_conv_head_0/weightssave_1/RestoreV2_1:13"/device:CPU:0*
validate_shape(*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_neib_att_conv_head_0/weights*&
_output_shapes
: 
ÿ
save_1/Assign_68Assign'layerfilter0_newfea_conv_head_0/weightssave_1/RestoreV2_1:14"/device:CPU:0*
use_locking(*
T0*:
_class0
.,loc:@layerfilter0_newfea_conv_head_0/weights*
validate_shape(*&
_output_shapes
:
 
õ
save_1/Assign_69Assign(layerfilter0_self_att_conv_head_0/biasessave_1/RestoreV2_1:15"/device:CPU:0*
use_locking(*
T0*;
_class1
/-loc:@layerfilter0_self_att_conv_head_0/biases*
validate_shape(*
_output_shapes
:

save_1/Assign_70Assign)layerfilter0_self_att_conv_head_0/weightssave_1/RestoreV2_1:16"/device:CPU:0*
use_locking(*
T0*<
_class2
0.loc:@layerfilter0_self_att_conv_head_0/weights*
validate_shape(*&
_output_shapes
: 
ß
save_1/Assign_71Assignlayerfilter1_edgefea_0/biasessave_1/RestoreV2_1:17"/device:CPU:0*
T0*0
_class&
$"loc:@layerfilter1_edgefea_0/biases*
validate_shape(*
use_locking(*
_output_shapes
:@
î
save_1/Assign_72Assignlayerfilter1_edgefea_0/weightssave_1/RestoreV2_1:18"/device:CPU:0*
use_locking(*
T0*1
_class'
%#loc:@layerfilter1_edgefea_0/weights*
validate_shape(*'
_output_shapes
:@
õ
save_1/Assign_73Assign(layerfilter1_neib_att_conv_head_0/biasessave_1/RestoreV2_1:19"/device:CPU:0*
use_locking(*
T0*;
_class1
/-loc:@layerfilter1_neib_att_conv_head_0/biases*
validate_shape(*
_output_shapes
:

save_1/Assign_74Assign)layerfilter1_neib_att_conv_head_0/weightssave_1/RestoreV2_1:20"/device:CPU:0*
use_locking(*
T0*<
_class2
0.loc:@layerfilter1_neib_att_conv_head_0/weights*
validate_shape(*&
_output_shapes
:@

save_1/Assign_75Assign'layerfilter1_newfea_conv_head_0/weightssave_1/RestoreV2_1:21"/device:CPU:0*:
_class0
.,loc:@layerfilter1_newfea_conv_head_0/weights*
validate_shape(*
use_locking(*
T0*'
_output_shapes
:@
õ
save_1/Assign_76Assign(layerfilter1_self_att_conv_head_0/biasessave_1/RestoreV2_1:22"/device:CPU:0*
use_locking(*
T0*;
_class1
/-loc:@layerfilter1_self_att_conv_head_0/biases*
validate_shape(*
_output_shapes
:

save_1/Assign_77Assign)layerfilter1_self_att_conv_head_0/weightssave_1/RestoreV2_1:23"/device:CPU:0*
use_locking(*
T0*<
_class2
0.loc:@layerfilter1_self_att_conv_head_0/weights*
validate_shape(*&
_output_shapes
:@
Æ
save_1/Assign_78Assignseg/conv2/biasessave_1/RestoreV2_1:24"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv2/biases*
validate_shape(*
_output_shapes	
:
È
save_1/Assign_79Assignseg/conv2/bn/betasave_1/RestoreV2_1:25"/device:CPU:0*
T0*$
_class
loc:@seg/conv2/bn/beta*
validate_shape(*
use_locking(*
_output_shapes	
:
Ê
save_1/Assign_80Assignseg/conv2/bn/gammasave_1/RestoreV2_1:26"/device:CPU:0*
validate_shape(*
use_locking(*
T0*%
_class
loc:@seg/conv2/bn/gamma*
_output_shapes	
:
Ð
save_1/Assign_81Assignseg/conv2/bn/pop_meansave_1/RestoreV2_1:27"/device:CPU:0*
validate_shape(*
use_locking(*
T0*(
_class
loc:@seg/conv2/bn/pop_mean*
_output_shapes	
:
Î
save_1/Assign_82Assignseg/conv2/bn/pop_varsave_1/RestoreV2_1:28"/device:CPU:0*
validate_shape(*
use_locking(*
T0*'
_class
loc:@seg/conv2/bn/pop_var*
_output_shapes	
:
Õ
save_1/Assign_83Assignseg/conv2/weightssave_1/RestoreV2_1:29"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv2/weights*
validate_shape(*(
_output_shapes
:
Æ
save_1/Assign_84Assignseg/conv3/biasessave_1/RestoreV2_1:30"/device:CPU:0*
validate_shape(*
use_locking(*
T0*#
_class
loc:@seg/conv3/biases*
_output_shapes	
:
È
save_1/Assign_85Assignseg/conv3/bn/betasave_1/RestoreV2_1:31"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv3/bn/beta*
validate_shape(*
_output_shapes	
:
Ê
save_1/Assign_86Assignseg/conv3/bn/gammasave_1/RestoreV2_1:32"/device:CPU:0*
T0*%
_class
loc:@seg/conv3/bn/gamma*
validate_shape(*
use_locking(*
_output_shapes	
:
Ð
save_1/Assign_87Assignseg/conv3/bn/pop_meansave_1/RestoreV2_1:33"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@seg/conv3/bn/pop_mean*
validate_shape(*
_output_shapes	
:
Î
save_1/Assign_88Assignseg/conv3/bn/pop_varsave_1/RestoreV2_1:34"/device:CPU:0*
validate_shape(*
use_locking(*
T0*'
_class
loc:@seg/conv3/bn/pop_var*
_output_shapes	
:
Õ
save_1/Assign_89Assignseg/conv3/weightssave_1/RestoreV2_1:35"/device:CPU:0*
validate_shape(*
use_locking(*
T0*$
_class
loc:@seg/conv3/weights*(
_output_shapes
:
Æ
save_1/Assign_90Assignseg/conv4/biasessave_1/RestoreV2_1:36"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv4/biases*
validate_shape(*
_output_shapes	
:
È
save_1/Assign_91Assignseg/conv4/bn/betasave_1/RestoreV2_1:37"/device:CPU:0*
T0*$
_class
loc:@seg/conv4/bn/beta*
validate_shape(*
use_locking(*
_output_shapes	
:
Ê
save_1/Assign_92Assignseg/conv4/bn/gammasave_1/RestoreV2_1:38"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@seg/conv4/bn/gamma*
validate_shape(*
_output_shapes	
:
Ð
save_1/Assign_93Assignseg/conv4/bn/pop_meansave_1/RestoreV2_1:39"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@seg/conv4/bn/pop_mean*
validate_shape(*
_output_shapes	
:
Î
save_1/Assign_94Assignseg/conv4/bn/pop_varsave_1/RestoreV2_1:40"/device:CPU:0*
use_locking(*
T0*'
_class
loc:@seg/conv4/bn/pop_var*
validate_shape(*
_output_shapes	
:
Õ
save_1/Assign_95Assignseg/conv4/weightssave_1/RestoreV2_1:41"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv4/weights*
validate_shape(*(
_output_shapes
:
Å
save_1/Assign_96Assignseg/conv5/biasessave_1/RestoreV2_1:42"/device:CPU:0*
use_locking(*
T0*#
_class
loc:@seg/conv5/biases*
validate_shape(*
_output_shapes
:
Ô
save_1/Assign_97Assignseg/conv5/weightssave_1/RestoreV2_1:43"/device:CPU:0*
use_locking(*
T0*$
_class
loc:@seg/conv5/weights*
validate_shape(*'
_output_shapes
:
ñ
save_1/restore_shard_1NoOp^save_1/Assign_54^save_1/Assign_55^save_1/Assign_56^save_1/Assign_57^save_1/Assign_58^save_1/Assign_59^save_1/Assign_60^save_1/Assign_61^save_1/Assign_62^save_1/Assign_63^save_1/Assign_64^save_1/Assign_65^save_1/Assign_66^save_1/Assign_67^save_1/Assign_68^save_1/Assign_69^save_1/Assign_70^save_1/Assign_71^save_1/Assign_72^save_1/Assign_73^save_1/Assign_74^save_1/Assign_75^save_1/Assign_76^save_1/Assign_77^save_1/Assign_78^save_1/Assign_79^save_1/Assign_80^save_1/Assign_81^save_1/Assign_82^save_1/Assign_83^save_1/Assign_84^save_1/Assign_85^save_1/Assign_86^save_1/Assign_87^save_1/Assign_88^save_1/Assign_89^save_1/Assign_90^save_1/Assign_91^save_1/Assign_92^save_1/Assign_93^save_1/Assign_94^save_1/Assign_95^save_1/Assign_96^save_1/Assign_97"/device:CPU:0
6
save_1/restore_all/NoOpNoOp^save_1/restore_shard
I
save_1/restore_all/NoOp_1NoOp^save_1/restore_shard_1"/device:CPU:0
P
save_1/restore_allNoOp^save_1/restore_all/NoOp^save_1/restore_all/NoOp_1"&B
save_1/Const:0save_1/Identity:0save_1/restore_all (5 @F8"ßL
trainable_variablesÇLÄL
Ó
)layerfilter0_newfea_conv_head_0/weights:0.layerfilter0_newfea_conv_head_0/weights/Assign.layerfilter0_newfea_conv_head_0/weights/read:02Dlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform:08
¹
)layerfilter0_newfea_conv_head_0/bn/beta:0.layerfilter0_newfea_conv_head_0/bn/beta/Assign.layerfilter0_newfea_conv_head_0/bn/beta/read:02*layerfilter0_newfea_conv_head_0/bn/Const:08
¾
*layerfilter0_newfea_conv_head_0/bn/gamma:0/layerfilter0_newfea_conv_head_0/bn/gamma/Assign/layerfilter0_newfea_conv_head_0/bn/gamma/read:02,layerfilter0_newfea_conv_head_0/bn/Const_1:08
¯
 layerfilter0_edgefea_0/weights:0%layerfilter0_edgefea_0/weights/Assign%layerfilter0_edgefea_0/weights/read:02;layerfilter0_edgefea_0/weights/Initializer/random_uniform:08
¢
layerfilter0_edgefea_0/biases:0$layerfilter0_edgefea_0/biases/Assign$layerfilter0_edgefea_0/biases/read:021layerfilter0_edgefea_0/biases/Initializer/Const:08

 layerfilter0_edgefea_0/bn/beta:0%layerfilter0_edgefea_0/bn/beta/Assign%layerfilter0_edgefea_0/bn/beta/read:02!layerfilter0_edgefea_0/bn/Const:08

!layerfilter0_edgefea_0/bn/gamma:0&layerfilter0_edgefea_0/bn/gamma/Assign&layerfilter0_edgefea_0/bn/gamma/read:02#layerfilter0_edgefea_0/bn/Const_1:08
Û
+layerfilter0_self_att_conv_head_0/weights:00layerfilter0_self_att_conv_head_0/weights/Assign0layerfilter0_self_att_conv_head_0/weights/read:02Flayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform:08
Î
*layerfilter0_self_att_conv_head_0/biases:0/layerfilter0_self_att_conv_head_0/biases/Assign/layerfilter0_self_att_conv_head_0/biases/read:02<layerfilter0_self_att_conv_head_0/biases/Initializer/Const:08
Á
+layerfilter0_self_att_conv_head_0/bn/beta:00layerfilter0_self_att_conv_head_0/bn/beta/Assign0layerfilter0_self_att_conv_head_0/bn/beta/read:02,layerfilter0_self_att_conv_head_0/bn/Const:08
Æ
,layerfilter0_self_att_conv_head_0/bn/gamma:01layerfilter0_self_att_conv_head_0/bn/gamma/Assign1layerfilter0_self_att_conv_head_0/bn/gamma/read:02.layerfilter0_self_att_conv_head_0/bn/Const_1:08
Û
+layerfilter0_neib_att_conv_head_0/weights:00layerfilter0_neib_att_conv_head_0/weights/Assign0layerfilter0_neib_att_conv_head_0/weights/read:02Flayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform:08
Î
*layerfilter0_neib_att_conv_head_0/biases:0/layerfilter0_neib_att_conv_head_0/biases/Assign/layerfilter0_neib_att_conv_head_0/biases/read:02<layerfilter0_neib_att_conv_head_0/biases/Initializer/Const:08
Á
+layerfilter0_neib_att_conv_head_0/bn/beta:00layerfilter0_neib_att_conv_head_0/bn/beta/Assign0layerfilter0_neib_att_conv_head_0/bn/beta/read:02,layerfilter0_neib_att_conv_head_0/bn/Const:08
Æ
,layerfilter0_neib_att_conv_head_0/bn/gamma:01layerfilter0_neib_att_conv_head_0/bn/gamma/Assign1layerfilter0_neib_att_conv_head_0/bn/gamma/read:02.layerfilter0_neib_att_conv_head_0/bn/Const_1:08
f
BiasAdd/biases:0BiasAdd/biases/AssignBiasAdd/biases/read:02"BiasAdd/biases/Initializer/zeros:08
w
gapnet00/weights:0gapnet00/weights/Assigngapnet00/weights/read:02-gapnet00/weights/Initializer/random_uniform:08
j
gapnet00/biases:0gapnet00/biases/Assigngapnet00/biases/read:02#gapnet00/biases/Initializer/Const:08
]
gapnet00/bn/beta:0gapnet00/bn/beta/Assigngapnet00/bn/beta/read:02gapnet00/bn/Const:08
b
gapnet00/bn/gamma:0gapnet00/bn/gamma/Assigngapnet00/bn/gamma/read:02gapnet00/bn/Const_1:08
w
gapnet01/weights:0gapnet01/weights/Assigngapnet01/weights/read:02-gapnet01/weights/Initializer/random_uniform:08
j
gapnet01/biases:0gapnet01/biases/Assigngapnet01/biases/read:02#gapnet01/biases/Initializer/Const:08
]
gapnet01/bn/beta:0gapnet01/bn/beta/Assigngapnet01/bn/beta/read:02gapnet01/bn/Const:08
b
gapnet01/bn/gamma:0gapnet01/bn/gamma/Assigngapnet01/bn/gamma/read:02gapnet01/bn/Const_1:08
Ó
)layerfilter1_newfea_conv_head_0/weights:0.layerfilter1_newfea_conv_head_0/weights/Assign.layerfilter1_newfea_conv_head_0/weights/read:02Dlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform:08
¹
)layerfilter1_newfea_conv_head_0/bn/beta:0.layerfilter1_newfea_conv_head_0/bn/beta/Assign.layerfilter1_newfea_conv_head_0/bn/beta/read:02*layerfilter1_newfea_conv_head_0/bn/Const:08
¾
*layerfilter1_newfea_conv_head_0/bn/gamma:0/layerfilter1_newfea_conv_head_0/bn/gamma/Assign/layerfilter1_newfea_conv_head_0/bn/gamma/read:02,layerfilter1_newfea_conv_head_0/bn/Const_1:08
¯
 layerfilter1_edgefea_0/weights:0%layerfilter1_edgefea_0/weights/Assign%layerfilter1_edgefea_0/weights/read:02;layerfilter1_edgefea_0/weights/Initializer/random_uniform:08
¢
layerfilter1_edgefea_0/biases:0$layerfilter1_edgefea_0/biases/Assign$layerfilter1_edgefea_0/biases/read:021layerfilter1_edgefea_0/biases/Initializer/Const:08

 layerfilter1_edgefea_0/bn/beta:0%layerfilter1_edgefea_0/bn/beta/Assign%layerfilter1_edgefea_0/bn/beta/read:02!layerfilter1_edgefea_0/bn/Const:08

!layerfilter1_edgefea_0/bn/gamma:0&layerfilter1_edgefea_0/bn/gamma/Assign&layerfilter1_edgefea_0/bn/gamma/read:02#layerfilter1_edgefea_0/bn/Const_1:08
Û
+layerfilter1_self_att_conv_head_0/weights:00layerfilter1_self_att_conv_head_0/weights/Assign0layerfilter1_self_att_conv_head_0/weights/read:02Flayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform:08
Î
*layerfilter1_self_att_conv_head_0/biases:0/layerfilter1_self_att_conv_head_0/biases/Assign/layerfilter1_self_att_conv_head_0/biases/read:02<layerfilter1_self_att_conv_head_0/biases/Initializer/Const:08
Á
+layerfilter1_self_att_conv_head_0/bn/beta:00layerfilter1_self_att_conv_head_0/bn/beta/Assign0layerfilter1_self_att_conv_head_0/bn/beta/read:02,layerfilter1_self_att_conv_head_0/bn/Const:08
Æ
,layerfilter1_self_att_conv_head_0/bn/gamma:01layerfilter1_self_att_conv_head_0/bn/gamma/Assign1layerfilter1_self_att_conv_head_0/bn/gamma/read:02.layerfilter1_self_att_conv_head_0/bn/Const_1:08
Û
+layerfilter1_neib_att_conv_head_0/weights:00layerfilter1_neib_att_conv_head_0/weights/Assign0layerfilter1_neib_att_conv_head_0/weights/read:02Flayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform:08
Î
*layerfilter1_neib_att_conv_head_0/biases:0/layerfilter1_neib_att_conv_head_0/biases/Assign/layerfilter1_neib_att_conv_head_0/biases/read:02<layerfilter1_neib_att_conv_head_0/biases/Initializer/Const:08
Á
+layerfilter1_neib_att_conv_head_0/bn/beta:00layerfilter1_neib_att_conv_head_0/bn/beta/Assign0layerfilter1_neib_att_conv_head_0/bn/beta/read:02,layerfilter1_neib_att_conv_head_0/bn/Const:08
Æ
,layerfilter1_neib_att_conv_head_0/bn/gamma:01layerfilter1_neib_att_conv_head_0/bn/gamma/Assign1layerfilter1_neib_att_conv_head_0/bn/gamma/read:02.layerfilter1_neib_att_conv_head_0/bn/Const_1:08
n
BiasAdd_1/biases:0BiasAdd_1/biases/AssignBiasAdd_1/biases/read:02$BiasAdd_1/biases/Initializer/zeros:08
w
gapnet11/weights:0gapnet11/weights/Assigngapnet11/weights/read:02-gapnet11/weights/Initializer/random_uniform:08
j
gapnet11/biases:0gapnet11/biases/Assigngapnet11/biases/read:02#gapnet11/biases/Initializer/Const:08
]
gapnet11/bn/beta:0gapnet11/bn/beta/Assigngapnet11/bn/beta/read:02gapnet11/bn/Const:08
b
gapnet11/bn/gamma:0gapnet11/bn/gamma/Assigngapnet11/bn/gamma/read:02gapnet11/bn/Const_1:08

global_expand/weights:0global_expand/weights/Assignglobal_expand/weights/read:022global_expand/weights/Initializer/random_uniform:08
~
global_expand/biases:0global_expand/biases/Assignglobal_expand/biases/read:02(global_expand/biases/Initializer/Const:08
q
global_expand/bn/beta:0global_expand/bn/beta/Assignglobal_expand/bn/beta/read:02global_expand/bn/Const:08
v
global_expand/bn/gamma:0global_expand/bn/gamma/Assignglobal_expand/bn/gamma/read:02global_expand/bn/Const_1:08
c
agg/weights:0agg/weights/Assignagg/weights/read:02(agg/weights/Initializer/random_uniform:08
V
agg/biases:0agg/biases/Assignagg/biases/read:02agg/biases/Initializer/Const:08
I
agg/bn/beta:0agg/bn/beta/Assignagg/bn/beta/read:02agg/bn/Const:08
N
agg/bn/gamma:0agg/bn/gamma/Assignagg/bn/gamma/read:02agg/bn/Const_1:08
{
seg/conv2/weights:0seg/conv2/weights/Assignseg/conv2/weights/read:02.seg/conv2/weights/Initializer/random_uniform:08
n
seg/conv2/biases:0seg/conv2/biases/Assignseg/conv2/biases/read:02$seg/conv2/biases/Initializer/Const:08
r
seg/conv2/bn/beta:0seg/conv2/bn/beta/Assignseg/conv2/bn/beta/read:02%seg/conv2/bn/beta/Initializer/zeros:08
u
seg/conv2/bn/gamma:0seg/conv2/bn/gamma/Assignseg/conv2/bn/gamma/read:02%seg/conv2/bn/gamma/Initializer/ones:08
{
seg/conv3/weights:0seg/conv3/weights/Assignseg/conv3/weights/read:02.seg/conv3/weights/Initializer/random_uniform:08
n
seg/conv3/biases:0seg/conv3/biases/Assignseg/conv3/biases/read:02$seg/conv3/biases/Initializer/Const:08
r
seg/conv3/bn/beta:0seg/conv3/bn/beta/Assignseg/conv3/bn/beta/read:02%seg/conv3/bn/beta/Initializer/zeros:08
u
seg/conv3/bn/gamma:0seg/conv3/bn/gamma/Assignseg/conv3/bn/gamma/read:02%seg/conv3/bn/gamma/Initializer/ones:08
{
seg/conv4/weights:0seg/conv4/weights/Assignseg/conv4/weights/read:02.seg/conv4/weights/Initializer/random_uniform:08
n
seg/conv4/biases:0seg/conv4/biases/Assignseg/conv4/biases/read:02$seg/conv4/biases/Initializer/Const:08
r
seg/conv4/bn/beta:0seg/conv4/bn/beta/Assignseg/conv4/bn/beta/read:02%seg/conv4/bn/beta/Initializer/zeros:08
u
seg/conv4/bn/gamma:0seg/conv4/bn/gamma/Assignseg/conv4/bn/gamma/read:02%seg/conv4/bn/gamma/Initializer/ones:08
{
seg/conv5/weights:0seg/conv5/weights/Assignseg/conv5/weights/read:02.seg/conv5/weights/Initializer/random_uniform:08
n
seg/conv5/biases:0seg/conv5/biases/Assignseg/conv5/biases/read:02$seg/conv5/biases/Initializer/Const:08"ñ¦
	variablesâ¦Þ¦
Ó
)layerfilter0_newfea_conv_head_0/weights:0.layerfilter0_newfea_conv_head_0/weights/Assign.layerfilter0_newfea_conv_head_0/weights/read:02Dlayerfilter0_newfea_conv_head_0/weights/Initializer/random_uniform:08
¹
)layerfilter0_newfea_conv_head_0/bn/beta:0.layerfilter0_newfea_conv_head_0/bn/beta/Assign.layerfilter0_newfea_conv_head_0/bn/beta/read:02*layerfilter0_newfea_conv_head_0/bn/Const:08
¾
*layerfilter0_newfea_conv_head_0/bn/gamma:0/layerfilter0_newfea_conv_head_0/bn/gamma/Assign/layerfilter0_newfea_conv_head_0/bn/gamma/read:02,layerfilter0_newfea_conv_head_0/bn/Const_1:08
å
playerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0ulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Assignulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:02layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
í
rlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0wlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Assignwlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02layerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
¯
 layerfilter0_edgefea_0/weights:0%layerfilter0_edgefea_0/weights/Assign%layerfilter0_edgefea_0/weights/read:02;layerfilter0_edgefea_0/weights/Initializer/random_uniform:08
¢
layerfilter0_edgefea_0/biases:0$layerfilter0_edgefea_0/biases/Assign$layerfilter0_edgefea_0/biases/read:021layerfilter0_edgefea_0/biases/Initializer/Const:08

 layerfilter0_edgefea_0/bn/beta:0%layerfilter0_edgefea_0/bn/beta/Assign%layerfilter0_edgefea_0/bn/beta/read:02!layerfilter0_edgefea_0/bn/Const:08

!layerfilter0_edgefea_0/bn/gamma:0&layerfilter0_edgefea_0/bn/gamma/Assign&layerfilter0_edgefea_0/bn/gamma/read:02#layerfilter0_edgefea_0/bn/Const_1:08

^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage:0clayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/Assignclayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:02playerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
¤
`layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0elayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Assignelayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02rlayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
Û
+layerfilter0_self_att_conv_head_0/weights:00layerfilter0_self_att_conv_head_0/weights/Assign0layerfilter0_self_att_conv_head_0/weights/read:02Flayerfilter0_self_att_conv_head_0/weights/Initializer/random_uniform:08
Î
*layerfilter0_self_att_conv_head_0/biases:0/layerfilter0_self_att_conv_head_0/biases/Assign/layerfilter0_self_att_conv_head_0/biases/read:02<layerfilter0_self_att_conv_head_0/biases/Initializer/Const:08
Á
+layerfilter0_self_att_conv_head_0/bn/beta:00layerfilter0_self_att_conv_head_0/bn/beta/Assign0layerfilter0_self_att_conv_head_0/bn/beta/read:02,layerfilter0_self_att_conv_head_0/bn/Const:08
Æ
,layerfilter0_self_att_conv_head_0/bn/gamma:01layerfilter0_self_att_conv_head_0/bn/gamma/Assign1layerfilter0_self_att_conv_head_0/bn/gamma/read:02.layerfilter0_self_att_conv_head_0/bn/Const_1:08
õ
tlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0ylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Assignylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:02layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
ý
vlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0{layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Assign{layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
Û
+layerfilter0_neib_att_conv_head_0/weights:00layerfilter0_neib_att_conv_head_0/weights/Assign0layerfilter0_neib_att_conv_head_0/weights/read:02Flayerfilter0_neib_att_conv_head_0/weights/Initializer/random_uniform:08
Î
*layerfilter0_neib_att_conv_head_0/biases:0/layerfilter0_neib_att_conv_head_0/biases/Assign/layerfilter0_neib_att_conv_head_0/biases/read:02<layerfilter0_neib_att_conv_head_0/biases/Initializer/Const:08
Á
+layerfilter0_neib_att_conv_head_0/bn/beta:00layerfilter0_neib_att_conv_head_0/bn/beta/Assign0layerfilter0_neib_att_conv_head_0/bn/beta/read:02,layerfilter0_neib_att_conv_head_0/bn/Const:08
Æ
,layerfilter0_neib_att_conv_head_0/bn/gamma:01layerfilter0_neib_att_conv_head_0/bn/gamma/Assign1layerfilter0_neib_att_conv_head_0/bn/gamma/read:02.layerfilter0_neib_att_conv_head_0/bn/Const_1:08
õ
tlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0ylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Assignylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:02layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
ý
vlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0{layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Assign{layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
f
BiasAdd/biases:0BiasAdd/biases/AssignBiasAdd/biases/read:02"BiasAdd/biases/Initializer/zeros:08
w
gapnet00/weights:0gapnet00/weights/Assigngapnet00/weights/read:02-gapnet00/weights/Initializer/random_uniform:08
j
gapnet00/biases:0gapnet00/biases/Assigngapnet00/biases/read:02#gapnet00/biases/Initializer/Const:08
]
gapnet00/bn/beta:0gapnet00/bn/beta/Assigngapnet00/bn/beta/read:02gapnet00/bn/Const:08
b
gapnet00/bn/gamma:0gapnet00/bn/gamma/Assigngapnet00/bn/gamma/read:02gapnet00/bn/Const_1:08
¬
Bgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage:0Ggapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/AssignGgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/read:02Tgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
´
Dgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage:0Igapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignIgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02Vgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
w
gapnet01/weights:0gapnet01/weights/Assigngapnet01/weights/read:02-gapnet01/weights/Initializer/random_uniform:08
j
gapnet01/biases:0gapnet01/biases/Assigngapnet01/biases/read:02#gapnet01/biases/Initializer/Const:08
]
gapnet01/bn/beta:0gapnet01/bn/beta/Assigngapnet01/bn/beta/read:02gapnet01/bn/Const:08
b
gapnet01/bn/gamma:0gapnet01/bn/gamma/Assigngapnet01/bn/gamma/read:02gapnet01/bn/Const_1:08
¬
Bgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage:0Ggapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/AssignGgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/read:02Tgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
´
Dgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage:0Igapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignIgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02Vgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
Ó
)layerfilter1_newfea_conv_head_0/weights:0.layerfilter1_newfea_conv_head_0/weights/Assign.layerfilter1_newfea_conv_head_0/weights/read:02Dlayerfilter1_newfea_conv_head_0/weights/Initializer/random_uniform:08
¹
)layerfilter1_newfea_conv_head_0/bn/beta:0.layerfilter1_newfea_conv_head_0/bn/beta/Assign.layerfilter1_newfea_conv_head_0/bn/beta/read:02*layerfilter1_newfea_conv_head_0/bn/Const:08
¾
*layerfilter1_newfea_conv_head_0/bn/gamma:0/layerfilter1_newfea_conv_head_0/bn/gamma/Assign/layerfilter1_newfea_conv_head_0/bn/gamma/read:02,layerfilter1_newfea_conv_head_0/bn/Const_1:08
å
playerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0ulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Assignulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:02layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
í
rlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0wlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Assignwlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02layerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
¯
 layerfilter1_edgefea_0/weights:0%layerfilter1_edgefea_0/weights/Assign%layerfilter1_edgefea_0/weights/read:02;layerfilter1_edgefea_0/weights/Initializer/random_uniform:08
¢
layerfilter1_edgefea_0/biases:0$layerfilter1_edgefea_0/biases/Assign$layerfilter1_edgefea_0/biases/read:021layerfilter1_edgefea_0/biases/Initializer/Const:08

 layerfilter1_edgefea_0/bn/beta:0%layerfilter1_edgefea_0/bn/beta/Assign%layerfilter1_edgefea_0/bn/beta/read:02!layerfilter1_edgefea_0/bn/Const:08

!layerfilter1_edgefea_0/bn/gamma:0&layerfilter1_edgefea_0/bn/gamma/Assign&layerfilter1_edgefea_0/bn/gamma/read:02#layerfilter1_edgefea_0/bn/Const_1:08

^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage:0clayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/Assignclayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:02playerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
¤
`layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0elayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Assignelayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02rlayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
Û
+layerfilter1_self_att_conv_head_0/weights:00layerfilter1_self_att_conv_head_0/weights/Assign0layerfilter1_self_att_conv_head_0/weights/read:02Flayerfilter1_self_att_conv_head_0/weights/Initializer/random_uniform:08
Î
*layerfilter1_self_att_conv_head_0/biases:0/layerfilter1_self_att_conv_head_0/biases/Assign/layerfilter1_self_att_conv_head_0/biases/read:02<layerfilter1_self_att_conv_head_0/biases/Initializer/Const:08
Á
+layerfilter1_self_att_conv_head_0/bn/beta:00layerfilter1_self_att_conv_head_0/bn/beta/Assign0layerfilter1_self_att_conv_head_0/bn/beta/read:02,layerfilter1_self_att_conv_head_0/bn/Const:08
Æ
,layerfilter1_self_att_conv_head_0/bn/gamma:01layerfilter1_self_att_conv_head_0/bn/gamma/Assign1layerfilter1_self_att_conv_head_0/bn/gamma/read:02.layerfilter1_self_att_conv_head_0/bn/Const_1:08
õ
tlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0ylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Assignylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:02layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
ý
vlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0{layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Assign{layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
Û
+layerfilter1_neib_att_conv_head_0/weights:00layerfilter1_neib_att_conv_head_0/weights/Assign0layerfilter1_neib_att_conv_head_0/weights/read:02Flayerfilter1_neib_att_conv_head_0/weights/Initializer/random_uniform:08
Î
*layerfilter1_neib_att_conv_head_0/biases:0/layerfilter1_neib_att_conv_head_0/biases/Assign/layerfilter1_neib_att_conv_head_0/biases/read:02<layerfilter1_neib_att_conv_head_0/biases/Initializer/Const:08
Á
+layerfilter1_neib_att_conv_head_0/bn/beta:00layerfilter1_neib_att_conv_head_0/bn/beta/Assign0layerfilter1_neib_att_conv_head_0/bn/beta/read:02,layerfilter1_neib_att_conv_head_0/bn/Const:08
Æ
,layerfilter1_neib_att_conv_head_0/bn/gamma:01layerfilter1_neib_att_conv_head_0/bn/gamma/Assign1layerfilter1_neib_att_conv_head_0/bn/gamma/read:02.layerfilter1_neib_att_conv_head_0/bn/Const_1:08
õ
tlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0ylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Assignylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:02layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
ý
vlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0{layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Assign{layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
n
BiasAdd_1/biases:0BiasAdd_1/biases/AssignBiasAdd_1/biases/read:02$BiasAdd_1/biases/Initializer/zeros:08
w
gapnet11/weights:0gapnet11/weights/Assigngapnet11/weights/read:02-gapnet11/weights/Initializer/random_uniform:08
j
gapnet11/biases:0gapnet11/biases/Assigngapnet11/biases/read:02#gapnet11/biases/Initializer/Const:08
]
gapnet11/bn/beta:0gapnet11/bn/beta/Assigngapnet11/bn/beta/read:02gapnet11/bn/Const:08
b
gapnet11/bn/gamma:0gapnet11/bn/gamma/Assigngapnet11/bn/gamma/read:02gapnet11/bn/Const_1:08
¬
Bgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage:0Ggapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/AssignGgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/read:02Tgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
´
Dgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage:0Igapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignIgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02Vgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0

global_expand/weights:0global_expand/weights/Assignglobal_expand/weights/read:022global_expand/weights/Initializer/random_uniform:08
~
global_expand/biases:0global_expand/biases/Assignglobal_expand/biases/read:02(global_expand/biases/Initializer/Const:08
q
global_expand/bn/beta:0global_expand/bn/beta/Assignglobal_expand/bn/beta/read:02global_expand/bn/Const:08
v
global_expand/bn/gamma:0global_expand/bn/gamma/Assignglobal_expand/bn/gamma/read:02global_expand/bn/Const_1:08
Ô
Lglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage:0Qglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/AssignQglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/read:02^global_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0
Ü
Nglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage:0Sglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/AssignSglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02`global_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
c
agg/weights:0agg/weights/Assignagg/weights/read:02(agg/weights/Initializer/random_uniform:08
V
agg/biases:0agg/biases/Assignagg/biases/read:02agg/biases/Initializer/Const:08
I
agg/bn/beta:0agg/bn/beta/Assignagg/bn/beta/read:02agg/bn/Const:08
N
agg/bn/gamma:0agg/bn/gamma/Assignagg/bn/gamma/read:02agg/bn/Const_1:08

8agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage:0=agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/Assign=agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/read:02Jagg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/Initializer/zeros:0

:agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage:0?agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/Assign?agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/read:02Lagg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/Initializer/zeros:0
{
seg/conv2/weights:0seg/conv2/weights/Assignseg/conv2/weights/read:02.seg/conv2/weights/Initializer/random_uniform:08
n
seg/conv2/biases:0seg/conv2/biases/Assignseg/conv2/biases/read:02$seg/conv2/biases/Initializer/Const:08
r
seg/conv2/bn/beta:0seg/conv2/bn/beta/Assignseg/conv2/bn/beta/read:02%seg/conv2/bn/beta/Initializer/zeros:08
u
seg/conv2/bn/gamma:0seg/conv2/bn/gamma/Assignseg/conv2/bn/gamma/read:02%seg/conv2/bn/gamma/Initializer/ones:08

seg/conv2/bn/pop_mean:0seg/conv2/bn/pop_mean/Assignseg/conv2/bn/pop_mean/read:02)seg/conv2/bn/pop_mean/Initializer/zeros:0
{
seg/conv2/bn/pop_var:0seg/conv2/bn/pop_var/Assignseg/conv2/bn/pop_var/read:02'seg/conv2/bn/pop_var/Initializer/ones:0
{
seg/conv3/weights:0seg/conv3/weights/Assignseg/conv3/weights/read:02.seg/conv3/weights/Initializer/random_uniform:08
n
seg/conv3/biases:0seg/conv3/biases/Assignseg/conv3/biases/read:02$seg/conv3/biases/Initializer/Const:08
r
seg/conv3/bn/beta:0seg/conv3/bn/beta/Assignseg/conv3/bn/beta/read:02%seg/conv3/bn/beta/Initializer/zeros:08
u
seg/conv3/bn/gamma:0seg/conv3/bn/gamma/Assignseg/conv3/bn/gamma/read:02%seg/conv3/bn/gamma/Initializer/ones:08

seg/conv3/bn/pop_mean:0seg/conv3/bn/pop_mean/Assignseg/conv3/bn/pop_mean/read:02)seg/conv3/bn/pop_mean/Initializer/zeros:0
{
seg/conv3/bn/pop_var:0seg/conv3/bn/pop_var/Assignseg/conv3/bn/pop_var/read:02'seg/conv3/bn/pop_var/Initializer/ones:0
{
seg/conv4/weights:0seg/conv4/weights/Assignseg/conv4/weights/read:02.seg/conv4/weights/Initializer/random_uniform:08
n
seg/conv4/biases:0seg/conv4/biases/Assignseg/conv4/biases/read:02$seg/conv4/biases/Initializer/Const:08
r
seg/conv4/bn/beta:0seg/conv4/bn/beta/Assignseg/conv4/bn/beta/read:02%seg/conv4/bn/beta/Initializer/zeros:08
u
seg/conv4/bn/gamma:0seg/conv4/bn/gamma/Assignseg/conv4/bn/gamma/read:02%seg/conv4/bn/gamma/Initializer/ones:08

seg/conv4/bn/pop_mean:0seg/conv4/bn/pop_mean/Assignseg/conv4/bn/pop_mean/read:02)seg/conv4/bn/pop_mean/Initializer/zeros:0
{
seg/conv4/bn/pop_var:0seg/conv4/bn/pop_var/Assignseg/conv4/bn/pop_var/read:02'seg/conv4/bn/pop_var/Initializer/ones:0
{
seg/conv5/weights:0seg/conv5/weights/Assignseg/conv5/weights/read:02.seg/conv5/weights/Initializer/random_uniform:08
n
seg/conv5/biases:0seg/conv5/biases/Assignseg/conv5/biases/read:02$seg/conv5/biases/Initializer/Const:08"ô
lossesé
æ
-layerfilter0_newfea_conv_head_0/weight_loss:0
$layerfilter0_edgefea_0/weight_loss:0
/layerfilter0_self_att_conv_head_0/weight_loss:0
/layerfilter0_neib_att_conv_head_0/weight_loss:0
gapnet00/weight_loss:0
gapnet01/weight_loss:0
-layerfilter1_newfea_conv_head_0/weight_loss:0
$layerfilter1_edgefea_0/weight_loss:0
/layerfilter1_self_att_conv_head_0/weight_loss:0
/layerfilter1_neib_att_conv_head_0/weight_loss:0
gapnet11/weight_loss:0
global_expand/weight_loss:0
agg/weight_loss:0"Ñ
cond_context¿»

1layerfilter0_newfea_conv_head_0/bn/cond/cond_text1layerfilter0_newfea_conv_head_0/bn/cond/pred_id:02layerfilter0_newfea_conv_head_0/bn/cond/switch_t:0 *ó
Ylayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Vlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Xlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Vlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
_layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
alayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Xlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
Rlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
[layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Xlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
Zlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Xlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
alayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
clayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Zlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Tlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
Hlayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/decay:0
<layerfilter0_newfea_conv_head_0/bn/cond/control_dependency:0
1layerfilter0_newfea_conv_head_0/bn/cond/pred_id:0
2layerfilter0_newfea_conv_head_0/bn/cond/switch_t:0
ulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
playerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0
wlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
rlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0
4layerfilter0_newfea_conv_head_0/bn/moments/Squeeze:0
6layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1:0f
1layerfilter0_newfea_conv_head_0/bn/cond/pred_id:01layerfilter0_newfea_conv_head_0/bn/cond/pred_id:0Ø
ulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0_layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
4layerfilter0_newfea_conv_head_0/bn/moments/Squeeze:0alayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1Í
playerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0Ylayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Ü
wlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0alayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
6layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1:0clayerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1Ñ
rlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0[layerfilter0_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
®
3layerfilter0_newfea_conv_head_0/bn/cond/cond_text_11layerfilter0_newfea_conv_head_0/bn/cond/pred_id:02layerfilter0_newfea_conv_head_0/bn/cond/switch_f:0*
>layerfilter0_newfea_conv_head_0/bn/cond/control_dependency_1:0
1layerfilter0_newfea_conv_head_0/bn/cond/pred_id:0
2layerfilter0_newfea_conv_head_0/bn/cond/switch_f:0f
1layerfilter0_newfea_conv_head_0/bn/cond/pred_id:01layerfilter0_newfea_conv_head_0/bn/cond/pred_id:0
Â
3layerfilter0_newfea_conv_head_0/bn/cond_1/cond_text3layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id:04layerfilter0_newfea_conv_head_0/bn/cond_1/switch_t:0 *
;layerfilter0_newfea_conv_head_0/bn/cond_1/Identity/Switch:1
4layerfilter0_newfea_conv_head_0/bn/cond_1/Identity:0
=layerfilter0_newfea_conv_head_0/bn/cond_1/Identity_1/Switch:1
6layerfilter0_newfea_conv_head_0/bn/cond_1/Identity_1:0
3layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id:0
4layerfilter0_newfea_conv_head_0/bn/cond_1/switch_t:0
4layerfilter0_newfea_conv_head_0/bn/moments/Squeeze:0
6layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1:0j
3layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id:03layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id:0s
4layerfilter0_newfea_conv_head_0/bn/moments/Squeeze:0;layerfilter0_newfea_conv_head_0/bn/cond_1/Identity/Switch:1w
6layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1:0=layerfilter0_newfea_conv_head_0/bn/cond_1/Identity_1/Switch:1
¦	
5layerfilter0_newfea_conv_head_0/bn/cond_1/cond_text_13layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id:04layerfilter0_newfea_conv_head_0/bn/cond_1/switch_f:0*
4layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_1:0
4layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_1:1
4layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_2:0
4layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_2:1
3layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id:0
4layerfilter0_newfea_conv_head_0/bn/cond_1/switch_f:0
ulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
wlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0j
3layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id:03layerfilter0_newfea_conv_head_0/bn/cond_1/pred_id:0­
ulayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:04layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_1:0¯
wlayerfilter0_newfea_conv_head_0/bn/layerfilter0_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:04layerfilter0_newfea_conv_head_0/bn/cond_1/Switch_2:0
Ç
(layerfilter0_edgefea_0/bn/cond/cond_text(layerfilter0_edgefea_0/bn/cond/pred_id:0)layerfilter0_edgefea_0/bn/cond/switch_t:0 *Ã
Playerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Mlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Olayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Mlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
Vlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
Xlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Olayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
Ilayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
Rlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Olayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
Qlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Olayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
Xlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
Zlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Qlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Klayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
?layerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/decay:0
3layerfilter0_edgefea_0/bn/cond/control_dependency:0
(layerfilter0_edgefea_0/bn/cond/pred_id:0
)layerfilter0_edgefea_0/bn/cond/switch_t:0
clayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage:0
elayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
`layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0
+layerfilter0_edgefea_0/bn/moments/Squeeze:0
-layerfilter0_edgefea_0/bn/moments/Squeeze_1:0T
(layerfilter0_edgefea_0/bn/cond/pred_id:0(layerfilter0_edgefea_0/bn/cond/pred_id:0½
clayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0Vlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
+layerfilter0_edgefea_0/bn/moments/Squeeze:0Xlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1²
^layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage:0Playerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Á
elayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0Xlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
-layerfilter0_edgefea_0/bn/moments/Squeeze_1:0Zlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1¶
`layerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0Rlayerfilter0_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
æ
*layerfilter0_edgefea_0/bn/cond/cond_text_1(layerfilter0_edgefea_0/bn/cond/pred_id:0)layerfilter0_edgefea_0/bn/cond/switch_f:0*â
5layerfilter0_edgefea_0/bn/cond/control_dependency_1:0
(layerfilter0_edgefea_0/bn/cond/pred_id:0
)layerfilter0_edgefea_0/bn/cond/switch_f:0T
(layerfilter0_edgefea_0/bn/cond/pred_id:0(layerfilter0_edgefea_0/bn/cond/pred_id:0
©
*layerfilter0_edgefea_0/bn/cond_1/cond_text*layerfilter0_edgefea_0/bn/cond_1/pred_id:0+layerfilter0_edgefea_0/bn/cond_1/switch_t:0 *
2layerfilter0_edgefea_0/bn/cond_1/Identity/Switch:1
+layerfilter0_edgefea_0/bn/cond_1/Identity:0
4layerfilter0_edgefea_0/bn/cond_1/Identity_1/Switch:1
-layerfilter0_edgefea_0/bn/cond_1/Identity_1:0
*layerfilter0_edgefea_0/bn/cond_1/pred_id:0
+layerfilter0_edgefea_0/bn/cond_1/switch_t:0
+layerfilter0_edgefea_0/bn/moments/Squeeze:0
-layerfilter0_edgefea_0/bn/moments/Squeeze_1:0X
*layerfilter0_edgefea_0/bn/cond_1/pred_id:0*layerfilter0_edgefea_0/bn/cond_1/pred_id:0a
+layerfilter0_edgefea_0/bn/moments/Squeeze:02layerfilter0_edgefea_0/bn/cond_1/Identity/Switch:1e
-layerfilter0_edgefea_0/bn/moments/Squeeze_1:04layerfilter0_edgefea_0/bn/cond_1/Identity_1/Switch:1
é
,layerfilter0_edgefea_0/bn/cond_1/cond_text_1*layerfilter0_edgefea_0/bn/cond_1/pred_id:0+layerfilter0_edgefea_0/bn/cond_1/switch_f:0*ß
+layerfilter0_edgefea_0/bn/cond_1/Switch_1:0
+layerfilter0_edgefea_0/bn/cond_1/Switch_1:1
+layerfilter0_edgefea_0/bn/cond_1/Switch_2:0
+layerfilter0_edgefea_0/bn/cond_1/Switch_2:1
*layerfilter0_edgefea_0/bn/cond_1/pred_id:0
+layerfilter0_edgefea_0/bn/cond_1/switch_f:0
clayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
elayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0X
*layerfilter0_edgefea_0/bn/cond_1/pred_id:0*layerfilter0_edgefea_0/bn/cond_1/pred_id:0
clayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0+layerfilter0_edgefea_0/bn/cond_1/Switch_1:0
elayerfilter0_edgefea_0/bn/layerfilter0_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0+layerfilter0_edgefea_0/bn/cond_1/Switch_2:0
ø
3layerfilter0_self_att_conv_head_0/bn/cond/cond_text3layerfilter0_self_att_conv_head_0/bn/cond/pred_id:04layerfilter0_self_att_conv_head_0/bn/cond/switch_t:0 *Ó
[layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Xlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Zlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Xlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
alayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
clayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Zlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
Tlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
]layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Zlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
\layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Zlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
clayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
elayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
\layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Vlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
Jlayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay:0
>layerfilter0_self_att_conv_head_0/bn/cond/control_dependency:0
3layerfilter0_self_att_conv_head_0/bn/cond/pred_id:0
4layerfilter0_self_att_conv_head_0/bn/cond/switch_t:0
ylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
tlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0
{layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
vlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0
6layerfilter0_self_att_conv_head_0/bn/moments/Squeeze:0
8layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1:0j
3layerfilter0_self_att_conv_head_0/bn/cond/pred_id:03layerfilter0_self_att_conv_head_0/bn/cond/pred_id:0Þ
ylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0alayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
6layerfilter0_self_att_conv_head_0/bn/moments/Squeeze:0clayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1Ó
tlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0[layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1â
{layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0clayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1¡
8layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1:0elayerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1×
vlayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0]layerfilter0_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
¾
5layerfilter0_self_att_conv_head_0/bn/cond/cond_text_13layerfilter0_self_att_conv_head_0/bn/cond/pred_id:04layerfilter0_self_att_conv_head_0/bn/cond/switch_f:0*
@layerfilter0_self_att_conv_head_0/bn/cond/control_dependency_1:0
3layerfilter0_self_att_conv_head_0/bn/cond/pred_id:0
4layerfilter0_self_att_conv_head_0/bn/cond/switch_f:0j
3layerfilter0_self_att_conv_head_0/bn/cond/pred_id:03layerfilter0_self_att_conv_head_0/bn/cond/pred_id:0
ä
5layerfilter0_self_att_conv_head_0/bn/cond_1/cond_text5layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id:06layerfilter0_self_att_conv_head_0/bn/cond_1/switch_t:0 *¹
=layerfilter0_self_att_conv_head_0/bn/cond_1/Identity/Switch:1
6layerfilter0_self_att_conv_head_0/bn/cond_1/Identity:0
?layerfilter0_self_att_conv_head_0/bn/cond_1/Identity_1/Switch:1
8layerfilter0_self_att_conv_head_0/bn/cond_1/Identity_1:0
5layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id:0
6layerfilter0_self_att_conv_head_0/bn/cond_1/switch_t:0
6layerfilter0_self_att_conv_head_0/bn/moments/Squeeze:0
8layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1:0n
5layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id:05layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id:0w
6layerfilter0_self_att_conv_head_0/bn/moments/Squeeze:0=layerfilter0_self_att_conv_head_0/bn/cond_1/Identity/Switch:1{
8layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1:0?layerfilter0_self_att_conv_head_0/bn/cond_1/Identity_1/Switch:1
Ð	
7layerfilter0_self_att_conv_head_0/bn/cond_1/cond_text_15layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id:06layerfilter0_self_att_conv_head_0/bn/cond_1/switch_f:0*¥
6layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_1:0
6layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_1:1
6layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_2:0
6layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_2:1
5layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id:0
6layerfilter0_self_att_conv_head_0/bn/cond_1/switch_f:0
ylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
{layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0n
5layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id:05layerfilter0_self_att_conv_head_0/bn/cond_1/pred_id:0³
ylayerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:06layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_1:0µ
{layerfilter0_self_att_conv_head_0/bn/layerfilter0_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:06layerfilter0_self_att_conv_head_0/bn/cond_1/Switch_2:0
ø
3layerfilter0_neib_att_conv_head_0/bn/cond/cond_text3layerfilter0_neib_att_conv_head_0/bn/cond/pred_id:04layerfilter0_neib_att_conv_head_0/bn/cond/switch_t:0 *Ó
[layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Xlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Zlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Xlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
alayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
clayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Zlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
Tlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
]layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Zlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
\layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Zlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
clayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
elayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
\layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Vlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
Jlayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay:0
>layerfilter0_neib_att_conv_head_0/bn/cond/control_dependency:0
3layerfilter0_neib_att_conv_head_0/bn/cond/pred_id:0
4layerfilter0_neib_att_conv_head_0/bn/cond/switch_t:0
ylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
tlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0
{layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
vlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0
6layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze:0
8layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1:0j
3layerfilter0_neib_att_conv_head_0/bn/cond/pred_id:03layerfilter0_neib_att_conv_head_0/bn/cond/pred_id:0Þ
ylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0alayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
6layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze:0clayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1Ó
tlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0[layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1â
{layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0clayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1¡
8layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1:0elayerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1×
vlayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0]layerfilter0_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
¾
5layerfilter0_neib_att_conv_head_0/bn/cond/cond_text_13layerfilter0_neib_att_conv_head_0/bn/cond/pred_id:04layerfilter0_neib_att_conv_head_0/bn/cond/switch_f:0*
@layerfilter0_neib_att_conv_head_0/bn/cond/control_dependency_1:0
3layerfilter0_neib_att_conv_head_0/bn/cond/pred_id:0
4layerfilter0_neib_att_conv_head_0/bn/cond/switch_f:0j
3layerfilter0_neib_att_conv_head_0/bn/cond/pred_id:03layerfilter0_neib_att_conv_head_0/bn/cond/pred_id:0
ä
5layerfilter0_neib_att_conv_head_0/bn/cond_1/cond_text5layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id:06layerfilter0_neib_att_conv_head_0/bn/cond_1/switch_t:0 *¹
=layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity/Switch:1
6layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity:0
?layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity_1/Switch:1
8layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity_1:0
5layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id:0
6layerfilter0_neib_att_conv_head_0/bn/cond_1/switch_t:0
6layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze:0
8layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1:0n
5layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id:05layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id:0w
6layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze:0=layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity/Switch:1{
8layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1:0?layerfilter0_neib_att_conv_head_0/bn/cond_1/Identity_1/Switch:1
Ð	
7layerfilter0_neib_att_conv_head_0/bn/cond_1/cond_text_15layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id:06layerfilter0_neib_att_conv_head_0/bn/cond_1/switch_f:0*¥
6layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_1:0
6layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_1:1
6layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_2:0
6layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_2:1
5layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id:0
6layerfilter0_neib_att_conv_head_0/bn/cond_1/switch_f:0
ylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
{layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0n
5layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id:05layerfilter0_neib_att_conv_head_0/bn/cond_1/pred_id:0³
ylayerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:06layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_1:0µ
{layerfilter0_neib_att_conv_head_0/bn/layerfilter0_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:06layerfilter0_neib_att_conv_head_0/bn/cond_1/Switch_2:0
û
gapnet00/bn/cond/cond_textgapnet00/bn/cond/pred_id:0gapnet00/bn/cond/switch_t:0 *¡
Bgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Agapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
?gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
Hgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
Jgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Agapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
;gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
Dgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Agapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
Cgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Agapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
Jgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
Lgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Cgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
=gapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
1gapnet00/bn/cond/ExponentialMovingAverage/decay:0
%gapnet00/bn/cond/control_dependency:0
gapnet00/bn/cond/pred_id:0
gapnet00/bn/cond/switch_t:0
Ggapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/read:0
Bgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage:0
Igapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
Dgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage:0
gapnet00/bn/moments/Squeeze:0
gapnet00/bn/moments/Squeeze_1:08
gapnet00/bn/cond/pred_id:0gapnet00/bn/cond/pred_id:0
Ggapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/read:0Hgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1k
gapnet00/bn/moments/Squeeze:0Jgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Bgapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage:0Bgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Igapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0Jgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1o
gapnet00/bn/moments/Squeeze_1:0Lgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Dgapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage:0Dgapnet00/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
ö
gapnet00/bn/cond/cond_text_1gapnet00/bn/cond/pred_id:0gapnet00/bn/cond/switch_f:0*
'gapnet00/bn/cond/control_dependency_1:0
gapnet00/bn/cond/pred_id:0
gapnet00/bn/cond/switch_f:08
gapnet00/bn/cond/pred_id:0gapnet00/bn/cond/pred_id:0
»
gapnet00/bn/cond_1/cond_textgapnet00/bn/cond_1/pred_id:0gapnet00/bn/cond_1/switch_t:0 *Û
$gapnet00/bn/cond_1/Identity/Switch:1
gapnet00/bn/cond_1/Identity:0
&gapnet00/bn/cond_1/Identity_1/Switch:1
gapnet00/bn/cond_1/Identity_1:0
gapnet00/bn/cond_1/pred_id:0
gapnet00/bn/cond_1/switch_t:0
gapnet00/bn/moments/Squeeze:0
gapnet00/bn/moments/Squeeze_1:0<
gapnet00/bn/cond_1/pred_id:0gapnet00/bn/cond_1/pred_id:0E
gapnet00/bn/moments/Squeeze:0$gapnet00/bn/cond_1/Identity/Switch:1I
gapnet00/bn/moments/Squeeze_1:0&gapnet00/bn/cond_1/Identity_1/Switch:1
Á
gapnet00/bn/cond_1/cond_text_1gapnet00/bn/cond_1/pred_id:0gapnet00/bn/cond_1/switch_f:0*á
gapnet00/bn/cond_1/Switch_1:0
gapnet00/bn/cond_1/Switch_1:1
gapnet00/bn/cond_1/Switch_2:0
gapnet00/bn/cond_1/Switch_2:1
gapnet00/bn/cond_1/pred_id:0
gapnet00/bn/cond_1/switch_f:0
Ggapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/read:0
Igapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0<
gapnet00/bn/cond_1/pred_id:0gapnet00/bn/cond_1/pred_id:0h
Ggapnet00/bn/gapnet00/bn/moments/Squeeze/ExponentialMovingAverage/read:0gapnet00/bn/cond_1/Switch_1:0j
Igapnet00/bn/gapnet00/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0gapnet00/bn/cond_1/Switch_2:0
û
gapnet01/bn/cond/cond_textgapnet01/bn/cond/pred_id:0gapnet01/bn/cond/switch_t:0 *¡
Bgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Agapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
?gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
Hgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
Jgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Agapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
;gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
Dgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Agapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
Cgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Agapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
Jgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
Lgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Cgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
=gapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
1gapnet01/bn/cond/ExponentialMovingAverage/decay:0
%gapnet01/bn/cond/control_dependency:0
gapnet01/bn/cond/pred_id:0
gapnet01/bn/cond/switch_t:0
Ggapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/read:0
Bgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage:0
Igapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
Dgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage:0
gapnet01/bn/moments/Squeeze:0
gapnet01/bn/moments/Squeeze_1:08
gapnet01/bn/cond/pred_id:0gapnet01/bn/cond/pred_id:0
Ggapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/read:0Hgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1k
gapnet01/bn/moments/Squeeze:0Jgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Bgapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage:0Bgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Igapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0Jgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1o
gapnet01/bn/moments/Squeeze_1:0Lgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Dgapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage:0Dgapnet01/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
ö
gapnet01/bn/cond/cond_text_1gapnet01/bn/cond/pred_id:0gapnet01/bn/cond/switch_f:0*
'gapnet01/bn/cond/control_dependency_1:0
gapnet01/bn/cond/pred_id:0
gapnet01/bn/cond/switch_f:08
gapnet01/bn/cond/pred_id:0gapnet01/bn/cond/pred_id:0
»
gapnet01/bn/cond_1/cond_textgapnet01/bn/cond_1/pred_id:0gapnet01/bn/cond_1/switch_t:0 *Û
$gapnet01/bn/cond_1/Identity/Switch:1
gapnet01/bn/cond_1/Identity:0
&gapnet01/bn/cond_1/Identity_1/Switch:1
gapnet01/bn/cond_1/Identity_1:0
gapnet01/bn/cond_1/pred_id:0
gapnet01/bn/cond_1/switch_t:0
gapnet01/bn/moments/Squeeze:0
gapnet01/bn/moments/Squeeze_1:0<
gapnet01/bn/cond_1/pred_id:0gapnet01/bn/cond_1/pred_id:0E
gapnet01/bn/moments/Squeeze:0$gapnet01/bn/cond_1/Identity/Switch:1I
gapnet01/bn/moments/Squeeze_1:0&gapnet01/bn/cond_1/Identity_1/Switch:1
Á
gapnet01/bn/cond_1/cond_text_1gapnet01/bn/cond_1/pred_id:0gapnet01/bn/cond_1/switch_f:0*á
gapnet01/bn/cond_1/Switch_1:0
gapnet01/bn/cond_1/Switch_1:1
gapnet01/bn/cond_1/Switch_2:0
gapnet01/bn/cond_1/Switch_2:1
gapnet01/bn/cond_1/pred_id:0
gapnet01/bn/cond_1/switch_f:0
Ggapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/read:0
Igapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0<
gapnet01/bn/cond_1/pred_id:0gapnet01/bn/cond_1/pred_id:0h
Ggapnet01/bn/gapnet01/bn/moments/Squeeze/ExponentialMovingAverage/read:0gapnet01/bn/cond_1/Switch_1:0j
Igapnet01/bn/gapnet01/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0gapnet01/bn/cond_1/Switch_2:0

1layerfilter1_newfea_conv_head_0/bn/cond/cond_text1layerfilter1_newfea_conv_head_0/bn/cond/pred_id:02layerfilter1_newfea_conv_head_0/bn/cond/switch_t:0 *ó
Ylayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Vlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Xlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Vlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
_layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
alayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Xlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
Rlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
[layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Xlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
Zlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Xlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
alayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
clayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Zlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Tlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
Hlayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/decay:0
<layerfilter1_newfea_conv_head_0/bn/cond/control_dependency:0
1layerfilter1_newfea_conv_head_0/bn/cond/pred_id:0
2layerfilter1_newfea_conv_head_0/bn/cond/switch_t:0
ulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
playerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0
wlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
rlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0
4layerfilter1_newfea_conv_head_0/bn/moments/Squeeze:0
6layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1:0f
1layerfilter1_newfea_conv_head_0/bn/cond/pred_id:01layerfilter1_newfea_conv_head_0/bn/cond/pred_id:0Ø
ulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0_layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
4layerfilter1_newfea_conv_head_0/bn/moments/Squeeze:0alayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1Í
playerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0Ylayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Ü
wlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0alayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
6layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1:0clayerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1Ñ
rlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0[layerfilter1_newfea_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
®
3layerfilter1_newfea_conv_head_0/bn/cond/cond_text_11layerfilter1_newfea_conv_head_0/bn/cond/pred_id:02layerfilter1_newfea_conv_head_0/bn/cond/switch_f:0*
>layerfilter1_newfea_conv_head_0/bn/cond/control_dependency_1:0
1layerfilter1_newfea_conv_head_0/bn/cond/pred_id:0
2layerfilter1_newfea_conv_head_0/bn/cond/switch_f:0f
1layerfilter1_newfea_conv_head_0/bn/cond/pred_id:01layerfilter1_newfea_conv_head_0/bn/cond/pred_id:0
Â
3layerfilter1_newfea_conv_head_0/bn/cond_1/cond_text3layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id:04layerfilter1_newfea_conv_head_0/bn/cond_1/switch_t:0 *
;layerfilter1_newfea_conv_head_0/bn/cond_1/Identity/Switch:1
4layerfilter1_newfea_conv_head_0/bn/cond_1/Identity:0
=layerfilter1_newfea_conv_head_0/bn/cond_1/Identity_1/Switch:1
6layerfilter1_newfea_conv_head_0/bn/cond_1/Identity_1:0
3layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id:0
4layerfilter1_newfea_conv_head_0/bn/cond_1/switch_t:0
4layerfilter1_newfea_conv_head_0/bn/moments/Squeeze:0
6layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1:0j
3layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id:03layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id:0s
4layerfilter1_newfea_conv_head_0/bn/moments/Squeeze:0;layerfilter1_newfea_conv_head_0/bn/cond_1/Identity/Switch:1w
6layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1:0=layerfilter1_newfea_conv_head_0/bn/cond_1/Identity_1/Switch:1
¦	
5layerfilter1_newfea_conv_head_0/bn/cond_1/cond_text_13layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id:04layerfilter1_newfea_conv_head_0/bn/cond_1/switch_f:0*
4layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_1:0
4layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_1:1
4layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_2:0
4layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_2:1
3layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id:0
4layerfilter1_newfea_conv_head_0/bn/cond_1/switch_f:0
ulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
wlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0j
3layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id:03layerfilter1_newfea_conv_head_0/bn/cond_1/pred_id:0­
ulayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:04layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_1:0¯
wlayerfilter1_newfea_conv_head_0/bn/layerfilter1_newfea_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:04layerfilter1_newfea_conv_head_0/bn/cond_1/Switch_2:0
Ç
(layerfilter1_edgefea_0/bn/cond/cond_text(layerfilter1_edgefea_0/bn/cond/pred_id:0)layerfilter1_edgefea_0/bn/cond/switch_t:0 *Ã
Playerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Mlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Olayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Mlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
Vlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
Xlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Olayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
Ilayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
Rlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Olayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
Qlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Olayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
Xlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
Zlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Qlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Klayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
?layerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/decay:0
3layerfilter1_edgefea_0/bn/cond/control_dependency:0
(layerfilter1_edgefea_0/bn/cond/pred_id:0
)layerfilter1_edgefea_0/bn/cond/switch_t:0
clayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage:0
elayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
`layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0
+layerfilter1_edgefea_0/bn/moments/Squeeze:0
-layerfilter1_edgefea_0/bn/moments/Squeeze_1:0T
(layerfilter1_edgefea_0/bn/cond/pred_id:0(layerfilter1_edgefea_0/bn/cond/pred_id:0½
clayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0Vlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
+layerfilter1_edgefea_0/bn/moments/Squeeze:0Xlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1²
^layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage:0Playerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1Á
elayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0Xlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
-layerfilter1_edgefea_0/bn/moments/Squeeze_1:0Zlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1¶
`layerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0Rlayerfilter1_edgefea_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
æ
*layerfilter1_edgefea_0/bn/cond/cond_text_1(layerfilter1_edgefea_0/bn/cond/pred_id:0)layerfilter1_edgefea_0/bn/cond/switch_f:0*â
5layerfilter1_edgefea_0/bn/cond/control_dependency_1:0
(layerfilter1_edgefea_0/bn/cond/pred_id:0
)layerfilter1_edgefea_0/bn/cond/switch_f:0T
(layerfilter1_edgefea_0/bn/cond/pred_id:0(layerfilter1_edgefea_0/bn/cond/pred_id:0
©
*layerfilter1_edgefea_0/bn/cond_1/cond_text*layerfilter1_edgefea_0/bn/cond_1/pred_id:0+layerfilter1_edgefea_0/bn/cond_1/switch_t:0 *
2layerfilter1_edgefea_0/bn/cond_1/Identity/Switch:1
+layerfilter1_edgefea_0/bn/cond_1/Identity:0
4layerfilter1_edgefea_0/bn/cond_1/Identity_1/Switch:1
-layerfilter1_edgefea_0/bn/cond_1/Identity_1:0
*layerfilter1_edgefea_0/bn/cond_1/pred_id:0
+layerfilter1_edgefea_0/bn/cond_1/switch_t:0
+layerfilter1_edgefea_0/bn/moments/Squeeze:0
-layerfilter1_edgefea_0/bn/moments/Squeeze_1:0X
*layerfilter1_edgefea_0/bn/cond_1/pred_id:0*layerfilter1_edgefea_0/bn/cond_1/pred_id:0a
+layerfilter1_edgefea_0/bn/moments/Squeeze:02layerfilter1_edgefea_0/bn/cond_1/Identity/Switch:1e
-layerfilter1_edgefea_0/bn/moments/Squeeze_1:04layerfilter1_edgefea_0/bn/cond_1/Identity_1/Switch:1
é
,layerfilter1_edgefea_0/bn/cond_1/cond_text_1*layerfilter1_edgefea_0/bn/cond_1/pred_id:0+layerfilter1_edgefea_0/bn/cond_1/switch_f:0*ß
+layerfilter1_edgefea_0/bn/cond_1/Switch_1:0
+layerfilter1_edgefea_0/bn/cond_1/Switch_1:1
+layerfilter1_edgefea_0/bn/cond_1/Switch_2:0
+layerfilter1_edgefea_0/bn/cond_1/Switch_2:1
*layerfilter1_edgefea_0/bn/cond_1/pred_id:0
+layerfilter1_edgefea_0/bn/cond_1/switch_f:0
clayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
elayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0X
*layerfilter1_edgefea_0/bn/cond_1/pred_id:0*layerfilter1_edgefea_0/bn/cond_1/pred_id:0
clayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0+layerfilter1_edgefea_0/bn/cond_1/Switch_1:0
elayerfilter1_edgefea_0/bn/layerfilter1_edgefea_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0+layerfilter1_edgefea_0/bn/cond_1/Switch_2:0
ø
3layerfilter1_self_att_conv_head_0/bn/cond/cond_text3layerfilter1_self_att_conv_head_0/bn/cond/pred_id:04layerfilter1_self_att_conv_head_0/bn/cond/switch_t:0 *Ó
[layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Xlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Zlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Xlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
alayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
clayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Zlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
Tlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
]layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Zlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
\layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Zlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
clayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
elayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
\layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Vlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
Jlayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay:0
>layerfilter1_self_att_conv_head_0/bn/cond/control_dependency:0
3layerfilter1_self_att_conv_head_0/bn/cond/pred_id:0
4layerfilter1_self_att_conv_head_0/bn/cond/switch_t:0
ylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
tlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0
{layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
vlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0
6layerfilter1_self_att_conv_head_0/bn/moments/Squeeze:0
8layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1:0j
3layerfilter1_self_att_conv_head_0/bn/cond/pred_id:03layerfilter1_self_att_conv_head_0/bn/cond/pred_id:0Þ
ylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0alayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
6layerfilter1_self_att_conv_head_0/bn/moments/Squeeze:0clayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1Ó
tlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0[layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1â
{layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0clayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1¡
8layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1:0elayerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1×
vlayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0]layerfilter1_self_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
¾
5layerfilter1_self_att_conv_head_0/bn/cond/cond_text_13layerfilter1_self_att_conv_head_0/bn/cond/pred_id:04layerfilter1_self_att_conv_head_0/bn/cond/switch_f:0*
@layerfilter1_self_att_conv_head_0/bn/cond/control_dependency_1:0
3layerfilter1_self_att_conv_head_0/bn/cond/pred_id:0
4layerfilter1_self_att_conv_head_0/bn/cond/switch_f:0j
3layerfilter1_self_att_conv_head_0/bn/cond/pred_id:03layerfilter1_self_att_conv_head_0/bn/cond/pred_id:0
ä
5layerfilter1_self_att_conv_head_0/bn/cond_1/cond_text5layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id:06layerfilter1_self_att_conv_head_0/bn/cond_1/switch_t:0 *¹
=layerfilter1_self_att_conv_head_0/bn/cond_1/Identity/Switch:1
6layerfilter1_self_att_conv_head_0/bn/cond_1/Identity:0
?layerfilter1_self_att_conv_head_0/bn/cond_1/Identity_1/Switch:1
8layerfilter1_self_att_conv_head_0/bn/cond_1/Identity_1:0
5layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id:0
6layerfilter1_self_att_conv_head_0/bn/cond_1/switch_t:0
6layerfilter1_self_att_conv_head_0/bn/moments/Squeeze:0
8layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1:0n
5layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id:05layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id:0w
6layerfilter1_self_att_conv_head_0/bn/moments/Squeeze:0=layerfilter1_self_att_conv_head_0/bn/cond_1/Identity/Switch:1{
8layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1:0?layerfilter1_self_att_conv_head_0/bn/cond_1/Identity_1/Switch:1
Ð	
7layerfilter1_self_att_conv_head_0/bn/cond_1/cond_text_15layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id:06layerfilter1_self_att_conv_head_0/bn/cond_1/switch_f:0*¥
6layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_1:0
6layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_1:1
6layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_2:0
6layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_2:1
5layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id:0
6layerfilter1_self_att_conv_head_0/bn/cond_1/switch_f:0
ylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
{layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0n
5layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id:05layerfilter1_self_att_conv_head_0/bn/cond_1/pred_id:0³
ylayerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:06layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_1:0µ
{layerfilter1_self_att_conv_head_0/bn/layerfilter1_self_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:06layerfilter1_self_att_conv_head_0/bn/cond_1/Switch_2:0
ø
3layerfilter1_neib_att_conv_head_0/bn/cond/cond_text3layerfilter1_neib_att_conv_head_0/bn/cond/pred_id:04layerfilter1_neib_att_conv_head_0/bn/cond/switch_t:0 *Ó
[layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Xlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Zlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Xlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
alayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
clayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Zlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
Tlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
]layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Zlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
\layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Zlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
clayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
elayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
\layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Vlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
Jlayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/decay:0
>layerfilter1_neib_att_conv_head_0/bn/cond/control_dependency:0
3layerfilter1_neib_att_conv_head_0/bn/cond/pred_id:0
4layerfilter1_neib_att_conv_head_0/bn/cond/switch_t:0
ylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
tlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0
{layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
vlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0
6layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze:0
8layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1:0j
3layerfilter1_neib_att_conv_head_0/bn/cond/pred_id:03layerfilter1_neib_att_conv_head_0/bn/cond/pred_id:0Þ
ylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0alayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
6layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze:0clayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1Ó
tlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage:0[layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1â
{layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0clayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1¡
8layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1:0elayerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1×
vlayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage:0]layerfilter1_neib_att_conv_head_0/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
¾
5layerfilter1_neib_att_conv_head_0/bn/cond/cond_text_13layerfilter1_neib_att_conv_head_0/bn/cond/pred_id:04layerfilter1_neib_att_conv_head_0/bn/cond/switch_f:0*
@layerfilter1_neib_att_conv_head_0/bn/cond/control_dependency_1:0
3layerfilter1_neib_att_conv_head_0/bn/cond/pred_id:0
4layerfilter1_neib_att_conv_head_0/bn/cond/switch_f:0j
3layerfilter1_neib_att_conv_head_0/bn/cond/pred_id:03layerfilter1_neib_att_conv_head_0/bn/cond/pred_id:0
ä
5layerfilter1_neib_att_conv_head_0/bn/cond_1/cond_text5layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id:06layerfilter1_neib_att_conv_head_0/bn/cond_1/switch_t:0 *¹
=layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity/Switch:1
6layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity:0
?layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity_1/Switch:1
8layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity_1:0
5layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id:0
6layerfilter1_neib_att_conv_head_0/bn/cond_1/switch_t:0
6layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze:0
8layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1:0n
5layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id:05layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id:0w
6layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze:0=layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity/Switch:1{
8layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1:0?layerfilter1_neib_att_conv_head_0/bn/cond_1/Identity_1/Switch:1
Ð	
7layerfilter1_neib_att_conv_head_0/bn/cond_1/cond_text_15layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id:06layerfilter1_neib_att_conv_head_0/bn/cond_1/switch_f:0*¥
6layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_1:0
6layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_1:1
6layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_2:0
6layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_2:1
5layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id:0
6layerfilter1_neib_att_conv_head_0/bn/cond_1/switch_f:0
ylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:0
{layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0n
5layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id:05layerfilter1_neib_att_conv_head_0/bn/cond_1/pred_id:0³
ylayerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze/ExponentialMovingAverage/read:06layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_1:0µ
{layerfilter1_neib_att_conv_head_0/bn/layerfilter1_neib_att_conv_head_0/bn/moments/Squeeze_1/ExponentialMovingAverage/read:06layerfilter1_neib_att_conv_head_0/bn/cond_1/Switch_2:0
û
gapnet11/bn/cond/cond_textgapnet11/bn/cond/pred_id:0gapnet11/bn/cond/switch_t:0 *¡
Bgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Agapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
?gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
Hgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
Jgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Agapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
;gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
Dgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Agapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
Cgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Agapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
Jgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
Lgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Cgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
=gapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
1gapnet11/bn/cond/ExponentialMovingAverage/decay:0
%gapnet11/bn/cond/control_dependency:0
gapnet11/bn/cond/pred_id:0
gapnet11/bn/cond/switch_t:0
Ggapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/read:0
Bgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage:0
Igapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
Dgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage:0
gapnet11/bn/moments/Squeeze:0
gapnet11/bn/moments/Squeeze_1:08
gapnet11/bn/cond/pred_id:0gapnet11/bn/cond/pred_id:0
Ggapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/read:0Hgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1k
gapnet11/bn/moments/Squeeze:0Jgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Bgapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage:0Bgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Igapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0Jgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1o
gapnet11/bn/moments/Squeeze_1:0Lgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Dgapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage:0Dgapnet11/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
ö
gapnet11/bn/cond/cond_text_1gapnet11/bn/cond/pred_id:0gapnet11/bn/cond/switch_f:0*
'gapnet11/bn/cond/control_dependency_1:0
gapnet11/bn/cond/pred_id:0
gapnet11/bn/cond/switch_f:08
gapnet11/bn/cond/pred_id:0gapnet11/bn/cond/pred_id:0
»
gapnet11/bn/cond_1/cond_textgapnet11/bn/cond_1/pred_id:0gapnet11/bn/cond_1/switch_t:0 *Û
$gapnet11/bn/cond_1/Identity/Switch:1
gapnet11/bn/cond_1/Identity:0
&gapnet11/bn/cond_1/Identity_1/Switch:1
gapnet11/bn/cond_1/Identity_1:0
gapnet11/bn/cond_1/pred_id:0
gapnet11/bn/cond_1/switch_t:0
gapnet11/bn/moments/Squeeze:0
gapnet11/bn/moments/Squeeze_1:0<
gapnet11/bn/cond_1/pred_id:0gapnet11/bn/cond_1/pred_id:0E
gapnet11/bn/moments/Squeeze:0$gapnet11/bn/cond_1/Identity/Switch:1I
gapnet11/bn/moments/Squeeze_1:0&gapnet11/bn/cond_1/Identity_1/Switch:1
Á
gapnet11/bn/cond_1/cond_text_1gapnet11/bn/cond_1/pred_id:0gapnet11/bn/cond_1/switch_f:0*á
gapnet11/bn/cond_1/Switch_1:0
gapnet11/bn/cond_1/Switch_1:1
gapnet11/bn/cond_1/Switch_2:0
gapnet11/bn/cond_1/Switch_2:1
gapnet11/bn/cond_1/pred_id:0
gapnet11/bn/cond_1/switch_f:0
Ggapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/read:0
Igapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0<
gapnet11/bn/cond_1/pred_id:0gapnet11/bn/cond_1/pred_id:0h
Ggapnet11/bn/gapnet11/bn/moments/Squeeze/ExponentialMovingAverage/read:0gapnet11/bn/cond_1/Switch_1:0j
Igapnet11/bn/gapnet11/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0gapnet11/bn/cond_1/Switch_2:0
ú
global_expand/bn/cond/cond_textglobal_expand/bn/cond/pred_id:0 global_expand/bn/cond/switch_t:0 *
Gglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
Dglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
Fglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
Dglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
Mglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
Oglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Fglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
@global_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
Iglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Fglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
Hglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
Fglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
Oglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
Qglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Hglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
Bglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
6global_expand/bn/cond/ExponentialMovingAverage/decay:0
*global_expand/bn/cond/control_dependency:0
global_expand/bn/cond/pred_id:0
 global_expand/bn/cond/switch_t:0
Qglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/read:0
Lglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage:0
Sglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
Nglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage:0
"global_expand/bn/moments/Squeeze:0
$global_expand/bn/moments/Squeeze_1:0B
global_expand/bn/cond/pred_id:0global_expand/bn/cond/pred_id:0¢
Qglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/read:0Mglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1u
"global_expand/bn/moments/Squeeze:0Oglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
Lglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage:0Gglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1¦
Sglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0Oglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1y
$global_expand/bn/moments/Squeeze_1:0Qglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
Nglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage:0Iglobal_expand/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1

!global_expand/bn/cond/cond_text_1global_expand/bn/cond/pred_id:0 global_expand/bn/cond/switch_f:0*µ
,global_expand/bn/cond/control_dependency_1:0
global_expand/bn/cond/pred_id:0
 global_expand/bn/cond/switch_f:0B
global_expand/bn/cond/pred_id:0global_expand/bn/cond/pred_id:0

!global_expand/bn/cond_1/cond_text!global_expand/bn/cond_1/pred_id:0"global_expand/bn/cond_1/switch_t:0 *¡
)global_expand/bn/cond_1/Identity/Switch:1
"global_expand/bn/cond_1/Identity:0
+global_expand/bn/cond_1/Identity_1/Switch:1
$global_expand/bn/cond_1/Identity_1:0
!global_expand/bn/cond_1/pred_id:0
"global_expand/bn/cond_1/switch_t:0
"global_expand/bn/moments/Squeeze:0
$global_expand/bn/moments/Squeeze_1:0F
!global_expand/bn/cond_1/pred_id:0!global_expand/bn/cond_1/pred_id:0O
"global_expand/bn/moments/Squeeze:0)global_expand/bn/cond_1/Identity/Switch:1S
$global_expand/bn/moments/Squeeze_1:0+global_expand/bn/cond_1/Identity_1/Switch:1
ª
#global_expand/bn/cond_1/cond_text_1!global_expand/bn/cond_1/pred_id:0"global_expand/bn/cond_1/switch_f:0*»
"global_expand/bn/cond_1/Switch_1:0
"global_expand/bn/cond_1/Switch_1:1
"global_expand/bn/cond_1/Switch_2:0
"global_expand/bn/cond_1/Switch_2:1
!global_expand/bn/cond_1/pred_id:0
"global_expand/bn/cond_1/switch_f:0
Qglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/read:0
Sglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0F
!global_expand/bn/cond_1/pred_id:0!global_expand/bn/cond_1/pred_id:0w
Qglobal_expand/bn/global_expand/bn/moments/Squeeze/ExponentialMovingAverage/read:0"global_expand/bn/cond_1/Switch_1:0y
Sglobal_expand/bn/global_expand/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0"global_expand/bn/cond_1/Switch_2:0
ú
agg/bn/cond/cond_textagg/bn/cond/pred_id:0agg/bn/cond/switch_t:0 *¯
=agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/read:0
8agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage:0
?agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
:agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage:0
=agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/mul:0
<agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub/x:0
:agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub:0
Cagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1
Eagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1
<agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1:0
6agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg:0
?agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
<agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/mul:0
>agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub/x:0
<agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub:0
Eagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1
Gagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1
>agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1:0
8agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1:0
,agg/bn/cond/ExponentialMovingAverage/decay:0
 agg/bn/cond/control_dependency:0
agg/bn/cond/pred_id:0
agg/bn/cond/switch_t:0
agg/bn/moments/Squeeze:0
agg/bn/moments/Squeeze_1:0.
agg/bn/cond/pred_id:0agg/bn/cond/pred_id:0
=agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/read:0Cagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch:1a
agg/bn/moments/Squeeze:0Eagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/sub_1/Switch_1:1y
8agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage:0=agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg/Switch:1
?agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0Eagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch:1e
agg/bn/moments/Squeeze_1:0Gagg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/sub_1/Switch_1:1}
:agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage:0?agg/bn/cond/ExponentialMovingAverage/AssignMovingAvg_1/Switch:1
Î
agg/bn/cond/cond_text_1agg/bn/cond/pred_id:0agg/bn/cond/switch_f:0*
"agg/bn/cond/control_dependency_1:0
agg/bn/cond/pred_id:0
agg/bn/cond/switch_f:0.
agg/bn/cond/pred_id:0agg/bn/cond/pred_id:0
æ
agg/bn/cond_1/cond_textagg/bn/cond_1/pred_id:0agg/bn/cond_1/switch_t:0 *
agg/bn/cond_1/Identity/Switch:1
agg/bn/cond_1/Identity:0
!agg/bn/cond_1/Identity_1/Switch:1
agg/bn/cond_1/Identity_1:0
agg/bn/cond_1/pred_id:0
agg/bn/cond_1/switch_t:0
agg/bn/moments/Squeeze:0
agg/bn/moments/Squeeze_1:02
agg/bn/cond_1/pred_id:0agg/bn/cond_1/pred_id:0;
agg/bn/moments/Squeeze:0agg/bn/cond_1/Identity/Switch:1?
agg/bn/moments/Squeeze_1:0!agg/bn/cond_1/Identity_1/Switch:1
Ø
agg/bn/cond_1/cond_text_1agg/bn/cond_1/pred_id:0agg/bn/cond_1/switch_f:0*
=agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/read:0
?agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0
agg/bn/cond_1/Switch_1:0
agg/bn/cond_1/Switch_1:1
agg/bn/cond_1/Switch_2:0
agg/bn/cond_1/Switch_2:1
agg/bn/cond_1/pred_id:0
agg/bn/cond_1/switch_f:02
agg/bn/cond_1/pred_id:0agg/bn/cond_1/pred_id:0Y
=agg/bn/agg/bn/moments/Squeeze/ExponentialMovingAverage/read:0agg/bn/cond_1/Switch_1:0[
?agg/bn/agg/bn/moments/Squeeze_1/ExponentialMovingAverage/read:0agg/bn/cond_1/Switch_2:0
¡
seg/conv2/bn/cond/cond_textseg/conv2/bn/cond/pred_id:0seg/conv2/bn/cond/switch_t:0 *Ä
seg/conv2/BiasAdd:0
seg/conv2/bn/beta/read:0
!seg/conv2/bn/cond/Assign/Switch:1
seg/conv2/bn/cond/Assign:0
#seg/conv2/bn/cond/Assign_1/Switch:1
seg/conv2/bn/cond/Assign_1:0
seg/conv2/bn/cond/add:0
seg/conv2/bn/cond/add_1:0
#seg/conv2/bn/cond/batchnorm/Rsqrt:0
#seg/conv2/bn/cond/batchnorm/add/y:0
!seg/conv2/bn/cond/batchnorm/add:0
#seg/conv2/bn/cond/batchnorm/add_1:0
(seg/conv2/bn/cond/batchnorm/mul/Switch:1
!seg/conv2/bn/cond/batchnorm/mul:0
#seg/conv2/bn/cond/batchnorm/mul_1:0
#seg/conv2/bn/cond/batchnorm/mul_2:0
(seg/conv2/bn/cond/batchnorm/sub/Switch:1
!seg/conv2/bn/cond/batchnorm/sub:0
-seg/conv2/bn/cond/moments/SquaredDifference:0
#seg/conv2/bn/cond/moments/Squeeze:0
%seg/conv2/bn/cond/moments/Squeeze_1:0
(seg/conv2/bn/cond/moments/StopGradient:0
'seg/conv2/bn/cond/moments/mean/Switch:1
2seg/conv2/bn/cond/moments/mean/reduction_indices:0
 seg/conv2/bn/cond/moments/mean:0
6seg/conv2/bn/cond/moments/variance/reduction_indices:0
$seg/conv2/bn/cond/moments/variance:0
seg/conv2/bn/cond/mul/Switch:1
seg/conv2/bn/cond/mul/y:0
seg/conv2/bn/cond/mul:0
seg/conv2/bn/cond/mul_1/y:0
seg/conv2/bn/cond/mul_1:0
 seg/conv2/bn/cond/mul_2/Switch:1
seg/conv2/bn/cond/mul_2/y:0
seg/conv2/bn/cond/mul_2:0
seg/conv2/bn/cond/mul_3/y:0
seg/conv2/bn/cond/mul_3:0
seg/conv2/bn/cond/pred_id:0
seg/conv2/bn/cond/switch_t:0
seg/conv2/bn/gamma/read:0
seg/conv2/bn/pop_mean/read:0
seg/conv2/bn/pop_mean:0
seg/conv2/bn/pop_var/read:0
seg/conv2/bn/pop_var:0:
seg/conv2/bn/cond/pred_id:0seg/conv2/bn/cond/pred_id:0>
seg/conv2/BiasAdd:0'seg/conv2/bn/cond/moments/mean/Switch:1>
seg/conv2/bn/pop_mean/read:0seg/conv2/bn/cond/mul/Switch:1<
seg/conv2/bn/pop_mean:0!seg/conv2/bn/cond/Assign/Switch:1?
seg/conv2/bn/pop_var/read:0 seg/conv2/bn/cond/mul_2/Switch:1=
seg/conv2/bn/pop_var:0#seg/conv2/bn/cond/Assign_1/Switch:1E
seg/conv2/bn/gamma/read:0(seg/conv2/bn/cond/batchnorm/mul/Switch:1D
seg/conv2/bn/beta/read:0(seg/conv2/bn/cond/batchnorm/sub/Switch:1
Ú	
seg/conv2/bn/cond/cond_text_1seg/conv2/bn/cond/pred_id:0seg/conv2/bn/cond/switch_f:0*ý
seg/conv2/BiasAdd:0
seg/conv2/bn/beta/read:0
%seg/conv2/bn/cond/batchnorm_1/Rsqrt:0
*seg/conv2/bn/cond/batchnorm_1/add/Switch:0
%seg/conv2/bn/cond/batchnorm_1/add/y:0
#seg/conv2/bn/cond/batchnorm_1/add:0
%seg/conv2/bn/cond/batchnorm_1/add_1:0
*seg/conv2/bn/cond/batchnorm_1/mul/Switch:0
#seg/conv2/bn/cond/batchnorm_1/mul:0
,seg/conv2/bn/cond/batchnorm_1/mul_1/Switch:0
%seg/conv2/bn/cond/batchnorm_1/mul_1:0
,seg/conv2/bn/cond/batchnorm_1/mul_2/Switch:0
%seg/conv2/bn/cond/batchnorm_1/mul_2:0
*seg/conv2/bn/cond/batchnorm_1/sub/Switch:0
#seg/conv2/bn/cond/batchnorm_1/sub:0
seg/conv2/bn/cond/pred_id:0
seg/conv2/bn/cond/switch_f:0
seg/conv2/bn/gamma/read:0
seg/conv2/bn/pop_mean/read:0
seg/conv2/bn/pop_var/read:0:
seg/conv2/bn/cond/pred_id:0seg/conv2/bn/cond/pred_id:0I
seg/conv2/bn/pop_var/read:0*seg/conv2/bn/cond/batchnorm_1/add/Switch:0G
seg/conv2/bn/gamma/read:0*seg/conv2/bn/cond/batchnorm_1/mul/Switch:0C
seg/conv2/BiasAdd:0,seg/conv2/bn/cond/batchnorm_1/mul_1/Switch:0L
seg/conv2/bn/pop_mean/read:0,seg/conv2/bn/cond/batchnorm_1/mul_2/Switch:0F
seg/conv2/bn/beta/read:0*seg/conv2/bn/cond/batchnorm_1/sub/Switch:0
Ø
seg/dp1/cond/cond_textseg/dp1/cond/pred_id:0seg/dp1/cond/switch_t:0 *
seg/conv2/Relu:0
seg/dp1/cond/dropout/Cast:0
#seg/dp1/cond/dropout/GreaterEqual:0
seg/dp1/cond/dropout/Shape:0
!seg/dp1/cond/dropout/mul/Switch:1
seg/dp1/cond/dropout/mul:0
seg/dp1/cond/dropout/mul_1:0
3seg/dp1/cond/dropout/random_uniform/RandomUniform:0
)seg/dp1/cond/dropout/random_uniform/max:0
)seg/dp1/cond/dropout/random_uniform/min:0
)seg/dp1/cond/dropout/random_uniform/mul:0
)seg/dp1/cond/dropout/random_uniform/sub:0
%seg/dp1/cond/dropout/random_uniform:0
seg/dp1/cond/dropout/rate:0
seg/dp1/cond/dropout/sub/x:0
seg/dp1/cond/dropout/sub:0
 seg/dp1/cond/dropout/truediv/x:0
seg/dp1/cond/dropout/truediv:0
seg/dp1/cond/pred_id:0
seg/dp1/cond/switch_t:00
seg/dp1/cond/pred_id:0seg/dp1/cond/pred_id:05
seg/conv2/Relu:0!seg/dp1/cond/dropout/mul/Switch:1
¢
seg/dp1/cond/cond_text_1seg/dp1/cond/pred_id:0seg/dp1/cond/switch_f:0*Ô
seg/conv2/Relu:0
seg/dp1/cond/Switch_1:0
seg/dp1/cond/Switch_1:1
seg/dp1/cond/pred_id:0
seg/dp1/cond/switch_f:00
seg/dp1/cond/pred_id:0seg/dp1/cond/pred_id:0+
seg/conv2/Relu:0seg/dp1/cond/Switch_1:0
¡
seg/conv3/bn/cond/cond_textseg/conv3/bn/cond/pred_id:0seg/conv3/bn/cond/switch_t:0 *Ä
seg/conv3/BiasAdd:0
seg/conv3/bn/beta/read:0
!seg/conv3/bn/cond/Assign/Switch:1
seg/conv3/bn/cond/Assign:0
#seg/conv3/bn/cond/Assign_1/Switch:1
seg/conv3/bn/cond/Assign_1:0
seg/conv3/bn/cond/add:0
seg/conv3/bn/cond/add_1:0
#seg/conv3/bn/cond/batchnorm/Rsqrt:0
#seg/conv3/bn/cond/batchnorm/add/y:0
!seg/conv3/bn/cond/batchnorm/add:0
#seg/conv3/bn/cond/batchnorm/add_1:0
(seg/conv3/bn/cond/batchnorm/mul/Switch:1
!seg/conv3/bn/cond/batchnorm/mul:0
#seg/conv3/bn/cond/batchnorm/mul_1:0
#seg/conv3/bn/cond/batchnorm/mul_2:0
(seg/conv3/bn/cond/batchnorm/sub/Switch:1
!seg/conv3/bn/cond/batchnorm/sub:0
-seg/conv3/bn/cond/moments/SquaredDifference:0
#seg/conv3/bn/cond/moments/Squeeze:0
%seg/conv3/bn/cond/moments/Squeeze_1:0
(seg/conv3/bn/cond/moments/StopGradient:0
'seg/conv3/bn/cond/moments/mean/Switch:1
2seg/conv3/bn/cond/moments/mean/reduction_indices:0
 seg/conv3/bn/cond/moments/mean:0
6seg/conv3/bn/cond/moments/variance/reduction_indices:0
$seg/conv3/bn/cond/moments/variance:0
seg/conv3/bn/cond/mul/Switch:1
seg/conv3/bn/cond/mul/y:0
seg/conv3/bn/cond/mul:0
seg/conv3/bn/cond/mul_1/y:0
seg/conv3/bn/cond/mul_1:0
 seg/conv3/bn/cond/mul_2/Switch:1
seg/conv3/bn/cond/mul_2/y:0
seg/conv3/bn/cond/mul_2:0
seg/conv3/bn/cond/mul_3/y:0
seg/conv3/bn/cond/mul_3:0
seg/conv3/bn/cond/pred_id:0
seg/conv3/bn/cond/switch_t:0
seg/conv3/bn/gamma/read:0
seg/conv3/bn/pop_mean/read:0
seg/conv3/bn/pop_mean:0
seg/conv3/bn/pop_var/read:0
seg/conv3/bn/pop_var:0:
seg/conv3/bn/cond/pred_id:0seg/conv3/bn/cond/pred_id:0>
seg/conv3/BiasAdd:0'seg/conv3/bn/cond/moments/mean/Switch:1>
seg/conv3/bn/pop_mean/read:0seg/conv3/bn/cond/mul/Switch:1<
seg/conv3/bn/pop_mean:0!seg/conv3/bn/cond/Assign/Switch:1?
seg/conv3/bn/pop_var/read:0 seg/conv3/bn/cond/mul_2/Switch:1=
seg/conv3/bn/pop_var:0#seg/conv3/bn/cond/Assign_1/Switch:1E
seg/conv3/bn/gamma/read:0(seg/conv3/bn/cond/batchnorm/mul/Switch:1D
seg/conv3/bn/beta/read:0(seg/conv3/bn/cond/batchnorm/sub/Switch:1
Ú	
seg/conv3/bn/cond/cond_text_1seg/conv3/bn/cond/pred_id:0seg/conv3/bn/cond/switch_f:0*ý
seg/conv3/BiasAdd:0
seg/conv3/bn/beta/read:0
%seg/conv3/bn/cond/batchnorm_1/Rsqrt:0
*seg/conv3/bn/cond/batchnorm_1/add/Switch:0
%seg/conv3/bn/cond/batchnorm_1/add/y:0
#seg/conv3/bn/cond/batchnorm_1/add:0
%seg/conv3/bn/cond/batchnorm_1/add_1:0
*seg/conv3/bn/cond/batchnorm_1/mul/Switch:0
#seg/conv3/bn/cond/batchnorm_1/mul:0
,seg/conv3/bn/cond/batchnorm_1/mul_1/Switch:0
%seg/conv3/bn/cond/batchnorm_1/mul_1:0
,seg/conv3/bn/cond/batchnorm_1/mul_2/Switch:0
%seg/conv3/bn/cond/batchnorm_1/mul_2:0
*seg/conv3/bn/cond/batchnorm_1/sub/Switch:0
#seg/conv3/bn/cond/batchnorm_1/sub:0
seg/conv3/bn/cond/pred_id:0
seg/conv3/bn/cond/switch_f:0
seg/conv3/bn/gamma/read:0
seg/conv3/bn/pop_mean/read:0
seg/conv3/bn/pop_var/read:0:
seg/conv3/bn/cond/pred_id:0seg/conv3/bn/cond/pred_id:0I
seg/conv3/bn/pop_var/read:0*seg/conv3/bn/cond/batchnorm_1/add/Switch:0G
seg/conv3/bn/gamma/read:0*seg/conv3/bn/cond/batchnorm_1/mul/Switch:0C
seg/conv3/BiasAdd:0,seg/conv3/bn/cond/batchnorm_1/mul_1/Switch:0L
seg/conv3/bn/pop_mean/read:0,seg/conv3/bn/cond/batchnorm_1/mul_2/Switch:0F
seg/conv3/bn/beta/read:0*seg/conv3/bn/cond/batchnorm_1/sub/Switch:0

seg/dp1_1/cond/cond_textseg/dp1_1/cond/pred_id:0seg/dp1_1/cond/switch_t:0 *¶
seg/conv3/Relu:0
seg/dp1_1/cond/dropout/Cast:0
%seg/dp1_1/cond/dropout/GreaterEqual:0
seg/dp1_1/cond/dropout/Shape:0
#seg/dp1_1/cond/dropout/mul/Switch:1
seg/dp1_1/cond/dropout/mul:0
seg/dp1_1/cond/dropout/mul_1:0
5seg/dp1_1/cond/dropout/random_uniform/RandomUniform:0
+seg/dp1_1/cond/dropout/random_uniform/max:0
+seg/dp1_1/cond/dropout/random_uniform/min:0
+seg/dp1_1/cond/dropout/random_uniform/mul:0
+seg/dp1_1/cond/dropout/random_uniform/sub:0
'seg/dp1_1/cond/dropout/random_uniform:0
seg/dp1_1/cond/dropout/rate:0
seg/dp1_1/cond/dropout/sub/x:0
seg/dp1_1/cond/dropout/sub:0
"seg/dp1_1/cond/dropout/truediv/x:0
 seg/dp1_1/cond/dropout/truediv:0
seg/dp1_1/cond/pred_id:0
seg/dp1_1/cond/switch_t:04
seg/dp1_1/cond/pred_id:0seg/dp1_1/cond/pred_id:07
seg/conv3/Relu:0#seg/dp1_1/cond/dropout/mul/Switch:1
¶
seg/dp1_1/cond/cond_text_1seg/dp1_1/cond/pred_id:0seg/dp1_1/cond/switch_f:0*â
seg/conv3/Relu:0
seg/dp1_1/cond/Switch_1:0
seg/dp1_1/cond/Switch_1:1
seg/dp1_1/cond/pred_id:0
seg/dp1_1/cond/switch_f:04
seg/dp1_1/cond/pred_id:0seg/dp1_1/cond/pred_id:0-
seg/conv3/Relu:0seg/dp1_1/cond/Switch_1:0
¡
seg/conv4/bn/cond/cond_textseg/conv4/bn/cond/pred_id:0seg/conv4/bn/cond/switch_t:0 *Ä
seg/conv4/BiasAdd:0
seg/conv4/bn/beta/read:0
!seg/conv4/bn/cond/Assign/Switch:1
seg/conv4/bn/cond/Assign:0
#seg/conv4/bn/cond/Assign_1/Switch:1
seg/conv4/bn/cond/Assign_1:0
seg/conv4/bn/cond/add:0
seg/conv4/bn/cond/add_1:0
#seg/conv4/bn/cond/batchnorm/Rsqrt:0
#seg/conv4/bn/cond/batchnorm/add/y:0
!seg/conv4/bn/cond/batchnorm/add:0
#seg/conv4/bn/cond/batchnorm/add_1:0
(seg/conv4/bn/cond/batchnorm/mul/Switch:1
!seg/conv4/bn/cond/batchnorm/mul:0
#seg/conv4/bn/cond/batchnorm/mul_1:0
#seg/conv4/bn/cond/batchnorm/mul_2:0
(seg/conv4/bn/cond/batchnorm/sub/Switch:1
!seg/conv4/bn/cond/batchnorm/sub:0
-seg/conv4/bn/cond/moments/SquaredDifference:0
#seg/conv4/bn/cond/moments/Squeeze:0
%seg/conv4/bn/cond/moments/Squeeze_1:0
(seg/conv4/bn/cond/moments/StopGradient:0
'seg/conv4/bn/cond/moments/mean/Switch:1
2seg/conv4/bn/cond/moments/mean/reduction_indices:0
 seg/conv4/bn/cond/moments/mean:0
6seg/conv4/bn/cond/moments/variance/reduction_indices:0
$seg/conv4/bn/cond/moments/variance:0
seg/conv4/bn/cond/mul/Switch:1
seg/conv4/bn/cond/mul/y:0
seg/conv4/bn/cond/mul:0
seg/conv4/bn/cond/mul_1/y:0
seg/conv4/bn/cond/mul_1:0
 seg/conv4/bn/cond/mul_2/Switch:1
seg/conv4/bn/cond/mul_2/y:0
seg/conv4/bn/cond/mul_2:0
seg/conv4/bn/cond/mul_3/y:0
seg/conv4/bn/cond/mul_3:0
seg/conv4/bn/cond/pred_id:0
seg/conv4/bn/cond/switch_t:0
seg/conv4/bn/gamma/read:0
seg/conv4/bn/pop_mean/read:0
seg/conv4/bn/pop_mean:0
seg/conv4/bn/pop_var/read:0
seg/conv4/bn/pop_var:0:
seg/conv4/bn/cond/pred_id:0seg/conv4/bn/cond/pred_id:0>
seg/conv4/BiasAdd:0'seg/conv4/bn/cond/moments/mean/Switch:1>
seg/conv4/bn/pop_mean/read:0seg/conv4/bn/cond/mul/Switch:1<
seg/conv4/bn/pop_mean:0!seg/conv4/bn/cond/Assign/Switch:1?
seg/conv4/bn/pop_var/read:0 seg/conv4/bn/cond/mul_2/Switch:1=
seg/conv4/bn/pop_var:0#seg/conv4/bn/cond/Assign_1/Switch:1E
seg/conv4/bn/gamma/read:0(seg/conv4/bn/cond/batchnorm/mul/Switch:1D
seg/conv4/bn/beta/read:0(seg/conv4/bn/cond/batchnorm/sub/Switch:1
Ú	
seg/conv4/bn/cond/cond_text_1seg/conv4/bn/cond/pred_id:0seg/conv4/bn/cond/switch_f:0*ý
seg/conv4/BiasAdd:0
seg/conv4/bn/beta/read:0
%seg/conv4/bn/cond/batchnorm_1/Rsqrt:0
*seg/conv4/bn/cond/batchnorm_1/add/Switch:0
%seg/conv4/bn/cond/batchnorm_1/add/y:0
#seg/conv4/bn/cond/batchnorm_1/add:0
%seg/conv4/bn/cond/batchnorm_1/add_1:0
*seg/conv4/bn/cond/batchnorm_1/mul/Switch:0
#seg/conv4/bn/cond/batchnorm_1/mul:0
,seg/conv4/bn/cond/batchnorm_1/mul_1/Switch:0
%seg/conv4/bn/cond/batchnorm_1/mul_1:0
,seg/conv4/bn/cond/batchnorm_1/mul_2/Switch:0
%seg/conv4/bn/cond/batchnorm_1/mul_2:0
*seg/conv4/bn/cond/batchnorm_1/sub/Switch:0
#seg/conv4/bn/cond/batchnorm_1/sub:0
seg/conv4/bn/cond/pred_id:0
seg/conv4/bn/cond/switch_f:0
seg/conv4/bn/gamma/read:0
seg/conv4/bn/pop_mean/read:0
seg/conv4/bn/pop_var/read:0:
seg/conv4/bn/cond/pred_id:0seg/conv4/bn/cond/pred_id:0I
seg/conv4/bn/pop_var/read:0*seg/conv4/bn/cond/batchnorm_1/add/Switch:0G
seg/conv4/bn/gamma/read:0*seg/conv4/bn/cond/batchnorm_1/mul/Switch:0C
seg/conv4/BiasAdd:0,seg/conv4/bn/cond/batchnorm_1/mul_1/Switch:0L
seg/conv4/bn/pop_mean/read:0,seg/conv4/bn/cond/batchnorm_1/mul_2/Switch:0F
seg/conv4/bn/beta/read:0*seg/conv4/bn/cond/batchnorm_1/sub/Switch:0
Ø
cond/cond_textcond/pred_id:0cond/switch_t:0 *¢
cond/Switch_1:0
cond/Switch_1:1
cond/pred_id:0
cond/switch_t:0
seg/conv5/BiasAdd:0 
cond/pred_id:0cond/pred_id:0&
seg/conv5/BiasAdd:0cond/Switch_1:1
ã
cond/cond_text_1cond/pred_id:0cond/switch_f:0*­
cond/Softmax/Switch:0
cond/Softmax:0
cond/pred_id:0
cond/switch_f:0
seg/conv5/BiasAdd:0 
cond/pred_id:0cond/pred_id:0,
seg/conv5/BiasAdd:0cond/Softmax/Switch:0"ï
model_variablesÛØ
f
BiasAdd/biases:0BiasAdd/biases/AssignBiasAdd/biases/read:02"BiasAdd/biases/Initializer/zeros:08
n
BiasAdd_1/biases:0BiasAdd_1/biases/AssignBiasAdd_1/biases/read:02$BiasAdd_1/biases/Initializer/zeros:08