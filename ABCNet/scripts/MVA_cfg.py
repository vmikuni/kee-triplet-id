from array import array
import numpy as np
import ROOT

histFillColor =['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#a65628','#f781bf','#999999','#ffff33']
def type_todtype(typ):
    if 'f' in typ.lower():
        return np.float32
    if 'i' in typ.lower():
        return np.int32

class ManageTTree:
    'Manage TTree branches from set of tvarlist_'
    def __init__(self,tvarlist_,tree):
        self.variables={}
        self.tree = tree
        for var in tvarlist_:
            #self.variables[var]=array(tvarlist_[var][1].lower(),tvarlist_[var][0]*[-100])
            self.variables[var]=np.array(tvarlist_[var][0]*[-100],dtype=type_todtype(tvarlist_[var][1]))
            self.tree.Branch(var,self.variables[var],var+'['+str(tvarlist_[var][0])+']/'+tvarlist_[var][1])

    def ZeroArray(self):
        for var in self.variables:
            for dummy in range(len(self.variables[var])):
                self.variables[var][dummy] = -10
    def Print(self,varlist=[]):
        for var in self.variables:
            if len(varlist)>0:
                if var not in varlist:
                    continue
            else:
                for dummy in range(len(self.variables[var])):
                    print(var, self.variables[var][dummy])
        print ('################################################################')
    def HaveBranch(self,bname):
        return hasattr(self.tree,bname)


tvars = {
    'pid': [1,'I'],
    'DNN': [1,'F'],
}
