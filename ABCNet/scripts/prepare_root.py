import pandas as pd
import h5py
import os, sys
#import pyjet as fj
import numpy as np
import ROOT
import copy
import MVA_cfg
from sklearn import metrics
import matplotlib.pyplot as plt
from scipy.stats import poisson
from optparse import OptionParser




parser = OptionParser(usage="%prog [opt]  inputFiles")

parser.add_option("-f","--file", type="string", default='BToKEE_charge', help="Specify the file to load. Default: %default")
parser.add_option("--folder", type="string", default='../h5', help="Specify the folder to load. Default: %default")
parser.add_option("--plot", type="string", default='../plots', help="Folder to store plots. Default: %default")
parser.add_option("--root", type="string", default='../root', help="Folder to store root files. Default: %default")
parser.add_option("-n", "--evtmax", type="long", default=-1, help="Number of events to save. Default: %default")
parser.add_option("--make_roc",  action="store_true",  default=False, help="Only makes the ROC curve. Default:%default")
parser.add_option("--auto",  action="store_true",  default=False, help="Automatic extraction of variables from h5 file. Default:%default")
(opt, args) = parser.parse_args()

autom_varlist = True #creates the root files with the same list of variables from h5 file

sample = '{0}.h5'.format(opt.file)

evtmax = opt.evtmax

def Make_root(data,NEVTS):
    for event in range(NEVTS):
        if event > evtmax and evtmax >0: break
        if event %10000==0: print('Processing event: ',event)
        for var in varlist.keys():
            if varlist[var][0]>1:
                if var in add_vars:
                    if analysis == 'KEE':
                        dict_var = {
                            'pt' : np.exp(data['data'][event,:,2]),
                            'eta' : data['data'][event,:,0],
                            'phi' : data['data'][event,:,1],
                            'mass' : data['data'][event,:,3],
                            'isElectron' : data['data'][event,:,6],
                            'charge' : data['data'][event,:,7],
                            'DNN' : data['data'][event,:,-1],
                        }
                    else:
                        dict_var = {
                            'pt' : np.exp(data['data'][event,:,2]),
                            'eta' : data['data'][event,:,0],
                            'phi' : data['data'][event,:,1],
                            'e' : np.exp(data['data'][event,:,3]),
                            'charge' : data['data'][event,:,4],
                            'ismuon' : data['data'][event,:,7],
                            
                        }
                    mytree.variables[var][:]=np.array(dict_var[var],dtype=MVA_cfg.type_todtype(varlist[var][1]))
                else:
                    mytree.variables[var][:]=np.array(data[var][event][:],dtype=MVA_cfg.type_todtype(varlist[var][1]))
                # for ipc in range(varlist[var][0]):                        
                #     mytree.variables[var][ipc]=data[var][event][ipc]                        
            else:
                #if 'qg' in var:continue
                mytree.variables[var][0]=data[var][event]
                
        tkin.Fill()
        mytree.ZeroArray()

def FindAnalysis(name):
    if 'Jpsi' in name:
        analysis = 'Jpsi'
    elif 'Kee' in name:
        analysis = 'KEE'
    elif 'Bs' in name:
        analysis = 'Bs'
    else:
        sys.exit("ERROR:No specific analysis found for file given")
    return analysis

f = h5py.File(os.path.join(opt.folder,sample),'r')

analysis = FindAnalysis(opt.file)
print("Analysis type: ",analysis)
tkin =  ROOT.TTree("tree","tree")


if opt.auto:
    vars = list(f.keys())
    varlist = {}
    for var in vars:
        if 'data' in var: 
            add_vars = ['pt','phi','eta','charge','DNN']
            varlist['pt'] = [f['data'].shape[1],'F']
            varlist['phi'] = [f['data'].shape[1],'F']
            varlist['eta'] = [f['data'].shape[1],'F']
            varlist['charge'] = [f['data'].shape[1],'F']
            varlist['DNN'] = [f['data'].shape[1],'F']
            if analysis == 'KEE':
                add_vars += ['mass','isElectron']
                varlist['mass'] = [f['data'].shape[1],'F']
                varlist['isElectron'] = [f['data'].shape[1],'F']
            if analysis == 'Jpsi':
                add_vars += ['e','isMuon']
                varlist['e'] = [f['data'].shape[1],'F']
                varlist['isMuon'] = [f['data'].shape[1],'F']
                
        else:
            varlist[var] = [f[var].shape[1],'F']
else:
    add_vars = varlist
    varlist = MVA_cfg.tvars #or other list defined in MVA_cfg


print ('List of branches: ',varlist.keys())
mytree = MVA_cfg.ManageTTree(varlist,tkin)

if opt.make_roc:
    idx = f['data'][:,:,0] != 0
    match = np.sum(f['label'],axis=1)==3
    masks = [idx]
    colors = [('m','combined')]
    if analysis == 'KEE':
        electrons = np.abs(f['data'][:,:,6]) == 1
        electrons = electrons*idx
        kaons = np.abs(f['data'][:,:,6]) == 0
        kaons = kaons*idx
        print('Total kaons: ',kaons.sum())
        print('Total electrons: ',electrons.sum())
        masks = [idx,electrons,kaons]
        colors = [('m','combined'),('g','electrons'),('b','kaons')]
    y_dnn = []
    y_lab = []
    y_sc = f['data'][:,:,-1]
    y_val = f['label'][:]
    for mask in masks:
        y_dnn.append(y_sc[mask].flatten())
        y_lab.append(y_val[mask].flatten())

    threshs = [
        0.6,
        0.65,
        0.7,
        0.75,
        0.8,
        0.85,
        0.92,
        0.95]
    
    results = []
    
    for label, score in zip(y_lab,y_dnn):
        results.append(metrics.roc_curve(label,score, pos_label=1))
        print('AUC: ',metrics.roc_auc_score(label,score))

    

    fig, base = plt.subplots(dpi=150)
    for color, result in zip(colors, results):
        print(color[1])
        p = base.semilogy(result[1], 1.0/result[0],color = color[0],label=color[1])
        for thresh in threshs:
            bin = np.argmax(result[1]>thresh)
            cut = result[2][bin]
            print ('eff: ',result[1][bin], ' fpr: ', result[0][bin],' cut: ', cut)


    #p2 = base.semilogy(tpr, fpr,color = 'g',label='test')
    # with open('Yuta_ROC.txt','w') as f:        
    #     for ic, cut in enumerate(tpr):
    #         f.write('{} {}\n'.format(cut,fpr[ic]))

    # np.savetxt('tpr_tau.txt',tpr,delimiter=',')
    # np.savetxt('fpr_tau.txt',fpr,delimiter=',')
    base.set_xlabel("True Postive Rate")
    base.set_ylabel("1.0/False Postive Rate")
    if analysis == 'KEE':
        base.legend()

    plt.grid(True)
    if not os.path.exists(opt.root):os.mkdir(opt.root)
    if not os.path.exists(os.path.join(opt.plot,'ROC')):os.makedirs(os.path.join(opt.plot,'ROC'))
    
    plt.savefig(os.path.join(opt.plot,'ROC','ROC_{0}.pdf'.format(sample.replace(".h5",""))))

out_root = os.path.join(opt.root,sample.replace('h5','root'))
root_file = ROOT.TFile(out_root,'recreate')

Make_root(f,f['data'].shape[0])
tkin.Write()

root_file.Write("",ROOT.TObject.kOverwrite)
