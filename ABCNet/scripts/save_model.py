import argparse
import h5py
from math import *
import subprocess
import tensorflow as tf
from tensorflow.python.tools import freeze_graph
import numpy as np
from datetime import datetime
import json
import os, ast
import sys
#import matplotlib.pyplot as plt
from sklearn import metrics
# from scipy.special import softmax
# from scipy.special import expit
#import ROOT
from itertools import combinations
#import importlib
np.set_printoptions(threshold=sys.maxsize)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(BASE_DIR)
sys.path.append(os.path.dirname(BASE_DIR))
sys.path.append(os.path.join(BASE_DIR,'..', 'models'))
sys.path.append(os.path.join(BASE_DIR,'..' ,'utils'))
#from MVA_cfg import *
#import provider
import gapnet as MODEL
#import  gapnet_classify_global as model


# DEFAULT SETTINGS
parser = argparse.ArgumentParser()
parser.add_argument('--params', default='[20,1,32,64,128,1,64,128,256,256,256,256]', help='DNN parameters[[k,H,A,F,F,F,H,A,F,C,F]]')
parser.add_argument('--model_path', default='../logs', help='Model checkpoint path')
parser.add_argument('--name', default='test', help='Output model name')
parser.add_argument('--modeln', type=int,default=0, help='Model number')
parser.add_argument('--num_point', type=int, default=100, help='Point Number [default: 400]')
parser.add_argument('--nfeat', type=int, default=10, help='Number of features [default: 5]')
parser.add_argument('--nglob', type=int, default=3, help='Number of features [default: 5]')
parser.add_argument('--ncat', type=int, default=2, help='Number of categories [default: 2]')





FLAGS = parser.parse_args()
MODEL_PATH = os.path.join(FLAGS.model_path,FLAGS.name)
params = ast.literal_eval(FLAGS.params)
#model = importlib.import_module(os.path.join(MODEL_PATH, 'dgcnn'))



# MAIN SCRIPT
NUM_POINT = FLAGS.num_point
NFEATURES = FLAGS.nfeat
NGLOB = FLAGS.nglob

NUM_CATEGORIES = FLAGS.ncat


#Only used to get how many parts per category
    
def printout(flog, data):
    print(data)
    flog.write(data + '\n')

  
def save_eval():

    with tf.Graph().as_default():
        pointclouds_pl,  labels_pl, global_pl = MODEL.placeholder_inputs(1, NUM_POINT,NFEATURES,NGLOB) #REMEMBER I CHANGED THE ORDER
        print (pointclouds_pl.name,labels_pl.name, global_pl.name)
        #batch = tf.Variable(0, trainable=False)

                        
        is_training_pl = tf.placeholder(tf.bool, shape=())
        pred = MODEL.get_model(pointclouds_pl, is_training=is_training_pl,global_pl = global_pl,params=params,num_class=NUM_CATEGORIES)
            
            
        loss  = MODEL.get_loss(pred,labels_pl)
            
        saver = tf.train.Saver()
          
        print (' is_training: ', is_training_pl )    
        config = tf.ConfigProto()
        sess = tf.Session(config=config)

        if FLAGS.modeln >0:
            saver.restore(sess,os.path.join(MODEL_PATH,'model_{0}.ckpt'.format(FLAGS.modeln)))
        else:
            saver.restore(sess,os.path.join(MODEL_PATH,'model.ckpt'))

        print('model restored')


        builder = tf.saved_model.builder.SavedModelBuilder("../pretrained/{}".format(FLAGS.name))
        builder.add_meta_graph_and_variables(sess, [tf.saved_model.tag_constants.SERVING])
        builder.save()


        #print([node.name for node in sess.graph.as_graph_def().node])
        print('prediction name:',pred.name)


            

################################################          
    
def load_eval():
    '''
    To evaluate the ABCNet score we have to create a tensorflow session, give the relevant distributions, and run the frozen model.
    The order of the particles does not necessairily matter, but priority should be given to trigger muon (if more than 1, take the one with the highest pT), electrons, and for last kaons, meaning that if you have more than 100 particles in one event, the kaons are the ones to be truncated.
    To use the code snipet underneath, just replace 'mock_data' and 'mock_glob' with the real data as decribed in the following lines.
    The only quantities that are needed per event are:
    Data:
          numpy array of shape [1,100,10] storing the properties of each particle up to 100 particles. If a variable is not applicable for some particles (ex: BDT and MVA information for kaons), just fill with zeros.
          Per particle, the variables to be given are:
          data[:,:,0] = particle eta - trigger muon eta
          data[:,:,1] = particle phi - trigger muon phi
          data[:,:,2] = np.log(particle pT)
          data[:,:,3] = particle mass
          data[:,:,4] = particle DXY
          data[:,:,5] = particle DZ
          data[:,:,6] = abs(particle pdgId == 11) (isElectron)
          data[:,:,7] = particle charge
          data[:,:,8] = Electron MVA (0 for kaons and muons)
          data[:,:,9] = Electron BDT (0 for kaons and muons)
    Global variables:
          numpy array of shape [1,3] storing quantities that only vary per event.
          Per event we need:
          global[:,0] = PV_npvsGood
          global[:,1] = PV_chi2
          global[:,2] = PV_score
    If you want to calculate the accuracy per particle, the labels with 1 for particles steaming from B decays and 0 for anything else cal also be given. Otherwise, just fill a mock list as shown with 'mock_label'
    '''



    with tf.Session(graph=tf.Graph()) as sess:
        tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], '../pretrained/{}'.format(FLAGS.name))
        output = sess.graph.get_tensor_by_name('Reshape_5:0')
        mock_data = np.ones((1,NUM_POINT,NFEATURES),dtype=float)
        mock_label = np.ones((1,NUM_POINT),dtype=float)
        mock_glob = np.ones((1,NGLOB),dtype=float)



        feed_dict = {
            'Placeholder:0': mock_data,
            'Placeholder_1:0': mock_label,
            'Placeholder_2:0': mock_glob,
            'Placeholder_3:0': False,
        }

        predictions = sess.run(output, feed_dict)
        #predictions store 1 value per particle. The shape is [1,100,2]. The probability as coming from the B decay is stored at position 2, for example, the probability for the first particle come from the B decay is stored at predictions[0,0,1], while for the second is stored at predictions[0,1,1] and so on.
        for pred in predictions: 
            print (pred)
        

if __name__=='__main__':
  #save_eval()
  load_eval()
