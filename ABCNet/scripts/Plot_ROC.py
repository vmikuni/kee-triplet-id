import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser
import os

parser = OptionParser(usage="%prog [opt]  inputFiles")
parser.add_option("--folder", type="string", default='../plots/ROC', help="Specify the folder to load. Default: %default")
parser.add_option("--plot", type="string", default='../plots', help="Folder to save plots. Default: %default")
(opt, args) = parser.parse_args()

fig, base = plt.subplots(dpi=150)
colors = ['green','red']
plt.figure(figsize=(6, 5))
for ifile, files in enumerate(args):
    results = np.load(os.path.join(opt.folder,files))
    p = base.semilogy(results[1], 1.0/results[0],color = colors[ifile])


base.set_xlabel("True Postive Rate")
base.set_ylabel("1.0/False Postive Rate")
plt.show()  
