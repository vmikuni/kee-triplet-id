import numpy as np
import ROOT
from sklearn import metrics
import tdrstyle
import itertools
import os
import matplotlib.pyplot as plt
import MVA_cfg
tdrstyle.setTDRStyle()
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

from optparse import OptionParser

class Particles():
    def __init__(self,name,mask):
        self.name = name
        self.mask = mask        
    def select(self,pt,eta,phi,mass,charge,DNN,label):
        self.pt = pt[self.mask]
        self.eta = eta[self.mask]
        self.phi = phi[self.mask]
        self.mass = mass[self.mask]
        self.charge = charge[self.mask]
        self.DNN = DNN[self.mask]
        self.label = label[self.mask]
        self.px = np.abs(self.pt)*np.cos(self.phi)
        self.py = np.abs(self.pt)*np.sin(self.phi)
        self.pz = np.abs(self.pt)/(np.tan(2*np.arctan(np.exp(-self.eta))))
        self.energy = np.sqrt(self.px**2+self.py**2+self.pz**2+self.mass**2)
        self.length = len(self.pt)
    def __getitem__(self,key):
        return Particle(
            self.px[key],
            self.py[key],
            self.pz[key],
            self.energy[key],
            self.pt[key],
            self.DNN[key],
            self.label[key],
        )

class Particle():
    def __init__(self,px,py,pz,e,pt,DNN,label):
        self.px = px
        self.py = py
        self.pz = pz
        self.pt = pt
        self.energy = e
        self.DNN = DNN
        self.label = label

class reco_B():
    def __init__(self,e1,e2,k):
        self.mass, self.pt  = self.inv_mass(e1,e2,k)
        self.sumDNN = e1.DNN + e2.DNN+ k.DNN
        self.match = e1.label + e2.label+ k.label
        self.sumPt = e1.pt + e2.pt+ k.pt
    def inv_mass(self,e1,e2,k):
        px=py=pz=e = 0
        for part in [e1,e2,k]:
            px+=part.px
            py+=part.py
            pz+=part.pz
            e+=part.energy
        return np.sqrt(abs(e**2 - px**2 - py**2 - pz**2)), np.sqrt(px**2+py**2)



def Combinations(electrons,kaons):
    e_combs = itertools.combinations(range(electrons.length),2)
    combs = []
    for comb in e_combs:
        if electrons.charge[comb[0]] + electrons.charge[comb[1]] ==0:
            for kaon in kaons:
                B = reco_B(electrons[comb[0]],electrons[comb[1]],kaon)
                if B.sumDNN > opt.sum_cut:
                    #and B.mass > 3.7:
                    combs.append(B)
    return combs

def Plot_masses(hists,ymax=0):
    c = ROOT.TCanvas('mycv','mycv',5,30,540,600)        
    yshift = 0
    xshift = -0.4
    legend = ROOT.TLegend(0.6+xshift,0.7+yshift,0.8+xshift,0.9+yshift)
    ic = 0
    for ih, hist in enumerate(hists):
        if 'mass' not in hist: continue
        legend.AddEntry(hists[hist],hist.split("_")[-1],'l')
        hists[hist].SetLineColor(ROOT.TColor.GetColor(MVA_cfg.histFillColor[ic]))
        hists[hist].SetMarkerColor(ROOT.TColor.GetColor(MVA_cfg.histFillColor[ic]))
        hists[hist].SetLineWidth(3)
        hists[hist].GetYaxis().SetTitle('Events / bin')
        hists[hist].GetXaxis().SetTitle('m(Ke^{+}e^{-}) [GeV]')
        hists[hist].GetYaxis().SetRangeUser(0,ymax*1.2)
        hists[hist].Draw("samehist")
        ic+=1
    legend.Draw("same")
    c.SaveAs("{}/mass_{}_e{}_kaon{}_sum{}.pdf".format(opt.plot,opt.name,opt.e_cut,opt.kaon_cut,opt.sum_cut))

def Plot_SumDNN(hists,ymax=0):
    c = ROOT.TCanvas('mycv2','mycv2',5,30,540,600)        
    yshift = 0
    xshift = -0.3
    legend = ROOT.TLegend(0.6+xshift,0.7+yshift,0.8+xshift,0.9+yshift)
    ic=0
    for ih, hist in enumerate(hists):
        if 'DNN' not in hist: continue
        legend.AddEntry(hists[hist],hist.split("_")[-1],'l')
        hists[hist].SetLineColor(ROOT.TColor.GetColor(MVA_cfg.histFillColor[ic]))
        hists[hist].SetMarkerColor(ROOT.TColor.GetColor(MVA_cfg.histFillColor[ic]))
        hists[hist].SetLineWidth(3)
        hists[hist].Scale(1.0/hists[hist].Integral())
        hists[hist].GetYaxis().SetTitle('Events / bin')
        #hists[hist].GetXaxis().SetTitle('m(Ke^{+}e^{-}) [GeV]')
        hists[hist].GetXaxis().SetTitle('Sum of the DNN score')
        #hists[hist].GetYaxis().SetRangeUser(0,ymax*1.2)
        hists[hist].Draw("samehist")
        ic+=1
    legend.Draw("same")
    c.SaveAs("{}/DNN_{}_e{}_kaon{}_sum{}.pdf".format(opt.plot,opt.name,opt.e_cut,opt.kaon_cut,opt.sum_cut))

parser = OptionParser(usage="%prog [opt]  inputFiles")
parser.add_option("-f","--file", type="string", default='BToKEE_charge', help="Specify the file to load. Default: %default")
parser.add_option("--folder", type="string", default='../root', help="Specify the folder to load. Default: %default")
parser.add_option("--plot", type="string", default='../plots', help="Folder to save plots. Default: %default")
parser.add_option("--name", type="string", default='lowPt', help="String to be appended to output file. Default: %default")
parser.add_option("-n", "--evtmax", type="long", default=-1, help="Number of events to save. Default: %default")
parser.add_option("--kaon_cut", type="float", default=0, help="Cut used for each particle. Default: %default")
parser.add_option("--e_cut", type="float", default=0, help="Cut used for each particle. Default: %default")
parser.add_option("--sum_cut", type="float", default=0, help="Cut used for each particle. Default: %default")
(opt, args) = parser.parse_args()

evtmax = opt.evtmax
sample = '{0}.root'.format(opt.file)
vars_list = ['mass','sumDNN']
cats = ['match0','match1','match2','match3']
hists = {}
for var in vars_list:
    for cat in cats:
        if 'DNN' in var:
            hists['{}_{}'.format(var,cat)]=ROOT.TH1D('{}_{}'.format(var,cat),'{}_{}'.format(var,cat),30,0,3)
        else:
            hists['{}_{}'.format(var,cat)]=ROOT.TH1D('{}_{}'.format(var,cat),'{}_{}'.format(var,cat),30,2.5,8)
hists['ncomb'] = ROOT.TH1D('ncomb','ncomb',100,0,2000)
hists['Bpt'] = ROOT.TH1D('Bpt','Bpt',10,0,10)
hists['sumidx'] = ROOT.TH1D('sumidx','sumidx',20,0,20)
hists['k_idx'] = ROOT.TH1D('k_idx','k_idx',100,0,100)
hists['e_idx'] = ROOT.TH1D('e_idx','e_idx',100,0,100)

f= ROOT.TFile(os.path.join(opt.folder,sample),"READ")
tree=f.Get('tree')
y_lab = []
y_score = []

all_triplets = 0
for e, event in enumerate(tree): #FixMe: Make it more optimal
    if e %1000==0: print('Processing event: ',e)
    if e > evtmax and evtmax>0: break

    parts = np.array(list(event.mass),dtype=np.bool)    

    if np.sum(list(tree.label)) !=3: continue

    DNN_mask_cut_e = np.array(list(tree.DNN))>=opt.e_cut
    DNN_mask_cut_kaon = np.array(list(tree.DNN))>=opt.kaon_cut
    parts[0] = False #First is always the trigger muon
    e_mask = np.array(list(tree.isElectron),dtype=np.bool) & (DNN_mask_cut_e)
    kaon_mask = (np.array(list(tree.isElectron),dtype=np.bool)==0) & (parts) & (DNN_mask_cut_kaon)
    
    electrons = Particles('electrons',e_mask)
    kaons = Particles('kaons',kaon_mask)
    #print(np.array(tree.eta))
    for obj in [electrons,kaons]:
        obj.select(
            np.array(list(tree.pt)),
            np.array(list(tree.eta)),
            np.array(list(tree.phi)),
            np.array(list(tree.mass)),
            np.array(list(tree.charge)),
            np.array(list(tree.DNN)),
            np.array(list(tree.label))
        )

    idx_e = [i[0] for i in enumerate(electrons.label) if i[1]==1] 
    idx_k = [i[0] for i in enumerate(kaons.label) if i[1]==1] 
    if len(idx_k)>0:
        hists['k_idx'].Fill(idx_k[0])
    for e in idx_e:hists['e_idx'].Fill(e)
    combs = Combinations(electrons,kaons)
    if len(combs) ==0: continue
    top = [i[0] for i in sorted(enumerate(combs),key=lambda comb: comb[1].sumDNN,reverse=True)]
    #top = [i[0] for i in sorted(enumerate(combs),key=lambda comb: comb[1].sumPt,reverse=True)]
    combs = [combs[i] for i in top]
    hists['ncomb'].Fill(len(combs))
    all_triplets+=len(combs)
    for ic, comb in enumerate(combs):
        if comb.match==3: 
            hists['sumidx'].Fill(ic)
            hists['Bpt'].Fill(comb.pt)
        y_lab.append(comb.match == 3)
        y_score.append(comb.sumDNN)
        for hist in hists:
            if hist in ['k_idx','e_idx','sumidx','ncomb','Bpt']: continue
            nmatch = int(''.join(x for x in hist if x.isdigit()))
            if comb.match == nmatch:
                if 'DNN' in hist:
                    hists[hist].Fill(comb.sumDNN)
                else:
                    hists[hist].Fill(comb.mass)
ymax_mass = ymax_sum= 0
for hist in hists:
    if 'mass' in hist:
        if ymax_mass<hists[hist].GetBinContent(hists[hist].GetMaximumBin()):
            ymax_mass = hists[hist].GetMaximum()
    if 'DNN' in hist:
        if ymax_sum<hists[hist].GetBinContent(hists[hist].GetMaximumBin()):
            ymax_sum = hists[hist].GetMaximum()

Plot_masses(hists,ymax_mass)
#Plot_Bpt(hists,ymax_mass)
Plot_SumDNN(hists,ymax_sum)

fig, base = plt.subplots(dpi=150)
threshs = [
    0.6,
    0.65,
    0.7,
    0.75,
    0.8,
    0.85,
    0.92,
    0.95]

results =metrics.roc_curve(y_lab,y_score, pos_label=1)
for thresh in threshs:
    bin = np.argmax(results[1]>thresh)
    cut = results[2][bin]
    print ('eff: ',results[1][bin], ' fpr: ', results[0][bin],' cut: ', cut)
p = base.semilogy(results[1], 1.0/results[0],color = 'm')
print('AUC: ',metrics.roc_auc_score(y_lab,y_score))
base.set_xlabel("True Postive Rate")
base.set_ylabel("1.0/False Postive Rate")
np.save("{}/ROC/ROC_{}_e{}_kaon{}_sum{}.npy".format(opt.plot,opt.name,opt.e_cut,opt.kaon_cut,opt.sum_cut), results)
print("Absolute number of triplets: ",all_triplets)
plt.grid(True)
plt.savefig("{}/ROC/ROC_{}_e{}_kaon{}_sum{}.pdf".format(opt.plot,opt.name,opt.e_cut,opt.kaon_cut,opt.sum_cut))
print("Saved ROC curve at: {}/ROC/ROC_{}_e{}_kaon{}_sum{}.pdf".format(opt.plot,opt.name,opt.e_cut,opt.kaon_cut,opt.sum_cut))

fout= ROOT.TFile(os.path.join(opt.folder,'{}_{}_e{}_kaon{}_sum{}.root'.format(opt.file,opt.name,opt.e_cut,opt.kaon_cut,opt.sum_cut)),"RECREATE")
for hist in hists:
    hists[hist].Write()
