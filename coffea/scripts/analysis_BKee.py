from coffea import hist
import uproot
from coffea.util import awkward
from coffea.analysis_objects import JaggedCandidateArray
import coffea.processor as processor
from coffea.processor import run_parsl_job
from coffea.processor.parsl.parsl_executor import parsl_executor
from coffea.lookup_tools import extractor
import numpy as np
import numba as nb
import matplotlib.pyplot as plt
import utils
import sys
import re
import h5py as h5
from optparse import OptionParser
import parsl
import os
from parsl.configs.local_threads import config
from parsl.providers import LocalProvider,SlurmProvider
from parsl.channels import LocalChannel,SSHChannel
from parsl.config import Config
from parsl.executors import HighThroughputExecutor
from parsl.launchers import SrunLauncher
from parsl.addresses import address_by_hostname

np.set_printoptions(threshold=sys.maxsize)

def Writeh5(output, name,folder):
    if not os.path.exists(folder):
        os.makedirs(folder)    
    with h5.File(os.path.join(folder,'BToKEE_{0}.h5'.format(name)), "w") as fh5:
        dset = fh5.create_dataset("data", data=output['data'])
        dset = fh5.create_dataset("label", data=output['label'])
        dset = fh5.create_dataset("global", data=output['global'])


class Channel():
    def __init__(self, name):
        self.name = name
        self.sel=None
        self.kaons= None
        self.electrons=None
        self.trigger_muons=None
        self.gen_parts=None
        self.electron_pid=None
        self.kaon_pid=None
        self.glob = None
        self.pid = None
        
    def apply_sel(self):
        self.kaons=self.kaons[self.sel]
        self.electrons=self.electrons[self.sel]
        self.trigger_muons=self.trigger_muons[self.sel]
        self.gen_parts=self.gen_parts[self.sel]
        self.glob = self.glob[self.sel]

    def apply_gen_sel(self):
        self.electron_pid = self.electrons.mass<-1
        self.kaon_pid = self.kaons.mass<-1

        gen_matched_electrons = self.electrons[self.electrons.gen_idx>=0]
        gen_electrons = self.gen_parts[gen_matched_electrons.gen_idx]

        #print(gen_electrons.pdgId)
        electron_mask = (np.abs(self.gen_parts[gen_electrons.gen_idxMother].pdgId)==521) & (np.abs(gen_electrons.pdgId)==11) 
        #& (self.gen_parts[gen_electrons.gen_idxMother].pt < 10)
        self.electron_pid[self.electrons.gen_idx>=0] = electron_mask
        #print(self.electron_pid)

        gen_matched_kaons = self.kaons[self.kaons.gen_idx>=0]
        gen_kaons = self.gen_parts[gen_matched_kaons.gen_idx]
        kaon_mask =  (np.abs(self.gen_parts[gen_kaons.gen_idxMother].pdgId)==521) & (np.abs(gen_kaons.pdgId)==321) 
        #& (self.gen_parts[gen_kaons.gen_idxMother].pt < 10)
        self.kaon_pid[self.kaons.gen_idx>=0] = kaon_mask
        #print(self.kaon_pid.sum()==1)
        muon_pid = np.zeros(self.kaon_pid.size,dtype=np.bool_)
        
    def datah5(self):         
        #Add a variable that is always 0 when property is not defined for a particle
        
        self.trigger_muons.zeros = self.trigger_muons.pt
        self.electrons.zeros = self.electrons.pt
        self.kaons.zeros = self.kaons.pt
        
        self.trigger_muons.zeros[self.trigger_muons.zeros>0]=0
        self.electrons.zeros[self.electrons.zeros>0]=0
        self.kaons.zeros[self.kaons.zeros>0]=0

        #Add MVAID to a single variable

        self.electrons.mvaId = self.electrons.lowpt_mvaId
        self.electrons.mvaId[self.electrons.lowpt_mvaId==20] = self.electrons.pf_mvaId[self.electrons.lowpt_mvaId==20] 
        parts = JaggedCandidateArray.candidatesfromcounts(
            self.trigger_muons.counts + self.kaons.counts +self.electrons.counts,
            pt = awkward.JaggedArray.concatenate((self.trigger_muons.pt,self.electrons.pt,self.kaons.pt),axis=1).content,
            eta = awkward.JaggedArray.concatenate((self.trigger_muons.eta,self.electrons.eta,self.kaons.eta),axis=1).content,
            phi = awkward.JaggedArray.concatenate((self.trigger_muons.phi,self.electrons.phi,self.kaons.phi),axis=1).content,
            mass = awkward.JaggedArray.concatenate((self.trigger_muons.mass,self.electrons.mass,self.kaons.mass),axis=1).content,
            charge = awkward.JaggedArray.concatenate((self.trigger_muons.charge,self.electrons.charge,self.kaons.charge),axis=1).content,
            dxy = awkward.JaggedArray.concatenate((self.trigger_muons.dxy,self.electrons.dxy,self.kaons.dxy),axis=1).content,
            dz = awkward.JaggedArray.concatenate((self.trigger_muons.dz,self.electrons.dz,self.kaons.dz),axis=1).content,
            dxyErr = awkward.JaggedArray.concatenate((self.trigger_muons.dxy/self.trigger_muons.dxyErr,self.electrons.dxy/self.electrons.dxyErr,self.kaons.dxyS),axis=1).content,
            dzErr = awkward.JaggedArray.concatenate((self.trigger_muons.dz/self.trigger_muons.dzErr,self.electrons.dz/self.electrons.dzErr,self.kaons.dzS),axis=1).content,

            pdgId = awkward.JaggedArray.concatenate((self.trigger_muons.pdgId,self.electrons.pdgId,self.kaons.pdgId),axis=1).content,
            
            label = awkward.JaggedArray.concatenate((self.trigger_muons.mass<-1,self.electron_pid,self.kaon_pid),axis=1).content,
            mva = awkward.JaggedArray.concatenate((self.trigger_muons.zeros,self.electrons.mvaId,self.kaons.zeros),axis=1).content,
            bdt = awkward.JaggedArray.concatenate((self.trigger_muons.zeros,self.electrons.BDT_unb,self.kaons.zeros),axis=1).content,
            # iso = awkward.JaggedArray.concatenate((self.trigger_muons.zeros,self.electrons.iso,self.kaons.zeros),axis=1).content,
            # ip3d = awkward.JaggedArray.concatenate((self.trigger_muons.zeros,self.electrons.ip3d,self.kaons.zeros),axis=1).content,
            # DCASig = awkward.JaggedArray.concatenate((self.trigger_muons.zeros,self.electrons.zeros,self.kaons.DCASig),axis=1).content,
            
            )


        nparts = self.trigger_muons.counts+ self.kaons.counts +self.electrons.counts
        data = np.zeros((self.kaons.size,opt.points,10))
        self.pid = np.zeros((self.kaons.size,opt.points))
        truncate = parts[nparts>=opt.points]
        pad = parts[nparts<opt.points]
        truncate_muons = self.trigger_muons[nparts>=opt.points]
        pad_muons = self.trigger_muons[nparts<opt.points]

        if len(data[nparts>=opt.points]) >0:
            data[nparts>=opt.points,:,0]+=np.array((truncate.eta[:,:opt.points]-truncate_muons.eta[:,0]).tolist())
            data[nparts>=opt.points,:,1]+=np.array((truncate.phi[:,:opt.points]-truncate_muons.phi[:,0]).tolist())
            data[nparts>=opt.points,:,2]+=np.log(np.array(truncate.pt[:,:opt.points].tolist()))
            data[nparts>=opt.points,:,3]+=np.array(truncate.mass[:,:opt.points].tolist())
            data[nparts>=opt.points,:,4]+=np.array(truncate.dxy[:,:opt.points].tolist())
            data[nparts>=opt.points,:,5]+=np.array(truncate.dz[:,:opt.points].tolist())
            data[nparts>=opt.points,:,6]+=np.array((np.abs(truncate.pdgId[:,:opt.points])==11).tolist())
            data[nparts>=opt.points,:,7]+=np.array(truncate.charge[:,:opt.points].tolist())
            data[nparts>=opt.points,:,8]+=np.array(truncate.mva[:,:opt.points].tolist())
            data[nparts>=opt.points,:,9]+=np.array(truncate.bdt[:,:opt.points].tolist())
            #data[nparts>=opt.points,:,9]+=np.array(truncate.ip3d[:,:opt.points].tolist())
            #data[nparts>=opt.points,:,10]+=np.array(truncate.iso[:,:opt.points].tolist())
            # data[nparts>=opt.points,:,10]+=np.array(truncate.dxyErr[:,:opt.points].tolist())
            # data[nparts>=opt.points,:,11]+=np.array(truncate.dzErr[:,:opt.points].tolist())
            #data[nparts>=opt.points,:,10]+=np.array(truncate.DCASig[:,:opt.points].tolist())
            self.pid[nparts>=opt.points]+=np.array(truncate.label[:,:opt.points].tolist())
            
        if len(data[nparts<opt.points]) >0:            
            data[nparts<opt.points,:,0] = ZeroPad(pad.eta-pad_muons.eta[:,0],data[nparts<opt.points,:,0])
            data[nparts<opt.points,:,1] = ZeroPad(pad.phi-pad_muons.phi[:,0],data[nparts<opt.points,:,1])
            data[nparts<opt.points,:,2] = ZeroPad(np.log(pad.pt),data[nparts<opt.points,:,2]) 
            data[nparts<opt.points,:,3] = ZeroPad(pad.mass,data[nparts<opt.points,:,3])
            data[nparts<opt.points,:,4] = ZeroPad(pad.dxy,data[nparts<opt.points,:,4])
            data[nparts<opt.points,:,5] = ZeroPad(pad.dz,data[nparts<opt.points,:,5])
            data[nparts<opt.points,:,6] = ZeroPad(np.abs(pad.pdgId)==11,data[nparts<opt.points,:,6])
            data[nparts<opt.points,:,7] = ZeroPad(pad.charge,data[nparts<opt.points,:,7])
            data[nparts<opt.points,:,8] = ZeroPad(pad.mva,data[nparts<opt.points,:,8])
            data[nparts<opt.points,:,9] = ZeroPad(pad.bdt,data[nparts<opt.points,:,9])
            #data[nparts<opt.points,:,9] = ZeroPad(pad.ip3d,data[nparts<opt.points,:,9])
            #data[nparts<opt.points,:,10] = ZeroPad(pad.iso,data[nparts<opt.points,:,10])
            # data[nparts<opt.points,:,10] = ZeroPad(pad.dxyErr,data[nparts<opt.points,:,10])
            # data[nparts<opt.points,:,11] = ZeroPad(pad.dzErr,data[nparts<opt.points,:,11])
            #data[nparts<opt.points,:,10] = ZeroPad(pad.DCASig,data[nparts<opt.points,:,10])
            self.pid[nparts<opt.points] = ZeroPad(pad.label,self.pid[nparts<opt.points])


        

        return data
            
class SignalProcessor(processor.ProcessorABC):

    def __init__(self):
        dataset_axis = hist.Cat("dataset", "")        
        nparts_axis = hist.Bin("nparts", "N$_{particles}$ ", 100, 0, 100)        
        pt_axis = hist.Bin("pt", "p_{T} ", 100, 0, 20)        
        dz_axis = hist.Bin("dz", "dz$ ", 50, -1, 1)        

        axis_list = {
            'nparts':nparts_axis, 
            'dz':dz_axis,
            'pt':pt_axis,
        }

        self.channels = ['electrons','kaons']
        # self.run_dict = {}
        dict_accumulator = {}
        ML_accumulator = {
            'data': processor.list_accumulator(),
            'label': processor.list_accumulator(),
            'global': processor.list_accumulator(),
        }
        dict_accumulator['ML'] = processor.dict_accumulator(ML_accumulator)

        for axis in axis_list:
            dict_accumulator["{}".format(axis)] = hist.Hist("Events", dataset_axis, axis_list[axis])                    
            for channel in self.channels:
                dict_accumulator["{}_{}".format(axis,channel)] = hist.Hist("Events", dataset_axis, axis_list[axis])        
               
        dict_accumulator['cutflow']= processor.defaultdict_accumulator(int)

        self._accumulator = processor.dict_accumulator( dict_accumulator )
        
    @property
    def accumulator(self):
        return self._accumulator
    
        
    def process(self, df):

        output = self.accumulator.identity()
        dataset = df["dataset"]
        
        kaons = JaggedCandidateArray.candidatesfromcounts(
            df['nProbeTracks'],
            pt=df['ProbeTracks_pt'].content,
            eta=df['ProbeTracks_eta'].content,
            phi=df['ProbeTracks_phi'].content,
            mass=df['ProbeTracks_mass'].content,
            charge=df['ProbeTracks_charge'].content,
            dxy=df['ProbeTracks_dxy'].content,
            dz=df['ProbeTracks_dz'].content,            
            DCASig=df['ProbeTracks_DCASig'].content,            
            dxyS=df['ProbeTracks_dxyS'].content,            
            dzS=df['ProbeTracks_dzS'].content,            
            pdgId=df['ProbeTracks_pdgId'].content,
            gen_idx=df['ProbeTracks_genPartIdx'].content if 'ProbeTracks_genPartIdx' in df else np.ones(df['ProbeTracks_genPartIdx'].content.shape), #dummy flag for data,
            )
        
        
        muons = JaggedCandidateArray.candidatesfromcounts(
            df['nMuon'],
            pt=df['Muon_pt'].content,
            eta=df['Muon_eta'].content,
            phi=df['Muon_phi'].content,
            mass=df['Muon_mass'].content,
            dxy=df['Muon_dxy'].content,
            dz=df['Muon_dz'].content,            
            dxyErr=df['Muon_dxyErr'].content,
            dzErr=df['Muon_dzErr'].content,            
            pdgId=df['Muon_pdgId'].content,            
            charge=df['Muon_charge'].content,            
            isTriggering=df['Muon_isTriggering'].content,            
            )

        electrons = JaggedCandidateArray.candidatesfromcounts(
            df['nElectron'],
            pt=df['Electron_pt'].content,
            eta=df['Electron_eta'].content,
            phi=df['Electron_phi'].content,
            mass=df['Electron_mass'].content,
            charge=df['Electron_charge'].content,
            dxy=df['Electron_dxy'].content,
            dz=df['Electron_dz'].content,
            dxyErr=df['Electron_dxyErr'].content,
            dzErr=df['Electron_dzErr'].content,
            isPF_overlap=df['Electron_isPFoverlap'].content,
            isPF=df['Electron_isPF'].content,
            isLowPt=df['Electron_isLowPt'].content,
            pdgId=df['Electron_pdgId'].content,
            BDT_unb=df['Electron_unBiased'].content,
            ip3d=df['Electron_ip3d'].content,
            lowpt_mvaId=df['Electron_mvaId'].content,
            pf_mvaId=df['Electron_pfmvaId'].content,
            iso=df['Electron_trkRelIso'].content,

            gen_idx=df['Electron_genPartIdx'].content if 'Electron_genPartIdx' in df else np.ones(df['Electron_genPartIdx'].content.shape), #dummy flag for data,            
            )

        gen_parts = JaggedCandidateArray.candidatesfromcounts(
            df['nGenPart'],
            pt=df['GenPart_pt'].content,
            eta=df['GenPart_eta'].content,
            phi=df['GenPart_phi'].content,
            mass=df['GenPart_mass'].content,
            gen_idxMother=df['GenPart_genPartIdxMother'].content, #dummy flag for data,            
            pdgId = df['GenPart_pdgId'].content
            )
        
            
        # This keeps track of how many events there are, as well as how many of each object exist in this events.
        output['cutflow']['all events'] += muons.size
        output['cutflow']['all kaons'] += kaons.counts.sum()
        output['cutflow']['all muons'] += muons.counts.sum()
        output['cutflow']['all electrons'] += electrons.counts.sum()

        kaons = kaons[
            (kaons.pt > 0.5)  & (np.abs(kaons.eta) < 2.4) &
            (np.abs(kaons.dz)<0.5) & 
            (kaons.mass > -1) 
            
        ]
        electrons = electrons[
            (electrons.pt > 0.5)  & (np.abs(electrons.eta) < 2.4) &
            (np.abs(electrons.dz)<0.5) & 
            #(electrons.isLowPt == 1) &
            #(electrons.isPF == 1) &
            (electrons.isPF_overlap == 0) &
            (electrons.mass > -1) 
        ]
        trigger_muons = muons[
            (muons.isTriggering==1) & 
            (muons.mass > -1)
        ]

        
        output['cutflow']['trigger muons'] += trigger_muons.counts.sum()
        output['cutflow']['filtered electrons'] += electrons.counts.sum()
        output['cutflow']['filtered kaons'] += kaons.counts.sum()
        
        # # # # # # # # # #
        # EVENT SELECTION #
        # # # # # # # # # #

        trigger_mask = (df['HLT_Mu9_IP6']==1) 
        BToKEE = Channel("BToKEE")
        BToKEE.sel = (electrons.counts >= 2) & (kaons.counts >= 1) & (trigger_muons.counts>=1)& (trigger_mask)
        BToKEE.kaons = kaons
        BToKEE.electrons = electrons
        BToKEE.trigger_muons = trigger_muons[:,:1] #Take only the first trigger muon
        BToKEE.gen_parts = gen_parts
        BToKEE.glob = np.concatenate((
            np.expand_dims(np.log(df['PV_npvsGood']),axis=-1),
            np.expand_dims(df['PV_chi2'],axis=-1),
            np.expand_dims(np.log(df['PV_score']),axis=-1) 
            ),axis=1)
        BToKEE.apply_sel()
        BToKEE.apply_gen_sel() #Match reco to gen particles
        BGen = BToKEE.gen_parts[abs(BToKEE.gen_parts.pdgId)==521]
        output['pt'].fill(dataset=dataset, pt=BGen.pt.flatten())
        
        output['cutflow']['{} events'.format(BToKEE.name)] += BToKEE.kaons.size
        output['cutflow']['{} events matched to Gen'.format(BToKEE.name)] += (((BToKEE.electron_pid).sum() + BToKEE.kaon_pid.sum())==3).sum()
        
        output['nparts'].fill(dataset=dataset, nparts=(BToKEE.kaons.counts+BToKEE.electrons.counts).flatten())
        output['nparts_electrons'].fill(dataset=dataset, nparts=(BToKEE.electrons.counts).flatten())
        output['nparts_kaons'].fill(dataset=dataset, nparts=(BToKEE.kaons.counts).flatten())
        
        output['dz_electrons'].fill(dataset=dataset, dz=(BToKEE.electrons[BToKEE.electron_pid==1].dz).flatten())
        output['dz_kaons'].fill(dataset=dataset, dz=(BToKEE.kaons[BToKEE.kaon_pid==1].dz).flatten())

        if opt.save_h5:
            datah5 = BToKEE.datah5()
            labels = BToKEE.pid
            #if not make_eval: #Use only eventd fully matched to gen level for training
            sel = (np.sum(labels,axis=-1)==3)
            datah5 = datah5[sel]
            labels = labels[sel]
            BToKEE.glob = BToKEE.glob[sel]
            output['ML']['data']+=datah5.tolist()
            output['ML']['label']+=labels.tolist()
            output['ML']['global']+=BToKEE.glob.tolist()
            output['cutflow']['{} events matched to Gen after truncating'.format(BToKEE.name)] += datah5.shape[0]

                
        
        return output

    def postprocess(self, accumulator):


        return accumulator

def ZeroPad(values,zeros):
    for ival, value in enumerate(values):    
        zeros[ival][:len(value)] += np.array(value)
    return zeros


def GetSamplename(first_file):
    sname = ''
    for string in first_file.split('/'):        
        if '13TeV' in string or 'Run' in string:
            if 'Run' in string:
                sname = string.split('-')[0]
            else:
                sname = string
            break
        else: #Local file for BToKee
            return 'BToKee'
    if sname == '': 
        print(first_file)
        print('ERROR: Cannot find the sample name')
        sys.exit()
    return utils.samples[sname]['name']


parser = OptionParser(usage="%prog [opt]  inputFiles")

parser.add_option("--samples",dest="samples", type="string", default='data', help="Specify which default samples to run [train/test/eval]. Default: data")
parser.add_option("-q", "--queue",  dest="queue", type="string", default="quick", help="Which queue to send the jobs. Default: %default")
parser.add_option("-p", "--nproc",  dest="nproc", type="long", default=1, help="Number of processes to use. Default: %default")
parser.add_option("--mem",  dest="mem", type="long", default=5000, help="Memory required in mb")
parser.add_option("--points", type="int", default=100, help="max number of particles to be used. Default %default")
parser.add_option("--cpu",  dest="cpu", type="long", default=2, help="Number of cpus to use. Default %default")
parser.add_option("--blocks",  dest="blocks", type="long", default=220, help="number of blocks. Default %default")
parser.add_option("--walltime",  dest="walltime", type="string", default="0:59:50", help="Max time for job run. Default %default")
parser.add_option("--chunk",  dest="chunk", type="long",  default=500, help="Chunk size. Default %default")
parser.add_option("--maxchunk",  dest="maxchunk", type="long",  default=2e6, help="Maximum number of chunks. Default %default")
parser.add_option("--version",  dest="version", type="string",  default="", help="nametag to append to output file.")
parser.add_option("--parsl",  dest="parsl", action="store_true",  default=False, help="Run using parsl. Default: False")

parser.add_option("--save_h5",  action="store_true",  default=False, help="Save a .h5 file for ML training. Default: False")
parser.add_option("--h5folder", type="string", default="data", help="Folder to store the h5 files. Default: %default")

(opt, args) = parser.parse_args()
samples = opt.samples
make_eval =False

if len(args) < 1:
    if 'COFFEADATA' not in os.environ:
        sys.exit("ERROR: Environment variables not set. Run setup.sh first!")
      
    if samples == 'eval': 
        file_name = os.path.join(os.environ['COFFEADATA'],'mc_BtoKEE_eval.txt')            
        fout_name = 'eval_{}.root'.format(opt.version)
        make_eval = True
    elif samples == 'train': 
        file_name = os.path.join(os.environ['COFFEADATA'],'mc_BtoKEE_train_nano.txt')    
        fout_name = 'train_{}.root'.format(opt.version)
    elif samples == 'test': 
        file_name = os.path.join(os.environ['COFFEADATA'],'mc_BtoKEE_test_nano.txt')    
        fout_name = 'test_{}.root'.format(opt.version)
    else:
        print("ERROR: You must specify what kind of dataset you want to run [--samples]")
    
    print('Loading sets from file')    
    files = []
    with open(os.path.join(file_name),'r') as fread:
        files = fread.readlines()
        files = [x.strip() for x in files] 
        idx = np.arange(len(files))
        np.random.shuffle(idx)
        files = np.array(files)[idx]
                    
else:
    files = args
    fout_name = 'out.root'

fileset={}
for f in files:
    name = GetSamplename(f)
    if name in fileset:
        fileset[name].append(f)
    else:
        fileset[name] = [f]

for sample in fileset:
    print('Files: {0}, sample name: {1}'.format(fileset[sample],sample))




nproc = opt.nproc
sched_options = '''
##SBATCH --account=gpu_gres 
##SBATCH --partition=gpu    
#SBATCH --cpus-per-task=%d
#SBATCH --mem-per-cpu=%d
##SBATCH --exclude=t3wn56
''' % (opt.cpu,opt.mem) 

x509_proxy = '.x509up_u%s'%(os.getuid())
wrk_init = '''
export XRD_RUNFORKHANDLER=1
export X509_USER_PROXY=/t3home/%s/%s
'''%(os.environ['USER'],x509_proxy)



if not opt.parsl:
    output = processor.run_uproot_job(fileset,
                                      treename='Events',
                                      processor_instance=SignalProcessor(),
                                      executor=processor.futures_executor,
                                      executor_args={'workers':nproc, 'xrootdtimeout':200, },
                                      maxchunks =opt.maxchunk,
                                      chunksize = opt.chunk
    )


else:
    print("id: ",os.getuid())
    print(wrk_init)
    #sched_opts = ''
    #wrk_init = ''

    slurm_htex = Config(
        executors=[
            HighThroughputExecutor(
                label="coffea_parsl_slurm",
                #worker_debug=True,
                address=address_by_hostname(),
                prefetch_capacity=0,  
                heartbeat_threshold=60,
                cores_per_worker=1,
                #cores_per_worker=opt.cpu,
                max_workers=opt.cpu,
                provider=SlurmProvider(
                    channel=LocalChannel(),
                    launcher=SrunLauncher(),
                    init_blocks=opt.blocks,
                    min_blocks = opt.blocks-20,                     
                    max_blocks=opt.blocks+20,
                    exclusive  = False,
                    parallelism=1,
                    nodes_per_block=1,
                    #cores_per_node = opt.cpu,
                    partition=opt.queue,
                    scheduler_options=sched_options,   # Enter scheduler_opt if needed
                    worker_init=wrk_init,         # Enter worker_init if needed
                    walltime=opt.walltime
                ),
            )
        ],
        initialize_logging=False,
        app_cache = True,
        retries=5,
        strategy='simple',
    )

    dfk = parsl.load(slurm_htex)


    output = processor.run_uproot_job(fileset,
                                      treename='Events',
                                      processor_instance=SignalProcessor(),
                                      executor=processor.parsl_executor,
                                      executor_args={'config':None, 'flatten': False,'xrootdtimeout':200},
                                      chunksize = opt.chunk
    )

    #np.save('test.npy', output)
for flow in output['cutflow']:
    print(flow, output['cutflow'][flow])
if opt.save_h5:
    Writeh5(output['ML'],opt.version,os.path.join(os.environ['COFFEAHOME'],'../',opt.h5folder))
    

if not os.path.exists(os.path.join(os.environ['COFFEAHOME'],"root")):
    os.makedirs(os.path.join(os.environ['COFFEAHOME'],"root"))

fout = uproot.recreate(os.path.join(os.environ['COFFEAHOME'],"root",fout_name))

for var in output:
    if var == 'cutflow' or var == 'ML':continue
    for dataset in fileset:
        if len(output[var][dataset].sum("dataset").values()) < 1: continue #Don't try to save an empty histogram
        fout["{0}_{1}".format(dataset,var)] = hist.export1d(output[var][dataset].sum("dataset"))
fout.close()

if opt.parsl:
    parsl.dfk().cleanup()
    parsl.clear()
